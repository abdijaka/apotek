<div class="container">
   
      <h2>Edit Pasien</h2>
      <form class="form-horizontal" action="<?php echo site_url("pasien/update_pasien"); ?>" method="POST">
		  <div class="control-group">
			<label class="control-label" for="inputID">Nomor ID Pasien</label>
			<div class="controls control-row">
			  <input type="text" id="inputID" name="inputID" placeholder="Nomor ID Pasien" value="<?php echo $pasien['ktp'] ?>" readonly>
			  <div class="input-prepend">
			  <span class="add-on"><i class="icon-tasks"></i></span>
			  <span class="add-on">Med Rec </span>
			  <input type="text" id="inputmedrec" name="inputmedrec" placeholder="Med Rec" value="<?php echo $pasien['med_rec'] ?>" readonly>
			  </div>
			  &nbsp;
			  
			  <label class="checkbox inline">
				<input type="checkbox" id="member" name="member" value="1" <?php if ($pasien['member']==1) echo "checked";  ?> > Member
			  </label>
			</div>
			
		  </div>
		    
		  <div class="control-group">
			<label class="control-label" for="inputNama">Nama Pasien</label>
			<div class="controls">
				<select name="initial" id="initial" class="input-small">
					<option value="0" <?php if ($pasien['initial']==0) echo "selected";  ?> >Tn.</option>
					<option value="1" <?php if ($pasien['initial']==1) echo "selected";  ?> >Ny.</option>
					<option value="2" <?php if ($pasien['initial']==2) echo "selected";  ?> >Nona</option>
					<option value="3" <?php if ($pasien['initial']==3) echo "selected";  ?> >Anak</option>
					<option value="4" <?php if ($pasien['initial']==4) echo "selected";  ?> >Bayi</option>
				</select>
			  <input type="text" id="inputNama" name="inputNama" class="span5" placeholder="Nama Pasien" value="<?php echo $pasien['nama_pasien'] ?>">
			</div>
		  </div>
		<?php
				
				$date=date("d-m-Y");
				$tgl = new DateTime($pasien['tanggal_lahir']);
				
			?>
		<div class="control-group">
			<label class="control-label" for="tanggallahir">Tanggal Lahir</label>
			<div class="controls control-row">
			
				<div class="input-append date" id="dp3" data-date="<?php echo $tgl->format('d-m-Y') ?>" data-date-format="dd-mm-yyyy">
					<input class="input-small" size="16" type="text" value="<?php echo $tgl->format('d-m-Y') ?>" name="tanggallahir" id="tanggallahir">
					
					<span class="add-on"><i class="icon-calendar"></i></span>
				</div>
				<input type="hidden" value="<?php echo $date; ?>" name="now" id="now">
				<div class="input-prepend input-append">
				  <span class="add-on">Umur </span>
				  <input type="text" class="span1" id="umur" name="umur" value="<?php echo $pasien['umur'] ?>" readonly>
				  <span class="add-on">Tahun </span>
				  <input type="text" class="span1" id="umur_bulan" name="umur_bulan" value="<?php echo $pasien['umur_bulan'] ?>" readonly>
				  <span class="add-on">Bulan </span>
				  <input type="text" class="span1" id="umur_hari" name="umur_hari" value="<?php echo $pasien['umur_hari'] ?>" readonly>
				  <span class="add-on">Hari </span>
				</div>
				
				
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="goldar">Golongan Darah</label>
			<div class="controls control-row">
				<select name="goldar" id="goldar" class="input-small">
					<option value="0" <?php if ($pasien['goldar']==0) echo "selected";?> >A</option>
					<option value="1" <?php if ($pasien['goldar']==1) echo "selected";?> >B</option>
					<option value="2" <?php if ($pasien['goldar']==2) echo "selected";?> >O</option>
					<option value="3" <?php if ($pasien['goldar']==3) echo "selected";?> >AB</option>
				</select>
					
				<div class="input-prepend">
				  <span class="add-on">Rhesus </span>
					<select name="resus" id="resus" class="input-medium">
					  <option value="0" <?php if ($pasien['rhesus']==0) echo "selected";?> >Positive</option>
					  <option value="1" <?php if ($pasien['rhesus']==1) echo "selected";?> >Negative</option>
					</select>
					
				</div>
				
				&nbsp;
				<div class="input-prepend input-append">
				  <span class="add-on">Jenis Kelamin </span>
					<select name="jeniskelamin" id="jeniskelamin" class="input-medium">
					  <option value="0" <?php if ($pasien['jenis_kelamin']==0) echo "selected";?> >Laki-laki</option>
					  <option value="1" <?php if ($pasien['jenis_kelamin']==1) echo "selected";?> >Perempuan</option>
					</select>
					
				</div>
				
			</div>
		  </div>
		  
		  <div class="control-group">
			
			<label class="control-label" for="alamat">Alamat</label>
			<div class="controls">
			<textarea name="alamat" id="alamat" rows="4" class="span6"><?php echo $pasien['alamat'] ?></textarea> 
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputKota">Kota</label>
			<div class="controls control-row">
			  <input type="text" id="inputKota" name="inputKota" class="input-xlarge" placeholder="Kota" value="<?php echo $pasien['kota'] ?>" autocomplete="off">
			  <div class="input-prepend">
					<span class="add-on">Kode Pos </span>
					<input type="text" id="inputKodepos" name="inputKodepos" class="span2" placeholder="Kode Pos" value="<?php echo $pasien['kodepos'] ?>" >
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputEmail">Email</label>
			<div class="controls control-row">
			  <input type="text" id="inputEmail" name="inputEmail" class="input-large" placeholder="Email" value="<?php echo $pasien['email'] ?>" >
			  <div class="input-prepend">
					<span class="add-on">Pekerjaan </span>
					<select name="pekerjaan" id="pekerjaan" class="input-large">
					  <option value="Pegawai Negeri" <?php if($pasien['pekerjaan']=="Pegawai Negeri") echo 'selected'; ?>>Pegawai Negeri</option>
					  <option value="Wiraswasta" <?php if($pasien['pekerjaan']=="Wiraswasta") echo 'selected'; ?>>Wiraswasta</option>
					  <option value="Ibu Rumah Tangga" <?php if($pasien['pekerjaan']=="Ibu Rumah Tangga") echo 'selected'; ?>>Ibu Rumah Tangga</option>
					  <option value="Karyawan Swasta" <?php if($pasien['pekerjaan']=="Karyawan Swasta") echo 'selected'; ?>>Karyawan Swasta</option>
					</select>
					
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputTelepon">Telepon</label>
			<div class="controls control-row">
			  <input type="text" id="inputTelepon" name="inputTelepon" class="input-large" placeholder="Telepon" value="<?php echo $pasien['telepon'] ?>" >
			  <div class="input-prepend">
					<span class="add-on">Hp / Flexi </span>
					<input type="text" id="hp" name="hp" class="span3" placeholder="Nomor HP" value="<?php echo $pasien['hp'] ?>" >
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="asuransi">Perusahaan/asuransi/CC</label>
			<div class="controls control-row">
			  <select name="asuransi" id="asuransi" class="span6">
				<?php foreach ($perusahaan as $perusahaan_item): ?>
					<option value="<?php echo $perusahaan_item['id_perusahaan'] ?>" <?php if($pasien['perusahaan']==$perusahaan_item['id_perusahaan']) echo 'selected'; ?> ><?php echo $perusahaan_item['nama_perusahaan'] ?></option>
				<?php endforeach; ?>
				  
				</select>
				
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="keterangan">Keterangan</label>
			<div class="controls control-row">
			  <input type="text" id="keterangan" name="keterangan" class="span6" placeholder="Keterangan" value="<?php echo $pasien['keterangan'] ?>" >
			</div>
		  </div>
		  
		  <div class="control-group">
			<div class="controls">
			  <button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Simpan</button>
			</div>
		  </div>
		</form>
   
</div>

<script type="text/javascript">
function dstrToUTC(ds) {
    var dsarr = ds.split("-");
     var dd = parseInt(dsarr[0],10);
     var mm = parseInt(dsarr[1],10);
     var yy = parseInt(dsarr[2],10);
     return Date.UTC(yy,mm-1,dd,0,0,0); 
	}
function datediff(ds1,ds2) 
	{
     var d1 = dstrToUTC(ds1);
     var d2 = dstrToUTC(ds2);
     var oneday = 86400000;
     return (d2-d1) / oneday;    
	}
function humanise (diff) {
  // The string we're working with to create the representation
  var str = '';
  // Map lengths of `diff` to different time periods
  var values = {
    'umur': 365, 
    'umur_bulan': 30, 
    'umur_hari': 1
  };

  // Iterate over the values...
  for (var x in values) {
    var amount = Math.floor(diff / values[x]);

    // ... and find the largest time value that fits into the diff
    if (amount >= 1) {
       // If we match, add to the string ('s' is for pluralization)
       str += amount + x + (amount > 1 ? 's' : '') + ' ';
	   $('#'+x).val(amount);
	   

       // and subtract from the diff
       diff -= amount * values[x];
    }
	else
	{
		$('#'+x).val(0);
	}
  }

  return str;
}
	
$(document).ready(function(){

var data2 = [<?php echo $testing2; ?>];

$('#inputKota').typeahead({
    source: function (query, process) {
        states2 = [];
		map2 = {};
		
		var source = [];
		$.each(data2, function (i, state) {
			map2[state.stateName] = state;
			states2.push(state.stateName);
		});
	 
		process(states2);
		
    },
    updater: function (item) {
        
		selectedState = map2[item].stateCode;
		selectedState2 = map2[item].stateDisplay;
		$("#idkota").val(selectedState);
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});


$('#dp3').datepicker()
		  .on('changeDate', function(ev){
			var tanggallahir=$('#tanggallahir').val();
			var now=$('#now').val();
			
			var diff=datediff(tanggallahir,now);
			humanise(diff);
		  });
});

$('#tanggallahir').keyup(function() {
			var tanggallahir=$('#tanggallahir').val();
			var now=$('#now').val();
			
			var diff=datediff(tanggallahir,now);
			humanise(diff);
});

$( "#initial" )
  .change(function () {
    var str = 0;
    $( "#initial option:selected" ).each(function() {
      str = $( this ).val();
    });
    if(str==0)
	{
		$("#jeniskelamin").val(0);
	}
	else if(str==1 || str==2)
	{
		$("#jeniskelamin").val(1);
	}
  })
  .change();


</script>