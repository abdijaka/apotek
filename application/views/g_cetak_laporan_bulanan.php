<?php 
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

function get_month($month)
{
	$labelmonth="Januari";

	if($month==2)
	$labelmonth="Februari";
	else if($month==3)
	$labelmonth="Maret";
	else if($month==4)
	$labelmonth="April";
	else if($month==5)
	$labelmonth="Mei";
	else if($month==6)
	$labelmonth="Juni";
	else if($month==7)
	$labelmonth="Juli";
	else if($month==8)
	$labelmonth="Agustus";
	else if($month==9)
	$labelmonth="September";
	else if($month==10)
	$labelmonth="Oktober";
	else if($month==11)
	$labelmonth="November";
	else if($month==12)
	$labelmonth="Desember";

	return $labelmonth;

}

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once 'PHPExcel/Classes/PHPExcel.php';

require_once 'PHPExcel/Classes/PHPExcel/IOFactory.php';

$fileName = 'PHPExcel/Examples/templates/3. Laporan Harian Baru.xlsx';
try {
    $objPHPExcel = PHPExcel_IOFactory::load($fileName);
} catch (Exception $e) {
    die("Error loading file: ".$e->getMessage()."<br />\n");
}

$pieces = explode("-", $mulai);

$objPHPExcel->getActiveSheet()->setCellValue('A1', "LAPORAN BULANAN");

$objPHPExcel->getActiveSheet()->setCellValue('A2', "Bulan  : ".get_month($pieces[0]).' '.$pieces[1]."");

$nomor=1;
$baseRow = 6;
$row=7;
$i=0;
$netto=0;
$totaltunai=0;
$totaldebit=0;
$totalkredit=0;

foreach ($pembelian as $list_item): 
	$i++;
	$netto=$netto+$list_item['total'];
	if($list_item['carabayar']==1)
	{
		$totaltunai=$totaltunai+$list_item['total'];
	}
	else if($list_item['carabayar']==2)
	{
		$totaldebit=$totaldebit+$list_item['total'];
	}
	else if($list_item['carabayar']==3)
	{
		$totalkredit=$totalkredit+$list_item['total'];
	}
	
	if($nomor==1)
	{
		$row=7;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $nomor)
								  ->setCellValue('B'.$row, $list_item['nomor_faktur'])
								  ->setCellValue('C'.$row, $list_item['nama_supplier'])
								  ->setCellValue('E'.$row, $list_item['total']);
	}
	else
	{
		$row = $baseRow + $nomor;
		$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);

		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $nomor)
								  ->setCellValue('B'.$row, $list_item['nomor_faktur'])
								  ->setCellValue('C'.$row, $list_item['nama_supplier'])
								  ->setCellValue('E'.$row, $list_item['total']);

	}


$nomor++;

endforeach;

$objPHPExcel->getActiveSheet()->setCellValue('E'.($row+1), $netto);
$objPHPExcel->getActiveSheet()->setCellValue('E'.($row+2), $totaltunai);
$objPHPExcel->getActiveSheet()->setCellValue('E'.($row+3), $totaldebit);
$objPHPExcel->getActiveSheet()->setCellValue('E'.($row+4), $totalkredit);


$baseRow = (15+$nomor-2);
$row=$baseRow+1;
$nomor=1;
$i=0;
$netto=0;
$totaltunai=0;
$totaldebit=0;
$totalkredit=0;

foreach ($penjualan as $list_item): 
	$i++;
	$netto=$netto+$list_item['total'];
	if($list_item['carabayar']==1)
	{
		$totaltunai=$totaltunai+$list_item['total'];
	}
	else if($list_item['carabayar']==2)
	{
		$totaldebit=$totaldebit+$list_item['total'];
	}
	else if($list_item['carabayar']==3)
	{
		$totalkredit=$totalkredit+$list_item['total'];
	}
	
	if($nomor==1)
	{
		$row=$baseRow+1;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $nomor)
								  ->setCellValue('B'.$row, $list_item['nomor_faktur'])
								  ->setCellValue('C'.$row, $list_item['nama_pasien'])
								  ->setCellValue('E'.$row, $list_item['total']);
	}
	else
	{
		$row = $baseRow + $nomor;
		$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);

		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $nomor)
								  ->setCellValue('B'.$row, $list_item['nomor_faktur'])
								  ->setCellValue('C'.$row, $list_item['nama_pasien'])
								  ->setCellValue('E'.$row, $list_item['total']);

	}


$nomor++;

endforeach;

$objPHPExcel->getActiveSheet()->setCellValue('E'.($row+1), $netto);
$objPHPExcel->getActiveSheet()->setCellValue('E'.($row+2), $totaltunai);
$objPHPExcel->getActiveSheet()->setCellValue('E'.($row+3), $totaldebit);
$objPHPExcel->getActiveSheet()->setCellValue('E'.($row+4), $totalkredit);


// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Laporan Bulanan');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Laporan Bulanan_'.get_month($pieces[0]).' '.$pieces[1].'.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>