<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
		
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">DETAIL TRANSAKSI</legend>

		
			<?php
			$tanggal =date('j-m-Y', strtotime($transaksi['tanggal']));
			$jam =date('G:i:s', strtotime($transaksi['tanggal']));
			$date=date("d-m-Y");
			?>
			
		
			<div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
					<label class="control-label" for="inputTanggal">Tanggal</label>
					<div class="input-append date" id="dp3" data-date="<?php echo $date; ?>" data-date-format="dd-mm-yyyy">
						<input type="text" value="<?php echo $tanggal; ?>" name="tanggalawal" id="tanggalawal" readonly>
					</div>	
				</div>	
				<div class="col-md-4">
						<label class="control-label" for="inputReg">No. Registrasi</label>
						<input type="text" id="inputReg" name="inputReg" value="<?php echo $transaksi['nomor_registrasi']; ?>" readonly>
				</div>		
				<div class="col-md-4">
					<label class="control-label" for="inputID">Dokter Pengirim</label>
			  
					<input type="text" id="referensi" name="referensi" placeholder="Referensi" autocomplete="off" value="<?php echo $transaksi['nama_dokter']; ?>" readonly>
					<input type="hidden" id="idreferensi" name="idreferensi" value="<?php echo $transaksi['referensi']; ?>" >
				</div>	
			
			</div>
			</div>
			
			<div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
					<label class="control-label" for="inputID">Nomor ID Pasien</label>
					<input readonly type="text" id="inputID" name="inputID" placeholder="Nomor ID Pasien" autocomplete="off" value="<?php echo $transaksi['pasien']; ?>">
					
				</div>
				<div class="col-md-4">
					<label class="control-label" for="inputID">Nama Pasien</label>
					<input type="text" id="inputID2" name="inputID2" readonly value="<?php echo strtoupper($transaksi['nama_pasien']); ?>">
				</div>
				<div class="col-md-4">
					<div class="form-group">
					<label class="control-label" for="pasien_baru">&nbsp;</label>
					<br>
					&nbsp;&nbsp;&nbsp;&nbsp;
					
					
				  <label class="checkbox inline">
					Pasien Baru<input type="checkbox" id="baru" name="baru" value="1" <?php if($transaksi['baru']==1) echo 'checked'; ?> disabled> 
				  </label>
				  </div>
				  
				</div>
			</div>
			</div>
		 
		  
		  <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
		  
		  
			
			<div class="row">
			<div class="col-md-12">
			<div class="alert alert-info">
				<div class="container-fluid" style="margin-top:-15px; ">
				<div class="row-fluid">
			
              <div class="col-md-12">
				  
					   <div class="row">
					   <div class="col-md-12">
					   <div class="col-md-12">
					   <div id="tabel_layanan"></div>
					   </div>
					   </div>
					   </div>
					   
					   <div class="row">
							<div class="col-md-12">
							<div class="col-md-4">
							<label class="control-label" for="totalharga">Sub Total</label>
							<input type="text" id="totalharga" name="totalharga" readonly value="0">
						  
							</div>	
							
							<div class="col-md-4">
							<label class="control-label" for="totalharga">Diskon (%)</label>
							<div class="input-group margin-bottom-sm">
							<input type="text" id="totaldiskon" name="totaldiskon" autocomplete="off"  value="<?php echo $transaksi['diskon']; ?>" readonly>
							<span class="input-group-addon">%</span>
							</div>
						  
							</div>	
							
							<div class="col-md-4">
							<label class="control-label" for="totalharga">Nominal Diskon (Rp)</label>
							<div class="input-group margin-bottom-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="totalnominaldiskon" name="totalnominaldiskon" autocomplete="off" value="<?php echo $transaksi['nominal_diskon']; ?>" readonly>
							</div>
						  
							</div>	
							</div>	
						 </div>
						 
						 <div class="row">
							<div class="col-md-12">
							<div class="col-md-6">
							<label class="control-label" for="totalharganetto">Harga Netto</label>
							<p id="labelnetto" style="color:#CC0000; font-size:32px; font-weight:bold;"> Rp 0 </p>
								<input type="hidden" id="totalharganetto" name="totalharganetto"  value="0" >
						  
							</div>	
							<input type="hidden" id="jumlahbayar" name="jumlahbayar"  value="<?php echo $transaksi['bayar']; ?>" >
							<input type="hidden" id="sisa" name="sisa"  value="0" >
							<div class="col-md-6" >
								<label class="control-label" for="totalharganetto">Kekurangan Pembayaran</label>
									
											<p id="labelsisa" style="color:#CC0000; font-size:32px; font-weight:bold;">Rp <?php echo strtoupper($transaksi['sisa']); ?>,- </p>
									
							</div>
							
							</div>	
						 </div>
						 <div class="row">
						 <div class="col-md-12">
						 <div class="col-md-12">
						 <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
						 </div>
						 </div>
						 </div>
						 
						 <div class="row">
							<div class="col-md-12">
							<div class="col-md-12">
								<table class="table table-bordered table-condensed " >
						<thead>
						<tr style="background-color: #90CA77; color:#fff; font-size:14px;">
						  <th>Tanggal</th>
						  <th>Cara Bayar</th>
						  <th>Pembayaran</th>
						  <th>Charge Bank</th>
						  <th>Total+Charge</th>
						  <th>Action</th>
						</tr>
					  </thead>
						<tbody style="font-size:14px;">
						<?php
						$i=0;
						$netto=0;
						
							foreach ($pembayaran as $pembayaran_item): 
							$tanggal_bayar =date('j-m-Y', strtotime($pembayaran_item['tanggal']));
							$jam_bayar =date('G:i', strtotime($pembayaran_item['tanggal']));
							?>
							
							<tr style="background-color: #fff;">
							  
							  <td><?php echo $tanggal_bayar.' '.$jam_bayar; ?></td>
							  <td><?php echo $pembayaran_item['namacarabayar']; ?></td>
							  <td style="text-align:right; "><p class="text-right"><?php echo formatRupiah($pembayaran_item['jumlah']); ?></p> </td>
							  <td style="text-align:right; "><p class="text-right"><?php echo formatRupiah($pembayaran_item['chargebank']); ?></p> </td>
							  <td style="text-align:right; "><p class="text-right"><?php echo formatRupiah($pembayaran_item['chargebank']+$pembayaran_item['jumlah']); ?></p> </td>
							  <td><a href="<?php echo site_url('transaksi/cetak_pembayaran/'.$transaksi['nomor_registrasi'].'/'.$pembayaran_item['tanggal']); ?>" class="btn btn-success"><i class="fa fa-print"></i> Print</a></td>
							</tr>
						<?php 
						endforeach ?>	
							<tr style="">
							  <td colspan="1" style="text-align:center; color:#CC0000; font-weight:bold; font-size:16px;">Total Pembayaran</td>
							  <td colspan="4" style="text-align:right;  font-size:20px; color:#CC0000; font-weight:bold;"><p class="text-right"><?php echo formatRupiah($transaksi['bayar']); ?></p></td>
							  <td></td>
							  
							</tr>
						  </tbody>
					</table>
					
							</div>	
							
							</div>	
						 </div>
					   
						<div class="row">
						 <div class="col-md-12">
						 <div class="col-md-12">
						 <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
						 </div>
						 </div>
						 </div>
					  <div class="row">
						<div class="col-md-12">
						<div class="col-md-9">
						  &nbsp;
						</div>
						<div class="col-md-3">
						  <a href="<?php echo site_url('transaksi/create_transaksi'); ?>" class="btn btn-large btn-info "><i class="fa fa-chevron-left "></i> Kembali ke Register</a>
						</div>
						</div>
					  </div>
					
				</div>
            </div>
			</div>
			</div>
			</div>
			
		  </div>
		  </div>
		  
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>

<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/transition.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/modal.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>

<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>
	
<script type="text/javascript">

function hitung_netto() 
	{
     var harga=$('#harga').val();
	 var nominaldiskon=$('#nominaldiskon').val();
     
	 var netto=harga-nominaldiskon;
     $("#harganetto").val(netto);
	 
	 
	}
function hitung_persen_diskon() 
	{
     var harga=$('#harga').val();
	 var nominaldiskon=$('#nominaldiskon').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#diskon").val(diskon);
	}

function hitung_nominal_diskon() 
	{
     var harga=$('#harga').val();
	 var diskon=$('#diskon').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#nominaldiskon").val(nominaldiskon);
	}
	
function hitung_total_netto() 
	{
     var harga=$('#totalharga').val();
	 var nominaldiskon=$('#totalnominaldiskon').val();
     
	 var netto=harga-nominaldiskon;
     $("#totalharganetto").val(netto);
	 $("#labelnetto").text('Rp '+formatRupiah(netto));
	 hitung_sisa();
	}

function hitung_total_persen_diskon() 
	{
     var harga=$('#totalharga').val();
	 var nominaldiskon=$('#totalnominaldiskon').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#totaldiskon").val(diskon);
	}

function hitung_total_nominal_diskon() 
	{
     var harga=$('#totalharga').val();
	 var diskon=$('#totaldiskon').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#totalnominaldiskon").val(nominaldiskon);
	}

	
function hitung_netto2() 
	{
     var harga=$('#harga2').val();
	 var nominaldiskon=$('#nominaldiskon2').val();
     
	 var netto=harga-nominaldiskon;
     $("#harganetto2").val(netto);
	}
function hitung_persen_diskon2() 
	{
     var harga=$('#harga2').val();
	 var nominaldiskon=$('#nominaldiskon2').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#diskon2").val(diskon);
	}

function hitung_nominal_diskon2() 
	{
     var harga=$('#harga2').val();
	 var diskon=$('#diskon2').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#nominaldiskon2").val(nominaldiskon);
	}
	
function hitung_total_netto2() 
	{
     var harga=$('#totalharga2').val();
	 var nominaldiskon=$('#totalnominaldiskon2').val();
     
	 var netto=harga-nominaldiskon;
     $("#totalharganetto2").val(netto);
	}
function hitung_total_persen_diskon2() 
	{
     var harga=$('#totalharga2').val();
	 var nominaldiskon=$('#totalnominaldiskon2').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#totaldiskon2").val(diskon);
	}

function hitung_total_nominal_diskon2() 
	{
     var harga=$('#totalharga2').val();
	 var diskon=$('#totaldiskon2').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#totalnominaldiskon2").val(nominaldiskon);
	}
	
function hitung_netto3() 
	{
     var harga=$('#harga3').val();
	 var nominaldiskon=$('#nominaldiskon3').val();
     
	 var netto=harga-nominaldiskon;
     $("#harganetto3").val(netto);
	}
function hitung_persen_diskon3() 
	{
     var harga=$('#harga3').val();
	 var nominaldiskon=$('#nominaldiskon3').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#diskon3").val(diskon);
	}

function hitung_nominal_diskon3() 
	{
     var harga=$('#harga3').val();
	 var diskon=$('#diskon3').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#nominaldiskon3").val(nominaldiskon);
	}
	
function hitung_total_netto3() 
	{
     var harga=$('#totalharga3').val();
	 var nominaldiskon=$('#totalnominaldiskon3').val();
     
	 var netto=harga-nominaldiskon;
     $("#totalharganetto3").val(netto);
	}
function hitung_total_persen_diskon3() 
	{
     var harga=$('#totalharga3').val();
	 var nominaldiskon=$('#totalnominaldiskon3').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#totaldiskon3").val(diskon);
	}

function hitung_total_nominal_diskon3() 
	{
     var harga=$('#totalharga3').val();
	 var diskon=$('#totaldiskon3').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#totalnominaldiskon3").val(nominaldiskon);
	}
function hitung_sisa()
	{
     
	 var harga=$('#totalharganetto').val();
	 var bayar=$('#jumlahbayar').val();
     
	 var sisa=harga-bayar;
     $("#sisa").val(sisa);
	 $("#labelsisa").text('Rp '+formatRupiah(sisa));
	}
function formatRupiah(nilaiUang2)
{
	var nilaiUang=nilaiUang2+"";
  var nilaiRupiah 	= "";
  var jumlahAngka 	= nilaiUang.length;
  
  while(jumlahAngka > 3)
  {
	
	sisaNilai = jumlahAngka-3;
    nilaiRupiah = "."+nilaiUang.substr(sisaNilai,3)+""+nilaiRupiah;
    
    nilaiUang = nilaiUang.substr(0,sisaNilai)+"";
    jumlahAngka = nilaiUang.length;
  }
 
  nilaiRupiah = nilaiUang+""+nilaiRupiah+",-";
  return nilaiRupiah;
}
	var controller = 'transaksi';
	var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';

$(document).ready(function(){



var reg='<?php echo $transaksi["nomor_registrasi"]; ?>';
$.ajax({
	'url' : base_url + controller + '/show_detail_pemeriksaan',
	'type' : 'POST', //the way you want to send data to your URL
	'data' : {'reg' : reg},
	'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
		var container = $('#tabel_layanan'); //jquery selector (get element by id)
		if(data){
			
			container.html(data);
			var temp = $('#temptotalharga').val();
			$('#totalharga').val(temp);
			hitung_total_netto();
		}
	}
});
});


</script>


</body></html>