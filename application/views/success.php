<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

if("".$transaksi['baru']==0)
$label_baru="L";
else
$label_baru="B";

if($transaksi['tipebayar']==1)
$carabayar="TUNAI";
else if($transaksi['tipebayar']==2)
$carabayar="KREDIT";
else
$carabayar="DEBIT";

if($transaksi['initial']==0)
$initial="TN.";
else if($transaksi['initial']==1)
$initial="NY.";
else if($transaksi['initial']==2)
$initial="NN.";
else if($transaksi['initial']==3)
$initial="ANAK";
else
$initial="BAYI";

$tanggal =date('j-m-Y', strtotime($transaksi['tanggal']));
$jam =date('G:i:s', strtotime($transaksi['tanggal']));
?>
<div class="container">

      <form class="form-horizontal" id="form1" >
			<?php
			$date=date("d-m-Y");
			?>
			
		<div class="control-group">
			<label class="control-label" for="inputTanggal">Tanggal</label>
			<div class="controls control-row">
				
				<input class="span2" size="16" type="text" value="<?php echo $tanggal; ?>" readonly>
				
				<div class="input-prepend" >
					<span class="add-on">Kwitansi Nomor : </span>
					<input type="text" value="<?php echo "No: ".$transaksi['nomor_registrasi']." (".$label_baru.")"; ?>" readonly>
					
				</div>
				
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputID">Nomor ID Pasien</label>
			<div class="controls control-row">
			  <input type="text" value="<?php echo $transaksi['pasien']; ?>" readonly>
			  <input type="text"  readonly value="<?php echo $initial." ".strtoupper($transaksi['nama_pasien']); ?>">
			  
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputID">Dokter Pengirim</label>
			<div class="controls control-row">
			  
					<input type="text" value="<?php echo $transaksi['nama_dokter']; ?>" readonly>
					
					
			</div>
			
		  </div>
		  
		  <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
		  
		  <div class="alert alert-info">
			<table class="table table-bordered table-condensed " >
			<thead>
			<tr style="background-color: #CC6600; color:#fff; font-size:16px;">
			  <th>No. </th>
			  <th>Pemeriksaan</th>
			  <th>Dokter</th>
			  <th>Harga</th>
			  <th>Diskon</th>
			  <th>Netto</th>
			</tr>
		  </thead>
			<tbody>
			<?php
			$i=0;
			$netto=0;
				foreach ($detail as $list_item): 
				$i++;
				$netto=$netto+$list_item['netto'];
				?>
				
				<tr style="background-color: #fff;">
				  <td style="text-align:center;"><?php echo $i; ?></td>
				  <td><?php echo $list_item['nama_layanan']; ?></td>
				  <td><?php echo $list_item['nama_dokter']; ?></td>
				  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($list_item['harga']); ?></p> </td>
				  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($list_item['nominal_diskon']); ?></p></td>
				  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($list_item['netto']); ?></p></td>
				  
				  
				  
				</tr>
			<?php 
			endforeach ?>	
				<tr style="">
				  <td colspan="2" style="text-align:center; color:#CC0000; font-weight:bold; font-size:16px;">Sub Total</td>
				  <td colspan="4" style="text-align:right;  font-size:20px; color:#CC0000; font-weight:bold;"><p class="text-right"><?php echo formatRupiah($netto); ?></p></td>
				  
				</tr>
			  </tbody>
		</table>
		
		<div class="control-group">
						<label class="control-label" for="totalharga">Sub Total</label>
						<div class="controls control-row">
						  <input type="text" class="input-small" readonly value="<?php echo formatRupiah($netto); ?>">
							<div class="input-prepend input-apppend" >
								<span class="add-on">Diskon</span>
								
								<input readonly type="text" id="totalnominaldiskon" name="totalnominaldiskon"  class="input-small" value="<?php echo formatRupiah($transaksi['nominal_diskon']); ?>">
								
							</div>
							
						</div>
						
					  </div>
		<div class="control-group" >
						<label class="control-label" for="totalharganetto">Harga Netto</label>
						<div class="controls">
						  
								<p id="labelnetto" style="color:#CC0000; font-size:32px; font-weight:bold;"> <?php echo formatRupiah($transaksi['harga_netto']); ?> </p>
								
						</div>
						
					  </div>
					  
		<div class="control-group" >
			<table class="table table-bordered table-condensed " >
			<thead>
			<tr style="background-color: #CC6600; color:#fff; font-size:16px;">
			  <th>Tanggal</th>
			  <th>Jumlah Pembayaran</th>
			  <th>Cara Bayar</th>
			  
			</tr>
		  </thead>
			<tbody>
			<?php
			$i=0;
			$netto=0;
			
				foreach ($pembayaran as $pembayaran_item): 
				$tanggal_bayar =date('j-m-Y', strtotime($pembayaran_item['tanggal']));
				$jam_bayar =date('G:i', strtotime($pembayaran_item['tanggal']));
				?>
				
				<tr style="background-color: #fff;">
				  
				  <td><?php echo $tanggal_bayar.' '.$jam_bayar; ?></td>
				  <td><?php echo $pembayaran_item['namacarabayar']; ?></td>
				  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($pembayaran_item['jumlah']); ?></p> </td>
				  
				</tr>
			<?php 
			endforeach ?>	
				<tr style="">
				  <td colspan="1" style="text-align:center; color:#CC0000; font-weight:bold; font-size:16px;">Total Pembayaran</td>
				  <td colspan="2" style="text-align:right;  font-size:20px; color:#CC0000; font-weight:bold;"><p class="text-right"><?php echo formatRupiah($transaksi['bayar']); ?></p></td>
				  
				</tr>
			  </tbody>
		</table>
		
		</div>
		<div class="control-group" >
			<label class="control-label" for="totalharganetto">Sisa</label>
				<div class="controls">
				  
						<p id="labelnetto" style="color:#CC0000; font-size:32px; font-weight:bold;">Rp <?php echo strtoupper($transaksi['sisa']); ?>,- </p>
						
				</div>			
						
		</div> 
		  </div>
		  <div class="control-group" >
		  <div class="controls">
		  <a href="<?php echo site_url('transaksi/create_transaksi'); ?>" class="btn btn-large btn-info "><i class="icon-chevron-left icon-white"></i> Kembali</a>
		  <a href="<?php echo site_url('transaksi/receipt/'.$transaksi['nomor_registrasi']); ?>" target="_blank" class="btn btn-large btn-info "><i class="icon-print icon-white"></i> Cetak</a>
		  
		  </div>
		  </div>
		  
		</form>
		
   
</div>
