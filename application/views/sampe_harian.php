<?php

function terbilang($x)
{
$abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
if ($x < 12)
return " " . $abil[$x];
elseif ($x < 20)
return Terbilang($x - 10) . "Belas";
elseif ($x < 100)
return Terbilang($x / 10) . " Puluh" . Terbilang($x % 10);
elseif ($x < 200)
return " seratus" . Terbilang($x - 100);
elseif ($x < 1000)
return Terbilang($x / 100) . " Ratus" . Terbilang($x % 100);
elseif ($x < 2000)
return " seribu" . Terbilang($x - 1000);
elseif ($x < 1000000)
return Terbilang($x / 1000) . " Ribu" . Terbilang($x % 1000);
elseif ($x < 1000000000)
return Terbilang($x / 1000000) . " Juta" . Terbilang($x % 1000000);
}

function html2ascii($s){
 // convert links
 $s = preg_replace('/<a\s+.*?href="?([^\" >]*)"?[^>]*>(.*?)<\/a>/i','$2 ($1)',$s);
 
 // convert p, br and hr tags
 $s = preg_replace('@<(b|h)r[^>]*>@i',"\n",$s);
 $s = preg_replace('@<p[^>]*>@i',"\n\n",$s);
 $s = preg_replace('@<div[^>]*>(.*)</div>@i',"\n".'$1'."\n",$s);  
  
 // convert bold and italic tags
 $s = preg_replace('@<b[^>]*>(.*?)</b>@i','*$1*',$s);
 $s = preg_replace('@<strong[^>]*>(.*?)</strong>@i','*$1*',$s);
 $s = preg_replace('@<i[^>]*>(.*?)</i>@i','_$1_',$s);
 $s = preg_replace('@<em[^>]*>(.*?)</em>@i','_$1_',$s);
   
 // decode any entities
 $s = strtr($s,array_flip(get_html_translation_table(HTML_ENTITIES)));
 
 // decode numbered entities
 $s = preg_replace('/&#(\d+);/e','chr(str_replace(";","",str_replace("&#","","$0")))',$s);
 
 // strip any remaining HTML tags
 $s = strip_tags($s);
 
 // return the string
 return $s;
}

function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . "";
  return $nilaiRupiah;
}


?>
<html>
   <!-- License:  LGPL 2.1 or QZ INDUSTRIES SOURCE CODE LICENSE -->
   <head><title>QZ Print Plugin</title>    
   	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.7.1.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.ascii.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/directPrint.js') ?>"></script>
	<script type="text/javascript">
      var qz;   // Our main applet.
	var tes="";
	$(document).ready(function() {
			tes=$("#hasil").ascii("raw");
			
			
		});
      function findPrinter() {
         
         if (qz != null) {
            // Searches for locally installed printer with "zebra" in the name
            qz.findPrinter("Zebra");
         }
         
         // *Note:  monitorFinding() still works but is too complicated and
         // outdated.  Instead create a JavaScript  function called 
         // "jzebraDoneFinding()" and handle your next steps there.
         monitorFinding();
      }

      function findPrinters() {
         
         if (qz != null) {
            // Searches for locally installed printer with "zebra" in the name
            qz.findPrinter("\\{dummy printer name for listing\\}");
         }

         monitorFinding2();
      }

      function print() {
         
         if (qz != null) {
	
            // Send characters/raw commands to qz using "append"
            // This example is for EPL.  Please adapt to your printer language
            // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"

	   qz.append("\x1B\x40"); // 1
qz.append("\x1B\x21\x08"); // 2
qz.append("MUSI HEART CLINIC \r\n");
qz.append("\x1B\x21\x01"); 
qz.append("\x0F"); // 3
qz.append(""+tes+"\r\n"); // 3
qz.append("\x0C"); 
            qz.print(); // send commands to printer

		
	 }
	 
         // *Note:  monitorPrinting() still works but is too complicated and
         // outdated.  Instead create a JavaScript  function called 
         // "jzebraDonePrinting()" and handle your next steps there.
	 monitorPrinting();
         
         /**
           *  PHP PRINTING:
           *  // Uses the php `"echo"` function in conjunction with qz-print `"append"` function
           *  // This assumes you have already assigned a value to `"$commands"` with php
           *  qz.append(<?php echo $commands; ?>);
           */
           
         /**
           *  SPECIAL ASCII ENCODING
           *  //qz.setEncoding("UTF-8");
           *  qz.setEncoding("Cp1252"); 
           *  qz.append("\xDA");

           *  qz.append(String.fromCharCode(218));
           *  qz.append(chr(218));
           */
         
      }
      
      
      
      // Gets the current url's path, such as http://site.com/example/dist/
      function getPath() {
          var path = window.location.href;
          return path.substring(0, path.lastIndexOf("/")) + "/";
      }
      
 
      function chr(i) {
         return String.fromCharCode(i);
      }
      
      // *Note:  monitorPrinting() still works but is too complicated and
      // outdated.  Instead create a JavaScript  function called 
      // "jzebraDonePrinting()" and handle your next steps there.
      function monitorPrinting() {
	
	if (qz != null) {
	   if (!qz.isDonePrinting()) {
	      window.setTimeout('monitorPrinting()', 100);
	   } else {
	      var e = qz.getException();
	      alert(e == null ? "Printed Successfully" : "Exception occured: " + e.getLocalizedMessage());
              qz.clearException();
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
      
      function monitorFinding() {
	
	if (qz != null) {
	   if (!qz.isDoneFinding()) {
	      window.setTimeout('monitorFinding()', 100);
	   } else {
	      var printer = qz.getPrinter();
              alert(printer == null ? "Printer not found" : "Printer \"" + printer + "\" found");
	   }
	} else {
            alert("Applet not loaded!");
        }
      }

      function monitorFinding2() {
	
	if (qz != null) {
	   if (!qz.isDoneFinding()) {
	      window.setTimeout('monitorFinding2()', 100);
	   } else {
              var printersCSV = qz.getPrinters();
              var printers = printersCSV.split(",");
              for (p in printers) {
                  alert(printers[p]);
              }
              
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
      
      // *Note:  monitorAppending() still works but is too complicated and
      // outdated.  Instead create a JavaScript  function called 
      // "jzebraDoneAppending()" and handle your next steps there.
      function monitorAppending() {
	
	if (qz != null) {
	   if (!qz.isDoneAppending()) {
	      window.setTimeout('monitorAppending()', 100);
	   } else {
	      qz.print(); // Don't print until all of the data has been appended
              
              // *Note:  monitorPrinting() still works but is too complicated and
              // outdated.  Instead create a JavaScript  function called 
              // "jzebraDonePrinting()" and handle your next steps there.
              monitorPrinting();
	   }
	} else {
            alert("Applet not loaded!");
        }
      }

      // *Note:  monitorAppending2() still works but is too complicated and
      // outdated.  Instead create a JavaScript  function called 
      // "jzebraDoneAppending()" and handle your next steps there.
      function monitorAppending2() {
	
	if (qz != null) {
	   if (!qz.isDoneAppending()) {
	      window.setTimeout('monitorAppending2()', 100);
	   } else {
	      qz.printPS(); // Don't print until all of the image data has been appended
              
              // *Note:  monitorPrinting() still works but is too complicated and
              // outdated.  Instead create a JavaScript  function called 
              // "jzebraDonePrinting()" and handle your next steps there.
              monitorPrinting();
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
      
      // *Note:  monitorAppending3() still works but is too complicated and
      // outdated.  Instead create a JavaScript  function called 
      // "jzebraDoneAppending()" and handle your next steps there.
      function monitorAppending3() {
	
	if (qz != null) {
	   if (!qz.isDoneAppending()) {
	      window.setTimeout('monitorAppending3()', 100);
	   } else {
	      qz.printHTML(); // Don't print until all of the image data has been appended
              
              
              // *Note:  monitorPrinting() still works but is too complicated and
              // outdated.  Instead create a JavaScript  function called 
              // "jzebraDonePrinting()" and handle your next steps there.
              monitorPrinting();
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
      
      function useDefaultPrinter() {
         
         if (qz != null) {
            // Searches for default printer
            qz.findPrinter();
         }
         
         monitorFinding();
      }
      
      function jzebraReady() {
          // Change title to reflect version
          qz = document.getElementById('qz');
	  
      }
      
      /**
       * By default, jZebra prevents multiple instances of the applet's main 
       * JavaScript listener thread to start up.  This can cause problems if
       * you have jZebra loaded on multiple pages at once. 

       * 
       * The downside to this is Internet Explorer has a tendency to initilize the
       * applet multiple times, so use this setting with care.
       */
      function allowMultiple() {
          
          if (qz != null) {
              var multiple = qz.getAllowMultipleInstances();
              qz.allowMultipleInstances(!multiple);
              alert('Allowing of multiple applet instances set to "' + !multiple + '"');
          }
      }
      
      function printPage() {
           $("#content").html2canvas({ 
                canvas: hidden_screenshot,
                onrendered: function() {printBase64Image($("canvas")[0].toDataURL('image/png'));}
           });
      }
      
      function printBase64Image(base64data) {
         
      	 if (qz != null) {
               qz.findPrinter("\\{dummy printer name for listing\\}");
               while (!qz.isDoneFinding()) {
                    // Note, endless while loops are bad practice.
               }

               var printers = qz.getPrinters().split(",");
               for (i in printers) {
		    if (printers[i].indexOf("Microsoft XPS") != -1 || 
			printers[i].indexOf("PDF") != -1) {
			   qz.setPrinter(i);      
		    }	       
               }
               
               // No suitable printer found, exit
               if (qz.getPrinter() == null) {
                   alert("Could not find a suitable printer for printing an image.");
                   return;
               }

               // Optional, set up custom page size.  These only work for PostScript printing.
               // setPaperSize() must be called before setAutoSize(), setOrientation(), etc.
               qz.setPaperSize("8.5in", "11.0in");  // US Letter
               qz.setAutoSize(true);
               qz.appendImage(base64data);
	    }

            // Very important for images, uses printPS() insetad of print()
            // *Note:  monitorAppending2() still works but is too complicated and
            // outdated.  Instead create a JavaScript  function called 
            // "jzebraDoneAppending()" and handle your next steps there.
	    monitorAppending2();
      }

      function logFeatures() {
          if (document.jzebra != null) {
              
              var logging = qz.getLogPostScriptFeatures();
              qz.setLogPostScriptFeatures(!logging);
              alert('Logging of PostScript printer capabilities to console set to "' + !logging + '"');
          }
      }
   
      function useAlternatePrinting() {
          
          if (qz != null) {
              var alternate = qz.isAlternatePrinting();
              qz.useAlternatePrinting(!alternate);
              alert('Alternate CUPS printing set to "' + !alternate + '"');
          }
      }
	  
	  function listSerialPorts() {
		
                if (qz != null) {
			qz.findPorts();
            while (!qz.isDoneFindingPorts()) {} // wait
			var ports = qz.getPorts().split(",");
			for (p in ports) {
				if (p == 0) {
					document.getElementById("port_name").value = ports[p];
				}
				alert(ports[p]);
			}
		}
	  }
	  
	  function openSerialPort() {
		
                if (qz != null) {
                    qz.openPort(document.getElementById("port_name").value);
		}
	  }
          
          function closeSerialPort() {
		
                if (qz != null) {
                    qz.closePort(document.getElementById("port_name").value);
		}
	  }
          
          // Automatically gets fired with the port is finished opening (even if it fails to open)
          function jzebraDoneOpeningPort(portName) {
              
              if (qz != null) {
                  var e = qz.getException();
                  if (e != null) {
                      alert("Could not open port [" + portName + "] \n\t" + e.getLocalizedMessage());
                      qz.clearException();
                  } else {
                      alert("Port [" + portName +  "] is open!");
                  }
              }
          }
          
          // Automatically gets fired with the port is finished closing (even if it fails to close)
          function jzebraDoneClosingPort(portName) {
              
              if (qz != null) {
                  var e = qz.getException();
                  if (e != null) {
                      alert("Could not close port [" + portName + "] \n\t" + e.getLocalizedMessage());
                      qz.clearException();
                  } else {
                      alert("Port [" + portName +  "] closed!");
                  }
              }
          }
          
          function sendSerialData() {
		
                if (qz != null) {
                    // Beggining and ending patterns that signify port has responded
                    // chr(2) and chr(13) surround data on a Mettler Toledo Scale
                    qz.setSerialBegin(chr(2));
                    qz.setSerialEnd(chr(13));
                    // Baud rate, data bits, stop bits, parity, flow control
                    // "9600", "7", "1", "even", "none" = Default for Mettler Toledo Scale
                    qz.setSerialProperties("9600", "7", "1", "even", "none");
                    // Send raw commands to the specified port.
                    // W = weight on Mettler Toledo Scale
                    qz.send(document.getElementById("port_name").value, "\nW\n");
                    
                    var e = qz.getException();
                    if (e != null) {
                        alert("Could not send data:\n\t" + e.getLocalizedMessage());
                        qz.clearException();  
                    }
		}
	  }
          
          // Automatically gets called when the serial port responds with data
          function jzebraSerialReturned(portName, data) {
            if (data == null || data == "") {       // Test for blank data
                alert("No data was returned.")
            } else if (data.indexOf("?") !=-1) {    // Test for bad data
                alert("Device not ready.  Please wait.")
            } else {                                // Display good data
                alert("Port [" + portName + "] returned data:\n\t" + data);
            }
          }

   </script>

   </head>
   <body id="content" bgcolor="#FFF380">
	
	
   <input type=button onClick="useDefaultPrinter()" value="Use Default Printer"><br /><br />
   <applet id="qz" name="QZ Print Plugin" code="qz.PrintApplet.class" width="55" height="55">
	  <param name="jnlp_href" value="<?php echo base_url('assets/qz/dist/qz-print_jnlp.jnlp') ?>">
          <param name="cache_option" value="plugin">
   </applet>
   
   
   
   <input type=button onClick="print()" value="Print" />    
   
   <table id="hasil" border="1">
		<tr>
			<td> No. Kwitansi
			</td>
			<td> Nama Pasien
			</td>
			<td> Dokter Pengirim
			</td>
			<td> Biaya
			</td>
			<td> Disc.
			</td>
			<td> Cust. Serv
			</td>
			<td> Pembayaran
			</td>
			
		</tr>
		<tr>
			<td>
			</td>
			<td> dan Tindakan
			</td>
			<td> dan Dokter Pembaca
			</td>
			<td>
			</td>
			<td>
			</td>
			<td> dan Netto
			</td>
			<td>
			</td>
			
		</tr>
		<tr>
			<td>============
			</td>
			<td> ============
			</td>
			<td> ==================
			</td>
			<td>=========
			</td>
			<td>=========
			</td>
			<td>=========
			</td>
			<td>=========
			</td>
			
		</tr>
		<?php 
		$totalharga=0;
		$totaldiskon=0;
		$totalnetto=0;
		$totalpembayaran1=0;
		$totalpembayaran2=0;
		foreach ($transaksihariini as $transaksi_item):?>
		<tr>
			<td> <?php echo $transaksi_item['nomor_registrasi'];?>
			</td>
			<td> <?php echo $transaksi_item['nama_pasien'];?>
			</td>
			<td> <?php echo $transaksi_item['nama_dokter'];?>
			</td>
			<td> 
			</td>
			<td> 
			</td>
			<td> Aaaa
			</td>
			<td> <?php 
			echo $pembayaran[''.$transaksi_item['nomor_registrasi']][0]['namacarabayar'];
			
			?>
			</td>
			
		</tr>
		<?php 
		$biaya=0;
		$diskon=0;
		$netto=0;;
		foreach ($detail[''.$transaksi_item['nomor_registrasi']] as $detail_item):
		$biaya=$biaya+$detail_item['harga'];
		$diskon=$diskon+$detail_item['nominal_diskon'];
		$netto=$netto+$detail_item['netto'];
		?>
		<tr>
			<td>
			</td>
			<td> <?php echo $detail_item['nama_layanan'];?>
			</td>
			<td> <?php echo $detail_item['nama_dokter'];?>
			</td>
			<td> <?php echo $detail_item['harga'];?>
			</td>
			<td> <?php echo $detail_item['nominal_diskon'];?>
			</td>
			<td> <?php echo $detail_item['netto'];?>
			</td>
			<td>
			</td>
			
		</tr>
		<?php endforeach; ?>
		<tr>
			<td>
			</td>
			<td> 
			</td>
			<td> SUBTOTAL
			</td>
			<td> <?php echo $biaya;?>
			</td>
			<td> <?php echo $diskon;?>
			</td>
			<td> <?php echo $netto;?>
			</td>
			<td><?php echo $pembayaran[''.$transaksi_item['nomor_registrasi']][0]['jumlah'];?>
			</td>
			
		</tr>
		<tr>
			<td> 
			</td>
			<td> 
			</td>
			<td> 
			</td>
			<td> 
			</td>
			<td> 
			</td>
			<td> 
			</td>
			<td> 
			</td>
		</tr>	
		<?php 
		$totalharga=$totalharga+$biaya;
		$totaldiskon=$totaldiskon+$diskon;
		$totalnetto=$totalnetto+$netto;
		if($pembayaran[''.$transaksi_item['nomor_registrasi']][0]['tipe']==1)
		$totalpembayaran1=$totalpembayaran1+$pembayaran[''.$transaksi_item['nomor_registrasi']][0]['jumlah'];
		else if ($pembayaran[''.$transaksi_item['nomor_registrasi']][0]['tipe']==2)
		$totalpembayaran2=$totalpembayaran2+$pembayaran[''.$transaksi_item['nomor_registrasi']][0]['jumlah'];
		
		$totalpembayaran=$totalpembayaran1+$totalpembayaran2;
		endforeach; ?>
		<tr>
			<td>============
			</td>
			<td> ============
			</td>
			<td> ==================
			</td>
			<td>=========
			</td>
			<td>=========
			</td>
			<td>=========
			</td>
			<td>=========
			</td>
			
		</tr>
		<tr>
			<td>
			</td>
			<td> 
			</td>
			<td> TOTAL
			</td>
			<td> <?php echo $totalharga; ?>
			</td>
			<td> <?php echo $totaldiskon; ?>
			</td>
			<td><?php echo $totalnetto; ?>
			</td> 
			<td> <?php echo $totalpembayaran; ?>
			</td>
			
		</tr>
	  </table>
   <div id="tes" style="font-family: monospace; font-size: 12px;"></div>
   
   </body>
</html>


