<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
		
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">EDIT TRANSAKSI</legend>

		<form class="form-horizontal" id="form1" action="<?php echo site_url("transaksi/update_transaksi"); ?>" method="POST">
			<?php
			$tanggal =date('j-m-Y', strtotime($transaksi['tanggal']));
			$jam =date('G:i:s', strtotime($transaksi['tanggal']));
			$date=date("d-m-Y");
			?>
			
		
			<div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
					<label class="control-label" for="inputTanggal">Tanggal</label>
					<div class="input-append date" id="dp3" data-date="<?php echo $date; ?>" data-date-format="dd-mm-yyyy">
						<input type="text" value="<?php echo $tanggal; ?>" name="tanggalawal" id="tanggalawal" readonly>
					</div>	
				</div>	
				<div class="col-md-4">
						<label class="control-label" for="inputReg">No. Registrasi</label>
						<input type="text" id="inputReg" name="inputReg" value="<?php echo $transaksi['nomor_registrasi']; ?>" readonly>
				</div>		
				<div class="col-md-4">
					<label class="control-label" for="inputID">Dokter Pengirim</label>
			  
					<input type="text" id="referensi" name="referensi" placeholder="Referensi" autocomplete="off" value="<?php echo $transaksi['nama_dokter']; ?>">
					<input type="hidden" id="idreferensi" name="idreferensi" value="<?php echo $transaksi['referensi']; ?>" >
				</div>	
			
			</div>
			</div>
			
			<div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
					<label class="control-label" for="inputID">Nomor ID Pasien</label>
					<input type="text" id="inputID" name="inputID" placeholder="Nomor ID Pasien" autocomplete="off" value="<?php echo $transaksi['pasien']; ?>">
					
				</div>
				<div class="col-md-4">
					<label class="control-label" for="inputID">Nama Pasien</label>
					<input type="text" id="inputID2" name="inputID2" readonly value="<?php echo strtoupper($transaksi['nama_pasien']); ?>">
				</div>
				<div class="col-md-4">
					<div class="form-group">
					<label class="control-label" for="pasien_baru">&nbsp;</label>
					<br>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button style="vertical-align:bottom;" type="button" onclick="pasienbaru(); return false;" id="pasien_baru" class="btn btn-info"> Pasien Baru</button>
					&nbsp;&nbsp;&nbsp;&nbsp;
				  <label class="checkbox inline">
					Pasien&nbsp;Baru?<input type="checkbox" id="baru" name="baru" value="1" <?php if($transaksi['baru']==1) echo 'checked'; ?> > 
				  </label>
				  </div>
				  
				</div>
			</div>
			</div>
		 
		  
		  <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
		  
		  
			
			<div class="row">
			<div class="col-md-12">
			<div class="alert alert-info">
				<div class="container-fluid" style="margin-top:-15px; ">
				<div class="row-fluid">
			
              <div class="col-md-4" style="background-color:#90CA77; padding-bottom:20px; padding-top:20px;">
			  
			  <div class="row">
				<div class="col-md-13">
				<div class="col-md-12">
				<label class="control-label" style="color:#fff;" for="pemeriksaan">Pemeriksaan</label>
			
				<input type="hidden" id="pemeriksaan" name="pemeriksaan" value="0">
				<input type="text" id="namapemeriksaan" name="namapemeriksaan" placeholder="Pemeriksaan" autocomplete="off">
			  
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-12">
				<label class="control-label" for="dokter" style="color:#fff;">Dokter Operator</label>
			
				<input type="text" id="dokter" name="dokter" autocomplete="off" placeholder="Dokter">
				<input type="hidden" id="iddokter" name="dokter" value="0">
			  
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-12">
				<label class="control-label" for="harga" style="color:#fff;">Harga</label>
				<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">Rp</span>
				<input type="text" id="harga" name="harga"  autocomplete="off">
				</div>
			  
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-5">
				<label class="control-label" for="diskon" style="color:#fff;">Diskon</label>
				<div class="input-group margin-bottom-sm">
				<input type="text" id="diskon" name="diskon" autocomplete="off">
				<span class="input-group-addon">%</span>
				</div>
				
				</div>	
				
				
				<div class="col-md-7">
				<label class="control-label" for="diskon">&nbsp;</label>
				<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">Rp</span>
				<input type="text" id="nominaldiskon" name="nominaldiskon"  autocomplete="off">
				</div>
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-12">
				<label class="control-label" for="harganetto" style="color:#fff;">Netto</label>
				<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">Rp</span>
				<input type="text" id="harganetto" name="harganetto"  autocomplete="off">
				</div>
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-12">
				<label class="control-label" for="fee1" style="color:#fff;">Fee Pengirim</label>
				<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">Rp</span>
				<input type="text" id="fee1" name="fee1" autocomplete="off">
				<input type="hidden" id="temp_fee1" name="temp_fee1" >
				</div>
				</div>	
				</div>	
			 </div>
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-12">
				<label class="control-label" for="fee2" style="color:#fff;">Fee Operator</label>
				<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">Rp</span>
				<input type="text" id="fee2" name="fee2"  autocomplete="off">
				<input type="hidden" id="temp_fee2" name="temp_fee2" >
				</div>
			  
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-12">
				<br>
				<button type="button" onclick="load_data_ajax(); return false;" id="tambah_layanan" class="btn btn-danger"> Tambahkan</button>
			  
				</div>	
				</div>	
			 </div>	
			
		</div>
            <div class="col-md-8">
				  
					
					   <div class="row">
					   <div class="col-md-12">
					   <div class="col-md-12">
					   <div id="tabel_layanan"></div>
					   </div>
					   </div>
					   </div>
					   
					   <div class="row">
							<div class="col-md-12">
							<div class="col-md-4">
							<label class="control-label" for="totalharga">Sub Total</label>
							<input type="text" id="totalharga" name="totalharga" readonly value="0">
						  
							</div>	
							
							<div class="col-md-4">
							<label class="control-label" for="totalharga">Diskon (%)</label>
							<div class="input-group margin-bottom-sm">
							<input type="text" id="totaldiskon" name="totaldiskon" autocomplete="off"  value="<?php echo $transaksi['diskon']; ?>">
							<span class="input-group-addon">%</span>
							</div>
						  
							</div>	
							
							<div class="col-md-4">
							<label class="control-label" for="totalharga">Nominal Diskon (Rp)</label>
							<div class="input-group margin-bottom-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="totalnominaldiskon" name="totalnominaldiskon" autocomplete="off" value="<?php echo $transaksi['nominal_diskon']; ?>">
							</div>
						  
							</div>	
							</div>	
						 </div>
						 
						 <div class="row">
							<div class="col-md-12">
							<div class="col-md-6">
							<label class="control-label" for="totalharganetto">Harga Netto</label>
							<p id="labelnetto" style="color:#CC0000; font-size:32px; font-weight:bold;"> Rp 0 </p>
								<input type="hidden" id="totalharganetto" name="totalharganetto"  value="0" >
						  
							</div>	
							<input type="hidden" id="jumlahbayar" name="jumlahbayar"  value="<?php echo $transaksi['bayar']; ?>" >
							<input type="hidden" id="sisa" name="sisa"  value="0" >
							<div class="col-md-6" >
								<label class="control-label" for="totalharganetto">Kekurangan Pembayaran</label>
									
											<p id="labelsisa" style="color:#CC0000; font-size:32px; font-weight:bold;">Rp <?php echo strtoupper($transaksi['sisa']); ?>,- </p>
									
							</div>
							
							</div>	
						 </div>
						 <div class="row">
						 <div class="col-md-12">
						 <div class="col-md-12">
						 <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
						 </div>
						 </div>
						 </div>
						 <div class="row">
							<div class="col-md-12">
							<div class="col-md-6">
							<p>
							<a href="<?php echo site_url("transaksi/tambah_pembayaran/".$transaksi['nomor_registrasi']); ?>"  data-target="#registerModal" data-toggle="modal" class="btn btn-small btn-warning" ><i class="fa fa-plus-circle"></i> Tambah Pembayaran</a>
							</p>
						  
							</div>	
							
							</div>	
						 </div>
						 
						 <div class="row">
							<div class="col-md-12">
							<div class="col-md-12">
								<table class="table table-bordered table-condensed " >
						<thead>
						<tr style="background-color: #90CA77; color:#fff; font-size:14px;">
						  <th>Tanggal</th>
						  <th>Cara Bayar</th>
						  <th>Jumlah Pembayaran</th>
						  <th>Charge Bank</th>
						  <th>Action</th>
						</tr>
					  </thead>
						<tbody style="font-size:14px;">
						<?php
						$i=0;
						$netto=0;
						
							foreach ($pembayaran as $pembayaran_item): 
							$tanggal_bayar =date('j-m-Y', strtotime($pembayaran_item['tanggal']));
							$jam_bayar =date('G:i', strtotime($pembayaran_item['tanggal']));
							?>
							
							<tr style="background-color: #fff;">
							  
							  <td><?php echo $tanggal_bayar ?></td>
							  <td><?php echo $pembayaran_item['namacarabayar']; ?></td>
							  <td style="text-align:right; "><p class="text-right"><?php echo formatRupiah($pembayaran_item['jumlah']); ?></p> </td>
							  <td style="text-align:right; "><p class="text-right"><?php echo formatRupiah($pembayaran_item['chargebank']); ?></p> </td>
							  <?php
							  $temp_fun="delete_pembayaran('".$transaksi['nomor_registrasi']."','".$pembayaran_item['tanggal']."',".$pembayaran_item['carabayar'].",".$pembayaran_item['jumlah'].")";
							  $tanggal_encode=str_replace(" ", "%20", $pembayaran_item['tanggal']);
							  ?>
							  <td>
							  
							  
							  <a href="<?php echo site_url("transaksi/edit_pembayaran/".$transaksi['nomor_registrasi']."/".$tanggal_encode."/".$pembayaran_item['carabayar']."/".$pembayaran_item['jumlah']); ?>"  data-target="#editModal" data-toggle="modal" class="btn btn-small btn-info" ><i class="fa fa-edit"></i> Edit</a>
							  
							  
							  <a href="<?php echo site_url("transaksi/delete_pembayaran/".$transaksi['nomor_registrasi']."/".$pembayaran_item['tanggal']."/".$pembayaran_item['carabayar']."/".$pembayaran_item['jumlah']); ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
							  
							  </td>
							</tr>
						<?php 
						endforeach ?>	
							<tr style="">
							  <td colspan="1" style="text-align:center; color:#CC0000; font-weight:bold; font-size:16px;">Total Pembayaran</td>
							  <td colspan="2" style="text-align:right;  font-size:20px; color:#CC0000; font-weight:bold;"><p class="text-right"><?php echo formatRupiah($transaksi['bayar']); ?></p></td>
							  <td></td>
							  
							</tr>
						  </tbody>
					</table>
					
							</div>	
							
							</div>	
						 </div>
					   
						<div class="row">
						 <div class="col-md-12">
						 <div class="col-md-12">
						 <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
						 </div>
						 </div>
						 </div>
					  <div class="row">
						<div class="col-md-12">
						<div class="col-md-9">
						  &nbsp;
						</div>
						<div class="col-md-3">
						  <button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Simpan Transaksi</button>
						</div>
						</div>
					  </div>
					
				</div>
            </div>
			</div>
			</div>
			</div>
			
		  </div>
		  </div>
		  
		  
		</form>
   		
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>




<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/transition.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/modal.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>

<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>
	
<script type="text/javascript">

function hitung_netto() 
	{
     var harga=$('#harga').val();
	 var nominaldiskon=$('#nominaldiskon').val();
     
	 var netto=harga-nominaldiskon;
     $("#harganetto").val(netto);
	 
	 var temp_fee1=$('#temp_fee1').val();
	 var temp_fee2=$('#temp_fee2').val();
	 
	 var fee1=(netto/harga)*temp_fee1;
	 var fee2=(netto/harga)*temp_fee2;
	 
	 $("#fee1").val(Math.floor(fee1));
	 $("#fee2").val(Math.floor(fee2));
	}
function hitung_persen_diskon() 
	{
     var harga=$('#harga').val();
	 var nominaldiskon=$('#nominaldiskon').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#diskon").val(diskon);
	}

function hitung_nominal_diskon() 
	{
     var harga=$('#harga').val();
	 var diskon=$('#diskon').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#nominaldiskon").val(nominaldiskon);
	}
	
function hitung_total_netto() 
	{
     var harga=$('#totalharga').val();
	 var nominaldiskon=$('#totalnominaldiskon').val();
     
	 var netto=harga-nominaldiskon;
     $("#totalharganetto").val(netto);
	 $("#labelnetto").text('Rp '+formatRupiah(netto));
	 hitung_sisa();
	}

function hitung_total_persen_diskon() 
	{
     var harga=$('#totalharga').val();
	 var nominaldiskon=$('#totalnominaldiskon').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#totaldiskon").val(diskon);
	}

function hitung_total_nominal_diskon() 
	{
     var harga=$('#totalharga').val();
	 var diskon=$('#totaldiskon').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#totalnominaldiskon").val(nominaldiskon);
	}

	
function hitung_netto2() 
	{
     var harga=$('#harga2').val();
	 var nominaldiskon=$('#nominaldiskon2').val();
     
	 var netto=harga-nominaldiskon;
     $("#harganetto2").val(netto);
	}
function hitung_persen_diskon2() 
	{
     var harga=$('#harga2').val();
	 var nominaldiskon=$('#nominaldiskon2').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#diskon2").val(diskon);
	}

function hitung_nominal_diskon2() 
	{
     var harga=$('#harga2').val();
	 var diskon=$('#diskon2').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#nominaldiskon2").val(nominaldiskon);
	}
	
function hitung_total_netto2() 
	{
     var harga=$('#totalharga2').val();
	 var nominaldiskon=$('#totalnominaldiskon2').val();
     
	 var netto=harga-nominaldiskon;
     $("#totalharganetto2").val(netto);
	}
function hitung_total_persen_diskon2() 
	{
     var harga=$('#totalharga2').val();
	 var nominaldiskon=$('#totalnominaldiskon2').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#totaldiskon2").val(diskon);
	}

function hitung_total_nominal_diskon2() 
	{
     var harga=$('#totalharga2').val();
	 var diskon=$('#totaldiskon2').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#totalnominaldiskon2").val(nominaldiskon);
	}
	
function hitung_netto3() 
	{
     var harga=$('#harga3').val();
	 var nominaldiskon=$('#nominaldiskon3').val();
     
	 var netto=harga-nominaldiskon;
     $("#harganetto3").val(netto);
	}
function hitung_persen_diskon3() 
	{
     var harga=$('#harga3').val();
	 var nominaldiskon=$('#nominaldiskon3').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#diskon3").val(diskon);
	}

function hitung_nominal_diskon3() 
	{
     var harga=$('#harga3').val();
	 var diskon=$('#diskon3').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#nominaldiskon3").val(nominaldiskon);
	}
	
function hitung_total_netto3() 
	{
     var harga=$('#totalharga3').val();
	 var nominaldiskon=$('#totalnominaldiskon3').val();
     
	 var netto=harga-nominaldiskon;
     $("#totalharganetto3").val(netto);
	}
function hitung_total_persen_diskon3() 
	{
     var harga=$('#totalharga3').val();
	 var nominaldiskon=$('#totalnominaldiskon3').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#totaldiskon3").val(diskon);
	}

function hitung_total_nominal_diskon3() 
	{
     var harga=$('#totalharga3').val();
	 var diskon=$('#totaldiskon3').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#totalnominaldiskon3").val(nominaldiskon);
	}
function hitung_sisa()
	{
     
	 var harga=$('#totalharganetto').val();
	 var bayar=$('#jumlahbayar').val();
     
	 var sisa=harga-bayar;
     $("#sisa").val(sisa);
	 $("#labelsisa").text('Rp '+formatRupiah(sisa));
	}

$(document).ready(function(){

$('body').on('hidden.bs.modal', '.modal', function () {
    $(this).removeData('bs.modal');
});

var data = [<?php echo $testing; ?>];
var data2 = [<?php echo $testing2; ?>];
var data3 = [<?php echo $testing3; ?>];
var reg='<?php echo $transaksi["nomor_registrasi"]; ?>';
$.ajax({
	'url' : base_url + controller + '/show_table',
	'type' : 'POST', //the way you want to send data to your URL
	'data' : {'reg' : reg},
	'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
		var container = $('#tabel_layanan'); //jquery selector (get element by id)
		if(data){
			
			container.html(data);
			var temp = $('#temptotalharga').val();
			$('#totalharga').val(temp);
			hitung_total_netto();
		}
	}
});

$('#inputID').typeahead({
    source: function (query, process) {
        states = [];
		map = {};
		
		var source = [];
		$.each(data, function (i, state) {
			map[state.stateName] = state;
			states.push(state.stateName);
		});
	 
		process(states);
		
    },
    updater: function (item) {
        
		selectedState = map[item].stateCode;
		selectedState2 = map[item].stateDisplay;
		$("#inputID2").val(selectedState2);
		return selectedState;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
    },
});

$('#referensi').typeahead({
    source: function (query, process) {
        states2 = [];
		map2 = {};
		
		var source = [];
		$.each(data2, function (i, state) {
			map2[state.stateName] = state;
			states2.push(state.stateName);
		});
	 
		process(states2);
		
    },
    updater: function (item) {
        
		selectedState = map2[item].stateCode;
		selectedState2 = map2[item].stateDisplay;
		$("#idreferensi").val(selectedState);
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#dokter').typeahead({
    source: function (query, process) {
        states3 = [];
		map3 = {};
		
		var source = [];
		$.each(data2, function (i, state) {
			map3[state.stateName] = state;
			states3.push(state.stateName);
		});
	 
		process(states3);
		
    },
    updater: function (item) {
        
		selectedState = map3[item].stateCode;
		selectedState2 = map3[item].stateDisplay;
		$("#iddokter").val(selectedState);
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#dokter2').typeahead({
    source: function (query, process) {
        states3 = [];
		map3 = {};
		
		var source = [];
		$.each(data2, function (i, state) {
			map3[state.stateName] = state;
			states3.push(state.stateName);
		});
	 
		process(states3);
		
    },
    updater: function (item) {
        
		selectedState = map3[item].stateCode;
		selectedState2 = map3[item].stateDisplay;
		$("#iddokter2").val(selectedState);
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#dokter3').typeahead({
    source: function (query, process) {
        states3 = [];
		map3 = {};
		
		var source = [];
		$.each(data2, function (i, state) {
			map3[state.stateName] = state;
			states3.push(state.stateName);
		});
	 
		process(states3);
		
    },
    updater: function (item) {
        
		selectedState = map3[item].stateCode;
		selectedState2 = map3[item].stateDisplay;
		$("#iddokter3").val(selectedState);
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#namapemeriksaan').typeahead({
    source: function (query, process) {
        states4 = [];
		map4 = {};
		
		var source = [];
		$.each(data3, function (i, state) {
			map4[state.stateName] = state;
			states4.push(state.stateName);
		});
	 
		process(states4);
		
    },
    updater: function (item) {
        
		selectedState = map4[item].stateCode;
		selectedState2 = map4[item].stateDisplay;
		harga = map4[item].harga;
		diskon = map4[item].diskon;
		fee1 = map4[item].fee1;
		fee2 = map4[item].fee2;
		$("#pemeriksaan").val(selectedState);
		$("#harga").val(harga);
		$("#diskon").val(diskon);
		$("#fee1").val(fee1);
		$("#fee2").val(fee2);
		$("#temp_fee1").val(fee1);
		$("#temp_fee2").val(fee2);
		hitung_nominal_diskon();
		hitung_netto();
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#namapemeriksaan2').typeahead({
    source: function (query, process) {
        states4 = [];
		map4 = {};
		
		var source = [];
		$.each(data3, function (i, state) {
			map4[state.stateName] = state;
			states4.push(state.stateName);
		});
	 
		process(states4);
		
    },
    updater: function (item) {
        
		selectedState = map4[item].stateCode;
		selectedState2 = map4[item].stateDisplay;
		harga = map4[item].harga;
		diskon = map4[item].diskon;
		fee1 = map4[item].fee1;
		fee2 = map4[item].fee2;
		$("#pemeriksaan2").val(selectedState);
		$("#harga2").val(harga);
		$("#diskon2").val(diskon);
		$("#fee12").val(fee1);
		$("#fee22").val(fee2);
		hitung_nominal_diskon2();
		hitung_netto2();
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#namapemeriksaan3').typeahead({
    source: function (query, process) {
        states4 = [];
		map4 = {};
		
		var source = [];
		$.each(data3, function (i, state) {
			map4[state.stateName] = state;
			states4.push(state.stateName);
		});
	 
		process(states4);
		
    },
    updater: function (item) {
        
		selectedState = map4[item].stateCode;
		selectedState2 = map4[item].stateDisplay;
		harga = map4[item].harga;
		diskon = map4[item].diskon;
		fee1 = map4[item].fee1;
		fee2 = map4[item].fee2;
		$("#pemeriksaan3").val(selectedState);
		$("#harga3").val(harga);
		$("#diskon3").val(diskon);
		$("#fee13").val(fee1);
		$("#fee23").val(fee2);
		hitung_nominal_diskon3();
		hitung_netto3();
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#diskon').keyup(function() {
			hitung_nominal_diskon();
			hitung_netto();
});

$('#nominaldiskon').keyup(function() {
			hitung_persen_diskon();
			hitung_netto();
});

$('#harga').keyup(function() {
			hitung_netto();
});

$('#nominaldiskon').keyup(function() {
			hitung_persen_diskon();
			hitung_netto();
});

$('#totaldiskon').keyup(function() {
			hitung_total_nominal_diskon();
			hitung_total_netto();
});

$('#totalnominaldiskon').keyup(function() {
			hitung_total_persen_diskon();
			hitung_total_netto();
});


$('#diskon2').keyup(function() {
			hitung_nominal_diskon2();
			hitung_netto2();
});

$('#nominaldiskon2').keyup(function() {
			hitung_persen_diskon2();
			hitung_netto2();
});

$('#totaldiskon2').keyup(function() {
			hitung_total_nominal_diskon2();
			hitung_total_netto2();
});

$('#totalnominaldiskon2').keyup(function() {
			hitung_total_persen_diskon2();
			hitung_total_netto2();
});

$('#diskon3').keyup(function() {
			hitung_nominal_diskon3();
			hitung_netto3();
});

$('#nominaldiskon3').keyup(function() {
			hitung_persen_diskon3();
			hitung_netto3();
});

$('#totaldiskon3').keyup(function() {
			hitung_total_nominal_diskon3();
			hitung_total_netto3();
});

$('#totalnominaldiskon3').keyup(function() {
			hitung_total_persen_diskon3();
			hitung_total_netto3();
});

$('#jumlahbayar').keyup(function() {
			hitung_sisa();
});

});

function formatRupiah(nilaiUang2)
{
	var nilaiUang=nilaiUang2+"";
  var nilaiRupiah 	= "";
  var jumlahAngka 	= nilaiUang.length;
  
  while(jumlahAngka > 3)
  {
	
	sisaNilai = jumlahAngka-3;
    nilaiRupiah = "."+nilaiUang.substr(sisaNilai,3)+""+nilaiRupiah;
    
    nilaiUang = nilaiUang.substr(0,sisaNilai)+"";
    jumlahAngka = nilaiUang.length;
  }
 
  nilaiRupiah = nilaiUang+""+nilaiRupiah+",-";
  return nilaiRupiah;
}

var controller = 'transaksi';
var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';

function load_data_ajax(){
var reg='<?php echo $transaksi["nomor_registrasi"]; ?>';
var id_layanan=$('#pemeriksaan').val();
var id_dokter=$('#iddokter').val();
var harga=$('#harga').val();
var diskon=$('#diskon').val();
var nominal_diskon=$('#nominaldiskon').val();
var netto=$('#harganetto').val();
var fee1=$('#fee1').val();
var fee2=$('#fee2').val();


if (id_layanan==0 || id_layanan=="")
{
	alert('Masukkan Layanan');
}
else
{
	if(id_dokter==0 || id_dokter=="")
	{
		alert('Masukkan Dokter');
	}
	else
	{
		$.ajax({
			'url' : base_url + controller + '/insert_layanan',
			'type' : 'POST', //the way you want to send data to your URL
			'data' : {'reg' : reg, 'id_layanan' : id_layanan, 'id_dokter': id_dokter, 'harga' : harga, 'diskon' : diskon, 'nominal_diskon' : nominal_diskon, 'netto' : netto, 'fee1' : fee1, 'fee2' : fee2},
			'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
				var container = $('#tabel_layanan'); //jquery selector (get element by id)
				if(data){
					container.html(data);
					var temp = $('#temptotalharga').val();
					$('#totalharga').val(temp);
					hitung_total_nominal_diskon();
					hitung_total_netto();
					
					$('#pemeriksaan').val(0);
					$('#iddokter').val(0);
					$('#harga').val("0");
					$('#diskon').val("0");
					$('#nominaldiskon').val("0");
					$('#harganetto').val("0");
					$('#namapemeriksaan').val("");
					$('#dokter').val("");
					$('#fee1').val(0);
					$('#fee2').val(0);
					/*
					$('#pemeriksaan3').val(0);
					$('#iddokter3').val(0);
					$('#harga3').val("0");
					$('#diskon3').val("0");
					$('#nominaldiskon3').val("0");
					$('#harganetto3').val("0");
					$('#namapemeriksaan3').val("");
					$('#dokter3').val("");
					$('#fee12').val(0);
					$('#fee22').val(0);
					
					$('#pemeriksaan2').val(0);
					$('#iddokter2').val(0);
					$('#harga2').val("0");
					$('#diskon2').val("0");
					$('#nominaldiskon2').val("0");
					$('#harganetto2').val("0");
					$('#namapemeriksaan2').val("");
					$('#dokter2').val("");
					$('#fee13').val(0);
					$('#fee23').val(0);
					*/
					
				}
			}
		});
	}
}
}

function pasienbaru(){
	window.location.href = base_url + '/pasien/pasien_baru/1';
}

function delete_layanan( nomor_register, id_layanan, id_dokter,	harga, diskon, nominal_diskon, netto	){

	$.ajax({
		'url' : base_url + controller + '/delete_layanan',
		'type' : 'POST', //the way you want to send data to your URL
		'data' : {'reg' : nomor_register, 'id_layanan' : id_layanan, 'id_dokter': id_dokter, 'harga' : harga, 'diskon' : diskon, 'nominal_diskon' : nominal_diskon, 'netto' : netto},
		'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
		
			var container = $('#tabel_layanan'); //jquery selector (get element by id)
			if(data){
				container.html(data);
				var temp = $('#temptotalharga').val();
				$('#totalharga').val(temp);
				
				hitung_total_nominal_diskon();
				hitung_total_netto();
				
				
				$('#pemeriksaan').val(0);
				$('#iddokter').val(0);
				$('#harga').val("0");
				$('#diskon').val("0");
				$('#nominaldiskon').val("0");
				$('#harganetto').val("0");
				$('#namapemeriksaan').val("");
				$('#dokter').val("");
				
				$('#pemeriksaan3').val(0);
				$('#iddokter3').val(0);
				$('#harga3').val("0");
				$('#diskon3').val("0");
				$('#nominaldiskon3').val("0");
				$('#harganetto3').val("0");
				$('#namapemeriksaan3').val("");
				$('#dokter3').val("");
				
				$('#pemeriksaan2').val(0);
				$('#iddokter2').val(0);
				$('#harga2').val("0");
				$('#diskon2').val("0");
				$('#nominaldiskon2').val("0");
				$('#harganetto2').val("0");
				$('#namapemeriksaan2').val("");
				$('#dokter2').val("");
				
				
			}
		}
	});
}


</script>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Tambah Pembayaran</h3>
  </div>
  <div class="modal-body">
	
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    
  </div>
</div>


<input type="hidden" id="flag0" name="flag0"  value="1" >
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
  
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
  
</div>



</body></html>