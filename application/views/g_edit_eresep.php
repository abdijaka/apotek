<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id">
<!-- Head -->
<head>
<!-- Meta -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI APPS" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>
<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
	<section class="content content-white">
    	<div class="container container-content"> 	
    		<div class="row">
    			<div class="col-md-12">
					<legend style="text-align:center;">E-RESEP</legend>
						<form class="form-horizontal" id="form1" action="<?php echo site_url("g_eresep/update_eresep"); ?>" method="POST">	
							<div class="row">
								<div class="col-md-4">
									<input type="hidden" id="id_eresep" name="id_eresep" value="<?php echo $eresep['id_eresep']; ?>">
									<label class="control-label" for="inputTanggal">Tanggal E-Resep</label>
									<input type="text" value="<?php echo $eresep['tgl_eresep']; ?>" name="tgl_eresep" id="tgl_eresep" data-date-format="yyyy-mm-dd">
								</div>
								<div class="col-md-4">
									<label class="control-label">Dokter</label>
									<select class="form-control" name="dokter" id="dokter">
										<option value="' . $dokter . '">Pilih Dokter</option>
										<?php foreach($dokter as $dokter_item) {?>
											<option <?php if ($eresep['dokter'] == $dokter_item['id_dokter']) echo "selected"; ?> value="<?php echo $dokter_item['id_dokter']?>"><?php echo $dokter_item['nama_dokter']?></option>
										<?php }?>
									</select>
								</div>		
							</div>
							<div class="row">
								<div class="col-md-4">
									<label class="control-label" for="inputID">Pasien </label>
									<select name="pasien" id="pasien" class="form-control">
										  <option selected value="0">Pasien Baru</option>
										  <?php foreach ($pasien as $pasien_item): ?>
										  <option <?php if ($eresep['pasien'] == $pasien_item['id_pasien']) echo "selected"; ?> value="<?php echo $pasien_item['id_pasien']; ?>"><?php echo $pasien_item['nama_pasien']; ?></option>
										  <?php endforeach ?>
									</select>
								</div>	
								<div class="col-md-4">
									<label class="control-label" for="inputID">Nama Pasien Baru</label>
										<input type="text" id="nama_pasien_baru" name="nama_pasien_baru" autocomplete="off" >
								</div>	
							</div>
						  	<hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-info">
										<div class="container-fluid" style="margin-top:-15px; ">
											<div class="row-fluid">
				              					<div class="col-md-4" style="background-color:#90CA77; padding-bottom:20px; padding-top:20px;">
							  
							  						<div class="row">
														<div class="col-md-13">
															<div class="col-md-12">
																<label class="control-label" style="color:#fff;" for="pemeriksaan">Nama Item</label>
																<input type="hidden" id="id_item" name="id_item" >
																<input type="text" id="namaitem" name="namaitem" placeholder="Nama Item" autocomplete="on">
															</div>	
														</div>	
							 						</div>	
							 
													<div class="row">
														<div class="col-md-13">
															<div class="col-md-5">
																<label class="control-label" for="diskon" style="color:#fff;">Jumlah</label>
																<div class="input-group margin-bottom-sm">
																	<input type="number" id="jumlah" name="jumlah" value="0">	
																</div>
																<div id="label_sisa" class="alert-danger"></div>
																<input type="hidden" id="temp_sisa" />
															</div>
															<div class="col-md-7">
																<label class="control-label" for="diskon" style="color:#fff;">Satuan</label>
																<div class="input-group margin-bottom-sm">
																	<input type="text" id="satuan" name="satuan" >
																</div>
															</div>	
														</div>	
													</div>
													<div class="row">
														<div class="col-md-13">
															<div class="col-md-12">
																<label class="control-label" style="color:#fff;" for="pemeriksaan">Aturan Pakai</label>
																<input type="text" id="aturanpakai" name="aturanpakai" placeholder="Aturan Pakai" autocomplete="off">			  
															</div>	
														</div>	
													</div>	
													<div class="row">
														<div class="col-md-13">
															<div class="col-md-12">
																<br>
																<button type="button" onclick="load_data_ajax(); return false;" id="tambah_layanan" class="btn btn-danger">Tambahkan <i class="fa fa-chevron-right"></i></button>
															</div>	
														</div>	
													</div>	
												</div>
									            <div class="col-md-8">
													<div class="row">
														<div class="col-md-12">
															<div class="col-md-12">
																<div id="tabel_layanan"></div>
															</div>
														</div>
													</div>	
													<div class="row">
														<div class="col-md-12">
															<div class="col-md-12">
																<hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
															</div>
														</div>
													</div>
													<div class="row">
												    	<div class="col-md-12">
															<div class="col-md-12">
																<button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Update E-Resep </button>
															</div>
														</div>
													</div>
												</div>
				            				</div>
										</div>
									</div>
								</div>
						  	</div>
						</form>
					</div>	  
				</div>
			</div>
		</div>
	</section>
</section>
<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            	<div class="bptik-copy hide-mini-footer">Musi Heart Clinic</div>
            	<div class="bptik-reserved  hide-mini-footer">Surabaya</div>
        	</div>
        </div>
    </div>
</footer>

<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	
<script>
	$('.navbar-toggle-side').click(function(e){
		toggleSide(e,false)
	});
	$('.navbar-side-to-search').click(function(e){
		toggleSide(e,true)
	});		

	function toggleSide(action,search){
		action.preventDefault();
		$('.navbar-side').toggleClass('mini-side');
		$('footer').toggleClass('mini-footer');
		$('#wrap').toggleClass('mini-side-open');
		if(search)$('.side-search-input').focus();
	}

	function ShowMenuNavJadwal(nama){
		if (nama != "-1"){
			$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
			$(nama).toggleClass("in").toggleClass("fadeInRight");
		} else{
			$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		}		
	}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		var id_eresep = $("#id_eresep").val();
		$.ajax({
			'url' 		: base_url + '' + controller + '/show_all_item',
			'type' 		: 'POST',
			'data' 		: {'reg' : id_eresep},
			'success' 	: function(data) { 
				var container = $('#tabel_layanan');
				if(data) {
					container.html(data);
				}
			}
		});

		var data3 = [<?php echo $items; ?>];
		$('#namaitem').typeahead({
		    source: function (query, process) {
		        states4 = [];
				map4 = {};
				
				var source = [];
				$.each(data3, function (i, state) {
					map4[state.stateName] = state;
					states4.push(state.stateName);
				});
			 
				process(states4);
				
		    },
		    updater: function (item) {
		        
				selectedState = map4[item].stateCode;
				selectedState2 = map4[item].stateDisplay;
				satuan = map4[item].satuanbeli;
				stock_gudang = map4[item].stock_gudang;
				
				$("#id_item").val(selectedState);
				$("#satuan").val(satuan);
				$("#label_sisa").html("&nbsp;&nbsp;&nbsp;&nbsp;Sisa " + stock_gudang + " Buah");
				$("#temp_sisa").val(stock_gudang);
				return selectedState2;
		    },
		    matcher: function (item) {
		        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
					return true;
				}
		    },
		    sorter: function (items) {
		        return items.sort();
		    },
		    highlighter: function (item) {
				var regex = new RegExp( '(' + this.query + ')', 'gi' );
				return item.replace( regex, "<strong>$1</strong>" );
				
		    },
		});
		
		$('#tgl_eresep').datepicker()
	    	.on('changeDate', function(ev){
		});
	});

	var controller = 'g_eresep';
	var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';

	function load_data_ajax(){
		var reg = $('#id_eresep').val();
		var id_item = $('#id_item').val();
		var jumlah = $('#jumlah').val();
		var satuan = $('#satuan').val();
		var tanggalmasuk = $('#tanggalmasuk').val();
		var aturanpakai = $('#aturanpakai').val();


		if (id_item==0 || id_item=="")
		{
			alert('Masukkan Item');
		}
		else
		{
			if(jumlah==0 || jumlah=="")
			{
				alert('Masukkan Jumlah');
			}
			else
			{
				$.ajax({
					'url' : base_url + '' + controller + '/insert_item',
					'type' : 'POST', //the way you want to send data to your URL
					'data' : {'reg' : reg, 'id_item' : id_item, 'jumlah': jumlah, 'satuan' : satuan, 'tanggalmasuk' : tanggalmasuk, 'aturanpakai' : aturanpakai},
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
						var container = $('#tabel_layanan'); //jquery selector (get element by id)
						if(data){
							container.html(data);
							$('#id_item').val(0);
							$('#jumlah').val(0);
							$('#namaitem').val("");
							$('#satuan').val("");
							$('#aturanpakai').val("");
							$('#label_sisa').empty();
							$('#temp_sisa').val("");
						}
					}
				});
			}
		}
	}

	function pasienbaru(){
		window.location.href = base_url + 'pasien/pasien_baru/1';
	}

	function delete_item(reg, eresep){

		$.ajax({
			'url' : base_url + '' + controller + '/delete_item',
			'type' : 'POST', //the way you want to send data to your URL
			'data' : {'reg' : reg, 'eresep':eresep},
			'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
				var container = $('#tabel_layanan'); //jquery selector (get element by id)
				if(data){
					container.html(data);					
				}
			}
		});
	}
</script>
</body></html>