<?php

require('fpdf17/fpdf.php');
require_once('fpdfi/fpdi.php');

function terbilang($x)
{
$abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
if ($x < 12)
return " " . $abil[$x];
elseif ($x < 20)
return Terbilang($x - 10) . "Belas";
elseif ($x < 100)
return Terbilang($x / 10) . " Puluh" . Terbilang($x % 10);
elseif ($x < 200)
return " seratus" . Terbilang($x - 100);
elseif ($x < 1000)
return Terbilang($x / 100) . " Ratus" . Terbilang($x % 100);
elseif ($x < 2000)
return " seribu" . Terbilang($x - 1000);
elseif ($x < 1000000)
return Terbilang($x / 1000) . " Ribu" . Terbilang($x % 1000);
elseif ($x < 1000000000)
return Terbilang($x / 1000000) . " Juta" . Terbilang($x % 1000000);
}

function formatRupiah($nilaiUang)
	{
	  $nilaiRupiah 	= "";
	  $jumlahAngka 	= strlen($nilaiUang);
	  while($jumlahAngka > 3)
	  {
		$nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
		$sisaNilai = strlen($nilaiUang) - 3;
		$nilaiUang = substr($nilaiUang,0,$sisaNilai);
		$jumlahAngka = strlen($nilaiUang);
	  }
	 
	  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . "";
	  return $nilaiRupiah;
	}

function convert_number_to_words($number) {
    
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'nol',
        1                   => 'satu',
        2                   => 'dua',
        3                   => 'tiga',
        4                   => 'empat',
        5                   => 'lima',
        6                   => 'enam',
        7                   => 'tujuh',
        8                   => 'delapan',
        9                   => 'sembilan',
        10                  => 'sepuluh',
        11                  => 'sebelas',
        12                  => 'dua belas',
        13                  => 'tiga belas',
        14                  => 'empat belas',
        15                  => 'lima belas',
        16                  => 'enam belas',
        17                  => 'tujuh belas',
        18                  => 'delapan belas',
        19                  => 'sembilan belas',
        20                  => 'dua puluh',
        30                  => 'tiga puluh',
        40                  => 'empat puluh',
        50                  => 'lima puluh',
        60                  => 'enam puluh',
        70                  => 'tujuh puluh',
        80                  => 'delapan puluh',
        90                  => 'sembilan puluh',
        100                 => 'ratus',
        1000                => 'ribu',
        1000000             => 'juta',
        1000000000          => 'milyar',
        1000000000000       => 'triliun',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );
    
    if (!is_numeric($number)) {
        return false;
    }
    
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
    
    $string = $fraction = null;
    
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
    
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
    
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
    
    return $string;
}


$pdf = new FPDI('P','mm','A4');
// add a page
$pdf->AddPage();
// set the sourcefile
//load template
$pdf->setSourceFile('kwitansi_besar.pdf');
// import page 1
$tplIdx = $pdf->importPage(1);
// use the imported page and place it at point 10,10 with a width of 100 mm
$pdf->useTemplate($tplIdx, 0, 0, 0);

$pdf->SetFont('Arial','B',16);
$pdf->SetTextColor(0,0,0);

$date=date("d");
$month=date("n");
$year=date("Y");

$labelmonth="Januari";

if($month==2)
$labelmonth="Februari";
else if($month==3)
$labelmonth="Maret";
else if($month==4)
$labelmonth="April";
else if($month==5)
$labelmonth="Mei";
else if($month==6)
$labelmonth="Juni";
else if($month==7)
$labelmonth="Juli";
else if($month==8)
$labelmonth="Agustus";
else if($month==9)
$labelmonth="September";
else if($month==10)
$labelmonth="Oktober";
else if($month==11)
$labelmonth="November";
else if($month==12)
$labelmonth="Desember";

if("".$transaksi['baru']==0)
$label_baru="L";
else
$label_baru="B";

if($transaksi['tipebayar']==1)
$carabayar="TUNAI";
else if($transaksi['tipebayar']==2)
$carabayar="KREDIT";
else
$carabayar="DEBIT";

if($transaksi['initial']==0)
$initial="TN.";
else if($transaksi['initial']==1)
$initial="NY.";
else if($transaksi['initial']==2)
$initial="NN.";
else if($transaksi['initial']==3)
$initial="ANAK";
else
$initial="BAYI";

$tanggal =date('j-m-Y', strtotime($transaksi['tanggal']));
$jam =date('G:i:s', strtotime($transaksi['tanggal']));

$pdf->SetXY(70, 33);
$pdf->Write(0, $transaksi['nomor_registrasi']."(".$label_baru.")");

$pdf->SetXY(70, 44);
$pdf->Write(0, strtoupper($transaksi['nama_pasien'].', '.$initial));

$pdf->SetFont('Arial','',13);
$pdf->SetTextColor(0,0,0);

/*
$pdf->SetXY(17.5, 34.5);
$pdf->Write(0, "".$transaksi['nama_pasien']);

$pdf->SetXY(72, 34.5);
$pdf->Write(0, $jam);

$pdf->SetXY(17.5, 38);
$pdf->Write(0, "".$transaksi['nama_dokter']);
*/
$letak=78;
$no=1;
$harga=0;
$diskon=0;
$netto=0;
foreach ($detail as $detail_item):

$pdf->SetXY(70, $letak);
$pdf->Write(0, "".$detail_item['nama_layanan'].": ".$transaksi['nama_pasien'].", ".$initial);

$pdf->SetXY(70, ($letak+6));
$pdf->Write(0, "Oleh Dokter: ".$detail_item['nama_dokter']);


$no++;
$letak=$letak+12;

$harga=$harga+$detail_item['harga'];
$diskon=$diskon+$detail_item['nominal_diskon'];
$netto=$netto+$detail_item['netto'];

endforeach;

$pdf->SetXY(70, 55);
$pdf->Write(0, "#".terbilang($netto)." Rupiah #");

$pdf->SetFont('Arial','BI',16);
$pdf->SetTextColor(0,0,0);

$pdf->SetXY(70, 107);
$pdf->Write(0, "# Rp ".formatRupiah($netto).",- #");


$pdf->SetFont('Arial','',16);
$pdf->SetTextColor(0,0,0);

$pdf->SetXY(159, 122);
$pdf->Write(0, $tanggal);

$user = $this->ion_auth->user()->row();

$pdf->SetXY(145, 136);
$pdf->Write(0, $user->username);



//$pdf->SetXY(14, 100.5);
//$pdf->Write(0, convert_number_to_words($transaksi['harga_netto'])." Rupiah");

//menyimpan hasil pengolahan pdf
$pdf->Output("kwitansi_".$transaksi['nomor_registrasi'].".pdf", "D");


?>
