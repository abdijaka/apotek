<?php
require('fpdf17/mc_table.php');

function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

function get_month($month)
{
	$labelmonth="Januari";

	if($month==2)
	$labelmonth="Februari";
	else if($month==3)
	$labelmonth="Maret";
	else if($month==4)
	$labelmonth="April";
	else if($month==5)
	$labelmonth="Mei";
	else if($month==6)
	$labelmonth="Juni";
	else if($month==7)
	$labelmonth="Juli";
	else if($month==8)
	$labelmonth="Agustus";
	else if($month==9)
	$labelmonth="September";
	else if($month==10)
	$labelmonth="Oktober";
	else if($month==11)
	$labelmonth="November";
	else if($month==12)
	$labelmonth="Desember";

	return $labelmonth;

}

$pdf=new PDF_MC_Table('P','mm','A4');
$pdf->AddPage();

$pdf->SetFont('Arial','B',12);

$pdf->SetY(5);

$pdf->Cell(210, 2.7, "Laporan Fee Dokter Bulanan", 0, 0, 'C');

$pdf->SetY(10);

$pdf->Cell(210, 2.7, "Musi Heart Clinic\n", 0, 0, 'C');

$pieces = explode("-", $mulai);
$pieces2 = explode("-", $sampai);

$pdf->SetY(15);

if($mulai==$sampai)
{
	$pdf->Cell(210, 2.7, "Bulan : ".get_month($pieces[0]).' '.$pieces[1]."", 0, 0, 'C');
	$nama_bulan=get_month($pieces[0]).' '.$pieces[1];
}
else
{
	$pdf->Cell(210, 2.7, "Bulan : ".get_month($pieces[0]).' '.$pieces[1]." sampai ".get_month($pieces2[0]).' '.$pieces2[1], 0, 0, 'C');
	$nama_bulan=get_month($pieces[0]).' '.$pieces[1]." sampai ".get_month($pieces2[0]).' '.$pieces2[1];
}

$pdf->SetFont('Arial','B',9);
//Table with 20 rows and 4 columns
$pdf->SetWidths(array(10,30,30,50,40,30));
$pdf->SetY(20);
foreach ($dokter as $dokter_item):
if($id_dokter==$dokter_item['id_dokter'])
{
	$pdf->Write(0, "Dokter : ".$dokter_item['nama_dokter']);
	$nama_dokter=$dokter_item['nama_dokter'];
}

endforeach;
$pdf->SetY(25);

$pdf->Write(0, "Sebagai Pengirim\n");
$pdf->SetY(30);

$pdf->Row(array("No", "Nomor Registrasi", "Tanggal", "Nama Layanan", "Operator", "Fee Pengirim"));
$pdf->SetFont('Arial','',8);

$nomor=1;
$total_fee=0;
foreach ($laporan as $laporan_item):
$pdf->Row(array($nomor, $laporan_item['nomor_register'], $laporan_item['tanggal'], $laporan_item['nama_layanan'], $laporan_item['operator'], formatRupiah($laporan_item['fee1'])));
$nomor++;
$total_fee=$total_fee+$laporan_item['fee1'];
endforeach;
$pdf->SetFont('Arial','B',8);
$pdf->Row(array("","","","","Total Fee : ",formatRupiah($total_fee)));

$temp=$pdf->GetY();
$pdf->SetY($temp+10);

$pdf->SetFont('Arial','B',9);

$pdf->Write(0, "Sebagai Operator");

$temp=$pdf->GetY();
$pdf->SetY($temp+5);

$pdf->Row(array("No", "Nomor Registrasi", "Tanggal", "Nama Layanan", "Pengirim", "Fee Operator"));
$pdf->SetFont('Arial','',8);

$nomor=1;
$total_fee_operator=0;
foreach ($laporan2 as $laporan_item):
$pdf->Row(array($nomor, $laporan_item['nomor_register'], $laporan_item['tanggal'], $laporan_item['nama_layanan'], $laporan_item['pengirim'], formatRupiah($laporan_item['fee2'])));
$nomor++;
$total_fee_operator=$total_fee_operator+$laporan_item['fee2'];
endforeach;
$pdf->SetFont('Arial','B',8);
$pdf->Row(array("","","","","Total Fee : ",formatRupiah($total_fee_operator)));

$temp=$pdf->GetY();
$pdf->SetY($temp+10);

$pdf->SetFont('Arial','B',10);
$pdf->Write(0, "Total Fee : ".formatRupiah($total_fee+$total_fee_operator));

$pdf->Output("Fee_".$nama_dokter."_".$nama_bulan.".pdf", "D");
?>