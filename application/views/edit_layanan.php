<div class="container">
   
      <h2>Edit Layanan</h2>
      <form class="form-horizontal" action="<?php echo site_url("layanan/update_layanan"); ?>" method="POST">
		    
		  <div class="control-group">
			<label class="control-label" for="id_layanan">ID Layanan</label>
			<div class="controls control-row">
			  <input type="text" id="id_layanan" name="id_layanan" value="<?php echo $layanan['id_layanan'] ?>" readonly>
			  
			</div>
			
		  </div>
		  <div class="control-group">
			<label class="control-label" for="inputNama">Nama layanan</label>
			<div class="controls">
			
			  <input type="text" id="inputNama" name="inputNama" class="span5" value="<?php echo $layanan['nama_layanan'] ?>" >
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="harga">Harga layanan</label>
			<div class="controls">
			  <div class="input-prepend">
				<span class="add-on">Rp </span>
				<input type="text" id="harga" name="harga" class="span4" value="<?php echo $layanan['harga'] ?>">
				
			  </div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="persendiskon">Diskon</label>
			<div class="controls control-row">
			  
			  <div class="input-append">
					<input type="text" id="persendiskon" name="persendiskon" class="span1" value="<?php echo $layanan['diskon'] ?>">
					<span class="add-on">% </span>
				</div>
				<div class="input-prepend">
					<span class="add-on">Rp</span>
					<input type="text" id="nominaldiskon" name="nominaldiskon" class="input-xlarge" value="<?php echo $layanan['nominal_diskon'] ?>">	
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="harganetto">Harga Netto</label>
			<div class="controls">
			  <div class="input-prepend">
				<span class="add-on">Rp </span>
				<input type="text" id="harganetto" name="harganetto" class="span4" value="0" readonly>
				
			  </div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="share1">Fee Pengirim</label>
			<div class="controls">
			  <div class="input-append">
			  <input type="text" id="share1" name="share1" class="span1" value="<?php echo $layanan['persenshare1'] ?>">
			  <span class="add-on">% </span>
			  </div>
			  <div class="input-prepend">
					<span class="add-on">Rp</span>
					<input type="text" id="nominalshare1" name="nominalshare1" class="input-xlarge" value="<?php echo $layanan['share1'] ?>">	
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="share2">Fee Pelaksana</label>
			<div class="controls">
			  <div class="input-append">
			  <input type="text" id="share2" name="share2" class="span1" value="<?php echo $layanan['persenshare2'] ?>">
			  <span class="add-on">% </span>
			  </div>
			  <div class="input-prepend">
					<span class="add-on">Rp</span>
					<input type="text" id="nominalshare2" name="nominalshare2" class="input-xlarge" value="<?php echo $layanan['share2'] ?>">	
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="share3">Share3</label>
			<div class="controls">
			  <div class="input-append">
			  <input type="text" id="share3" name="share3" class="span1" value="<?php echo $layanan['persenshare3'] ?>">
			  <span class="add-on">% </span>
			  </div>
			  <div class="input-prepend">
					<span class="add-on">Rp</span>
					<input type="text" id="nominalshare3" name="nominalshare3" class="input-xlarge" value="<?php echo $layanan['share3'] ?>">	
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="share1">Share4</label>
			<div class="controls">
			  <div class="input-append">
			  <input type="text" id="share4" name="share4" class="span1" value="<?php echo $layanan['persenshare4'] ?>">
			  <span class="add-on">% </span>
			  </div>
			  <div class="input-prepend">
					<span class="add-on">Rp</span>
					<input type="text" id="nominalshare4" name="nominalshare4" class="input-xlarge" value="<?php echo $layanan['share4'] ?>">	
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="keterangan">Keterangan</label>
			<div class="controls control-row">
			  <input type="text" id="keterangan" name="keterangan" class="span6" value="<?php echo $layanan['keterangan'] ?>">
			</div>
		  </div>
		  
		  <div class="control-group">
			<div class="controls">
			  <button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Simpan</button>
			</div>
		  </div>
		</form>
   
</div>

<script type="text/javascript">
function dstrToUTC(ds) {
    var dsarr = ds.split("-");
     var dd = parseInt(dsarr[0],10);
     var mm = parseInt(dsarr[1],10);
     var yy = parseInt(dsarr[2],10);
     return Date.UTC(yy,mm-1,dd,0,0,0); 
	}
function datediff(ds1,ds2) 
	{
     var d1 = dstrToUTC(ds1);
     var d2 = dstrToUTC(ds2);
     var oneday = 86400000;
     return (d2-d1) / oneday;    
	}
function humanise (diff) {
  // The string we're working with to create the representation
  var str = '';
  // Map lengths of `diff` to different time periods
  var values = {
    'umur': 365, 
    'umur_bulan': 30, 
    'umur_hari': 1
  };

  // Iterate over the values...
  for (var x in values) {
    var amount = Math.floor(diff / values[x]);

    // ... and find the largest time value that fits into the diff
    if (amount >= 1) {
       // If we match, add to the string ('s' is for pluralization)
       str += amount + x + (amount > 1 ? 's' : '') + ' ';
	   $('#'+x).val(amount);
	   

       // and subtract from the diff
       diff -= amount * values[x];
    }
	else
	{
		$('#'+x).val(0);
	}
  }

  return str;
}

function hitung_netto() 
	{
     var harga=$('#harga').val();
	 var nominaldiskon=$('#nominaldiskon').val();
     
	 var netto=harga-nominaldiskon;
     $("#harganetto").val(netto);
	}

function update_share() 
	{
		var harga=$('#harga').val();
		var persendiskon=$('#persendiskon').val();
		
		var nominaldiskon=harga*persendiskon/100;
		$("#nominaldiskon").val(nominaldiskon);
		hitung_netto();
		
		var share1=$('#share1').val();
		var nominalshare1=harga*share1/100;
		$("#nominalshare1").val(nominalshare1);
		
		var share2=$('#share2').val();
		var nominalshare2=harga*share2/100;
		$("#nominalshare2").val(nominalshare2);
		
		var share3=$('#share3').val();
		var nominalshare3=harga*share3/100;
		$("#nominalshare3").val(nominalshare3);
		
		var share4=$('#share4').val();
		var nominalshare4=harga*share4/100;
		$("#nominalshare4").val(nominalshare4);
		
	 
	}
	
$(document).ready(function(){

$('#persendiskon').keyup(function() {
			var persendiskon=$('#persendiskon').val();
			var harga=$('#harga').val();
			
			var nominaldiskon=harga*persendiskon/100;
			$("#nominaldiskon").val(nominaldiskon);
			hitung_netto();
});

$('#nominaldiskon').keyup(function() {
			var nominaldiskon=$('#nominaldiskon').val();
			var harga=$('#harga').val();
			
			var persendiskon=nominaldiskon*100/harga;
			$("#persendiskon").val(persendiskon);
			hitung_netto();
});

$('#share1').keyup(function() {
			var share1=$('#share1').val();
			var harga=$('#harga').val();
			
			var nominalshare1=harga*share1/100;
			$("#nominalshare1").val(nominalshare1);
			
});

$('#nominalshare1').keyup(function() {
			var nominalshare1=$('#nominalshare1').val();
			var harga=$('#harga').val();
			
			var share1=nominalshare1*100/harga;
			$("#share1").val(share1);
});

$('#share2').keyup(function() {
			var share2=$('#share2').val();
			var harga=$('#harga').val();
			
			var nominalshare2=harga*share2/100;
			$("#nominalshare2").val(nominalshare2);
			
});

$('#nominalshare2').keyup(function() {
			var nominalshare2=$('#nominalshare2').val();
			var harga=$('#harga').val();
			
			var share2=nominalshare2*100/harga;
			$("#share2").val(share2);
});

$('#share3').keyup(function() {
			var share3=$('#share3').val();
			var harga=$('#harga').val();
			
			var nominalshare3=harga*share3/100;
			$("#nominalshare3").val(nominalshare3);
			
});

$('#nominalshare3').keyup(function() {
			var nominalshare3=$('#nominalshare3').val();
			var harga=$('#harga').val();
			
			var share3=nominalshare3*100/harga;
			$("#share3").val(share3);
});

$('#share4').keyup(function() {
			var share4=$('#share4').val();
			var harga=$('#harga').val();
			
			var nominalshare4=harga*share4/100;
			$("#nominalshare4").val(nominalshare4);
			
});

$('#nominalshare4').keyup(function() {
			var nominalshare4=$('#nominalshare4').val();
			var harga=$('#harga').val();
			
			var share4=nominalshare4*100/harga;
			$("#share4").val(share4);
});

$('#harga').keyup(function() {
	update_share();
});
update_share();
});

</script>