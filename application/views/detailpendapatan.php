<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}
?>	
<body class="light">
        <div class="layout">
            <div class="header-text">Laporan Klinik Jantung MUSI</div>
            <div class="img-theme" data-bind="click:toggleTheme"></div>
            <div class="content helth">
                
                <div class="row">
                    <div class="legend-row">
                        <div class="legend">Laporan Omzet Klinik </div>
                        <div class="hr"></div>
                    </div>
                    <div class="clear"></div>
					<?php
						$total=($laporanharianpendapatan==NULL ? 0:$laporanharianpendapatan['jumlah']);
						
						$month=date("n");
						$year=date("Y");

						$labelmonth="Januari";

						if($month==2)
						$labelmonth="Februari";
						else if($month==3)
						$labelmonth="Maret";
						else if($month==4)
						$labelmonth="April";
						else if($month==5)
						$labelmonth="Mei";
						else if($month==6)
						$labelmonth="Juni";
						else if($month==7)
						$labelmonth="Juli";
						else if($month==8)
						$labelmonth="Agustus";
						else if($month==9)
						$labelmonth="September";
						else if($month==10)
						$labelmonth="Oktober";
						else if($month==11)
						$labelmonth="November";
						else if($month==12)
						$labelmonth="Desember";
						
						?>
                    <div class="col1">
                        <div class="text">Laporan Omzet Bisnis <?php echo $labelmonth.' '.$year; ?>.</div>
                        <label id="CPU" class="label-value" ><?php echo $pendapatanbulanini/1000000; ?><font style=" font-family:Arial; font-weight:lighter;"> juta</font></label>
                    </div>
                    <div class="col1" >
						<div class="text">Laporan Omzet Bisnis Hari ini </div>
						
                        <label id="CPU" class="label-value" ><?php echo formatRupiah($total); ?></label>
					</div>
                    <div class="col3" >
					<div style="position: relative;">
					<canvas id="canvas2" height="160" width="600"></canvas>
					<div id="tooltip2"></div>
					
					</div>
					<script>
					var pendlabels=<?php echo $pendlabels; ?>;
					var pendvalues=<?php echo $pendvalues; ?>;
					
					var lineChartData = {
						labels : pendlabels,
						datasets : [
							{
								fillColor : "rgba(151,187,205,0.5)",
								strokeColor : "rgba(151,187,205,1)",
								pointColor : "rgba(151,187,205,1)",
								pointStrokeColor : "#fff",
								data : pendvalues,
								mouseover: function(data) {
									// data returns details about the point hovered, such as x and y position and index, as
									// well as details about the hover event in data.event
									// You can do whatever you like here, but here is a sample implementation of a tooltip
									var active_value = pendvalues[data.point.dataPointIndex];
									var active_date = pendlabels[data.point.dataPointIndex];
									var temp=active_value/1000000;
									// For details about where this tooltip came from, read below
									//alert(active_value);
									var base_url = "<?php echo base_url(''); //you have to load the "url_helper" to use this function ?>";
									$("#tooltip2").html("<img class='arrow' src='"+base_url+"/assets/css/arrow.png' /><table style='height:200%;'><tr ><td style='background-color:#75c0e0; color:#fff;'><strong style='font-size: 1.5em; font-weight: bold;'>"+temp +"jt</strong></td><td><font style='font-size:1em;'>Omzet<hr style='margin: 0px;'>"+active_date+" </font></td></tr></table>").css("position", "absolute").css("left", data.point.x+17).css("top", data.point.y).css("display", 'block').css("background-color", '#FFF');
								  },
								  mouseout: function (data) {
									// Hide the tooltip
									$("#tooltip2").css("display","none");
								  }
							}
						]
						
					}
					var maxi=Math.max.apply(Math,pendvalues);
					
					var options={
						scaleOverlay : true,
						scaleOverride : true,
				
						//** Required if scaleOverride is true **
						//Number - The number of steps in a hard coded scale
						scaleSteps : 5,
						//Number - The value jump in the hard coded scale
						scaleStepWidth : maxi/5,
						//Number - The scale starting value
						scaleStartValue : 0,
					}

				var myLine = new Chart(document.getElementById("canvas2").getContext("2d")).Line(lineChartData, options);
				
				</script>
	
				
					</div>
					
                    
                </div>
                <div class="row" style="height:500px;">
                    <div class="legend-row">
                        <div class="legend">Statistik Pendapatan Layanan <font style="color:#75c0e0"><?php echo $layananini['nama_layanan'];?> </font> <?php echo $mulai;?> s/d <?php echo $sampai;?></div>
                        <div class="hr"></div>
                    </div>
                    <div class="clear"></div>
					
                    <div class="col1"  >
					<div class="text">Laporan Layanan Bulan</div>
					<div class="text">Dari Bulan:</div>
					<form action="<?php echo site_url("laporan/detailkeuangan"); ?>" method="POST">
					<div class="input-append date" id="dp3" data-date="<?php echo $mulai;?>" data-date-format="mm-yyyy">
					<input class="input-small" size="16" type="text" value="<?php echo $mulai;?>" name="mulai" id="mulai">
					
					<span class="add-on"><i class="icon-calendar"></i></span>
					</div>
					<div class="text">Sampai Bulan :</div>
					<div class="input-append date" id="dp4" data-date="<?php echo $sampai;?>" data-date-format="mm-yyyy">
					<input class="input-small" size="16" type="text" value="<?php echo $sampai;?>" name="sampai" id="sampai">
					
					<span class="add-on"><i class="icon-calendar"></i></span>
					</div>
					<div class="text">Pilih Layanan :</div>
					<select name="layanan" id="layanan" >
					
							<?php foreach ($layanan as $layanan_item): ?>
								<option value="<?php echo $layanan_item['id_layanan'] ?>" <?php if($idlayanan==$layanan_item['id_layanan']) echo 'selected'; ?>><?php echo $layanan_item['nama_layanan'] ?></option>
							<?php endforeach; ?>
							  
							</select>
					<button type="submit" class="btn btn-info"><i class="icon-ok-sign icon-white"></i> Tampilkan</button></a>
					</form>
					</div>
                    
					<div class="col6"  >
					<div class="text">Laporan Pendapatan Layanan <?php echo $layananini['nama_layanan'];?></div>
					<br>
					<div class="clear"></div>
					<div style="position: relative;">
						<canvas id="canvas" height="250" width="420"></canvas>
						<div id="tooltip"></div>
					</div>
					<script>
					var layananlabels=<?php echo $layananlabels; ?>;
					var layananvalues=<?php echo $layananvalues; ?>;
					
					var lineChartData = {
						labels : layananlabels,
						datasets : [
							{
								fillColor : "rgba(151,187,205,0.5)",
								strokeColor : "rgba(151,187,205,1)",
								pointColor : "rgba(151,187,205,1)",
								pointStrokeColor : "#fff",
								data : layananvalues,
								mouseover: function(data) {
									// data returns details about the point hovered, such as x and y position and index, as
									// well as details about the hover event in data.event
									// You can do whatever you like here, but here is a sample implementation of a tooltip
									var active_value = layananvalues[data.point.dataPointIndex];
									var active_date = layananlabels[data.point.dataPointIndex];
									var temp=active_value/1000000;
									// For details about where this tooltip came from, read below
									//alert(active_value);
									var base_url = "<?php echo base_url(''); //you have to load the "url_helper" to use this function ?>";
									$("#tooltip").html("<img class='arrow' src='"+base_url+"/assets/css/arrow.png' /><table style='height:200%;'><tr ><td style='background-color:#75c0e0; color:#fff;'><strong style='font-size: 1.5em; font-weight: bold;'>"+temp +"jt</strong></td><td><font style='font-size:1em;'>Pendapatan<hr style='margin: 0px;'>"+active_date+" </font></td></tr></table>").css("position", "absolute").css("left", data.point.x+17).css("top", data.point.y).css("display", 'block').css("background-color", '#FFF');
								  },
								  mouseout: function (data) {
									// Hide the tooltip
									$("#tooltip").css("display","none");
								  }
							}
						]
						
					}
					var maxi=Math.max.apply(Math,layananvalues);
					
					var options={
						scaleOverlay : true,
						scaleOverride : true,
				
						//** Required if scaleOverride is true **
						//Number - The number of steps in a hard coded scale
						scaleSteps : 5,
						//Number - The value jump in the hard coded scale
						scaleStepWidth : maxi/5,
						//Number - The scale starting value
						scaleStartValue : 0,
					}

				var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Line(lineChartData, options);
				
				</script>
	
					</div>
					
					
					<div class="col6"  >
					<div class="text">Laporan Jumlah Pemakaian Layanan <?php echo $layananini['nama_layanan'];?> </div>
					
					<div style="position: relative;">
					<div class="clear"></div>
					<br>
					<div style="position: relative;">
						<canvas id="canvas3" height="250" width="420"></canvas>
						<div id="tooltip3"></div>
					</div>
					<script>
					
					var layananvalues2=<?php echo $layananvalues2; ?>;
					
					var lineChartData = {
						labels : layananlabels,
						datasets : [
							{
								fillColor : "rgba(151,187,205,0.5)",
								strokeColor : "rgba(151,187,205,1)",
								pointColor : "rgba(151,187,205,1)",
								pointStrokeColor : "#fff",
								data : layananvalues2,
								mouseover: function(data) {
									// data returns details about the point hovered, such as x and y position and index, as
									// well as details about the hover event in data.event
									// You can do whatever you like here, but here is a sample implementation of a tooltip
									var active_value = layananvalues2[data.point.dataPointIndex];
									var active_date = layananlabels[data.point.dataPointIndex];
									var temp=active_value;
									// For details about where this tooltip came from, read below
									//alert(active_value);
									var base_url = "<?php echo base_url(''); //you have to load the "url_helper" to use this function ?>";
									$("#tooltip3").html("<img class='arrow' src='"+base_url+"/assets/css/arrow.png' /><table style='height:200%;'><tr ><td style='background-color:#75c0e0; color:#fff;'><strong style='font-size: 1.5em; font-weight: bold;'>"+temp +"</strong></td><td><font style='font-size:1em;'>Jumlah Pemakaian<hr style='margin: 0px;'>"+active_date+" </font></td></tr></table>").css("position", "absolute").css("left", data.point.x+17).css("top", data.point.y).css("display", 'block').css("background-color", '#FFF');
								  },
								  mouseout: function (data) {
									// Hide the tooltip
									$("#tooltip3").css("display","none");
								  }
							}
						]
						
					}
					var maxi=Math.max.apply(Math,layananvalues2);
					
					var options={
						scaleOverlay : true,
						scaleOverride : true,
				
						//** Required if scaleOverride is true **
						//Number - The number of steps in a hard coded scale
						scaleSteps : 5,
						//Number - The value jump in the hard coded scale
						scaleStepWidth : maxi/5,
						//Number - The scale starting value
						scaleStartValue : 0,
					}

				var myLine = new Chart(document.getElementById("canvas3").getContext("2d")).Line(lineChartData, options);
				
				</script>
	
					</div>
					
					</div>
					
                    
                </div>
                
            </div>
        </div>
<script type="text/javascript">
$(document).ready(function(){
		$('#dp3').datepicker(
			{
			format: "mm-yyyy",
			viewMode: "months", 
			minViewMode: "months"
			}
			)
		  .on('changeDate', function(ev){
			
		});
		$('#dp4').datepicker(
			{
			format: "mm-yyyy",
			viewMode: "months", 
			minViewMode: "months"
			}
			)
		  .on('changeDate', function(ev){
			
		});
});
</script>