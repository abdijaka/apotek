<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-inner">
	<div class="container">
	  <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	  </a>
	  <a class="brand" href="#">Clinic</a>
	  <div class="nav-collapse collapse">
		 <p class="navbar-text pull-right">
		 <?php
		 $user = $this->ion_auth->user()->row();
		 if (!$this->ion_auth->logged_in())
		{
			echo '<a href="#" class="btn btn-danger ">Login&nbsp;<i class="icon-chevron-right"></i></a>';
		}
		else
		{
		 ?>
              Halo,  <a href="#" class="navbar-link"><?php echo $user->username; ?></a>
			  <a href="<?php echo site_url('auth/logout') ?>" class="btn btn-danger ">Logout&nbsp;<i class="icon-off"></i></a>
		<?php
		}
		?>		
            </p>
		<ul class="nav">
		  
		  <li><a href="<?php echo site_url("pasien"); ?>">Pasien</a></li>
		  <li><a href="<?php echo site_url("dokter"); ?>">Dokter</a></li>
		  <li><a href="<?php echo site_url("layanan"); ?>">Layanan</a></li>
		  <li><a href="<?php echo site_url("transaksi/create_transaksi"); ?>">Register</a></li>
		  <li><a href="<?php echo site_url("transaksi/view_transaksi"); ?>">Transaksi</a></li>
		  
		 <li class="dropdown">
    <a class="dropdown-toggle"
       data-toggle="dropdown"
       href="#">
        Laporan
        <b class="caret"></b>
      </a>
    <ul class="dropdown-menu">
      <li><a href="<?php echo site_url("laporan"); ?>">Laporan Rumah Sakit</a></li>
	  <li><a href="<?php echo site_url("laporan/view_laporan"); ?>">Laporan Pendapatan per Bulan</a></li>
	<li><a href="<?php echo site_url("transaksi/laporan_harian"); ?>">Laporan Harian</a></li>
	  <li><a href="<?php echo site_url("laporan/laporan_dokter"); ?>">Laporan Pendapatan per Dokter</a></li>
    </ul>
  </li>
		</ul>
	  </div><!--/.nav-collapse -->
	</div>
  </div>
</div>
<br><br>
	
