<nav class="navbar navbar-default navbar-fixed-top navbar-no-margin-bottom" role="navigation">
    <button type="button" class="navbar-toggle navbar-toggle-side">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
<div class="container">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href=""><img class="logo-ptiik-apps" src="<?php echo base_url('assets/img/musi.png'); ?>" alt="MUSI APPS logo"></a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-blue-collapse">
    <ul class="nav navbar-nav navbar-side mini-side">
    <!-- search -->
    <form class="navbar-form form-side" role="search">
      <div class="form-group">
        <input type="text" class="form-control side-search-input" placeholder="Search">
      </div>
      <button type="submit" class="btn btn-default btn-no-radius btn-search-side"><span class="fa fa-search"></span></button>
      <button type="submit" class="btn btn-default btn-no-radius btn-search-side navbar-side-to-search"><span class="fa fa-search"></span></button>
    </form>
    <!-- end search -->
	
	<li><a class="" href="<?php echo site_url("g_stock/view_stock_item"); ?>"><span class="fa fa-home fa-lg"></span><span class="text-side">Home</span></a></li>
	
		
		<li class="dropdown"><a class="dropdown-toggle dropdown-toggle-side" data-toggle="dropdown" role="button" href=""><span class="fa fa-clipboard fa-lg"></span><span class="text-side">Database<span class="fa fa-angle-down icon-in-side-dropdown"></span></span></a>
			<ul role="menu" class="dropdown-menu">
                <li role=""><a href="<?php echo site_url("g_item"); ?>" tabindex="-1" role="menuitem">Data Item</a></li>
				<li role=""><a href="<?php echo site_url("dokter"); ?>" tabindex="-1" role="menuitem">Data Dokter</a></li>				
				<li role=""><a href="<?php echo site_url("pasien"); ?>" tabindex="-1" role="menuitem">Data Pasien</a></li>
				<li role=""><a href="<?php echo site_url("g_supplier"); ?>" tabindex="-1" role="menuitem">Data Suplier</a></li>
				
				
      		</ul>
        </li>
		
		<li class="dropdown"><a class="dropdown-toggle dropdown-toggle-side" data-toggle="dropdown" role="button" href=""><span class="fa fa-folder-open-o fa-lg"></span><span class="text-side">Laporan<span class="fa fa-angle-down icon-in-side-dropdown"></span></span></a>
			<ul role="menu" class="dropdown-menu">
                <li role=""><a href="<?php echo site_url("g_laporan/laporan_harian"); ?>" tabindex="-1" role="menuitem">Laporan Harian</a></li>
				<li role=""><a href="<?php echo site_url("g_laporan/laporan_bulanan"); ?>" tabindex="-1" role="menuitem">Laporan Bulanan</a></li>
				<li role=""><a href="<?php echo site_url("g_stock/view_stock_item"); ?>" tabindex="-1" role="menuitem">Laporan Stock</a></li>
				
                <li role=""><a href="<?php echo site_url("g_laporan/laporan_item"); ?>" tabindex="-1" role="menuitem">Laporan Transaksi per Item</a></li>
				
				<!-- <li role=""><a href="<?php echo site_url("g_laporan/laporan_pembelian_item"); ?>" tabindex="-1" role="menuitem">Laporan Pembelian per Item</a></li>
				
				<li role=""><a href="<?php echo site_url("g_laporan/laporan_itemkeluar_item"); ?>" tabindex="-1" role="menuitem">Laporan Barang Keluar per Item</a></li>
				
				<li role=""><a href="<?php echo site_url("laporan/detailpasien"); ?>" tabindex="-1" role="menuitem">Laporan Pembelian per Supplier</a></li>
				
				<li role=""><a href="<?php echo site_url("laporan/detailpasien"); ?>" tabindex="-1" role="menuitem">Laporan Pembelian Bulanan</a></li>
				
				<li role=""><a href="<?php echo site_url("laporan/detailpasien"); ?>" tabindex="-1" role="menuitem">Laporan Barang Keluar Bulanan</a></li> -->
				
      		</ul>
        </li>
		
		<li class="dropdown"><a class="dropdown-toggle dropdown-toggle-side" data-toggle="dropdown" role="button" href=""><span class="fa fa-shopping-cart fa-lg"></span><span class="text-side">Pembelian<span class="fa fa-angle-down icon-in-side-dropdown"></span></span></a>
			<ul role="menu" class="dropdown-menu">
                <li role=""><a href="<?php echo site_url("g_pembelian"); ?>" tabindex="-1" role="menuitem">Transaksi Pembelian</a></li>
				<li role=""><a href="<?php echo site_url("g_pembelian/create_pembelian"); ?>" tabindex="-1" role="menuitem">Tambah Pembelian</a></li>
      		</ul>
        </li>
		
		<li class="dropdown"><a class="dropdown-toggle dropdown-toggle-side" data-toggle="dropdown" role="button" href=""><span class="fa fa-credit-card fa-lg"></span><span class="text-side">Keuangan<span class="fa fa-angle-down icon-in-side-dropdown"></span></span></a>
			<ul role="menu" class="dropdown-menu">
                <li role=""><a href="<?php echo site_url("g_keuangan_yunan"); ?>" tabindex="-1" role="menuitem">Kartu Hutang</a></li>
                <li role=""><a href="<?php echo site_url("g_tanda_terima_yunan"); ?>" tabindex="-1" role="menuitem">Tanda Terima</a></li>
      		</ul>
        </li>
		
		<li class="dropdown"><a class="dropdown-toggle dropdown-toggle-side" data-toggle="dropdown" role="button" href=""><span class="fa fa-envelope fa-lg"></span><span class="text-side">E-Resep<span class="fa fa-angle-down icon-in-side-dropdown"></span></span></a>
			<ul role="menu" class="dropdown-menu">
                <li role=""><a href="<?php echo site_url("g_eresep"); ?>" tabindex="-1" role="menuitem">Transaksi E-Resep</a></li>
      		</ul>
        </li>

        <li class="dropdown"><a class="dropdown-toggle dropdown-toggle-side" data-toggle="dropdown" role="button" href=""><span class="fa fa-stethoscope fa-lg"></span><span class="text-side">Penjualan<span class="fa fa-angle-down icon-in-side-dropdown"></span></span></a>
			<ul role="menu" class="dropdown-menu">
                <li role=""><a href="<?php echo site_url("g_penjualan"); ?>" tabindex="-1" role="menuitem">Transaksi Penjualan</a></li>
				<li role=""><a href="<?php echo site_url("g_penjualan/create_penjualan"); ?>" tabindex="-1" role="menuitem">Tambah Penjualan</a></li>
      		</ul>
        </li>
    	
		<li class="dropdown"><a class="dropdown-toggle dropdown-toggle-side" data-toggle="dropdown" role="button" href=""><span class="fa fa-reply-all fa-lg"></span><span class="text-side">Pemusnahan/Retur Supplier<span class="fa fa-angle-down icon-in-side-dropdown"></span></span></a>
			<ul role="menu" class="dropdown-menu">
                <li role=""><a href="<?php echo site_url("g_retursupp"); ?>" tabindex="-1" role="menuitem">Transaksi Retur Suplier</a></li>
				<li role=""><a href="<?php echo site_url("g_retursupp/create_retursupp"); ?>" tabindex="-1" role="menuitem">Tambah Retur Supplier</a></li>
      		</ul>
        </li>
		
		<li class="dropdown"><a class="dropdown-toggle dropdown-toggle-side" data-toggle="dropdown" role="button" href=""><span class="fa fa-trash-o fa-lg"></span><span class="text-side">Pemusnahan<span class="fa fa-angle-down icon-in-side-dropdown"></span></span></a>
			<ul role="menu" class="dropdown-menu">
                <li role=""><a href="<?php echo site_url("g_itemkeluar"); ?>" tabindex="-1" role="menuitem">Transaksi Pemusnahan</a></li>
				<li role=""><a href="<?php echo site_url("g_itemkeluar/itemkeluar_baru"); ?>" tabindex="-1" role="menuitem">Tambah Pemusnahan</a></li>
      		</ul>
        </li>
		<li><a class="" href="<?php echo site_url("auth/create_user"); ?>"><span class="fa fa-github-alt fa-lg"></span><span class="text-side">Buat User Baru</span></a></li>
	  
    </ul>
    <ul class="nav navbar-nav navbar-right">
	
      <li>
	  <br>
	  <button class="btn " >
		
		<i class="fa fa-user "></i> <?php
		 $user = $this->ion_auth->user()->row();
		 if (!$this->ion_auth->logged_in())
		{
			echo '<a href="'.site_url('auth/logout').'" class="btn btn-danger ">Login&nbsp;<i class="icon-chevron-right"></i></a>';
		}
		else
		{
		 ?>
              Halo,  <a href="#" class="navbar-link"><?php echo $user->username; ?></a>
			  <a href="<?php echo site_url('auth/logout') ?>" class="btn btn-danger ">Logout&nbsp;<i class="fa fa-sign-out fa-sm"></i></i></a>
		<?php
		}
		?></button>
      
      </li>
	  
    </ul>
  </div><!-- /.navbar-collapse -->
  </div>
</nav>
