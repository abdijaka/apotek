<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
<nav class="navbar navbar-default navbar-fixed-top navbar-no-margin-bottom" role="navigation">
    <button type="button" class="navbar-toggle navbar-toggle-side">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
<div class="container">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href=""><img class="logo-ptiik-apps" src="<?php echo base_url('assets/img/musi.png'); ?>" alt="MUSI APPS logo"></a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-blue-collapse">
    <ul class="nav navbar-nav navbar-side mini-side">
    <!-- search -->
    <form class="navbar-form form-side" role="search">
      <div class="form-group">
        <input type="text" class="form-control side-search-input" placeholder="Search">
      </div>
      <button type="submit" class="btn btn-default btn-no-radius btn-search-side"><span class="fa fa-search"></span></button>
      <button type="submit" class="btn btn-default btn-no-radius btn-search-side navbar-side-to-search"><span class="fa fa-search"></span></button>
    </form>
    <!-- end search -->
    <li class="dropdown"><a role="button" href="<?php echo site_url("dokter"); ?>"><span class="fa fa-user-md fa-lg"></span> <span class="text-side">Dokter<span class="fa fa-angle-down icon-in-side-dropdown"></span></span></a>
      
              </li>
              
      <li><a class="" href="<?php echo site_url("pasien"); ?>"><span class="fa fa-user fa-lg"></span><span class="text-side">Pasien</span></a></li>
      
      <li><a class="" href="<?php echo site_url("layanan"); ?>"><span class="fa fa-stethoscope fa-lg"></span><span class="text-side">Layanan</span></a></li>
      <li><a class="" href="<?php echo site_url("transaksi/create_transaksi"); ?>"><span class="fa fa-plus-square"></span><span class="text-side">Register</span></a></li>
	  <li><a class="" href="<?php echo site_url("transaksi/view_transaksi"); ?>"><span class="fa fa-clipboard fa-lg"></span><span class="text-side">Transaksi</span></a></li>
      <li class="dropdown"><a class="dropdown-toggle dropdown-toggle-side" data-toggle="dropdown" role="button" href=""><span class="fa fa-folder-open-o fa-lg"></span><span class="text-side">Laporan<span class="fa fa-angle-down icon-in-side-dropdown"></span></span></a>
      <ul role="menu" class="dropdown-menu">
                <li role=""><a href="<?php echo site_url("laporan"); ?>" tabindex="-1" role="menuitem">Laporan Rumahsakit</a></li>
                <li role=""><a href="<?php echo site_url("laporan/view_laporan"); ?>" tabindex="-1" role="menuitem">Laporan Pendapatan per Bulan</a></li>
				<li role=""><a href="<?php echo site_url("transaksi/laporan_harian"); ?>" tabindex="-1" role="menuitem">Laporan Harian</a></li>
				<li role=""><a href="<?php echo site_url("laporan/laporan_dokter"); ?>" tabindex="-1" role="menuitem">Laporan Pendapatan per Dokter</a></li>
      			</ul>
              </li>
      
      <li><a class="" href=""><span class="fa fa-phone fa-lg"></span><span class="text-side">Kontak</span></a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
	
      <li>
	  <br>
	  <button class="btn " >
		
		<i class="fa fa-user "></i> <?php
		 $user = $this->ion_auth->user()->row();
		 if (!$this->ion_auth->logged_in())
		{
			echo '<a href="#" class="btn btn-danger ">Login&nbsp;<i class="icon-chevron-right"></i></a>';
		}
		else
		{
		 ?>
              Halo,  <a href="#" class="navbar-link"><?php echo $user->username; ?></a>
			  <a href="<?php echo site_url('auth/logout') ?>" class="btn btn-danger ">Logout&nbsp;<i class="fa fa-sign-out fa-sm"></i></i></a>
		<?php
		}
		?></button>
      
      </li>
	  
    </ul>
  </div><!-- /.navbar-collapse -->
  </div>
</nav>

</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">PENDAFTARAN PASIEN</legend>
	
		<form class="form-horizontal" action="<?php echo site_url("pasien/create_pasien"); ?>" method="POST">
		<div class="alert alert-info">
		  <div class="row">
			<div class="col-md-12">
			<div class="col-md-3">
		  
			<label class="control-label" for="inputID">Nomor ID Pasien</label>
			
			  <input type="hidden" id="dari" name="dari" value="<?php echo $dari; ?>">
			  
			  <input type="text" id="inputID" name="inputID" placeholder="Nomor ID Pasien">
			</div>
			
			<div class="col-md-3">
		  
			<label class="control-label" for="inputID">Med Rec</label>
			
			  <input type="text" id="inputmedrec" name="inputmedrec" placeholder="Med Rec" disabled>
			  
			</div>
			
			<div class="col-md-1">
		  
			<label class="control-label" for="inputID">&nbsp;</label>
			
			  
			  <label class="checkbox inline">
				<input type="checkbox" id="member" name="member" value="1"> Member
			  </label>
			  
			</div>
			<div class="col-md-5">
			<label class="control-label" for="inputID">&nbsp;</label><br>
			<span id="peringatan" class="add-on alert alert-warning">Pastikan Nomor ID Pasien sesuai dengan Nomor KTP Pasien </span>
			</div>
			</div>
			
		  </div>
		  
		  <div class="row">
			<div class="col-md-12">
			<div class="col-md-3">
			<label class="control-label" for="inputNama">Nama Pasien</label>
			
				<select name="initial" id="initial" class="input-small">
					<option value="0">Tn.</option>
					<option value="1">Ny.</option>
					<option value="2">Nona</option>
					<option value="3">Anak</option>
					<option value="4">Bayi</option>
				</select>
			  
			</div>
			<div class="col-md-3">
				<label class="control-label" for="inputNama">&nbsp;</label>
				<input type="text" id="inputNama" name="inputNama"  placeholder="Nama Pasien">
			</div>
			<div class="col-md-2">
				<label class="control-label" for="inputNama">Jenis Kelamin</label>
				<select name="jeniskelamin" id="jeniskelamin" class="input-medium">
					  <option value="0">Laki-laki</option>
					  <option value="1">Perempuan</option>
					</select>
			</div>
			<div class="col-md-2">
				<label class="control-label" for="inputNama">Gol. Darah</label>
				<select name="goldar" id="goldar" class="input-small">
					<option value="0">A</option>
					<option value="1">B</option>
					<option value="2">O</option>
					<option value="3">AB</option>
				</select>
			</div>
			<div class="col-md-2">
				<label class="control-label" for="inputNama">Rhesus</label>
				<select name="resus" id="resus" class="input-medium">
					  <option value="0">Positive</option>
					  <option value="1">Negative</option>
					</select>
			</div>
			</div>
		  </div>
		<?php
				
				$date=date("d-m-Y");
			?>
		<div class="row">
			<div class="col-md-12" style="z-index:99;">
			<div class="col-md-3" >
			<label class="control-label" for="tanggallahir">Tanggal Lahir</label>
			
			
				
					<input type="text" value="01-01-1989" name="tanggallahir" id="tanggallahir" data-date-format="dd-mm-yyyy">
				
				<input type="hidden" value="<?php echo $date; ?>" name="now" id="now">
			</div>
			<div class="col-md-2">	
				<div class="input-prepend input-append">
				  <label class="control-label" for="tanggallahir">Umur</label>
					<div class="input-group margin-bottom-sm">
					<input type="text" id="umur" name="umur" value="0" readonly>
					<span class="input-group-addon">Tahun</span>
					</div>
				  
				</div>
			</div>	
			<div class="col-md-2">	
				<div class="input-prepend input-append">
				  <label class="control-label" for="tanggallahir">&nbsp;</label>
				  <div class="input-group margin-bottom-sm">
				  <input type="text" id="umur_bulan" name="umur_bulan" value="0" readonly>
				  <span class="input-group-addon">Bulan</span>
				  </div>
				</div>
			</div>	
			<div class="col-md-2">	
				<div class="input-prepend input-append">
				  <label class="control-label" for="tanggallahir">&nbsp;</label>
				  <div class="input-group margin-bottom-sm">
				  <input type="text" id="umur_hari" name="umur_hari" value="0" readonly>
				  <span class="input-group-addon">Hari</span>
				  </div>
				</div>
			</div>	
				
			
			</div>
		</div>
		  
		  <div class="row">
			<div class="col-md-12">
			<div class="col-md-6">
			<label class="control-label" for="alamat">Alamat</label>
			
			<textarea name="alamat" id="alamat" rows="4" ></textarea>
			</div>
			<div class="col-md-3">
			<label class="control-label" for="alamat">Kota</label>
			
			<input type="text" id="inputKota" name="inputKota" class="input-xlarge" placeholder="Kota" autocomplete="off">
			  <input type="hidden" id="idkota" name="idkota" value="0" >
			</div>
			<div class="col-md-3">
			<label class="control-label" for="alamat">Kode Pos</label>
			
			<input type="text" id="inputKodepos" name="inputKodepos" placeholder="Kode Pos">
			</div>
			
			</div>
		  </div>
		  
		  
		  <div class="row">
		  <div class="col-md-12">
			
			<div class="col-md-3">
			<label class="control-label" for="inputEmail">Pekerjaan</label>
			  
					<select name="pekerjaan" id="pekerjaan" class="input-large">
					  <option value="Pegawai Negeri">Pegawai Negeri</option>
					  <option value="Wiraswasta">Wiraswasta</option>
					  <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
					  <option value="Karyawan Swasta">Karyawan Swasta</option>
					</select>
				
			</div>
			<div class="col-md-3">
			<label class="control-label" for="inputEmail">Email</label>
			
			  <input type="text" id="inputEmail" name="inputEmail" class="input-large" placeholder="Email">
			  
			</div>
			
			<div class="col-md-3">
			<label class="control-label" for="inputEmail">Telepon</label>
			
			  <input type="text" id="inputTelepon" name="inputTelepon" class="input-large" placeholder="Telepon">
			  
			</div>
			<div class="col-md-3">
			<label class="control-label" for="inputEmail">HP/ Flexi</label>
			
			  <input type="text" id="hp" name="hp" placeholder="Nomor HP">
			  
			</div>
		  </div>
		  </div>
		  
		  <div class="row">
		  <div class="col-md-12">
			<div class="col-md-6">
		  
			<label class="control-label" for="asuransi">Perusahaan/asuransi/CC</label>
			
				<select name="asuransi" id="asuransi" >
				<?php foreach ($perusahaan as $perusahaan_item): ?>
					<option value="<?php echo $perusahaan_item['id_perusahaan'] ?>"><?php echo $perusahaan_item['nama_perusahaan'] ?></option>
				<?php endforeach; ?>
				  
				</select>
			
			
			</div>
			<div class="col-md-6">
				<label class="control-label" for="asuransi">Keterangan</label>
				<input type="text" id="keterangan" name="keterangan" placeholder="Keterangan">
			</div>
		  </div>
		  </div>
		  
		  <div class="row">
			<div class="col-md-12">
			<div class="col-md-12">
			  <br>
			  <button type="submit" class="btn btn-info btn-large"><i class="fa fa-save"></i> Simpan</button>
			</div>
			</div>
		  </div>
		</div>
		</form>
   
		
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>



<!-- form register -->
<form action="https://apps.ptiik.ub.ac.id/apps/registrasi" id="form-save-user" class="form-horizontal" role="form" method="POST">
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content modal-content-no-radius">
      <div class="modal-header modal-header-blue">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title modal-title-center" id="registerModalLabel">Register</h4>
      </div>
      <div class="modal-body">
      <div class="alert alert-block alert-info"><p>
      Masukkan alamat e-mail aktif anda yang terdaftar pada biodata kepegawaian. Lakukan aktifasi melalui link yang kami kirimkan ke alamat e-mail tersebut.
      </p></div>
        <div class="form-group">
        	<label for="inputEmail1" class="col-lg-2 control-label">Email</label>
        	<div class="col-lg-10">
          	<input type="email" name="email" class="form-control" id="inputEmail1" placeholder="Email">
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="b_reg" class="btn btn-content-ready btn-no-radius">Register</button>
      </div>
    </div>
  </div>
</div>
</form>




<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>
<script type="text/javascript">
function ubah_tanggallahir() {
    var str=$('#inputID').val();
			
	var hari=str.substring(6,8);
	
	if(hari>31)
	{
		hari=hari-40;
		$("#jeniskelamin").val(1);
		$("#jeniskelamin").val(1);
		$("#initial").val(1);
		if(hari<10)hari="0"+hari;
		
	}
	var tgl=hari+"-"+str.substring(8,10)+"-19"+str.substring(10,12);
	
	$('#tanggallahir').val(tgl);
	$('#inputmedrec').val(str);
	var tanggallahir=$('#tanggallahir').val();
	var now=$('#now').val();
	
	var diff=datediff(tanggallahir,now);
	humanise(diff);
	}
function dstrToUTC(ds) {
    var dsarr = ds.split("-");
     var dd = parseInt(dsarr[0],10);
     var mm = parseInt(dsarr[1],10);
     var yy = parseInt(dsarr[2],10);
     return Date.UTC(yy,mm-1,dd,0,0,0); 
	}
function datediff(ds1,ds2) 
	{
     var d1 = dstrToUTC(ds1);
     var d2 = dstrToUTC(ds2);
     var oneday = 86400000;
     return (d2-d1) / oneday;    
	}
function humanise (diff) {
  // The string we're working with to create the representation
  var str = '';
  // Map lengths of `diff` to different time periods
  var values = {
    'umur': 365, 
    'umur_bulan': 30, 
    'umur_hari': 1
  };

  // Iterate over the values...
  for (var x in values) {
    var amount = Math.floor(diff / values[x]);

    // ... and find the largest time value that fits into the diff
    if (amount >= 1) {
       // If we match, add to the string ('s' is for pluralization)
       str += amount + x + (amount > 1 ? 's' : '') + ' ';
	   $('#'+x).val(amount);
	   

       // and subtract from the diff
       diff -= amount * values[x];
    }
	else
	{
		$('#'+x).val(0);
	}
  }

  return str;
}
	
$(document).ready(function(){

var data2 = [<?php echo $testing2; ?>];

$('#inputKota').typeahead({
    source: function (query, process) {
        states2 = [];
		map2 = {};
		
		var source = [];
		$.each(data2, function (i, state) {
			map2[state.stateName] = state;
			states2.push(state.stateName);
		});
	 
		process(states2);
		
    },
    updater: function (item) {
        
		selectedState = map2[item].stateCode;
		selectedState2 = map2[item].stateDisplay;
		$("#idkota").val(selectedState);
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});



$('#tanggallahir').datepicker()
		  .on('changeDate', function(ev){
			var tanggallahir=$('#tanggallahir').val();
			var now=$('#now').val();
			
			var diff=datediff(tanggallahir,now);
			humanise(diff);
		  });
});

$('#tanggallahir').keyup(function() {
			var tanggallahir=$('#tanggallahir').val();
			var now=$('#now').val();
			
			var diff=datediff(tanggallahir,now);
			humanise(diff);
});

$('#inputID').keyup(function() {
			var inputID=$('#inputID').val();
			$('#inputmedrec').val(inputID);	
			var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
			$.ajax({
				'url' : base_url + '/pasien/cek_ada',
				'type' : 'POST', //the way you want to send data to your URL
				'data' : {'input' : inputID},
				'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					
					if(data>0){
						$('#peringatan').addClass("alert-error");
						$("#peringatan").removeClass("alert-info")
						$("#peringatan").html("Nomor KTP ini pernah digunakan, tidak bisa didaftarkan lagi!!!")
					}
					else
					{
						$('#peringatan').addClass("alert-info");
						$("#peringatan").removeClass("alert-error")
						$("#peringatan").html("Pastikan Nomor ID Pasien sesuai dengan Nomor KTP")
					}
				}
			});
});

$('#inputID').change(function() {
			
			ubah_tanggallahir();
			
});

$( "#initial" )
  .change(function () {
    var str = 0;
    $( "#initial option:selected" ).each(function() {
      str = $( this ).val();
    });
    if(str==0)
	{
		$("#jeniskelamin").val(0);
	}
	else if(str==1 || str==2)
	{
		$("#jeniskelamin").val(1);
	}
	
  })
  .change();



</script>
</body></html>