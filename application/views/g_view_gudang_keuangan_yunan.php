<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

function get_month($month)
{
	$labelmonth="Januari";

	if($month==2)
	$labelmonth="Februari";
	else if($month==3)
	$labelmonth="Maret";
	else if($month==4)
	$labelmonth="April";
	else if($month==5)
	$labelmonth="Mei";
	else if($month==6)
	$labelmonth="Juni";
	else if($month==7)
	$labelmonth="Juli";
	else if($month==8)
	$labelmonth="Agustus";
	else if($month==9)
	$labelmonth="September";
	else if($month==10)
	$labelmonth="Oktober";
	else if($month==11)
	$labelmonth="November";
	else if($month==12)
	$labelmonth="Desember";

	return $labelmonth;

}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
<link href="<?php echo base_url('assets/css/DT_bootstrap.css') ?>" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">DAFTAR KARTU HUTANG</legend>
		<!--<h2>
			<a class="btn btn-primary btn-success" href="<?php echo site_url("g_keuangan_yunan/create_pembelian"); ?>">Masukkan Pembelian Baru</a>
		</h2>-->
		<?php if($status==1) 
		{
		?>
		<div class="alert alert-success">
		Data Berhasil Dimasukkan!!
		</div>
		<?php } ?>
		
		<?php if($status==2) 
		{
		?>
		<div class="alert alert-info">
		Data Berhasil Diupdate!!
		</div>
		<?php } ?>
		
		<?php if($status==3) 
		{
		?>
		<div class="alert alert-danger">
		Data Berhasil Dihapus!!
		</div>
		<?php } ?>
		<?php
			// echo "<pre>"; 
			// var_dump($pembelian_sort);
			// echo "</pre>";
		?>
		<div class="row">
			<form id="form1" action="<?php echo site_url("g_keuangan_yunan"); ?>" method="POST">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-2">
							<a><b>Sorting Jatuh Tempo :</b></a>
						</div>
						<div class="col-md-3">
							<select name="rentang" id="rentang" >
								<option value="">Semua</option>
								<option value="1">1 Hari</option>
								<option value="7">1 Minggu</option>
								<option value="14">2 Minggu</option>
								<option value="30">1 Bulan</option>
								<option value="60">2 Bulan</option>	
							</select>
						</div>
						<div class="col-md-1">
							<button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Tampilkan</button>
						</div>
					</div>
				</div>
			</form>
		</div>
			
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
			<thead>
				<tr>
					<th>No Faktur</th>
					<th>Tanggal Faktur</th>
					<th>Supplier</th>
					<!--<th>Jatuh Tempo</th>-->
					<th>Netto</th>
					<!--<th>Total Bayar</th>-->
					<th style="width:100px">Status/Jatuh Tempo</th>
					<th>Kekurangan</th>
					<th>Edit</th>
					<th>Delete</th>
					<th>Print</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ($pembelian_sort as $pembelian_item):
				if($pembelian_item['nomor_faktur'] == "")	
				{
					// echo "<pre>";
					// var_dump($pembelian_item);
					// echo "</pre>";
					continue;
				}
				$date=date("d-m-Y");
				$tgl = new DateTime($pembelian_item['tgl_faktur']);
				$tgl_faktur = explode("-", $pembelian_item['tgl_faktur']);
				$jatuh_tempo = explode("-", $pembelian_item['jatuh_tempo']);
			?>
			<tr class="odd gradeX">
					<td><?php echo $pembelian_item['nomor_faktur'] ?></td>
					<td><?php echo $tgl_faktur[2].' '.get_month($tgl_faktur[1]).' '.$tgl_faktur[0]; ?></td>
					<td><?php echo $pembelian_item['nama_supplier'] ?></td>
					<!--<td><?php echo date("d-m-Y", strtotime($pembelian_item['jatuh_tempo']))?></td>-->
					<td><?php echo formatRupiah($pembelian_item['total']) ?></td>
					<!--<td><?php echo formatRupiah($pembelian_item['jumlah_bayar'])?></td>-->
					<!--<td><?php if($pembelian_item['status_jatuh_tempo']['status']) {echo '<span class="label label-warning">Belum Lunas, ';if($pembelian_item['status_jatuh_tempo']['diff_date']!=NULL) if($pembelian_item['status_jatuh_tempo']['diff_date']->days==0){echo 'Hari Ini</span>';}else{echo 'dalam '. $pembelian_item['status_jatuh_tempo']['diff_date']->days.' Hari</span>';}} else{echo '<span class="label label-success">Lunas</span>';}?></td>-->
					<td>
						<?php 
							if($pembelian_item['status'] == 0)
							{
								echo '<span class="label label-warning">Belum Bayar</span></br>'.$jatuh_tempo[2].' '.get_month($jatuh_tempo[1]).' '.$jatuh_tempo[0];
							}
							elseif($pembelian_item['status'] == 1)
							{
								echo '<span class="label label-warning">Belum Lunas</span></br>'.$jatuh_tempo[2].' '.get_month($jatuh_tempo[1]).' '.$jatuh_tempo[0];
							}
							elseif($pembelian_item['status'] == 2)
							{
								echo '<span class="label label-success">Lunas</span></br>'.$jatuh_tempo[2].' '.get_month($jatuh_tempo[1]).' '.$jatuh_tempo[0];
							}
							else
							{
								echo '<span class="label label-warning">No. Tanda Terima: '.$pembelian_item['no_tanda_terima'].'</span></br>'.$jatuh_tempo[2].' '.get_month($jatuh_tempo[1]).' '.$jatuh_tempo[0];	
							}
						?>
					</td>
					<td>
						<?php 
							if($pembelian_item['status'] == 3)
							{
								echo ""; 
							}
							else
							{
								echo formatRupiah($pembelian_item['kurangbayar']); 
							}
						?>
					</td>
					<td class="center">
						<?php 
							if($pembelian_item['status'] != 3)
							{?>
								<a class="btn btn-primary" href="<?php echo site_url("g_keuangan_yunan/edit_hutang/".$pembelian_item['id_pembelian']); ?>"><i class="fa fa-edit"></i> Edit</a>	
							<?php }
						?> 
					</td>
					<td class="center">
						<?php 
							if($pembelian_item['status'] != 3)
							{?>
								<a class="btn btn-danger" href="<?php echo site_url("g_keuangan_yunan/delete_hutang/".$pembelian_item['id_pembelian']); ?>"><i class="fa fa-trash-o"></i> Delete</a>
							<?php }
						?>
					</td>
					<td class="center">
						<?php 
							if($pembelian_item['status'] != 3 && $pembelian_item['status'] == 2)
							{?>
								<a class="btn btn-success" href="<?php echo site_url("g_keuangan_yunan/view_detail2/".$pembelian_item['id_pembelian']); ?>"><i class="fa fa-print"></i> Print</a>
							<?php }
						?>
					</td>
				</tr>
		
			<?php 
			
			endforeach ?>
		
				
				
			</tbody>
		</table>
      
		
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>




<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/js/DT_bootstrap.js') ?>"></script>

<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}
$(document).ready(function(){
	$('#example').dataTable(
	{
	  "bSort" : false,
	  "bDestroy": true
	});
	if (localStorage.selectVal) {
            // Select the value stored
        $('select').val( localStorage.selectVal );
    }
});
$('select').on('change', function(){
    var currentVal = $(this).val();
    localStorage.setItem('selectVal', currentVal );
});
</script>

</body></html>