<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">EDIT ITEM KELUAR</legend>
		<form id="form1" class="form-horizontal" action="<?php echo site_url("g_itemkeluar/update_itemkeluar"); ?>" method="POST">
		    
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">Tanggal :</label>
					<div class="col-lg-10">
					<div class="col-lg-7">
						<?php
						$date=date("d-m-Y");
						$tgl = new DateTime($itemkeluar['tgl_keluar']);
						?>
						<input type="text" value="<?php echo $tgl->format('d-m-Y'); ?>" name="tgl_itemkeluar" id="tgl_itemkeluar" data-date-format="dd-mm-yyyy">
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">NAMA ITEM :</label>
					<div class="col-lg-10">
					
					<div class="col-lg-7">
						<input type="hidden" id="id_itemkeluar" name="id_itemkeluar" value="<?php echo $itemkeluar['id_itemkeluar']; ?>">
						
						<input type="hidden" id="id_item" name="id_item" value="<?php echo $itemkeluar['item']; ?>">
						<input type="text" id="namaitem" name="namaitem" placeholder="Nama Item" autocomplete="off" value="<?php echo $itemkeluar['nama_item']; ?>">
					</div>  
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">JUMLAH :</label>
					<div class="col-lg-10">
					<div class="col-lg-3">
						<input type="number" id="jumlah" name="jumlah" value="<?php echo $itemkeluar['jumlah']; ?>" autocomplete="off">
						
						<input type="hidden" id="selisih" name="selisih" value="0" autocomplete="off">
						
						<input type="hidden" id="temp_jumlah" name="temp_jumlah" value="<?php echo $itemkeluar['jumlah']; ?>" autocomplete="off">
						<div id="peringatan"></div>
					</div>
					<div class="col-lg-4">
						<div class="input-group margin-bottom-sm">
							<span class="input-group-addon">Sisa Stock Gudang : </span>  
							<input type="text" id="stock_gudang" name="stock_gudang" value="<?php echo $itemkeluar['stock_gudang']+$itemkeluar['stock_awal']; ?>" autocomplete="off" readonly>
							<input type="hidden" id="temp_stock_gudang" name="temp_stock_gudang" value="<?php echo $itemkeluar['stock_gudang']+$itemkeluar['stock_awal']; ?>" autocomplete="off" readonly>
						</div>
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">KETERANGAN :</label>
					<div class="col-lg-10">
					
					<div class="col-lg-7">
						<input type="text" id="keterangan" name="keterangan" placeholder="Keterangan" autocomplete="off" value="<?php echo $itemkeluar['keterangan']; ?>">
					</div>  
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">&nbsp;</label>
					<div class="col-lg-10">
					<div class="col-lg-7">
						<button type="submit" class="btn btn-info "><i class="fa fa-save  "></i> Update</button>
					</div>
					
					</div>
			</div>
		  
		  
		</form>
   
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>



<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}
$(document).ready(function(){

$( "#form1" ).submit(function( event ) {
  
  var id_item=$('#id_item').val();
  var jumlah=$('#jumlah').val();
  
	if(id_item==0 || id_item=="" || id_item=="0")
	{
		alert("Item Belum diisi");
		event.preventDefault();
	}
	else if(jumlah==0 || jumlah=="" || jumlah=="0")
	{
		alert("Jumlah harus positif");
		event.preventDefault();
	}
	
});

var data3 = [<?php echo $testing3; ?>];

$('#namaitem').typeahead({
    source: function (query, process) {
        states4 = [];
		map4 = {};
		
		var source = [];
		$.each(data3, function (i, state) {
			map4[state.stateName] = state;
			states4.push(state.stateName);
		});
	 
		process(states4);
		
    },
    updater: function (item) {
        
		selectedState = map4[item].stateCode;
		selectedState2 = map4[item].stateDisplay;
		stock_gudang = map4[item].stock_gudang;
		
		$("#id_item").val(selectedState);
		$("#stock_gudang").val(stock_gudang);
		$("#temp_stock_gudang").val(stock_gudang);
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#jumlah').keyup(function() {
	var jumlah = $("#jumlah").val();
	var temp_jumlah = $("#temp_jumlah").val();
	var selisih = jumlah-temp_jumlah;
	
	var temp_stock_gudang = $("#temp_stock_gudang").val();
	var sisa=temp_stock_gudang-selisih;
	$("#stock_gudang").val(sisa);
	$("#selisih").val(selisih);
	if(sisa<0)
	{
		$('#jumlah').focus();
		$('#peringatan').addClass("alert-error");
		$("#peringatan").removeClass("alert-info");
		$("#peringatan").html("Jumlah Barang keluar tidak boleh melebihi Stock");
	}
	else
	{
		$("#peringatan").html("");
	}
});

$('#tgl_itemkeluar').datepicker()
		  .on('changeDate', function(ev){
			
		  });


});

</script>


</body></html>