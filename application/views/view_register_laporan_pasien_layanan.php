<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

function get_month($month)
{
	$labelmonth="Januari";

	if($month==2)
	$labelmonth="Februari";
	else if($month==3)
	$labelmonth="Maret";
	else if($month==4)
	$labelmonth="April";
	else if($month==5)
	$labelmonth="Mei";
	else if($month==6)
	$labelmonth="Juni";
	else if($month==7)
	$labelmonth="Juli";
	else if($month==8)
	$labelmonth="Agustus";
	else if($month==9)
	$labelmonth="September";
	else if($month==10)
	$labelmonth="Oktober";
	else if($month==11)
	$labelmonth="November";
	else if($month==12)
	$labelmonth="Desember";

	return $labelmonth;

}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
<link href="<?php echo base_url('assets/css/DT_bootstrap.css') ?>" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">LAPORAN LAYANAN BULANAN</legend>
		<legend style="text-align:center;">BULAN <?php $pieces = explode("-", $mulai); echo get_month($pieces[0]).' '.$pieces[1];?></legend>
		
		<form id="form1" action="<?php echo site_url("laporan/detail_layanan_pasien"); ?>" method="POST">
					
			<div class="row">
				<div class="col-md-12">
				<div class="col-md-1">
				<a class="btn">Bulan :</a>
				</div>
				<div class="col-md-2">
				<input  type="text" value="<?php echo $mulai;?>" name="mulai" id="mulai" data-date-format="mm-yyyy">
				</div>
				<div class="col-md-3">
				<select name="id_layanan" id="id_layanan" >
				<?php
				foreach ($layanan as $layanan_item): ?>
					<option <?php if($id_layanan==$layanan_item['id_layanan']) {echo 'selected'; $nama_layanan=$layanan_item['nama_layanan'];} ?> value="<?php echo $layanan_item['id_layanan']; ?>"><?php echo $layanan_item['nama_layanan']; ?></option>
				<?php 
				endforeach ?>	
				</select>
				</div>
				<div class="col-md-1">
				<button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Tampilkan</button>
				</div>
				
		
	  </form>
	  
	  <form action="<?php echo site_url("laporan/detail_layanan_bulanan2"); ?>" method="POST">
				<input  type="hidden" value="<?php echo $mulai;?>" name="mulai2" id="mulai2" >
				
				
				<div class="col-md-2">
				<!--
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-danger btn-large"><i class="icon-ok-sign icon-white"></i> Download</button>
				-->
				</div>
				</div>
			</div>
	  </form>
	  
		<h3>Laporan Layanan <?php echo $nama_layanan; ?></h3>
		<table  cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" >
			<thead>
				<tr style="background-color: #5bc0de; color:#fff;">
					<th>No</th>
					<th>Nomor Registrasi</th><th>Nomor Registrasi</th>
					<th>Tanggal</th>
					<th>Nama Pasien</th>
					<th>Kota</th>
					<th>Netto</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$nomor=1;
			$total_pendapatan=0;
			foreach ($laporan as $laporan_item): ?>
			
			<tr class="odd gradeX">
					<td><?php echo $nomor; ?></td>
					<td><?php echo $laporan_item['nama_layanan'] ?></td>
					<td><?php echo $laporan_item['nomor_register'] ?></td>
					<td><?php echo $laporan_item['tanggal'] ?></td>
					<td><?php echo $laporan_item['nama_pasien'] ?></td>
					<td><?php echo $laporan_item['kota'] ?></td>
					<td style="text-align:right"><?php echo formatRupiah($laporan_item['netto']) ?></td>
					
				</tr>
		
			<?php 
			$nomor++;
			$total_pendapatan=$total_pendapatan+$laporan_item['netto'];
			endforeach ?>
			<tr >
					
					<td style="text-align:right; font-weight:bold;" colspan="6">Total Pemasukan</td>
					<td style="text-align:right; width:120px; font-weight:bold;"><?php echo formatRupiah($total_pendapatan); ?></td>
			</tr>
			
			</tbody>
		</table>
		
		
	  <h3 style="color:#fa6805; font-weight:bold;">Total Pendapatan : <?php echo formatRupiah($total_pendapatan); ?></h3>
		
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>


<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.dataTables.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/DT_bootstrap.js') ?>"></script>
	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>
<script type="text/javascript">
$(document).ready(function(){
		$('#mulai').datepicker(
			{
			format: "mm-yyyy",
			viewMode: "months", 
			minViewMode: "months"
			
			}
			)
		  .on('changeDate', function(ev){
			
		});
		$('#sampai').datepicker(
			{
			format: "mm-yyyy",
			viewMode: "months", 
			minViewMode: "months"
			}
			)
		  .on('changeDate', function(ev){
			
		});
});
</script>

</body></html>