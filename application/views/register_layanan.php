<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
<nav class="navbar navbar-default navbar-fixed-top navbar-no-margin-bottom" role="navigation">
    <button type="button" class="navbar-toggle navbar-toggle-side">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
<div class="container">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href=""><img class="logo-ptiik-apps" src="<?php echo base_url('assets/img/musi.png'); ?>" alt="MUSI APPS logo"></a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-blue-collapse">
    <ul class="nav navbar-nav navbar-side mini-side">
    <!-- search -->
    <form class="navbar-form form-side" role="search">
      <div class="form-group">
        <input type="text" class="form-control side-search-input" placeholder="Search">
      </div>
      <button type="submit" class="btn btn-default btn-no-radius btn-search-side"><span class="fa fa-search"></span></button>
      <button type="submit" class="btn btn-default btn-no-radius btn-search-side navbar-side-to-search"><span class="fa fa-search"></span></button>
    </form>
    <!-- end search -->
    <li class="dropdown"><a role="button" href="<?php echo site_url("dokter"); ?>"><span class="fa fa-user-md fa-lg"></span> <span class="text-side">Dokter<span class="fa fa-angle-down icon-in-side-dropdown"></span></span></a>
      
              </li>
              
      <li><a class="" href="<?php echo site_url("pasien"); ?>"><span class="fa fa-user fa-lg"></span><span class="text-side">Pasien</span></a></li>
      
      <li><a class="" href="<?php echo site_url("layanan"); ?>"><span class="fa fa-stethoscope fa-lg"></span><span class="text-side">Layanan</span></a></li>
      <li><a class="" href="<?php echo site_url("transaksi/create_transaksi"); ?>"><span class="fa fa-plus-square"></span><span class="text-side">Register</span></a></li>
	  <li><a class="" href="<?php echo site_url("transaksi/view_transaksi"); ?>"><span class="fa fa-clipboard fa-lg"></span><span class="text-side">Transaksi</span></a></li>
      <li class="dropdown"><a class="dropdown-toggle dropdown-toggle-side" data-toggle="dropdown" role="button" href=""><span class="fa fa-folder-open-o fa-lg"></span><span class="text-side">Laporan<span class="fa fa-angle-down icon-in-side-dropdown"></span></span></a>
      <ul role="menu" class="dropdown-menu">
                <li role=""><a href="<?php echo site_url("transaksi/laporan_harian"); ?>" tabindex="-1" role="menuitem">Laporan Harian</a></li>
				<li role=""><a href="<?php echo site_url("laporan"); ?>" tabindex="-1" role="menuitem">Laporan Umum Rumahsakit</a></li>
                <li role=""><a href="<?php echo site_url("laporan/detailkeuangan"); ?>" tabindex="-1" role="menuitem">Laporan Pendapatan</a></li>
				<li role=""><a href="<?php echo site_url("laporan/laporan_dokter"); ?>" tabindex="-1" role="menuitem">Laporan Pendapatan Dokter</a></li>
				<li role=""><a href="<?php echo site_url("laporan/detailpasien"); ?>" tabindex="-1" role="menuitem">Laporan Detail Pasien</a></li>
      			</ul>
              </li>
      
      <li><a class="" href=""><span class="fa fa-phone fa-lg"></span><span class="text-side">Kontak</span></a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
	
      <li>
	  <br>
	  <button class="btn " >
		
		<i class="fa fa-user "></i> <?php
		 $user = $this->ion_auth->user()->row();
		 if (!$this->ion_auth->logged_in())
		{
			echo '<a href="#" class="btn btn-danger ">Login&nbsp;<i class="icon-chevron-right"></i></a>';
		}
		else
		{
		 ?>
              Halo,  <a href="#" class="navbar-link"><?php echo $user->username; ?></a>
			  <a href="<?php echo site_url('auth/logout') ?>" class="btn btn-danger ">Logout&nbsp;<i class="fa fa-sign-out fa-sm"></i></i></a>
		<?php
		}
		?></button>
      
      </li>
	  
    </ul>
  </div><!-- /.navbar-collapse -->
  </div>
</nav>

</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">REGISTER LAYANAN</legend>
		<form class="form-horizontal" action="<?php echo site_url("layanan/create_layanan"); ?>" method="POST">
		    
		  <div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">NAMA LAYANAN :</label>
					<div class="col-lg-10">
					<div class="col-lg-8">
						<input type="text" id="inputNama" name="inputNama" placeholder="Nama layanan">
					</div>
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">HARGA LAYANAN :</label>
					<div class="col-lg-10">
					<div class="col-lg-8">
						<div class="input-group margin-bottom-sm">
						<span class="input-group-addon">Rp</span>
						<input type="text" id="harga" name="harga" value="0">
						</div>
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">DISKON LAYANAN :</label>
					<div class="col-lg-10">
					<div class="col-lg-2">
						<div class="input-group margin-bottom-sm">
						<input type="text" id="persendiskon" name="persendiskon" value="0">
						<span class="input-group-addon">%</span>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="input-group margin-bottom-sm">
						<span class="input-group-addon">Rp</span>
						<input type="text" id="nominaldiskon" name="nominaldiskon" class="input-xlarge" value="0">	
						</div>
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">HARGA NETTO :</label>
					<div class="col-lg-10">
					<div class="col-lg-8">
						<div class="input-group margin-bottom-sm">
						<span class="input-group-addon">Rp</span>
						<input type="text" id="harganetto" name="harganetto" value="0" readonly>
						</div>
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">FEE DOKTER PENGIRIM :</label>
					<div class="col-lg-10">
					<div class="col-lg-2">
						<div class="input-group margin-bottom-sm">
						<input type="text" id="share1" name="share1" value="0">
						<span class="input-group-addon">%</span>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="input-group margin-bottom-sm">
						<span class="input-group-addon">Rp</span>
						<input type="text" id="nominalshare1" name="nominalshare1" value="0">	
						</div>
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">FEE DOKTER OPERATOR :</label>
					<div class="col-lg-10">
					<div class="col-lg-2">
						<div class="input-group margin-bottom-sm">
						<input type="text" id="share2" name="share2" value="0">
						<span class="input-group-addon">%</span>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="input-group margin-bottom-sm">
						<span class="input-group-addon">Rp</span>
						<input type="text" id="nominalshare2" name="nominalshare2" value="0">	
						</div>
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">FEE INVESTOR :</label>
					<div class="col-lg-10">
					<div class="col-lg-2">
						<div class="input-group margin-bottom-sm">
						<input type="text" id="share3" name="share3" value="0">
						<span class="input-group-addon">%</span>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="input-group margin-bottom-sm">
						<span class="input-group-addon">Rp</span>
						<input type="text" id="nominalshare3" name="nominalshare3" value="0">	
						</div>
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">FEE INVESTOR :</label>
					<div class="col-lg-10">
					<div class="col-lg-2">
						<div class="input-group margin-bottom-sm">
						<input type="text" id="share4" name="share4" value="0">
						<span class="input-group-addon">%</span>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="input-group margin-bottom-sm">
						<span class="input-group-addon">Rp</span>
						<input type="text" id="nominalshare4" name="nominalshare4" value="0">	
						</div>
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">KETERANGAN :</label>
					<div class="col-lg-10">
					<div class="col-lg-8">
						<input type="text" id="keterangan" name="keterangan" placeholder="Keterangan">
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">&nbsp;</label>
					<div class="col-lg-10">
					
					<div class="col-lg-8">
						<button type="submit" class="btn btn-info btn-large"><i class="fa fa-save"></i> Simpan</button>
					</div>
					
					</div>
			</div>
		  
		</form>
   
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>


<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>

<script type="text/javascript">
function dstrToUTC(ds) {
    var dsarr = ds.split("-");
     var dd = parseInt(dsarr[0],10);
     var mm = parseInt(dsarr[1],10);
     var yy = parseInt(dsarr[2],10);
     return Date.UTC(yy,mm-1,dd,0,0,0); 
	}
function datediff(ds1,ds2) 
	{
     var d1 = dstrToUTC(ds1);
     var d2 = dstrToUTC(ds2);
     var oneday = 86400000;
     return (d2-d1) / oneday;    
	}
function humanise (diff) {
  // The string we're working with to create the representation
  var str = '';
  // Map lengths of `diff` to different time periods
  var values = {
    'umur': 365, 
    'umur_bulan': 30, 
    'umur_hari': 1
  };

  // Iterate over the values...
  for (var x in values) {
    var amount = Math.floor(diff / values[x]);

    // ... and find the largest time value that fits into the diff
    if (amount >= 1) {
       // If we match, add to the string ('s' is for pluralization)
       str += amount + x + (amount > 1 ? 's' : '') + ' ';
	   $('#'+x).val(amount);
	   

       // and subtract from the diff
       diff -= amount * values[x];
    }
	else
	{
		$('#'+x).val(0);
	}
  }

  return str;
}

function hitung_netto() 
	{
     var harga=$('#harga').val();
	 var nominaldiskon=$('#nominaldiskon').val();
     
	 var netto=harga-nominaldiskon;
     $("#harganetto").val(netto);
	}

function update_share() 
	{
		var harga=$('#harga').val();
		var persendiskon=$('#persendiskon').val();
		
		var nominaldiskon=harga*persendiskon/100;
		$("#nominaldiskon").val(nominaldiskon);
		hitung_netto();
		
		var share1=$('#share1').val();
		var nominalshare1=harga*share1/100;
		$("#nominalshare1").val(nominalshare1);
		
		var share2=$('#share2').val();
		var nominalshare2=harga*share2/100;
		$("#nominalshare2").val(nominalshare2);
		
		var share3=$('#share3').val();
		var nominalshare3=harga*share3/100;
		$("#nominalshare3").val(nominalshare3);
		
		var share4=$('#share4').val();
		var nominalshare4=harga*share4/100;
		$("#nominalshare4").val(nominalshare4);
		
	 
	}
	
$(document).ready(function(){

$('#persendiskon').keyup(function() {
			var persendiskon=$('#persendiskon').val();
			var harga=$('#harga').val();
			
			var nominaldiskon=harga*persendiskon/100;
			$("#nominaldiskon").val(nominaldiskon);
			hitung_netto();
});

$('#nominaldiskon').keyup(function() {
			var nominaldiskon=$('#nominaldiskon').val();
			var harga=$('#harga').val();
			
			var persendiskon=nominaldiskon*100/harga;
			$("#persendiskon").val(persendiskon);
			hitung_netto();
});

$('#share1').keyup(function() {
			var share1=$('#share1').val();
			var harga=$('#harga').val();
			
			var nominalshare1=harga*share1/100;
			$("#nominalshare1").val(nominalshare1);
			
});

$('#nominalshare1').keyup(function() {
			var nominalshare1=$('#nominalshare1').val();
			var harga=$('#harga').val();
			
			var share1=nominalshare1*100/harga;
			$("#share1").val(share1);
});

$('#share2').keyup(function() {
			var share2=$('#share2').val();
			var harga=$('#harga').val();
			
			var nominalshare2=harga*share2/100;
			$("#nominalshare2").val(nominalshare2);
			
});

$('#nominalshare2').keyup(function() {
			var nominalshare2=$('#nominalshare2').val();
			var harga=$('#harga').val();
			
			var share2=nominalshare2*100/harga;
			$("#share2").val(share2);
});

$('#share3').keyup(function() {
			var share3=$('#share3').val();
			var harga=$('#harga').val();
			
			var nominalshare3=harga*share3/100;
			$("#nominalshare3").val(nominalshare3);
			
});

$('#nominalshare3').keyup(function() {
			var nominalshare3=$('#nominalshare3').val();
			var harga=$('#harga').val();
			
			var share3=nominalshare3*100/harga;
			$("#share3").val(share3);
});

$('#share4').keyup(function() {
			var share4=$('#share4').val();
			var harga=$('#harga').val();
			
			var nominalshare4=harga*share4/100;
			$("#nominalshare4").val(nominalshare4);
			
});

$('#nominalshare4').keyup(function() {
			var nominalshare4=$('#nominalshare4').val();
			var harga=$('#harga').val();
			
			var share4=nominalshare4*100/harga;
			$("#share4").val(share4);
});

$('#harga').keyup(function() {
	update_share();
});

});

</script>

</body></html>