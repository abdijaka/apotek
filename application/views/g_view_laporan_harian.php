<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>

	<?php
		$this->load->view('templates/menubar_kiri');
	?>

</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	

    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">LAPORAN TRANSAKSI TANGGAL <?php echo date('d F Y'); ?></legend>
		
		
		<form id="form1" action="<?php echo site_url("g_laporan/laporan_harian"); ?>" method="POST">
					
			<div class="row">
				<div class="col-md-12">
				<div class="col-md-1">
				<a class="btn">Tanggal :</a>
				</div>
				<div class="col-md-2">
				<input  type="text" value="<?php echo $mulai;?>" name="mulai" id="mulai" data-date-format="dd-mm-yyyy">
				</div>
				
				<div class="col-md-1">
				<button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Tampilkan</button>
				</div>
				
		
	  </form>
	  <form action="<?php echo site_url("g_laporan/cetak_laporan_harian"); ?>" method="POST">
				<input  type="hidden" value="<?php echo $mulai;?>" name="mulai2" id="mulai2" >
				
				<div class="col-md-2">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-danger btn-large"><i class="fa fa-print fa-sm"></i> Print Laporan</button>
				</div>
				</div>
			</div>
	  </form>
		
		<h3>Laporan Pembelian : </h3>
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" >
			<thead>
				<tr style="background-color: #90CA77; color:#fff;">
					<th>No</th>
					<th>Nomor Faktur</th>
					<th>Supplier</th>
					<th>Netto</th>
					
				</tr>
			</thead>
			<tbody>
			<?php

			$nomor=1;
			$totalnetto=0;
			$totaltunai=0;
			$totaldebit=0;
			$totalkredit=0;
			
			foreach ($pembelian as $detail_item): 
			
			//$tgl = new DateTime($detail_item['tgl_faktur']);
			$totalnetto=$totalnetto+$detail_item['total'];
			if($detail_item['carabayar']==1)
			{
				$totaltunai=$totaltunai+$detail_item['total'];
			}
			else if($detail_item['carabayar']==2)
			{
				$totaldebit=$totaldebit+$detail_item['total'];
			}
			else if($detail_item['carabayar']==3)
			{
				$totalkredit=$totalkredit+$detail_item['total'];
			}
			
			?>
			<tr class="odd gradeX">
					<td><?php echo $nomor; ?></td>
					<td><?php echo $detail_item['nomor_faktur'] ?></td>
					<td><?php echo $detail_item['nama_supplier'] ?></td>
					<td style="text-align:right;">Rp <?php echo formatRupiah($detail_item['total']) ?></td>
					
			</tr>
			<?php 
			$nomor++;
			
			endforeach ?>
			
			<tr class="odd gradeX">
					<td></td>
					<td colspan="2" align="right"><b>Total</b></td>
					<td style="text-align:right;">Rp <?php echo formatRupiah($totalnetto) ?></td>
			</tr>
			<tr class="odd gradeX">
					<td></td>
					<td colspan="2" align="right"><b>Cash</b></td>
					<td style="text-align:right;">Rp <?php echo formatRupiah($totaltunai) ?></td>
			</tr>
			<tr class="odd gradeX">
					<td></td>
					<td colspan="2" align="right"><b>Debit</b></td>
					<td style="text-align:right;">Rp <?php echo formatRupiah($totaldebit) ?></td>
			</tr>
			<tr class="odd gradeX">
					<td></td>
					<td colspan="2" align="right"><b>Kredit</b></td>
					<td style="text-align:right;">Rp <?php echo formatRupiah($totalkredit) ?></td>
			</tr>
			
			</tbody>
		</table>
		
		<!--
		<h3>Laporan Pengiriman : </h3>
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" >
			<thead>
				<tr style="background-color: #90CA77; color:#fff;">
					<th>No</th>
					<th>Nomor Pengiriman</th>
					<th>Pengiriman Kepada</th>
					
				</tr>
			</thead>
			<tbody>
			<?php

			$nomor=1;
			
			foreach ($distribusi as $detail_item): 
			
			//$tgl = new DateTime($detail_item['tgl_distribusi']);
			
			?>
			
			<tr class="odd gradeX">
					<td><?php echo $nomor; ?></td>
					<td><?php echo $detail_item['nomor_distribusi'] ?></td>
					<td><?php echo $detail_item['nama_ruangan'] ?></td>
			</tr>
		
			<?php 
			$nomor++;
			
			endforeach ?>
			
			</tbody>
		</table>
		
		-->
		
		<h3>Laporan Penjualan : </h3>
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" >
			<thead>
				<tr style="background-color: #90CA77; color:#fff;">
					<th>No</th>
					<th>Nomor Kwitansi</th>
					<th>Pasien</th>
					<th>Netto</th>
					
				</tr>
			</thead>
			<tbody>
			<?php

			$nomor=1;
			$totalnetto=0;
			$totaltunai=0;
			$totaldebit=0;
			$totalkredit=0;
			
			foreach ($penjualan as $detail_item): 
			
			//$tgl = new DateTime($detail_item['tgl_faktur']);
			$totalnetto=$totalnetto+$detail_item['total'];
			if($detail_item['carabayar']==1)
			{
				$totaltunai=$totaltunai+$detail_item['total'];
			}
			else if($detail_item['carabayar']==2)
			{
				$totaldebit=$totaldebit+$detail_item['total'];
			}
			else if($detail_item['carabayar']==3)
			{
				$totalkredit=$totalkredit+$detail_item['total'];
			}
			
			?>
			
			<tr class="odd gradeX">
					<td><?php echo $nomor; ?></td>
					<td><?php echo $detail_item['nomor_faktur'] ?></td>
					<td><?php echo $detail_item['nama_pasien'] ?></td>
					<td style="text-align:right;">Rp <?php echo formatRupiah($detail_item['total']) ?></td>
					
			</tr>
			
		
			<?php 
			$nomor++;
			
			endforeach ?>
			<tr class="odd gradeX">
					<td></td>
					<td colspan="2" align="right"><b>Total</b></td>
					<td style="text-align:right;">Rp <?php echo formatRupiah($totalnetto) ?></td>
			</tr>
			<tr class="odd gradeX">
					<td></td>
					<td colspan="2" align="right"><b>Cash</b></td>
					<td style="text-align:right;">Rp <?php echo formatRupiah($totaltunai) ?></td>
			</tr>
			<tr class="odd gradeX">
					<td></td>
					<td colspan="2" align="right"><b>Debit</b></td>
					<td style="text-align:right;">Rp <?php echo formatRupiah($totaldebit) ?></td>
			</tr>
			<tr class="odd gradeX">
					<td></td>
					<td colspan="2" align="right"><b>Kredit</b></td>
					<td style="text-align:right;">Rp <?php echo formatRupiah($totalkredit) ?></td>
			</tr>
			
			</tbody>
		</table>
		
</div>
</div>

</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>


<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}
</script>

<script type="text/javascript">
$(document).ready(function(){
		$('#mulai').datepicker(
			{
			
			}
			)
		  .on('changeDate', function(ev){
			
		});
});
</script>

</body></html>