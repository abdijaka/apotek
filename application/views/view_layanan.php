<div class="container">
   
      <h2>Layanan</h2>
	  <h2>
	  <a class="btn btn-primary btn-large" href="<?php echo site_url("layanan/layanan_baru"); ?>">Daftarkan layanan Baru</a>
	  </h2>
		<?php if($status==1) 
		{
		?>
		<div class="alert alert-success">
		Data Berhasil Dimasukkan!!
		</div>
		<?php } ?>
		
		<?php if($status==2) 
		{
		?>
		<div class="alert alert-info">
		Data Berhasil Diupdate!!
		</div>
		<?php } ?>
		
		<?php if($status==3) 
		{
		?>
		<div class="alert alert-danger">
		Data Berhasil Dihapus!!
		</div>
		<?php } ?>
		
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
			<thead>
				<tr>
					<th>Nama layanan</th>
					<th>Harga</th>
					<th>Diskon</th>
					<th>Nominal Diskon</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ($layanan as $layanan_item): ?>
			
			<tr class="odd gradeX">
					<td><?php echo $layanan_item['nama_layanan'] ?></td>
					<td><?php echo $layanan_item['harga'] ?></td>
					<td><?php echo $layanan_item['diskon'] ?></td>
					<td><?php echo $layanan_item['nominal_diskon'] ?></td>
					<td class="center"> <a class="btn btn-primary" href="<?php echo site_url("layanan/edit_layanan/".$layanan_item['id_layanan']); ?>">Edit</a> </td>
					<td class="center"><a class="btn btn-danger" href="<?php echo site_url("layanan/delete_layanan/".$layanan_item['id_layanan']); ?>">Delete</a></td>
				</tr>
		
			<?php 
			
			endforeach ?>
		
				
				
			</tbody>
		</table>
      
   
</div>
