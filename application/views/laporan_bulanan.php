<div class="container">

      <h2>Laporan Bulanan</h2>
	  
	  <form class="form-horizontal" id="form1" action="<?php echo site_url("laporan/view_laporan"); ?>" method="POST">
	  <div class="control-group">
		<label class="control-label" for="inputNama">Bulan</label>
		
		<div class="controls">
			<select name="bulan1" id="bulan1" class="input-medium">
				<option <?php if($bulan1==1) echo 'selected'; ?> value="1">Januari</option>
				<option <?php if($bulan1==2) echo 'selected'; ?> value="2">Februari</option>
				<option <?php if($bulan1==3) echo 'selected'; ?> value="3">Maret</option>
				<option <?php if($bulan1==4) echo 'selected'; ?> value="4">April</option>
				<option <?php if($bulan1==5) echo 'selected'; ?> value="5">Mei</option>
				<option <?php if($bulan1==6) echo 'selected'; ?> value="6">Juni</option>
				<option <?php if($bulan1==7) echo 'selected'; ?> value="7">Juli</option>
				<option <?php if($bulan1==8) echo 'selected'; ?> value="8">Agustus</option>
				<option <?php if($bulan1==9) echo 'selected'; ?> value="9">September</option>
				<option <?php if($bulan1==10) echo 'selected'; ?> value="10">Oktober</option>
				<option <?php if($bulan1==11) echo 'selected'; ?> value="11">Nopember</option>
				<option <?php if($bulan1==12) echo 'selected'; ?> value="12">Desember</option>
				
			</select>
			sampai
			<select name="bulan2" id="bulan2" class="input-medium">
				<option <?php if($bulan2==1) echo 'selected'; ?> value="1">Januari</option>
				<option <?php if($bulan2==2) echo 'selected'; ?> value="2">Februari</option>
				<option <?php if($bulan2==3) echo 'selected'; ?> value="3">Maret</option>
				<option <?php if($bulan2==4) echo 'selected'; ?> value="4">April</option>
				<option <?php if($bulan2==5) echo 'selected'; ?> value="5">Mei</option>
				<option <?php if($bulan2==6) echo 'selected'; ?> value="6">Juni</option>
				<option <?php if($bulan2==7) echo 'selected'; ?> value="7">Juli</option>
				<option <?php if($bulan2==8) echo 'selected'; ?> value="8">Agustus</option>
				<option <?php if($bulan2==9) echo 'selected'; ?> value="9">September</option>
				<option <?php if($bulan2==10) echo 'selected'; ?> value="10">Oktober</option>
				<option <?php if($bulan2==11) echo 'selected'; ?> value="11">Nopember</option>
				<option <?php if($bulan2==12) echo 'selected'; ?> value="12">Desember</option>
			</select>
		  <input type="text" id="tahun" name="tahun" class="span1" value="<?php echo $tahun ?>">
		  <button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Tampilkan</button>
		</div>
		
	  </div>
	  </form>
	  
		
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Layanan</th>
					<th>Jumlah</th>
					<th>Total Harga</th>
					<th>Total Diskon</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
			<?php

			$nomor=1;
			foreach ($laporan as $laporan_item): ?>
			
			<tr class="odd gradeX">
					<td><?php echo $nomor; ?></td>
					<td><?php echo $laporan_item['nama_layanan'] ?></td>
					<td><?php echo $laporan_item['jumlah'] ?></td>
					<td><?php echo $laporan_item['jumlah_harga'] ?></td>
					<td><?php echo $laporan_item['jumlah_diskon'] ?></td>
					<td><?php echo $laporan_item['total_netto'] ?></td>
					
				</tr>
		
			<?php 
			$nomor++;
			endforeach ?>
		
				
				
			</tbody>
		</table>
      
   
</div>
