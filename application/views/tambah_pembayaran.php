<form class="form-horizontal" method="POST" action="<?php echo site_url('transaksi/insert_pembayaran') ?>">
<div class="modal-dialog">
    <div class="modal-content modal-content-no-radius">
      <div class="modal-header modal-header-blue">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title modal-title-center" id="registerModalLabel">Tambah Pembayaran</h4>
      </div>
      <div class="modal-body">
			<?php
			$date=date("d-m-Y");
			?>
			
			<div class="row">
			  <div class="col-md-12">
				<div class="col-md-6">
				<label class="control-label" for="pb_tanggal">Tanggal</label>
					<input type="text" value="<?php echo $date; ?>" name="pb_tanggal" id="pb_tanggal" readonly>
				</div>
				
				<div class="col-md-6">
					<label class="control-label" for="nota">Nomor Registrasi</label>
					<input type="text" id="pb_nota" name="pb_nota" value="<?php echo $transaksi['nomor_registrasi']; ?>" readonly >
				</div>
				</div>
			</div>
			  
			  <div class="row">
			  <div class="col-md-12">
				<div class="col-md-6">
				<label class="control-label" for="pb_carabayar">Cara Bayar</label>
				
				  <select name="pb_carabayar" id="pb_carabayar" >
									<?php foreach ($carabayar as $carabayar_item): ?>
										<option value="<?php echo $carabayar_item['id'] ?>"><?php echo $carabayar_item['nama'] ?></option>
									<?php endforeach; ?>
									  
									</select>
				
				</div>
				
				<div class="col-md-6">
					<label class="control-label" for="jumlahbayar">Jumlah Yang Dibayarkan</label>
					<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">Rp</span>
				  <input type="text" id="pb_jumlahbayar2" name="pb_jumlahbayar2"  value="<?php echo $transaksi['sisa']; ?>" >
				  <input type="hidden" id="pb_totalharganetto" name="pb_totalharganetto"  value="<?php echo $transaksi['harga_netto']; ?>" >
				  <input type="hidden" id="pb_totalbayar" name="pb_totalbayar"  value="<?php echo $transaksi['bayar']; ?>" >
				</div>
				</div>
			  </div>
			  </div>
			  
			  <div class="row">
			  <div class="col-md-12">
				<div class="col-md-6">
				<label class="control-label" for="eb_carabayar">Charge Bank</label>
					<div class="input-group margin-bottom-sm">
					<input type="text" id="pb_persencharge" name="pb_persencharge"  value="0" >
					<span class="input-group-addon">%</span><span class="input-group-addon">Rp</span>
					
					<input type="text" id="pb_chargebank" name="pb_chargebank"  value="0" readonly>
				</div>
				</div>
				
				<div class="col-md-6">
					<label class="control-label" for="jumlahbayar">Total+Charge</label>
					<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">Rp</span>
				  <input type="text" id="pb_bayarpluscharge" name="pb_bayarpluscharge"  value="0" readonly>
				</div>
				</div>
				
			  </div>
			  </div>
			  
			  <div class="row">
				<div class="col-md-12">
				<div class="col-md-12">
				<label class="control-label" for="sisa">Sisa</label>
					<div class="input-group margin-bottom-sm">
					<span class="input-group-addon">Rp</span>
					<input type="text" id="pb_sisa2" name="pb_sisa2"  value="<?php echo $transaksi['sisa']; ?>" readonly>
					<input type="hidden" id="flag" name="flag"  value="0" >
					</div>
				</div>
				</div>
			  </div>
			  <div class="row">
				<div class="col-md-12">
				<div class="col-md-12">
				<label class="control-label" for="memo">Memo</label>
				
				  <textarea id="memo" name="memo"></textarea>
				</div>
				</div>
			  </div>
			  
		<script type="text/javascript">
		function pb_hitung_sisa()
			{
			 var harga=$('#pb_totalharganetto').val();
			 var bayar=$('#pb_jumlahbayar2').val();
			 var pb_totalbayar=$('#pb_totalbayar').val();
			 
			 var sisa=harga-bayar-pb_totalbayar;
			 
			 $("#pb_sisa2").val(sisa);
			}
		function pb_hitung_charge()
			{
			 var persencharge=$('#pb_persencharge').val();
			 var bayar=$('#pb_jumlahbayar2').val();
			 var charge=Math.ceil(persencharge*bayar/100);
			 $("#pb_chargebank").val(charge);
			}
		function pb_hitung_totalcharge()
			{
			 var chargebank=$('#pb_chargebank').val();
			 var bayar=$('#pb_jumlahbayar2').val();
			 var total_charge=chargebank*1+bayar*1;
			 $("#pb_bayarpluscharge").val(total_charge);
			}
		$(document).ready(function(){
		pb_hitung_sisa();
		$('#pb_jumlahbayar2').keyup(function() {
					pb_hitung_charge();
					pb_hitung_totalcharge();
					pb_hitung_sisa();
		});
		
		$('#pb_persencharge').keyup(function() {
					pb_hitung_charge();
					pb_hitung_totalcharge();
		});
		
		$('#pb_carabayar').change(function() {
				var carabayar=$("#pb_carabayar").val();
				if(carabayar==3)
				{
					$("#pb_persencharge").val(2.5);
					pb_hitung_charge();
					pb_hitung_totalcharge();
				}
				else
				{
					$("#pb_persencharge").val(0);
					pb_hitung_charge();
					pb_hitung_totalcharge();
				}
		});
		
		var flag=$('#flag0').val();
		$('#flag').val(flag);
		});
		</script>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Tambahkan</button>
      </div>
    </div>
  </div>
</form>