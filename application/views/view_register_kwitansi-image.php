<?php

function terbilang($x)
{
$abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
if ($x < 12)
return " " . $abil[$x];
elseif ($x < 20)
return Terbilang($x - 10) . "Belas";
elseif ($x < 100)
return Terbilang($x / 10) . " Puluh" . Terbilang($x % 10);
elseif ($x < 200)
return " seratus" . Terbilang($x - 100);
elseif ($x < 1000)
return Terbilang($x / 100) . " Ratus" . Terbilang($x % 100);
elseif ($x < 2000)
return " seribu" . Terbilang($x - 1000);
elseif ($x < 1000000)
return Terbilang($x / 1000) . " Ribu" . Terbilang($x % 1000);
elseif ($x < 1000000000)
return Terbilang($x / 1000000) . " Juta" . Terbilang($x % 1000000);
}

function html2ascii($s){
 // convert links
 $s = preg_replace('/<a\s+.*?href="?([^\" >]*)"?[^>]*>(.*?)<\/a>/i','$2 ($1)',$s);
 
 // convert p, br and hr tags
 $s = preg_replace('@<(b|h)r[^>]*>@i',"\n",$s);
 $s = preg_replace('@<p[^>]*>@i',"\n\n",$s);
 $s = preg_replace('@<div[^>]*>(.*)</div>@i',"\n".'$1'."\n",$s);  
  
 // convert bold and italic tags
 $s = preg_replace('@<b[^>]*>(.*?)</b>@i','*$1*',$s);
 $s = preg_replace('@<strong[^>]*>(.*?)</strong>@i','*$1*',$s);
 $s = preg_replace('@<i[^>]*>(.*?)</i>@i','_$1_',$s);
 $s = preg_replace('@<em[^>]*>(.*?)</em>@i','_$1_',$s);
   
 // decode any entities
 $s = strtr($s,array_flip(get_html_translation_table(HTML_ENTITIES)));
 
 // decode numbered entities
 $s = preg_replace('/&#(\d+);/e','chr(str_replace(";","",str_replace("&#","","$0")))',$s);
 
 // strip any remaining HTML tags
 $s = strip_tags($s);
 
 // return the string
 return $s;
}

function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . "";
  return $nilaiRupiah;
}
			
$isi_layanan="";
if("".$transaksi['baru']==0)
$label_baru="L";
else
$label_baru="B";

$carabayar=$pembayaran[0]['namacarabayar'];

if($transaksi['initial']==0)
$initial="TN.";
else if($transaksi['initial']==1)
$initial="NY.";
else if($transaksi['initial']==2)
$initial="NN.";
else if($transaksi['initial']==3)
$initial="ANAK";
else
$initial="BAYI";

$tanggal =date('j-m-Y', strtotime($transaksi['tanggal']));
$jam =date('G:i:s', strtotime($transaksi['tanggal']));

$tanggal_pembayaran =date('j-m-Y', strtotime($pembayaran[0]['tanggal']));
$jam_pembayaran =date('G:i:s', strtotime($pembayaran[0]['tanggal']));

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
<link href="<?php echo base_url('assets/css/DT_bootstrap.css') ?>" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">

	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.7.1.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.ascii.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/directPrint.js') ?>"></script>
	<script type="text/javascript">
      var qz;   // Our main applet.
	var tes="";
	var info="";
	
	$(document).ready(function() {
			tes=$("#tablenya").ascii("raw");
			tes=tes.replace(/\|/g,"");
			
			info=$("#info2").ascii("raw");
			
		});
      function findPrinter() {
         
         if (qz != null) {
            // Searches for locally installed printer with "zebra" in the name
            qz.findPrinter("Zebra");
         }
         
         // *Note:  monitorFinding() still works but is too complicated and
         // outdated.  Instead create a JavaScript  function called 
         // "jzebraDoneFinding()" and handle your next steps there.
         monitorFinding();
      }
		function rightpad (str, max) {
		  return str.length < max ? rightpad(str+" ", max) : str;
		}
		
		function leftpad (str, max) {
		  return str.length < max ? leftpad(" " + str, max) : str;
		}

      function findPrinters() {
         
         if (qz != null) {
            // Searches for locally installed printer with "zebra" in the name
            qz.findPrinter("\\{dummy printer name for listing\\}");
         }

         monitorFinding2();
      }

      function print() {
         qz.findPrinter();
         if (qz != null) {
	
            // Send characters/raw commands to qz using "append"
            // This example is for EPL.  Please adapt to your printer language
            // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"

		var lokasi="<?php echo base_url('assets/img/musi.bmp') ?>"; 
      
	  qz.appendHTML("<html><p>halo</p></html>");
      qz.printHTML();

			qz.append("\x1B\x40"); // 1
	   
qz.append("\x1B\x21\x08"); // 2
qz.append("MUSI HEART CLINIC \r\n");
qz.append("\x1B\x21\x01"); 
qz.append("\x0F"); 
var tanggal="<?php echo $tanggal; ?>";
qz.append("Jl. Musi No. 48 Surabaya                      Surabaya, "+tanggal+" \r\n"); // 3
qz.append("Telp : 031-5682964\r\n\r\n"); // 3
qz.append("\x1B\x4D"); // 2
qz.append("\x1B\x70\x31"); // 2
qz.append("\x1B\x4D"); // 2
qz.append("\x1B\x21\x08"); // 2
qz.append("          K W I T A N S I  \r\n");

var nomor="<?php echo $transaksi['nomor_registrasi']; ?>";
var baru="<?php echo $label_baru; ?>";
qz.append("\x1B\x21\x01"); 
qz.append("\x0F"); // 3
qz.append("                          NO :"+nomor+"\r\n\r\n"); // 3
var bayar ="<?php echo formatRupiah($pembayaran[0]['jumlah']); ?>";
var memo ="<?php echo $pembayaran[0]['memo']; ?>";
var tanggal_bayar ="<?php echo $tanggal_pembayaran.' '.$jam_pembayaran; ?>";
var sisa ="<?php echo formatRupiah($transaksi['sisa']); ?>";

	var pasien =rightpad("Nomor ID    : "+"<?php echo $transaksi['pasien']; ?>"+" ("+baru+")", 45);
	var carabayar="Type Bayar : "+"<?php echo $carabayar; ?>";
	var nama_pasien=rightpad("Nama Pasien : "+"<?php echo $transaksi['nama_pasien'].', '.$initial; ?>", 45);
	var jam="Jam        : "+"<?php echo $jam; ?>";
	var nama_dokter=rightpad("Nama Dokter : "+"<?php echo $transaksi['nama_dokter']; ?>", 45);
	var halaman="Halaman    : #1";
	
//qz.append(""+info+""); // 3
qz.append(pasien+" "+carabayar+"\r\n"); // 3
qz.append(nama_pasien+" "+jam+"\r\n"); // 3
qz.append(nama_dokter+" "+halaman+"\r\n"); // 3
qz.append("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\r\n"); // 3
qz.append(rightpad(" NO",4));
qz.append(rightpad("TINDAKAN",34));
qz.append(leftpad("BIAYA(Rp)",12));
qz.append(leftpad("DISC",7));
qz.append(leftpad("NETTO(Rp)",12));
qz.append("\r\n"); // 3
qz.append(""+tes+""); // 3

var isi_subtotal=$("#isi_subtotal").val();
var isi_total=$("#isi_total").val();

qz.append(""+isi_subtotal+"\r\n"); // 3
qz.append(""+isi_total+"\r\n"); // 3
qz.append("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\r\n"); // 3

var terbilang ="<?php echo terbilang($transaksi['harga_netto']); ?>";
qz.append("Terbilang :"+terbilang+" Rupiah");
var id_layanan=$("#id_layanan").val();
if(id_layanan==14 || id_layanan==16)
{
qz.append("\r\nMemo      : "+memo);
qz.append("\r\nBayar     : Rp "+bayar+",-");
qz.append("\r\nKurang    : Rp "+sisa+",-\r\n\r\n\r\n\r\n");
}

/*
qz.append(""+tes+"\r\n"); // 3

var terbilang ="<?php echo terbilang($transaksi['harga_netto']); ?>";
qz.append("\r\n    Terbilang     : "+terbilang+" Rupiah");
qz.append("\r\n    Tanggal Bayar : Rp "+bayar+",-");
qz.append("\r\n    Jumlah        : Rp "+bayar+",-");
qz.append("\r\n    Memo          : "+memo);
qz.append("\r\n    Sisa          : Rp "+sisa+",-");
qz.append("\x0C"); 
*/
            qz.print(); // send commands to printer

		
	 }
	 
         // *Note:  monitorPrinting() still works but is too complicated and
         // outdated.  Instead create a JavaScript  function called 
         // "jzebraDonePrinting()" and handle your next steps there.
	 monitorPrinting();
         
         /**
           *  PHP PRINTING:
           *  // Uses the php `"echo"` function in conjunction with qz-print `"append"` function
           *  // This assumes you have already assigned a value to `"$commands"` with php
           *  qz.append(<?php echo $commands; ?>);
           */
           
         /**
           *  SPECIAL ASCII ENCODING
           *  //qz.setEncoding("UTF-8");
           *  qz.setEncoding("Cp1252"); 
           *  qz.append("\xDA");
           *  qz.append(String.fromCharCode(218));
           *  qz.append(chr(218));
           */
         
      }
      
      
      
      // Gets the current url's path, such as http://site.com/example/dist/
      function getPath() {
          var path = window.location.href;
          return path.substring(0, path.lastIndexOf("/")) + "/";
      }
      
 
      function chr(i) {
         return String.fromCharCode(i);
      }
      
      // *Note:  monitorPrinting() still works but is too complicated and
      // outdated.  Instead create a JavaScript  function called 
      // "jzebraDonePrinting()" and handle your next steps there.
      function monitorPrinting() {
	
	if (qz != null) {
	   if (!qz.isDonePrinting()) {
	      window.setTimeout('monitorPrinting()', 100);
	   } else {
	      var e = qz.getException();
	      alert(e == null ? "Printed Successfully" : "Exception occured: " + e.getLocalizedMessage());
              qz.clearException();
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
      
      function monitorFinding() {
	
	if (qz != null) {
	   if (!qz.isDoneFinding()) {
	      window.setTimeout('monitorFinding()', 100);
	   } else {
	      var printer = qz.getPrinter();
              //alert(printer == null ? "Printer not found" : "Printer \"" + printer + "\" found");
	   }
	} else {
            alert("Applet not loaded!");
        }
      }

      function monitorFinding2() {
	
	if (qz != null) {
	   if (!qz.isDoneFinding()) {
	      window.setTimeout('monitorFinding2()', 100);
	   } else {
              var printersCSV = qz.getPrinters();
              var printers = printersCSV.split(",");
              for (p in printers) {
                  alert(printers[p]);
              }
              
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
      
      // *Note:  monitorAppending() still works but is too complicated and
      // outdated.  Instead create a JavaScript  function called 
      // "jzebraDoneAppending()" and handle your next steps there.
      function monitorAppending() {
	
	if (qz != null) {
	   if (!qz.isDoneAppending()) {
	      window.setTimeout('monitorAppending()', 100);
	   } else {
	      qz.print(); // Don't print until all of the data has been appended
              
              // *Note:  monitorPrinting() still works but is too complicated and
              // outdated.  Instead create a JavaScript  function called 
              // "jzebraDonePrinting()" and handle your next steps there.
              monitorPrinting();
	   }
	} else {
            alert("Applet not loaded!");
        }
      }

      // *Note:  monitorAppending2() still works but is too complicated and
      // outdated.  Instead create a JavaScript  function called 
      // "jzebraDoneAppending()" and handle your next steps there.
      function monitorAppending2() {
	
	if (qz != null) {
	   if (!qz.isDoneAppending()) {
	      window.setTimeout('monitorAppending2()', 100);
	   } else {
	      qz.printPS(); // Don't print until all of the image data has been appended
              
              // *Note:  monitorPrinting() still works but is too complicated and
              // outdated.  Instead create a JavaScript  function called 
              // "jzebraDonePrinting()" and handle your next steps there.
              monitorPrinting();
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
      
      // *Note:  monitorAppending3() still works but is too complicated and
      // outdated.  Instead create a JavaScript  function called 
      // "jzebraDoneAppending()" and handle your next steps there.
      function monitorAppending3() {
	
	if (qz != null) {
	   if (!qz.isDoneAppending()) {
	      window.setTimeout('monitorAppending3()', 100);
	   } else {
	      qz.printHTML(); // Don't print until all of the image data has been appended
              
              
              // *Note:  monitorPrinting() still works but is too complicated and
              // outdated.  Instead create a JavaScript  function called 
              // "jzebraDonePrinting()" and handle your next steps there.
              monitorPrinting();
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
      
      function useDefaultPrinter() {
         
         if (qz != null) {
            // Searches for default printer
            qz.findPrinter();
         }
         
         monitorFinding();
      }
      
      function jzebraReady() {
          // Change title to reflect version
          qz = document.getElementById('qz');
	  
      }
      
      /**
       * By default, jZebra prevents multiple instances of the applet's main 
       * JavaScript listener thread to start up.  This can cause problems if
       * you have jZebra loaded on multiple pages at once. 
       * 
       * The downside to this is Internet Explorer has a tendency to initilize the
       * applet multiple times, so use this setting with care.
       */
      function allowMultiple() {
          
          if (qz != null) {
              var multiple = qz.getAllowMultipleInstances();
              qz.allowMultipleInstances(!multiple);
              alert('Allowing of multiple applet instances set to "' + !multiple + '"');
          }
      }
      
      function printPage() {
           $("#content").html2canvas({ 
                canvas: hidden_screenshot,
                onrendered: function() {printBase64Image($("canvas")[0].toDataURL('image/png'));}
           });
      }
      
      function printBase64Image(base64data) {
         
      	 if (qz != null) {
               qz.findPrinter("\\{dummy printer name for listing\\}");
               while (!qz.isDoneFinding()) {
                    // Note, endless while loops are bad practice.
               }

               var printers = qz.getPrinters().split(",");
               for (i in printers) {
		    if (printers[i].indexOf("Microsoft XPS") != -1 || 
			printers[i].indexOf("PDF") != -1) {
			   qz.setPrinter(i);      
		    }	       
               }
               
               // No suitable printer found, exit
               if (qz.getPrinter() == null) {
                   alert("Could not find a suitable printer for printing an image.");
                   return;
               }

               // Optional, set up custom page size.  These only work for PostScript printing.
               // setPaperSize() must be called before setAutoSize(), setOrientation(), etc.
               qz.setPaperSize("8.5in", "11.0in");  // US Letter
               qz.setAutoSize(true);
               qz.appendImage(base64data);
	    }

            // Very important for images, uses printPS() insetad of print()
            // *Note:  monitorAppending2() still works but is too complicated and
            // outdated.  Instead create a JavaScript  function called 
            // "jzebraDoneAppending()" and handle your next steps there.
	    monitorAppending2();
      }

      function logFeatures() {
          if (document.jzebra != null) {
              
              var logging = qz.getLogPostScriptFeatures();
              qz.setLogPostScriptFeatures(!logging);
              alert('Logging of PostScript printer capabilities to console set to "' + !logging + '"');
          }
      }
   
      function useAlternatePrinting() {
          
          if (qz != null) {
              var alternate = qz.isAlternatePrinting();
              qz.useAlternatePrinting(!alternate);
              alert('Alternate CUPS printing set to "' + !alternate + '"');
          }
      }
	  
	  function listSerialPorts() {
		
                if (qz != null) {
			qz.findPorts();
            while (!qz.isDoneFindingPorts()) {} // wait
			var ports = qz.getPorts().split(",");
			for (p in ports) {
				if (p == 0) {
					document.getElementById("port_name").value = ports[p];
				}
				alert(ports[p]);
			}
		}
	  }
	  
	  function openSerialPort() {
		
                if (qz != null) {
                    qz.openPort(document.getElementById("port_name").value);
		}
	  }
          
          function closeSerialPort() {
		
                if (qz != null) {
                    qz.closePort(document.getElementById("port_name").value);
		}
	  }
          
          // Automatically gets fired with the port is finished opening (even if it fails to open)
          function jzebraDoneOpeningPort(portName) {
              
              if (qz != null) {
                  var e = qz.getException();
                  if (e != null) {
                      alert("Could not open port [" + portName + "] \n\t" + e.getLocalizedMessage());
                      qz.clearException();
                  } else {
                      alert("Port [" + portName +  "] is open!");
                  }
              }
          }
          
          // Automatically gets fired with the port is finished closing (even if it fails to close)
          function jzebraDoneClosingPort(portName) {
              
              if (qz != null) {
                  var e = qz.getException();
                  if (e != null) {
                      alert("Could not close port [" + portName + "] \n\t" + e.getLocalizedMessage());
                      qz.clearException();
                  } else {
                      alert("Port [" + portName +  "] closed!");
                  }
              }
          }
          
          function sendSerialData() {
		
                if (qz != null) {
                    // Beggining and ending patterns that signify port has responded
                    // chr(2) and chr(13) surround data on a Mettler Toledo Scale
                    qz.setSerialBegin(chr(2));
                    qz.setSerialEnd(chr(13));
                    // Baud rate, data bits, stop bits, parity, flow control
                    // "9600", "7", "1", "even", "none" = Default for Mettler Toledo Scale
                    qz.setSerialProperties("9600", "7", "1", "even", "none");
                    // Send raw commands to the specified port.
                    // W = weight on Mettler Toledo Scale
                    qz.send(document.getElementById("port_name").value, "\nW\n");
                    
                    var e = qz.getException();
                    if (e != null) {
                        alert("Could not send data:\n\t" + e.getLocalizedMessage());
                        qz.clearException();  
                    }
		}
	  }
          
          // Automatically gets called when the serial port responds with data
          function jzebraSerialReturned(portName, data) {
            if (data == null || data == "") {       // Test for blank data
                alert("No data was returned.")
            } else if (data.indexOf("?") !=-1) {    // Test for bad data
                alert("Device not ready.  Please wait.")
            } else {                                // Display good data
                alert("Port [" + portName + "] returned data:\n\t" + data);
            }
          }

   </script>


</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">CETAK KWITANSI</legend>
	
	<div class="row">
	<div class="col-md-12">
	<div class="col-md-12">
   <h3><input type="button" class="btn btn-info" onClick="print()" value="Print Kwitansi" />    
   <applet id="qz" name="QZ Print Plugin" code="qz.PrintApplet.class" width="55" height="30">
	  <param name="jnlp_href" value="<?php echo base_url('assets/qz/dist/qz-print_jnlp.jnlp') ?>">
          <param name="cache_option" value="plugin">
   </applet></h3>
   </div></div>
   </div>
   
<table  id="tablenya" border="1" style="display:none" >
			
			<?php
			$i=0;
			$jml_row=5;
			$netto=0;
			$harga=0;
			$diskon=0;
			$isi_layanan="";
			$id_layanan=0;
				foreach ($detail as $list_item): 
				$i++;
				$netto=$netto+$list_item['netto'];
				$harga=$harga+$list_item['harga'];
				$diskon=$diskon+$list_item['nominal_diskon'];
				$id_layanan=$list_item['id_layanan'];
				$isi_layanan=$isi_layanan.''.str_pad($i,3," ").''.str_pad($list_item['nama_layanan'],31," ").''.str_pad(formatRupiah($list_item['harga']),12," ",STR_PAD_LEFT).''.str_pad(formatRupiah($list_item['nominal_diskon']),9," ",STR_PAD_LEFT).''.str_pad(formatRupiah($list_item['netto']),12," ",STR_PAD_LEFT).'<br>   '.$list_item['nama_dokter'].'<br>';
				?>
				
				<tr >
				  <td ><?php echo $i; ?></td>
				  <td><?php echo str_pad($list_item['nama_layanan'], 32); ?></td>
				  
				  <td ><?php echo str_pad(formatRupiah($list_item['harga']),10," ", STR_PAD_LEFT); ?></td>
				  <td ><?php echo str_pad(formatRupiah($list_item['nominal_diskon']),5," ",STR_PAD_LEFT); ?></td>
				  <td ><?php echo str_pad(formatRupiah($list_item['netto']),10, " ", STR_PAD_LEFT); ?></td>
				  
				</tr>
				<tr>
				<td  ></td>
				  <td  ><?php echo $list_item['nama_dokter']; ?></td>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
				</tr>  
			<?php 
			endforeach;
			for($j=$i; $j<$jml_row; $j++)
			{
			?>
			<tr>
				<td  ></td>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
			</tr>
			<tr>
				<td  ></td>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
			</tr>
			<?php 
			}
			?>	
</table>	

			<?php $isi_subtotal="    ".str_pad("Sub Total",34)."".str_pad(formatRupiah($harga), 12, " ", STR_PAD_LEFT)."".str_pad(formatRupiah($diskon), 7," ", STR_PAD_LEFT)."".str_pad(formatRupiah($netto), 12," ", STR_PAD_LEFT);
			?>
				  
				  
			
			<?php $isi_total="    ".str_pad("Total",34)."".str_pad("", 12)."".str_pad(formatRupiah($transaksi['nominal_diskon']), 7," ", STR_PAD_LEFT)."".str_pad(formatRupiah($transaksi['harga_netto']), 12," ", STR_PAD_LEFT);
			?>
	
			
	

	<table id="info2" style="display:none;">
	<tr>
		<td>Nomor ID    : <?php echo $transaksi['pasien']; ?></td>     <td>Type Bayar : <?php echo $carabayar; ?></td>
	</tr>
	<tr>
		<td>Nama Pasien : <?php echo $initial.' '.$transaksi['nama_pasien']; ?></td><td>Jam        : <?php echo $jam; ?></td>
	</tr>
	<tr>
		<td>Nama Dokter : <?php echo $transaksi['nama_dokter']; ?></td><td>Halaman    : #1</td>
	</tr>
	</table>

<div class="row">
		<div class="col-md-12">
			<div class="col-md-8" style="text-align:center;">
				<h3>K W I T A N S I</h3>
				<?php echo $transaksi['nomor_registrasi'].'('.$label_baru.')'; ?>
			</div>
			<div class="col-md-4">
				
			</div>
		</div>
	</div>
<br>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				Nomor ID    : <?php echo $transaksi['pasien']; ?><br>
				Nama Pasien : <?php echo $initial.' '.$transaksi['nama_pasien']; ?><br>
				Nama Dokter : <?php echo $transaksi['nama_dokter']; ?>
			</div>
			<div class="col-md-6">
				Type Bayar : <?php echo $carabayar; ?><br>
				Jam        : <?php echo $jam; ?><br>
				Halaman    : #1
			</div>
		</div>
	</div>
<br>	
<div class="row">
	<div class="col-md-12">
	<div class="col-md-8">
<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" >
		<thead>
		<tr style="background-color: #90CA77; color:#fff;">
		<th>NO</th><th>TINDAKAN</th><th>BIAYA(Rp)</th><th>DISC</th><th>NETTO(Rp)</th>
		</tr>
		</thead>

			
			<?php
			$i=0;
			$netto=0;
			$harga=0;
			$diskon=0;
				foreach ($detail as $list_item): 
				$i++;
				$netto=$netto+$list_item['netto'];
				$harga=$harga+$list_item['harga'];
				$diskon=$diskon+$list_item['nominal_diskon'];
				
				?>
				
				<tr >
				  <td ><?php echo $i; ?></td>
				  <td><?php echo $list_item['nama_layanan']; ?><br><?php echo $list_item['nama_dokter']; ?></td>
				  
				  <td style="text-align:right; vertical-align:middle;" ><?php echo formatRupiah($list_item['harga']); ?></td>
				  <td style="text-align:right; vertical-align:middle;" ><?php echo formatRupiah($list_item['nominal_diskon']); ?></td>
				  <td style="text-align:right; vertical-align:middle;" ><?php echo formatRupiah($list_item['netto']); ?></td>
				  
				</tr>
				
			<?php 
			endforeach ?>	
				

			<tr>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
			</tr>
			
			<tr>
				  
				  <td colspan="2" style="text-align:right; vertical-align:middle; font-weight:bold;" >Sub Total</td>
				  <td  style="text-align:right; vertical-align:middle; font-weight:bold;" ><?php echo formatRupiah($harga); ?></td>
				  <td style="text-align:right; vertical-align:middle; font-weight:bold;" ><?php echo formatRupiah($diskon); ?></td>
				  <td  style="text-align:right; vertical-align:middle; font-weight:bold;" ><?php echo formatRupiah($netto); ?></td>
				  
			
			</tr>
			<tr>
				  
				  <td colspan="2" style="text-align:right; vertical-align:middle; font-weight:bold;" >Total</td>
				  <td  ></td>
				  <td  style="text-align:right; vertical-align:middle; font-weight:bold;" ><?php echo formatRupiah($transaksi['nominal_diskon']) ?></td>
				  <td style="text-align:right; vertical-align:middle; font-weight:bold;" ><?php echo formatRupiah($transaksi['harga_netto']); ?></td>
				  
			
			</tr>
			
	</table>
</div>
</div>
</div>
<input id="isi_layanan" type="hidden" value="<?php echo $isi_layanan ;?>" />
<input id="isi_subtotal" type="hidden" value="<?php echo $isi_subtotal ;?>" />
<input id="isi_total" type="hidden" value="<?php echo $isi_total ;?>" />
<input id="id_layanan" type="hidden" value="<?php echo $id_layanan ;?>" />
<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				Tanggal Pembayaran    : <?php echo $tanggal_pembayaran.' '.$jam_pembayaran; ?><br>
				Jumlah Pembayaran    : Rp <?php echo formatRupiah($pembayaran[0]['jumlah']); ?><br>
				Memo    : <?php echo $pembayaran[0]['memo']; ?><br>
				Sisa : <?php echo formatRupiah($transaksi['sisa']); ?>

			</div>
			<div class="col-md-6">
				
			</div>
		</div>
	</div>
<br>

	
</div>
</div>
</div>
</section>
</section>

<div id="tampiltabel">
	
</div>
<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>

<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>

</body></html>