<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id">
<!-- Head -->
<head>
<!-- Meta -->
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>MUSI APPS</title>
	<meta content="MUSI Apps" name="description">
	<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
	<meta content="MUSI" name="author">
	<meta content="yes" name="apple-mobile-web-app-capable">
	<meta content="black" name="apple-mobile-web-app-status-bar-style">

	<!-- Style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
	<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
	<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>

<body style="">
	<header>
		<?php
			$this->load->view('templates/menubar_kiri');
		?>
	</header>
	<section id="wrap">
		<section class="content content-white">
			<div class="container container-content"> 	
				<div class="row">
					<div class="col-md-12">
						<legend style="text-align:center;">REGISTER USER</legend>
						<div id="infoMessage"><?php echo $message;?></div>
						<!--<form class="form-horizontal" action="<?php //echo site_url("auth/do_create_user");?>" method="POST">-->
						<?php echo form_open("auth/create_user");?>
							<div class="form-group">
								<label class="control-label col-lg-3" for="first_name">NAMA DEPAN :</label>
								<div class="col-lg-9">
									<div class="col-lg-5">
										<?php echo form_input($first_name);?></br>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-3" for="last_name">NAMA BELAKANG :</label>
								<div class="col-lg-9">
									<div class="col-lg-5">
										<?php echo form_input($last_name);?></br>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-3" for="company">PERUSAHAAN :</label>
								<div class="col-lg-9">
									<div class="col-lg-5">
										<?php echo form_input($company);?></br>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-3" for="email">EMAIL :</label>
								<div class="col-lg-9">
									<div class="col-lg-5">
										<?php echo form_input($email);?></br>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-3" for="phone">NOMOR TELEPON :</label>
								<div class="col-lg-9">
									<div class="col-lg-5">
										<?php echo form_input($phone);?></br>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-3" for="password">PASSWORD :</label>
								<div class="col-lg-9">
									<div class="col-lg-5">
										<?php echo form_input($password);?></br>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-3" for="password_confirm">KONFIRMASI PASSWORD :</label>
								<div class="col-lg-9">
									<div class="col-lg-5">
										<?php echo form_input($password_confirm);?></br>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-3" for="password_confirm"></label>
								<div class="col-lg-9">
									<div class="col-lg-5">
										<button type="submit" class="btn btn-info"><i class="fa fa-save  "></i> Simpan</button>
									</div>
								</div>
							</div>

						<!--</form>--> 
						<?php echo form_close();?>
					</div>
				</div>
			</div>
		</section>
	</section>
</body>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
	            <div class="bptik-copy hide-mini-footer">
	            	Musi Heart Clinic
	            </div>
	            <div class="bptik-reserved  hide-mini-footer">
	            	Surabaya
	            </div>
            </div>
        </div>
    </div>
</footer>