<div class="container">
   
      <h2>Edit dokter</h2>
      <form class="form-horizontal" action="<?php echo site_url("dokter/update_dokter"); ?>" method="POST">
		  <div class="control-group">
			<label class="control-label" for="inputID">Nomor ID dokter</label>
			<div class="controls control-row">
			  <input type="text" id="inputID" name="inputID" placeholder="Nomor ID dokter" value="<?php echo $dokter['id_dokter'] ?>" readonly>
			  
			  &nbsp;
			  
			  <label class="checkbox inline">
				<input type="checkbox" id="member" name="member" value="1" <?php if ($dokter['internal']==1) echo "checked";  ?> > Internal
			  </label>
			</div>
			
		  </div>
		    
		  <div class="control-group">
			<label class="control-label" for="inputNama">Nama dokter</label>
			<div class="controls">
				<select name="initial" id="initial" class="input-small">
					<option value="0" <?php if ($dokter['initial']==0) echo "selected";  ?> >Tn.</option>
					<option value="1" <?php if ($dokter['initial']==1) echo "selected";  ?> >Ny.</option>
				</select>
			  <input type="text" id="inputNama" name="inputNama" class="span5" placeholder="Nama dokter" value="<?php echo $dokter['nama_dokter'] ?>">
			</div>
		  </div>

		  <div class="control-group">
			<label class="control-label" for="inputSip">Nomor SIP</label>
			<div class="controls">
				<select name="initial" id="initial" class="input-small">
					<option value="0">Tn.</option>
					<option value="1">Ny.</option>
				</select>
			  <input type="text" id="inputSip" name="inputSip" class="span5" placeholder="Nomor  SIP">
			  <label class="checkbox inline">
				<input type="checkbox" id="member" name="member" value="1"> Internal
			  </label>
			</div>
			
		  </div>	

		<?php
				
				$date=date("d-m-Y");
				$tgl = new DateTime($dokter['tanggal_lahir']);
				
			?>
		<div class="control-group">
			<label class="control-label" for="tanggallahir">Tanggal Lahir</label>
			<div class="controls control-row">
			
				<div class="input-append date" id="dp3" data-date="<?php echo $tgl->format('d-m-Y') ?>" data-date-format="dd-mm-yyyy">
					<input class="input-small" size="16" type="text" value="<?php echo $tgl->format('d-m-Y') ?>" name="tanggallahir" id="tanggallahir">
					
					<span class="add-on"><i class="icon-calendar"></i></span>
				</div>
				<input type="hidden" value="<?php echo $date; ?>" name="now" id="now">
				<div class="input-prepend input-append">
				  <span class="add-on">Umur </span>
				  <input type="text" class="span1" id="umur" name="umur" value="<?php echo $dokter['umur'] ?>" readonly>
				  <span class="add-on">Tahun </span>
				  <input type="text" class="span1" id="umur_bulan" name="umur_bulan" value="<?php echo $dokter['umur_bulan'] ?>" readonly>
				  <span class="add-on">Bulan </span>
				  <input type="text" class="span1" id="umur_hari" name="umur_hari" value="<?php echo $dokter['umur_hari'] ?>" readonly>
				  <span class="add-on">Hari </span>
				</div>
				
				
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="jeniskelamin">Jenis Kelamin</label>
			<div class="controls control-row">
					<select name="jeniskelamin" id="jeniskelamin" class="input-medium">
					  <option value="0">Laki-laki</option>
					  <option value="1">Perempuan</option>
					</select>
				
			</div>
		  </div>
		  
		  <div class="control-group">
			
			<label class="control-label" for="alamat">Alamat</label>
			<div class="controls">
			<textarea name="alamat" id="alamat" rows="4" class="span6"><?php echo $dokter['alamat'] ?></textarea> 
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputKota">Kota</label>
			<div class="controls control-row">
			  <input type="text" id="inputKota" name="inputKota" class="input-xlarge" placeholder="Kota" value="<?php echo $dokter['kota'] ?>">
			  <div class="input-prepend">
					<span class="add-on">Kode Pos </span>
					<input type="text" id="inputKodepos" name="inputKodepos" class="span2" placeholder="Kode Pos" value="<?php echo $dokter['kodepos'] ?>" >
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputEmail">Email</label>
			<div class="controls control-row">
			  <input type="text" id="inputEmail" name="inputEmail" class="input-large" placeholder="Email" value="<?php echo $dokter['email'] ?>" >
			  <div class="input-prepend">
					<span class="add-on">Pekerjaan </span>
					<input type="text" id="pekerjaan" name="pekerjaan" class="span3" placeholder="Pekerjaan" value="<?php echo $dokter['pekerjaan'] ?>" >
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputTelepon">Telepon</label>
			<div class="controls control-row">
			  <input type="text" id="inputTelepon" name="inputTelepon" class="input-large" placeholder="Telepon" value="<?php echo $dokter['telepon'] ?>" >
			  <div class="input-prepend">
					<span class="add-on">Hp / Flexi </span>
					<input type="text" id="hp" name="hp" class="span3" placeholder="Nomor HP" value="<?php echo $dokter['hp'] ?>" >
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputPerusahaan">Perusahaan</label>
			<div class="controls control-row">
			  <input type="text" id="inputPerusahaan" name="inputPerusahaan" class="span6" placeholder="Perusahaan" value="<?php echo $dokter['perusahaan'] ?>">
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="keterangan">Keterangan</label>
			<div class="controls control-row">
			  <input type="text" id="keterangan" name="keterangan" class="span6" placeholder="Keterangan" value="<?php echo $dokter['keterangan'] ?>" >
			</div>
		  </div>
		  
		  <div class="control-group">
			<div class="controls">
			  <button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Simpan</button>
			</div>
		  </div>
		</form>
   
</div>

<script type="text/javascript">
function dstrToUTC(ds) {
    var dsarr = ds.split("-");
     var dd = parseInt(dsarr[0],10);
     var mm = parseInt(dsarr[1],10);
     var yy = parseInt(dsarr[2],10);
     return Date.UTC(yy,mm-1,dd,0,0,0); 
	}
function datediff(ds1,ds2) 
	{
     var d1 = dstrToUTC(ds1);
     var d2 = dstrToUTC(ds2);
     var oneday = 86400000;
     return (d2-d1) / oneday;    
	}
function humanise (diff) {
  // The string we're working with to create the representation
  var str = '';
  // Map lengths of `diff` to different time periods
  var values = {
    'umur': 365, 
    'umur_bulan': 30, 
    'umur_hari': 1
  };

  // Iterate over the values...
  for (var x in values) {
    var amount = Math.floor(diff / values[x]);

    // ... and find the largest time value that fits into the diff
    if (amount >= 1) {
       // If we match, add to the string ('s' is for pluralization)
       str += amount + x + (amount > 1 ? 's' : '') + ' ';
	   $('#'+x).val(amount);
	   

       // and subtract from the diff
       diff -= amount * values[x];
    }
	else
	{
		$('#'+x).val(0);
	}
  }

  return str;
}
	
$(document).ready(function(){
$('#dp3').datepicker()
		  .on('changeDate', function(ev){
			var tanggallahir=$('#tanggallahir').val();
			var now=$('#now').val();
			
			var diff=datediff(tanggallahir,now);
			humanise(diff);
		  });
});

$('#tanggallahir').keyup(function() {
			var tanggallahir=$('#tanggallahir').val();
			var now=$('#now').val();
			
			var diff=datediff(tanggallahir,now);
			humanise(diff);
});

$( "#initial" )
  .change(function () {
    var str = 0;
    $( "#initial option:selected" ).each(function() {
      str = $( this ).val();
    });
    if(str==0)
	{
		$("#jeniskelamin").val(0);
	}
	else if(str==1)
	{
		$("#jeniskelamin").val(1);
	}
  })
  .change();


</script>