<div class="container">
   
      <h2>Dokter</h2>
	  <h2>
	  <a class="btn btn-primary btn-large" href="<?php echo site_url("dokter/dokter_baru"); ?>">Daftarkan Dokter Baru</a>
	  </h2>
		<?php if($status==1) 
		{
		?>
		<div class="alert alert-success">
		Data Berhasil Dimasukkan!!
		</div>
		<?php } ?>
		
		<?php if($status==2) 
		{
		?>
		<div class="alert alert-info">
		Data Berhasil Diupdate!!
		</div>
		<?php } ?>
		
		<?php if($status==3) 
		{
		?>
		<div class="alert alert-danger">
		Data Berhasil Dihapus!!
		</div>
		<?php } ?>
		
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
			<thead>
				<tr>
					<th>Nama Dokter</th>
					<th>Nomor SIP</th>
					<th>Alamat</th>
					<th>Nomor HP</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ($dokter as $dokter_item): ?>
			
			<tr class="odd gradeX">
					
					<td><?php echo $dokter_item['nama_dokter'] ?></td>
					<td><?php echo $dokter_item['nomor_sip'] ?></td>
					<td><?php echo $dokter_item['alamat'] ?></td>
					<td><?php echo $dokter_item['hp'] ?></td>
					<td class="center"> <a class="btn btn-primary" href="<?php echo site_url("dokter/edit_dokter/".$dokter_item['id_dokter']); ?>">Edit</a> </td>
					<td class="center"><a class="btn btn-danger" href="<?php echo site_url("dokter/delete_dokter/".$dokter_item['id_dokter']); ?>">Delete</a></td>
				</tr>
		
			<?php 
			
			endforeach ?>
		
				
				
			</tbody>
		</table>
      
   
</div>
