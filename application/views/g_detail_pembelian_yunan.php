<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
		
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">DETAIL PEMBELIAN</legend>

			<?php
			$tanggal =date('j-m-Y', strtotime($pembelian['tgl_faktur']));
			$jam =date('G:i:s', strtotime($pembelian['tgl_faktur']));
			$date=date("d-m-Y");
			?>
			
		
			<div class="row">
			<div class="col-md-12">
				<div class="col-md-3">
					<label class="control-label" for="inputTanggal">Tanggal</label>
					<div class="input-append date" id="dp3" data-date="<?php echo $date; ?>" data-date-format="dd-mm-yyyy">
						<input type="text" value="<?php echo $tanggal; ?>" name="tanggalawal" id="tanggalawal" readonly>
					</div>	
				</div>	
				<div class="col-md-3">
						<label class="control-label" for="inputReg">No. Faktur</label>
						<input type="text" id="inputReg" name="inputReg" value="<?php echo $pembelian['nomor_faktur']; ?>" readonly>
				</div>
				<div class="col-md-3">
						<label class="control-label" for="inputReg">No. Delivery</label>
						<input type="text" id="no_delivery" name="no_delivery" value="<?php echo $pembelian['no_delivery']; ?>" readonly>
				</div>		
				<div class="col-md-3">
					<label class="control-label" for="inputID">Supplier</label>
			  
					<input type="text" id="referensi" name="referensi" placeholder="Referensi" autocomplete="off" value="<?php echo $pembelian['nama_supplier']; ?>" readonly>
				</div>	
			
			</div>
			</div>
			
		  <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
		  
		  
			
			<div class="row">
			<div class="col-md-12">
			<div class="alert alert-info">
				<div class="container-fluid" style="margin-top:-15px; ">
				<div class="row-fluid">
			
              <div class="col-md-12">
				  
					   <div class="row">
					   <div class="col-md-12">
					   <div class="col-md-12">
					   <div id="tabel_layanan"></div>
					   </div>
					   </div>
					   </div>
					   
					   <div class="row">
							<div class="col-md-12">
								<div class="col-md-4">
									<label class="control-label" for="totalharga">Total</label>
									<input type="text" id="totalharga" name="totalharga" readonly value="<?php echo formatRupiah($pembelian['subtotal']); ?>">
									<input type="hidden" id="totalharga_hidden" name="totalharga_hidden" readonly value="<?php echo $pembelian['subtotal']; ?>">
								</div>	
								<div class="col-md-4">
									<label class="control-label" for="totalharga">Diskon (%)</label>
									<div class="input-group margin-bottom-sm">
										<input type="text" id="totaldiskon" name="totaldiskon" autocomplete="off"  value="<?php echo $pembelian["diskon"]; ?>" readonly>
										<span class="input-group-addon">%</span>
									</div>
								</div>	
								<div class="col-md-4">
									<label class="control-label" for="totalharga">Nominal Diskon (Rp)</label>
									<div class="input-group margin-bottom-sm">
										<span class="input-group-addon">Rp</span>
										<input type="text" id="totalnominaldiskon" name="totalnominaldiskon" autocomplete="off" value="<?php echo formatRupiah($pembelian["nominal_diskon"]); ?>" readonly>
										<input type="hidden" id="totalnominaldiskon_hidden" name="totalnominaldiskon_hidden" autocomplete="off" value="<?php echo $pembelian["nominal_diskon"]; ?>" readonly>
									</div>
								</div>	
							</div>	
						 </div>
						</br>
						 <div class="row">
						 	<div class="col-md-12">
								<div class="col-md-4">
									<label class="control-label" for="totalharga">Total Setelah Didiskon</label>
									<input type="text" id="total_diskon" name="total_diskon" readonly value="0">
								</div>	
								<div class="col-md-4">
									<label class="control-label" for="totalharga">PPN (%)</label>
									<div class="input-group margin-bottom-sm">
										<input type="text" id="totaldiskon" name="totaldiskon" autocomplete="off"  value="<?php echo $pembelian["ppn"]; ?>" readonly>
										<span class="input-group-addon">%</span>
									</div>
								</div>	
								<div class="col-md-4">
									<label class="control-label" for="totalharga">Nominal PPN (Rp)</label>
									<div class="input-group margin-bottom-sm">
										<span class="input-group-addon">Rp</span>
										<input type="text" id="totalnominaldiskon" name="totalnominaldiskon" autocomplete="off" value="<?php echo formatRupiah($pembelian["nominal_ppn"]); ?>" readonly>
									</div>
								</div>	
							</div>	
						 </div>
						 <br>
						 <div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<!--
									<table class="table table-bordered table-condensed " >
										<thead>
										<tr style="background-color: #90CA77; color:#fff; font-size:14px;">
										  <th>Tanggal</th>
										  <th>Cara Bayar</th>
										  <th>Pembayaran</th>
										</tr>
									  </thead>
										<tbody style="font-size:14px;">
										<?php
										$i=0;
										$total_bayar=0;
											foreach ($pembayaran as $pembayaran_item): 
											$tanggal_bayar =date('j-m-Y', strtotime($pembayaran_item['tanggal']));
											$jam_bayar =date('G:i', strtotime($pembayaran_item['tanggal']));
											$total_bayar=$total_bayar+$pembayaran_item['jumlah'];
											?>
											<tr style="background-color: #fff;">
											  <td><?php echo $tanggal_bayar.' '.$jam_bayar; ?></td>
											  <td><?php echo $pembayaran_item['namacarabayar']; ?></td>
											  <td style="text-align:right; "><p class="text-right"><?php echo formatRupiah($pembayaran_item['jumlah']); ?></p> </td>
											</tr>
										<?php 
										endforeach ?>	
											<tr style="">
											  <td colspan="1" style="text-align:center; color:#CC0000; font-weight:bold; font-size:16px;">Total Pembayaran</td>
											  <td colspan="4" style="text-align:right;  font-size:20px; color:#CC0000; font-weight:bold;"><p class="text-right"><?php echo formatRupiah($total_bayar); ?></p></td>
											  <td></td>
											  
											</tr>
										  </tbody>
										
									</table>-->
								</div>	
							
							</div>	
						 </div>
					   
					   <div class="row">
							<div class="col-md-12">
							<div class="col-md-6">
							<label class="control-label" for="totalharganetto">Harga Netto</label>
							<p id="labelnetto" style="color:#CC0000; font-size:32px; font-weight:bold;"> <?php echo formatRupiah($pembelian['total']); ?> </p>
								
							</div>	
							
							<div class="col-md-6" >
								<label class="control-label" for="totalharganetto">Kekurangan Pembayaran</label>
									
											<p id="labelsisa" style="color:#CC0000; font-size:32px; font-weight:bold;"><?php echo formatRupiah($pembelian['kurangbayar']); ?> </p>
									
							</div>
							
							
							</div>	
						 </div>
						 
					  <div class="row">
						<div class="col-md-12">
						<div class="col-md-8">
						  &nbsp;
						</div>
						<div class="col-md-1">
						  <a href="<?php echo site_url('g_pembelian/view_detail2/'.$pembelian['id_pembelian']); ?>" class="btn btn-large btn-success "><i class="fa fa-print fa-lg "></i> Print</a>
						</div>
						<div class="col-md-3">
						  <a href="<?php echo site_url('g_pembelian/create_pembelian'); ?>" class="btn btn-large btn-info "><i class="fa fa-chevron-left "></i> Kembali ke Pembelian</a>
						</div>
						</div>
					  </div>
					
				</div>
            </div>
			</div>
			</div>
			</div>
			
		  </div>
		  </div>
		  
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>

<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/transition.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/modal.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>

<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>
	
<script type="text/javascript">

function hitung_total_diskon()
{
	var totalharga=$("#totalharga_hidden").val();
	var totalnominaldiskon=$("#totalnominaldiskon_hidden").val();
	console.log(totalharga);
	console.log(totalnominaldiskon);
	var total_diskon=totalharga-totalnominaldiskon;
	$("#total_diskon").val(formatRupiah(total_diskon));
}

function formatRupiah(nilaiUang2)
{
	var nilaiUang=nilaiUang2+"";
	var nilaiRupiah 	= "";
	var jumlahAngka 	= nilaiUang.length;

	while(jumlahAngka > 3)
	{

	sisaNilai = jumlahAngka-3;
	nilaiRupiah = "."+nilaiUang.substr(sisaNilai,3)+""+nilaiRupiah;

	nilaiUang = nilaiUang.substr(0,sisaNilai)+"";
	jumlahAngka = nilaiUang.length;
	}

	nilaiRupiah = nilaiUang+""+nilaiRupiah+",-";
	return nilaiRupiah;
}

var controller = 'g_pembelian';
var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';

$(document).ready(function()
{
	var reg='<?php echo $pembelian["id_pembelian"]; ?>';
	$.ajax({
		'url' : base_url + controller + '/show_detail_item',
		'type' : 'POST', //the way you want to send data to your URL
		'data' : {'reg' : reg},
		'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
			var container = $('#tabel_layanan'); //jquery selector (get element by id)
			hitung_total_diskon();
			if(data){
				container.html(data);
			}
		}
	});
});


</script>


</body></html>