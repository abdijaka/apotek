<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">PEMBELIAN ITEM</legend>

		<form class="form-horizontal" id="form1" action="<?php echo site_url("g_pembelian/insert_pembelian"); ?>" method="POST">
			<?php
			$date=date("d-m-Y");
			?>
			
		
			<div class="row">
			<div class="col-md-12">
				<div class="col-md-2">
					<label class="control-label" for="inputTanggal">Tanggal Faktur</label>
						<input type="text" value="<?php echo $date; ?>" name="tanggalfaktur" id="tanggalfaktur" data-date-format="dd-mm-yyyy">
				</div>	
				<div class="col-md-3">
						<label class="control-label" for="inputReg">No. Faktur</label>
						<input type="text" id="nomorfaktur" name="nomorfaktur" autocomplete="off">
						<div id="peringatan" value="0"></div>
				</div>		
				<div class="col-md-2">
					<label class="control-label" for="inputJatuhTempo">Jatuh Tempo</label>
					<input type="text" value="<?php echo date('d-m-Y'); ?>" name="inputJatuhTempo" id="inputJatuhTempo" data-date-format="dd-mm-yyyy">
				</div>
				<div class="col-md-3">
					<label class="control-label" for="inputID">Supplier</label>
			  
						<select name="supplier" id="supplier" class="input-large">
							  <?php foreach ($supplier as $supplier_item): ?>
							  <option <?php if($idsup==$supplier_item['id_supplier']) echo "selected"; ?> value="<?php echo $supplier_item['id_supplier']; ?>"><?php echo $supplier_item['nama_supplier']; ?></option>
							  <?php endforeach ?>
						</select>
				</div>	
				<div class="col-md-1">
					<label class="control-label" for="pasien_baru">&nbsp;</label>
					<br>
					<a style="vertical-align:bottom;" type="button" href="<?php echo site_url('g_supplier/supplier_baru/1'); ?>" class="btn btn-info">Suppl Baru</a>
				</div>	
			
			</div>
			</div>
			
		  <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
		  
		  
			
			<div class="row">
			<div class="col-md-12">
			<div class="alert alert-info">
				<div class="container-fluid" style="margin-top:-15px; ">
				<div class="row-fluid">
			
              <div class="col-md-4" style="background-color:#90CA77; padding-bottom:20px; padding-top:20px;">
			  
			  <div class="row">
				<div class="col-md-13">
				<div class="col-md-12">
				<label class="control-label" style="color:#fff;" for="pemeriksaan">Nama Item</label>
			
				<input type="hidden" id="id_item" name="id_item" >
				<input type="text" id="namaitem" name="namaitem" placeholder="Nama Item" autocomplete="off">
			  
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-5">
				<label class="control-label" for="diskon" style="color:#fff;">Jumlah</label>
				<div class="input-group margin-bottom-sm">
				<input type="number" id="jumlah" name="jumlah" value="0">
				
				</div>
				
				</div>	
				
				
				<div class="col-md-7">
				<label class="control-label" for="diskon" style="color:#fff;">Satuan</label>
				<div class="input-group margin-bottom-sm">
				<input type="text" id="satuan" name="satuan" >
				</div>
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-6">
				<label class="control-label" for="harga" style="color:#fff;">Harga Satuan</label>
				<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">Rp</span>
				<input type="text" id="harga" name="harga"  autocomplete="off" value="0">
				</div>
				<div id="label_harga_satuan" class="alert-danger"></div>
				</div>	
				
				<div class="col-md-6">
				<label class="control-label" for="harga" style="color:#fff;">SubTotal</label>
				<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">Rp</span>
				<input type="hidden" id="harga_subtotal" name="harga_subtotal"  autocomplete="off" value="0" readonly>
				<input type="text" id="label_harga_subtotal" name="label_harga_subtotal"  autocomplete="off" value="0" readonly>
				</div>
			  
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-5">
				<label class="control-label" for="diskon" style="color:#fff;">Diskon</label>
				<div class="input-group margin-bottom-sm">
				<input type="text" id="diskon" name="diskon" autocomplete="off" value="0">
				<span class="input-group-addon">%</span>
				</div>
				
				</div>	
				
				
				<div class="col-md-7">
				<label class="control-label" for="diskon">&nbsp;</label>
				<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">Rp</span>
				<input type="text" id="nominaldiskon" name="nominaldiskon"  autocomplete="off" value="0">
				</div>
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-12">
				<label class="control-label" for="harganetto" style="color:#fff;">Netto</label>
				<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">Rp</span>
				<input type="hidden" id="harganetto" name="harganetto"  autocomplete="off">
				<input type="text" id="label_harganetto" name="label_harganetto"  autocomplete="off" readonly>
				</div>
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-6">
				<label class="control-label" for="diskon" style="color:#fff;">Tanggal Masuk</label>
					<input type="text" value="<?php echo $date; ?>" name="tanggalmasuk" id="tanggalmasuk" data-date-format="dd-mm-yyyy">
				</div>	
				
				
				<div class="col-md-6">
				<label class="control-label" for="diskon" style="color:#fff;">Expired Date</label>
					<input type="text" value="<?php echo $date; ?>" name="expdate" id="expdate" data-date-format="dd-mm-yyyy">
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-12">
				<br>
				<button type="button" onclick="load_data_ajax(); return false;" id="tambah_layanan" class="btn btn-danger">Tambahkan <i class="fa fa-chevron-right"></i></button>
			  
				</div>	
				</div>	
			 </div>	
			
		</div>
            <div class="col-md-8">
				  
					
					   <div class="row">
					   <div class="col-md-12">
					   <div class="col-md-12">
					   <div id="tabel_layanan"></div>
					   </div>
					   </div>
					   </div>
					   
					   <div class="row">
							<div class="col-md-12">
							<div class="col-md-4">
							<label class="control-label" for="totalharga">Sub Total</label>
							<div class="input-group margin-bottom-sm">
							<span class="input-group-addon">Rp</span>
							<input type="hidden" id="totalharga" name="totalharga" class="input-small" readonly value="0">
							<input type="text" id="label_totalharga" name="label_totalharga" class="input-small" readonly value="0">
							</div>
							</div>	
							
							<div class="col-md-4">
							<label class="control-label" for="totalharga">PPN (%)</label>
							<div class="input-group margin-bottom-sm">
							<input type="text" id="totalppn" name="totalppn"  value="10" autocomplete="off" readonly>
							<span class="input-group-addon">%</span>
							</div>
						  
							</div>	
							
							<div class="col-md-4">
							<label class="control-label" for="totalharga">Nominal PPN (Rp)</label>
							<div class="input-group margin-bottom-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="totalnominalppn" name="totalnominalppn"  class="input-small" value="0" autocomplete="off">
							</div>
						  
							</div>	
							</div>	
						 </div>
						 
						 <div class="row">
							<div class="col-md-12">
							<div class="col-md-12">
							<label class="control-label" for="totalharganetto">Harga Netto</label>
							<p id="labelnetto" style="color:#CC0000; font-size:32px; font-weight:bold;"> Rp 0 </p>
								<input type="hidden" id="totalharganetto" name="totalharganetto"  class="input-large" value="0" >
						  
							</div>	
							
							</div>	
						 </div>
						 
						 <div class="row">
						 <div class="col-md-12">
						 <div class="col-md-12">
						 <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
						 </div>
						 </div>
						 </div>
					  <div class="row" >
							<div class="col-md-12" style="z-index:1;">
							<div  class="col-md-4">
							<label class="control-label" for="totalharga">Cara Bayar</label>
							
							<select name="carabayar" id="carabayar" >
							<?php foreach ($carabayar as $carabayar_item): ?>
								<option value="<?php echo $carabayar_item['id'] ?>"><?php echo $carabayar_item['nama'] ?></option>
							<?php endforeach; ?>
							  
							</select>
						  
							</div>	
							
							<div class="col-md-4">
							<label class="control-label" for="totalharga">Bayar (Rp)</label>
							<div class="input-group margin-bottom-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="jumlahbayar" name="jumlahbayar"  value="0" autocomplete="off" >
							</div>
							<div id="label_bayar" class="alert-danger" ></div>
							</div>	
							
							<div class="col-md-4">
							<label class="control-label" for="totalharga">Kurang (Rp)</label>
							<div class="input-group margin-bottom-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="sisa" name="sisa"  value="0"  readonly>
							</div>
						  
							</div>	
							</div>	
						 </div>
						 
					  <div class="row">
						<div class="col-md-12">
						<div class="col-md-12">
						  <label class="control-label" for="totalharga">&nbsp;</label><br>
						  <button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Simpan Pembelian</button>
						</div>
						</div>
					  </div>
					
				</div>
            </div>
			</div>
			</div>
			</div>
			
		  </div>
		  </div>
		  
		  
		</form>
   		
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>



<!-- form register -->
<form action="https://apps.ptiik.ub.ac.id/apps/registrasi" id="form-save-user" class="form-horizontal" role="form" method="POST">
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content modal-content-no-radius">
      <div class="modal-header modal-header-blue">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title modal-title-center" id="registerModalLabel">Register</h4>
      </div>
      <div class="modal-body">
      <div class="alert alert-block alert-info"><p>
      Masukkan alamat e-mail aktif anda yang terdaftar pada biodata kepegawaian. Lakukan aktifasi melalui link yang kami kirimkan ke alamat e-mail tersebut.
      </p></div>
        <div class="form-group">
        	<label for="inputEmail1" class="col-lg-2 control-label">Email</label>
        	<div class="col-lg-10">
          	<input type="email" name="email" class="form-control" id="inputEmail1" placeholder="Email">
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="b_reg" class="btn btn-content-ready btn-no-radius">Register</button>
      </div>
    </div>
  </div>
</div>
</form>




<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>
<script type="text/javascript">

function hitung_netto()
{
	var harga=$("#harga_subtotal").val();
	var nominal_diskon=$("#nominaldiskon").val();
	var netto=harga-nominal_diskon;
	$("#harganetto").val(Math.floor(netto));
	$("#label_harganetto").val(formatRupiah(Math.floor(netto)));
	label_harga_satuan();
}

function hitung_harga_subtotal()
{
	var harga=$("#harga").val();
	var jumlah=$("#jumlah").val();
	var subtotal=harga*jumlah;
	$("#harga_subtotal").val(subtotal);
	$("#label_harga_subtotal").val(formatRupiah(subtotal));
	hitung_netto();
}

function hitung_nominal_diskon()
{
	var subtotal=$("#harga_subtotal").val();
	var diskon=$("#diskon").val();
	var nominaldiskon=subtotal*diskon/100;
	$("#nominaldiskon").val(Math.floor(nominaldiskon));
	hitung_netto();
}

function hitung_persen_diskon()
{
	var subtotal=$("#harga_subtotal").val();
	var nominaldiskon=$("#nominaldiskon").val();
	var diskon=nominaldiskon*100/subtotal;
	$("#diskon").val(Math.ceil(diskon));
	hitung_netto();
}

function hitung_total_netto() 
	{
     var harga=$('#totalharga').val();
	 var nominaldiskon=$('#totalnominalppn').val();
     
	 var netto=harga*1+nominaldiskon*1;
     $("#totalharganetto").val(Math.floor(netto));
	 $("#labelnetto").html('Rp '+formatRupiah(Math.floor(netto)));
	 $("#jumlahbayar").val(Math.floor(netto));
	 hitung_kurang_bayar();
	
	}

function hitung_total_persen_diskon() 
	{
     var harga=$('#totalharga').val();
	 var nominaldiskon=$('#totalnominalppn').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#totalppn").val(Math.ceil(diskon));
	 hitung_total_netto();
	}

function hitung_total_nominal_diskon() 
	{
     var harga=$('#totalharga').val();
	 var diskon=$('#totalppn').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#totalnominalppn").val(Math.floor(nominaldiskon));
	 hitung_total_netto();
	}

function hitung_kurang_bayar() 
	{
     var totalharganetto=$('#totalharganetto').val();
	 var jumlahbayar=$('#jumlahbayar').val();
     
	 var kurang=totalharganetto-jumlahbayar;
     $("#sisa").val(formatRupiah(Math.floor(kurang)));
	 label_bayar();
	}

function label_bayar() 
	{
	 var jumlahbayar=$('#jumlahbayar').val();
     $("#label_bayar").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp'+formatRupiah(Math.floor(jumlahbayar)));
	}
function label_harga_satuan() 
	{
	 var hargasatuan=$('#harga').val();
     $("#label_harga_satuan").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp'+formatRupiah(Math.floor(hargasatuan)));
	}



$(document).ready(function(){

$( "#form1" ).submit(function( event ) {
  
  var nomorfaktur=$('#nomorfaktur').val();
  var tanggalfaktur=$('#tanggalfaktur').val();
  var unik=$('#peringatan').val();
  
	if(nomorfaktur==0 || nomorfaktur=="" || nomorfaktur=="0")
	{
		alert("Nomor Faktur Belum Terisi");
		event.preventDefault();
	}
	else if(tanggalfaktur==0 || tanggalfaktur=="" || tanggalfaktur=="0")
	{
		alert("Tanggal Belum terisi");
		event.preventDefault();
	}
	else if(unik==1 || unik=="1")
	{
		alert("Nomor Faktur ini Sudah Pernah Didaftarkan!!");
		event.preventDefault();
	}
	
});

var data3;

$('#namaitem').keyup(function(){
	if($(this).val()!="")
	{
		$.ajax({
				type: "POST",
				url: "<?php echo base_url('g_penjualan/ajaxTypeheadItem') ;?>",
				data: { src_param: $(this).val() },
				cache: true,
				success:
					function(result)
					{
						data3 = JSON.parse(result);
					}
				});
	}
});

$('#namaitem').typeahead({
    source: function (query, process) {
        states4 = [];
		map4 = {};
		
		var source = [];
		$.each(data3, function (i, state) {
			map4[state.stateName] = state;
			states4.push(state.stateName);
		});
	 
		process(states4);
		
    },
    updater: function (item) {
        
		selectedState = map4[item].stateCode;
		selectedState2 = map4[item].stateDisplay;
		harga = map4[item].harga;
		satuan = map4[item].satuanbeli;
		
		$("#id_item").val(selectedState);
		$("#harga").val(harga);
		$("#satuan").val(satuan);
		hitung_harga_subtotal();
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#jumlah').keyup(function() {
	hitung_harga_subtotal();
});

$('#harga').keyup(function() {
	hitung_harga_subtotal();
});

$('#diskon').keyup(function() {
	hitung_nominal_diskon();
});

$('#nominaldiskon').keyup(function() {
	hitung_persen_diskon();
});

$('#totalppn').keyup(function() {
	hitung_total_nominal_diskon();
});

$('#totalnominalppn').keyup(function() {
	hitung_total_persen_diskon();
});

$('#jumlahbayar').keyup(function() {
	hitung_kurang_bayar();
});

$('#nomorfaktur').keyup(function() {
			var reg=$('#nomorfaktur').val();
			
			var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
			$.ajax({
				'url' : base_url + '/g_pembelian/checkfaktur',
				'type' : 'POST', //the way you want to send data to your URL
				'data' : {'reg' : reg},
				'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					
					if(data>0){
						$('#peringatan').addClass("alert-error");
						$("#peringatan").removeClass("alert-info");
						$("#peringatan").html("Nomor Faktur ini sudah didaftarkan");
						$("#peringatan").val(1);
						
						$('#nomorfaktur').focus();
						
					}
					else
					{
						$('#peringatan').addClass("alert-info");
						$("#peringatan").removeClass("alert-error");
						$("#peringatan").html("");
						$("#peringatan").val(0);
					}
				}
			});
});


$('#tanggalfaktur').datepicker()
		  .on('changeDate', function(ev){
			
		  });
$('#inputJatuhTempo').datepicker()
		  .on('changeDate', function(ev){
			
		  });
$('#tanggalmasuk').datepicker()
		  .on('changeDate', function(ev){
			
		  });
$('#expdate').datepicker()
		  .on('changeDate', function(ev){
			
		  });
		  

});

function formatRupiah(nilaiUang2)
{
	var nilaiUang=nilaiUang2+"";
  var nilaiRupiah 	= "";
  var jumlahAngka 	= nilaiUang.length;
  
  while(jumlahAngka > 3)
  {
	
	sisaNilai = jumlahAngka-3;
    nilaiRupiah = "."+nilaiUang.substr(sisaNilai,3)+""+nilaiRupiah;
    
    nilaiUang = nilaiUang.substr(0,sisaNilai)+"";
    jumlahAngka = nilaiUang.length;
  }
 
  nilaiRupiah = nilaiUang+""+nilaiRupiah+",-";
  return nilaiRupiah;
}

var controller = 'g_pembelian';
var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';

function load_data_ajax(){
var reg='0';
var id_item=$('#id_item').val();
var jumlah=$('#jumlah').val();
var harga=$('#harga').val();
var harga_subtotal=$('#harga_subtotal').val();
var diskon=$('#diskon').val();
var nominal_diskon=$('#nominaldiskon').val();
var netto=$('#harganetto').val();
var satuan=$('#satuan').val();
var tanggalmasuk=$('#tanggalmasuk').val();
var expdate=$('#expdate').val();


if (id_item==0 || id_item=="")
{
	alert('Masukkan Item');
}
else
{
	if(jumlah==0 || jumlah=="")
	{
		alert('Masukkan Jumlah');
	}
	else
	{
		$.ajax({
			'url' : base_url + '/' + controller + '/insert_item',
			'type' : 'POST', //the way you want to send data to your URL
			'data' : {'reg' : reg, 'id_item' : id_item, 'jumlah': jumlah, 'harga' : harga, 'diskon' : diskon, 'nominal_diskon' : nominal_diskon, 'netto' : netto, 'harga_subtotal' : harga_subtotal, 'satuan' : satuan, 'tanggalmasuk' : tanggalmasuk, 'expdate' : expdate},
			'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
				var container = $('#tabel_layanan'); //jquery selector (get element by id)
				if(data){
					container.html(data);
					var temp = $('#temptotalharga').val();
					$('#totalharga').val(temp);
					$('#label_totalharga').val(formatRupiah(temp));
					hitung_total_nominal_diskon();
					hitung_total_netto();
					
					$('#id_item').val(0);
					$('#jumlah').val(0);
					$('#harga').val("0");
					$('#harga_subtotal').val("0");
					$('#label_harga_subtotal').val("0");
					$('#diskon').val("0");
					$('#nominaldiskon').val("0");
					$('#harganetto').val("0");
					$('#label_harganetto').val("0");
					$('#namaitem').val("");
					$('#satuan').val("");
				}
			}
		});
	}
}
}

function supplierbaru(){
	window.location.href = base_url + '/g_supplier/supplier_baru/1';
}

function delete_layanan(reg, netto, pembelian){

	$.ajax({
		'url' : base_url + '/' + controller + '/delete_item',
		'type' : 'POST', //the way you want to send data to your URL
		'data' : {'reg' : reg, 'netto' : netto, 'pembelian':pembelian},
		'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
			var container = $('#tabel_layanan'); //jquery selector (get element by id)
			if(data){
				container.html(data);
				var temp = $('#temptotalharga').val();
				$('#totalharga').val(temp);
				$('#label_totalharga').val(formatRupiah(temp));
				hitung_total_nominal_diskon();
				hitung_total_netto();
				
			}
		}
	});
}



</script>
</body></html>