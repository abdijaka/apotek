<?php 
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

function get_month($month)
{
	$labelmonth="Januari";

	if($month==2)
	$labelmonth="Februari";
	else if($month==3)
	$labelmonth="Maret";
	else if($month==4)
	$labelmonth="April";
	else if($month==5)
	$labelmonth="Mei";
	else if($month==6)
	$labelmonth="Juni";
	else if($month==7)
	$labelmonth="Juli";
	else if($month==8)
	$labelmonth="Agustus";
	else if($month==9)
	$labelmonth="September";
	else if($month==10)
	$labelmonth="Oktober";
	else if($month==11)
	$labelmonth="November";
	else if($month==12)
	$labelmonth="Desember";

	return $labelmonth;

}

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once 'PHPExcel/Classes/PHPExcel.php';

require_once 'PHPExcel/Classes/PHPExcel/IOFactory.php';

$fileName = 'PHPExcel/Examples/templates/templatependapatanharian.xlsx';
try {
    $objPHPExcel = PHPExcel_IOFactory::load($fileName);
} catch (Exception $e) {
    die("Error loading file: ".$e->getMessage()."<br />\n");
}

$objPHPExcel->getActiveSheet()->setCellValue('A3', "Tanggal : ".$mulai."");
foreach ($dokter as $dokter_item):
if($id_dokter==$dokter_item['id_dokter'])
{
	$objPHPExcel->getActiveSheet()->setCellValue('A4', "Dokter : ".$dokter_item['nama_dokter']);
	$nama_dokter=$dokter_item['nama_dokter'];
}

endforeach;

$nomor=0;
$total_fee=0;
$baseRow = 8;
foreach ($laporan as $laporan_item):
if($laporan_item['fee1']!=0)
{
$row = $baseRow + $nomor;
$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);

$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $nomor+1)
							  ->setCellValue('B'.$row, $laporan_item['tanggal'])
							  ->setCellValue('C'.$row, $laporan_item['nama_pasien'])
							  ->setCellValue('D'.$row, $laporan_item['nama_layanan'])
							  ->setCellValue('E'.$row, $laporan_item['operator'])
							  ->setCellValue('F'.$row, formatRupiah($laporan_item['netto']))
							  ->setCellValue('G'.$row, formatRupiah($laporan_item['fee1']));

$nomor++;
$total_fee=$total_fee+$laporan_item['fee1'];
}
endforeach;

$objPHPExcel->getActiveSheet()->setCellValue('G'.($baseRow+$nomor+1), formatRupiah($total_fee));

$baseRow = $nomor+14;
$nomor=0;
$total_fee_operator=0;

foreach ($laporan2 as $laporan_item):

$row = $baseRow + $nomor;
$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);

$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $nomor+1)
							  ->setCellValue('B'.$row, $laporan_item['tanggal'])
							  ->setCellValue('C'.$row, $laporan_item['nama_pasien'])
							  ->setCellValue('D'.$row, $laporan_item['nama_layanan'])
							  ->setCellValue('E'.$row, $laporan_item['pengirim'])
							  ->setCellValue('F'.$row, formatRupiah($laporan_item['netto']))
							  ->setCellValue('G'.$row, formatRupiah($laporan_item['fee2']));

$nomor++;
$total_fee_operator=$total_fee_operator+$laporan_item['fee2'];

endforeach;

$objPHPExcel->getActiveSheet()->setCellValue('G'.($baseRow+$nomor+1), formatRupiah($total_fee_operator));

$objPHPExcel->getActiveSheet()->setCellValue('G'.($baseRow+$nomor+3), formatRupiah($total_fee+$total_fee_operator));

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Fee Dokter Harian');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Fee_'.$nama_dokter.'_'.$mulai.'.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>