<div class="container">

      <h2>Laporan Bulanan</h2>
	  
	  <form id="form1" action="<?php echo site_url("laporan/laporan_dokter"); ?>" method="POST">
					
					<input class="input-small" size="16" type="text" value="<?php echo $mulai;?>" name="mulai" id="mulai" data-date-format="mm-yyyy">
					
					
					
					Sampai Bulan :
					
					<input class="input-small" size="16" type="text" value="<?php echo $sampai;?>" name="sampai" id="sampai" data-date-format="mm-yyyy">
					
		  <select name="id_dokter" id="id_dokter" class="input-medium">
			<?php
			foreach ($dokter as $dokter_item): ?>
				<option <?php if($id_dokter==$dokter_item['id_dokter']) echo 'selected'; ?> value="<?php echo $dokter_item['id_dokter']; ?>"><?php echo $dokter_item['nama_dokter']; ?></option>
			<?php 
			endforeach ?>	
			</select>
		  <button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Tampilkan</button>
		
	  </form>
	  
		<h3>Sebagai Pengirim</h3>
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" >
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Dokter</th>
					<th>Nama Layanan</th>
					<th>Nomor Registrasi</th>
					<th>Harga</th>
					<th>Diskon</th>
					<th>Total</th>
					<th>Fee</th>
				</tr>
			</thead>
			<tbody>
			<?php

			$nomor=1;
			foreach ($laporan as $laporan_item): ?>
			
			<tr class="odd gradeX">
					<td><?php echo $nomor; ?></td>
					<td><?php echo $laporan_item['nama_dokter'] ?></td>
					<td><?php echo $laporan_item['nama_layanan'] ?></td>
					<td><?php echo $laporan_item['nomor_register'] ?></td>
					<td><?php echo $laporan_item['harga'] ?></td>
					<td><?php echo $laporan_item['nominal_diskon'] ?></td>
					<td><?php echo $laporan_item['netto'] ?></td>
					<td><?php echo $laporan_item['fee1'] ?></td>
					
				</tr>
		
			<?php 
			$nomor++;
			endforeach ?>
		
				
				
			</tbody>
		</table>
		
		<h3>Sebagai Operator</h3>
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" >
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Dokter</th>
					<th>Nama Layanan</th>
					<th>Nomor Registrasi</th>
					<th>Harga</th>
					<th>Diskon</th>
					<th>Total</th>
					<th>Fee</th>
				</tr>
			</thead>
			<tbody>
			<?php

			$nomor=1;
			foreach ($laporan2 as $laporan_item): ?>
			
			<tr class="odd gradeX">
					<td><?php echo $nomor; ?></td>
					<td><?php echo $laporan_item['nama_dokter'] ?></td>
					<td><?php echo $laporan_item['nama_layanan'] ?></td>
					<td><?php echo $laporan_item['nomor_register'] ?></td>
					<td><?php echo $laporan_item['harga'] ?></td>
					<td><?php echo $laporan_item['nominal_diskon'] ?></td>
					<td><?php echo $laporan_item['netto'] ?></td>
					<td><?php echo $laporan_item['fee2'] ?></td>
					
				</tr>
		
			<?php 
			$nomor++;
			endforeach ?>
		
				
				
			</tbody>
		</table>
      
   
</div>
