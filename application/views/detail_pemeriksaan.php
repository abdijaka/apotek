<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}
?>
<br>
<table class="table table-bordered table-condensed " >
	<thead>
	<tr style="background-color: #90CA77; color:#fff; font-size:14px;">
	  <th>No. </th>
	  <th>Pemeriksaan</th>
	  <th>Dokter</th>
	  <th>Harga</th>
	  <th>Diskon</th>
	  <th>Netto</th>
	</tr>
  </thead>
	<tbody style="font-size:13px;">
	<?php
	$i=0;
	$netto=0;
		foreach ($list as $list_item): 
		$i++;
		$netto=$netto+$list_item['netto'];
		?>
		
		<tr style="background-color: #fff;">
		  <td style="text-align:center;"><?php echo $i; ?></td>
		  <td><?php echo $list_item['nama_layanan']; ?></td>
		  <td><?php echo $list_item['nama_dokter']; ?></td>
		  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($list_item['harga']); ?></p> </td>
		  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($list_item['nominal_diskon']); ?></p></td>
		  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($list_item['netto']); ?></p></td>
		  
		  
		</tr>
	<?php 		
	endforeach ?>	
		<!---
		<tr style="">
		  <td colspan="2" style="text-align:center; color:#CC0000; font-weight:bold; font-size:16px;">Sub Total</td>
		  <td colspan="4" style="text-align:right;  font-size:20px; color:#CC0000; font-weight:bold;"><p class="text-right"><?php echo formatRupiah($netto); ?></p></td>
		  <td >&nbsp;</td>
		  
		</tr>
		-->
	  </tbody>
</table>
<input type="hidden" id="temptotalharga" name="temptotalharga" class="input-small" value="<?php echo $netto; ?>" >