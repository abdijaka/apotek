<?php
require('fpdf17/mc_table.php');

function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

function get_month($month)
{
	$labelmonth="Januari";

	if($month==2)
	$labelmonth="Februari";
	else if($month==3)
	$labelmonth="Maret";
	else if($month==4)
	$labelmonth="April";
	else if($month==5)
	$labelmonth="Mei";
	else if($month==6)
	$labelmonth="Juni";
	else if($month==7)
	$labelmonth="Juli";
	else if($month==8)
	$labelmonth="Agustus";
	else if($month==9)
	$labelmonth="September";
	else if($month==10)
	$labelmonth="Oktober";
	else if($month==11)
	$labelmonth="November";
	else if($month==12)
	$labelmonth="Desember";

	return $labelmonth;

}

$pdf=new PDF_MC_Table('P','mm','A4');
$pdf->AddPage();

$pdf->SetFont('Arial','B',12);

$pdf->SetY(5);

$pdf->Cell(210, 2.7, "Laporan Pasien Bulanan", 0, 0, 'C');

$pdf->SetY(10);

$pdf->Cell(210, 2.7, "Musi Heart Clinic", 0, 0, 'C');

$pieces = explode("-", $mulai);
$pieces2 = explode("-", $sampai);

$pdf->SetY(15);

if($mulai==$sampai)
{
	$pdf->Cell(210, 2.7, "Bulan : ".get_month($pieces[0]).' '.$pieces[1]."", 0, 0, 'C');
	$nama_bulan=get_month($pieces[0]).' '.$pieces[1];
}
else
{
	$pdf->Cell(210, 2.7, "Bulan : ".get_month($pieces[0]).' '.$pieces[1]." sampai ".get_month($pieces2[0]).' '.$pieces2[1], 0, 0, 'C');
	$nama_bulan=get_month($pieces[0]).' '.$pieces[1]." sampai ".get_month($pieces2[0]).' '.$pieces2[1];
}

$pdf->SetFont('Arial','B',9);

$pdf->SetY(25);
$pdf->SetX(50);
$pdf->Write(0, "Jumlah Pasien\n");
$pdf->SetY(30);



$nomor=1;
foreach ($laporanbulananpasien as $laporan_item):
	$pdf->SetFont('Arial','B',9);
	//Table with 20 rows and 4 columns
	$pdf->SetWidths(array(50,30, 30));
	$pdf->Setx(50);
	$pdf->Row(array("Bulan", "Jumlah Total Pasien", "Jumlah Pasien Baru"));
	$pdf->SetFont('Arial','',9);
	$pdf->SetX(50);
	$pdf->Row(array(get_month($laporan_item['bulan'])." ".$laporan_item['tahun'], $laporan_item['jumlah'], $laporan_item['jumlahbaru']));
	$pdf->SetY($pdf->GetY()+5);
	$pdf->SetWidths(array(60,50));
	$pdf->SetX(50);
	$pdf->Row(array("Nama Pasien Baru", "Kota"));
	
	foreach ($data_nama_pasien[$nomor] as $nama_pasien):
		$pdf->SetX(50);
		$pdf->Row(array($nama_pasien['nama_pasien'], $nama_pasien['kota']));
	endforeach;
	
	$nomor++;
endforeach;

$pdf->SetY(($pdf->GetY())+10);
$pdf->SetFont('Arial','B',9);
//Table with 20 rows and 4 columns
$pdf->SetWidths(array(10,50,30, 30));
$pdf->SetX(50);
$pdf->Write(0, "Asal Kota Pasien\n");
$pdf->SetY(($pdf->GetY())+5);
$pdf->SetX(50);
$pdf->Row(array("No", "Nama Kota", "Jumlah Total Pasien", "Jumlah Pasien Baru"));
$pdf->SetFont('Arial','',9);

$nomor=1;
foreach ($laporankotapasien as $laporan_item):
$pdf->SetX(50);
$pdf->Row(array($nomor, $laporan_item['kota'], $laporan_item['jumlah'], $laporan_item['jumlahbaru']));
$nomor++;
endforeach;

$pdf->Output("Laporan Pasien_".$nama_bulan.".pdf", "D");
//$pdf->Output();
?>