<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}
?>
<br>
<table class="table table-bordered table-condensed " >
	<thead>
	<tr style="background-color: #90CA77; color:#fff; font-size:14px;">
	  <th>No. Faktur</th>
	  <th>Tanggal</th>
	  <th>Total</th>
	  <th>Edit</th>
	  <th>Delete</th>
	</tr>
  </thead>
	<tbody style="font-size:13px;">
	<?php
	$i=0;
	$netto=0;
		foreach ($list as $list_faktur):
		$netto=$netto+$list_faktur['total']; 
		?>
		
		<tr style="background-color: #fff;">
		  <td><?php echo $list_faktur['nomor_faktur']; ?></td>
		  <td><?php echo date('j-m-Y', strtotime($list_faktur['tgl_faktur'])); ?></td>
		  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($list_faktur['total']); ?></p></td>
		  
		  <?php
		  $temp_fun="delete_faktur('".$list_faktur['nomor_faktur']."',".$list_faktur['tgl_faktur'].",".$list_faktur['total'].")";
		  ?>
		  
		  <td><p class="text-center"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal"><i class="fa fa-edit"></i> </button></p></td>
		  <td><p class="text-center"><button type="button" onclick="<?php echo $temp_fun; ?>; return false;" class="btn btn-danger"><i class="fa fa-trash-o "></i> </button></p></td>
		  
		</tr>
		<div id="modal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<a class="close" data-dismiss="modal">×</a>
						<h3 class="text-center">Edit Pembelian</h3>
					</div>
					<div class="modal-body">
						<form class="feedback" name="feedback">
							<strong>Nomor Faktur</strong>
							<br />
							<input type="text" id="no_faktur_modal" name="no_faktur" value="<?php echo $list_faktur['nomor_faktur']; ?>" readonly>
							<input type="hidden" id="id_pembelian_modal" name="id_pembelian_modal" value="<?php echo $list_faktur['id_pembelian']; ?>" readonly>
							<br />
							<strong>Tanggal Faktur</strong>
							<br />
							<input type="text" data-date-format="dd-mm-yyyy" id="tgl_faktur_modal" name="tgl_faktur" value="<?php echo date('j-m-Y', strtotime($list_faktur['tgl_faktur'])); ?>" readonly>
							<br />
							<strong>Total</strong>
							<br />
							<input type="text" id="total_faktur_modal" name="no_faktur" value="<?php echo formatRupiah($list_faktur['total']); ?>" readonly>
							<br />
							<?php 
								if($list_faktur['sp'] == 1)
								{
									echo '<input type="checkbox" id="sp_modal" name="sp" value="1" checked>';
								}
								else
								{
									echo '<input type="checkbox" id="sp_modal" name="sp" value="1">';
								}
							?>
							<label class="control-label" for="diskon">SP</label>
							<br />
							<?php 
								if($list_faktur['sj'] == 1)
								{
									echo '<input type="checkbox" id="sj_modal" name="sp" value="1" checked>';
								}
								else
								{
									echo '<input type="checkbox" id="sj_modal" name="sp" value="1">';
								}
							?>
							<label class="control-label" for="diskon">SJ</label>
							<br />
							<?php 
								if($list_faktur['faktur_pajak'] == 1)
								{
									echo '<input type="checkbox" id="fak_pajak_modal" name="fak_pajak" value="1" checked>';
								}
								else
								{
									echo '<input type="checkbox" id="fak_pajak_modal" name="fak_pajak" value="1">';
								}
							?>
							<label class="control-label" for="diskon">Faktur pajak</label>
							<br />
							<?php 
								if($list_faktur['faktur_beli'] == 1)
								{
									echo '<input type="checkbox" id="fak_beli_modal" name="fak_beli" value="1" checked>';
								}
								else
								{
									echo '<input type="checkbox" id="fak_beli_modal" name="fak_beli" value="1">';
								}
							?>
							<label class="control-label" for="diskon">Faktur Beli</label>
							<br /><br />
							<label class="control-label" for="diskon">Keterangan</label>
							<textarea id="keterangan_modal" name="keterangan" value="<?php echo $list_faktur['keterangan']; ?>"></textarea>
							<br />
							<input class="verified" type="checkbox" id="verified_modal" name="fak_beli" value="1">
							<label class="control-label" for="diskon" style="color:#0000FF;">Verified</label>
						</form>
					</div>
					<?php
						//$destination = "edit_faktur('".$list_faktur['id_pembelian']."',".$list_faktur['sp'].",".$list_faktur['sj'].",".$list_faktur['faktur_pajak'].",".$list_faktur['faktur_beli'].",".$list_faktur['keterangan'].")";
					?>
					<div class="modal-footer">
						<button class="btn btn-success" id="submit" onclick="edit_faktur(); return false; " data-dismiss="modal" data-backdrop="false">Update</button>
						<a href="#" class="btn" data-dismiss="modal">Close</a>
					</div>
				</div>
			</div>		
		</div>
	<?php 		
	endforeach ?>	
		<!---
		<tr style="">
		  <td colspan="2" style="text-align:center; color:#CC0000; font-weight:bold; font-size:16px;">Sub Total</td>
		  <td colspan="4" style="text-align:right;  font-size:20px; color:#CC0000; font-weight:bold;"><p class="text-right"><?php echo formatRupiah($netto); ?></p></td>
		  <td >&nbsp;</td>
		  
		</tr>
		-->
	  </tbody>
</table>
<input type="hidden" id="temptotalharga" name="temptotalharga" class="input-small" value="<?php echo $netto; ?>" >
