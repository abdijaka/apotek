<div class="container">
   
      <h2>Register</h2>
      <form class="form-horizontal" id="form1" action="<?php echo site_url("transaksi/insert_transaksi"); ?>" method="POST">
		  <?php
				
				$date=date("d-m-Y");
			?>
			
		<div class="control-group">
			<label class="control-label" for="inputTanggal">Tanggal</label>
			<div class="controls control-row">
			
				<div class="input-append date" id="dp3" data-date="<?php echo $date; ?>" data-date-format="dd-mm-yyyy">
					
					<input class="span2" size="16" type="text" value="<?php echo $date; ?>" name="tanggalawal" id="tanggalawal" readonly>
					<span class="add-on"><i class="icon-calendar"></i></span>
					
					
				</div>	
				<div class="input-prepend" >
					<span class="add-on">No. Register</span>
					<input type="text" id="inputReg" name="inputReg" value="<?php echo $nomor_baru ?>">
					
				</div>
				
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputID">Nomor ID Pasien</label>
			<div class="controls control-row">
			  <input type="text" id="inputID" name="inputID" placeholder="Nomor ID Pasien" autocomplete="off" class="input-medium" value="<?php echo $id_pasien; ?>">
			  <input type="text" id="inputID2" name="inputID2" readonly value="<?php echo urldecode( $nama ); ?>">
			  <button type="button" onclick="pasienbaru(); return false;" id="pasien_baru" class="btn btn-info"><i class="icon-ok-sign icon-white"></i> Pasien Baru</button>
			  <label class="checkbox inline">
				Pasien Baru?<input type="checkbox" id="baru" name="baru" value="1" <?php if($id_pasien!="") echo 'checked'; ?>> 
			  </label>
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputID">Dokter Pengirim</label>
			<div class="controls control-row">
			  
					<input type="text" id="referensi" name="referensi" placeholder="Referensi" class="input-xlarge" autocomplete="off">
					<input type="hidden" id="idreferensi" name="idreferensi" value="0" >
					
			</div>
			
		  </div>
		  
		  <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
		  
		  <div class="alert alert-info">
			
			<div class="row-fluid show-grid">
              <div class="span4">
			  <div class="control-group">
			<label class="control-label" for="pemeriksaan">Pemeriksaan</label>
			<div class="controls control-row">
			  <input type="hidden" id="pemeriksaan" name="pemeriksaan" value="0">
			  <input type="text" id="namapemeriksaan" name="namapemeriksaan" placeholder="Pemeriksaan" class="span11">
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="dokter">Dokter</label>
			<div class="controls control-row">
			  <input type="text" id="dokter" name="dokter" class="span11">
			  <input type="hidden" id="iddokter" name="dokter" class="input-xlarge" value="0">
			  
			</div>
			
		  </div>
		  
		  
		  <div class="control-group">
			<label class="control-label" for="harga">Harga</label>
			<div class="controls control-row">
			  <input type="text" id="harga" name="harga" class="input-small" >
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="diskon">Diskon</label>
			<div class="controls control-row">
			  
				<div class="input-prepend input-apppend" >
					<input type="text" id="diskon" name="diskon"  class="span3">
					<span class="add-on">%</span>
					<span class="add-on">Rp</span>
					<input type="text" id="nominaldiskon" name="nominaldiskon"  class="input-small">
					
				</div>
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="harganetto">Netto</label>
			<div class="controls control-row">
			  
				<div class="input-prepend input-apppend" >
					<span class="add-on">Rp</span>
					<input type="text" id="harganetto" name="harganetto"  class="input-small">
				</div>
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="fee1">Fee Pengirim</label>
			<div class="controls control-row">
			  
				<div class="input-prepend input-apppend" >
					<span class="add-on">Rp</span>
					<input type="text" id="fee1" name="fee1"  class="input-small">
				</div>
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="fee2">Fee Operator</label>
			<div class="controls control-row">
			  
				<div class="input-prepend input-apppend" >
					<span class="add-on">Rp</span>
					<input type="text" id="fee2" name="fee2"  class="input-small">
				</div>
				
			</div>
			
		  </div>

			  </div>
              <div class="span4">
			  <div class="control-group">
			<label class="control-label" for="pemeriksaan2">Pemeriksaan</label>
			<div class="controls control-row">
			  <input type="hidden" id="pemeriksaan2" name="pemeriksaan2" value="0">
			  <input type="text" id="namapemeriksaan2" name="namapemeriksaan2" placeholder="Pemeriksaan" class="span11">
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="dokter2">Dokter</label>
			<div class="controls control-row">
			  <input type="text" id="dokter2" name="dokter2" class="span11">
			  <input type="hidden" id="iddokter2" name="iddokter2" class="input-xlarge" value="0">
			  
			</div>
			
		  </div>
		  
		  
		  <div class="control-group">
			<label class="control-label" for="harga2">Harga</label>
			<div class="controls control-row">
			  <input type="text" id="harga2" name="harga2" class="input-small" >
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="diskon2">Diskon</label>
			<div class="controls control-row">
			  
				<div class="input-prepend input-apppend" >
					<input type="text" id="diskon2" name="diskon2"  class="span3">
					<span class="add-on">%</span>
					<span class="add-on">Rp</span>
					<input type="text" id="nominaldiskon2" name="nominaldiskon2"  class="input-small">
					
				</div>
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="harganetto2">Netto</label>
			<div class="controls control-row">
			  
				<div class="input-prepend input-apppend" >
					<span class="add-on">Rp</span>
					<input type="text" id="harganetto2" name="harganetto2"  class="input-small">
				</div>
				
			</div>
			
		  </div>
		  
		  		  <div class="control-group">
			<label class="control-label" for="fee12">Fee Pengirim</label>
			<div class="controls control-row">
			  
				<div class="input-prepend input-apppend" >
					<span class="add-on">Rp</span>
					<input type="text" id="fee12" name="fee12"  class="input-small">
				</div>
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="fee22">Fee Operator</label>
			<div class="controls control-row">
			  
				<div class="input-prepend input-apppend" >
					<span class="add-on">Rp</span>
					<input type="text" id="fee22" name="fee22"  class="input-small">
				</div>
				
			</div>
			
		  </div>

			  </div>
              
              <div class="span4">
			  <div class="control-group">
			<label class="control-label" for="pemeriksaan3">Pemeriksaan</label>
			<div class="controls control-row">
			  <input type="hidden" id="pemeriksaan3" name="pemeriksaan3" value="0">
			  <input type="text" id="namapemeriksaan3" name="namapemeriksaan3" placeholder="Pemeriksaan" class="span11">
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="dokter3">Dokter</label>
			<div class="controls control-row">
			  <input type="text" id="dokter3" name="dokter3" class="span11">
			  <input type="hidden" id="iddokter3" name="iddokter3" class="input-xlarge" value="0">
			  
			</div>
			
		  </div>
		  
		  
		  <div class="control-group">
			<label class="control-label" for="harga3">Harga</label>
			<div class="controls control-row">
			  <input type="text" id="harga3" name="harga3" class="input-small" >
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="diskon3">Diskon</label>
			<div class="controls control-row">
			  
				<div class="input-prepend input-apppend" >
					<input type="text" id="diskon3" name="diskon3"  class="span3">
					<span class="add-on">%</span>
					<span class="add-on">Rp</span>
					<input type="text" id="nominaldiskon3" name="nominaldiskon3"  class="input-small">
					
				</div>
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="harganetto3">Netto</label>
			<div class="controls control-row">
			  
				<div class="input-prepend input-apppend" >
					<span class="add-on">Rp</span>
					<input type="text" id="harganetto3" name="harganetto3"  class="input-small">
				</div>
				
			</div>
			
		  </div>
		  
		  		  <div class="control-group">
			<label class="control-label" for="fee13">Fee Pengirim</label>
			<div class="controls control-row">
			  
				<div class="input-prepend input-apppend" >
					<span class="add-on">Rp</span>
					<input type="text" id="fee13" name="fee13"  class="input-small">
				</div>
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="fee23">Fee Operator</label>
			<div class="controls control-row">
			  
				<div class="input-prepend input-apppend" >
					<span class="add-on">Rp</span>
					<input type="text" id="fee23" name="fee23"  class="input-small">
				</div>
				
			</div>
			
		  </div>

			  </div>
              
            </div>
			
		  
		  <div class="control-group">
			<div class="controls">
			  <button type="button" onclick="load_data_ajax(); return false;" id="tambah_layanan" class="btn btn-success"><i class="icon-ok-sign icon-white"></i> Tambahkan</button>
			</div>
		  </div>
		  <div class="control-group">
			<div class="controls">
			   <div id="tabel_layanan"></div>
			</div>
		  </div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="totalharga">Total Harga</label>
			<div class="controls control-row">
			  <input type="text" id="totalharga" name="totalharga" class="input-small" readonly value="0">
				<div class="input-prepend input-apppend" >
					<span class="add-on">Diskon</span>
					<input type="text" id="totaldiskon" name="totaldiskon"  class="input-mini" value="0">
					<span class="add-on">%</span>
					<span class="add-on">Rp</span>
					<input type="text" id="totalnominaldiskon" name="totalnominaldiskon"  class="input-small" value="0">
					<span class="add-on">Harga Netto : Rp</span>
					<input type="text" id="totalharganetto" name="totalharganetto"  class="input-small" value="0">
				</div>
				
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<div class="controls">
			  <button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Cetak</button>
			</div>
		  </div>
		</form>
   
</div>
 
<script type="text/javascript">

function hitung_netto() 
	{
     var harga=$('#harga').val();
	 var nominaldiskon=$('#nominaldiskon').val();
     
	 var netto=harga-nominaldiskon;
     $("#harganetto").val(netto);
	 
	}
function hitung_persen_diskon() 
	{
     var harga=$('#harga').val();
	 var nominaldiskon=$('#nominaldiskon').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#diskon").val(diskon);
	}

function hitung_nominal_diskon() 
	{
     var harga=$('#harga').val();
	 var diskon=$('#diskon').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#nominaldiskon").val(nominaldiskon);
	}
	
function hitung_total_netto() 
	{
     var harga=$('#totalharga').val();
	 var nominaldiskon=$('#totalnominaldiskon').val();
     
	 var netto=harga-nominaldiskon;
     $("#totalharganetto").val(netto);
	}
function hitung_total_persen_diskon() 
	{
     var harga=$('#totalharga').val();
	 var nominaldiskon=$('#totalnominaldiskon').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#totaldiskon").val(diskon);
	}

function hitung_total_nominal_diskon() 
	{
     var harga=$('#totalharga').val();
	 var diskon=$('#totaldiskon').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#totalnominaldiskon").val(nominaldiskon);
	}

	
function hitung_netto2() 
	{
     var harga=$('#harga2').val();
	 var nominaldiskon=$('#nominaldiskon2').val();
     
	 var netto=harga-nominaldiskon;
     $("#harganetto2").val(netto);
	}
function hitung_persen_diskon2() 
	{
     var harga=$('#harga2').val();
	 var nominaldiskon=$('#nominaldiskon2').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#diskon2").val(diskon);
	}

function hitung_nominal_diskon2() 
	{
     var harga=$('#harga2').val();
	 var diskon=$('#diskon2').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#nominaldiskon2").val(nominaldiskon);
	}
	
function hitung_total_netto2() 
	{
     var harga=$('#totalharga2').val();
	 var nominaldiskon=$('#totalnominaldiskon2').val();
     
	 var netto=harga-nominaldiskon;
     $("#totalharganetto2").val(netto);
	}
function hitung_total_persen_diskon2() 
	{
     var harga=$('#totalharga2').val();
	 var nominaldiskon=$('#totalnominaldiskon2').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#totaldiskon2").val(diskon);
	}

function hitung_total_nominal_diskon2() 
	{
     var harga=$('#totalharga2').val();
	 var diskon=$('#totaldiskon2').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#totalnominaldiskon2").val(nominaldiskon);
	}
	
function hitung_netto3() 
	{
     var harga=$('#harga3').val();
	 var nominaldiskon=$('#nominaldiskon3').val();
     
	 var netto=harga-nominaldiskon;
     $("#harganetto3").val(netto);
	}
function hitung_persen_diskon3() 
	{
     var harga=$('#harga3').val();
	 var nominaldiskon=$('#nominaldiskon3').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#diskon3").val(diskon);
	}

function hitung_nominal_diskon3() 
	{
     var harga=$('#harga3').val();
	 var diskon=$('#diskon3').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#nominaldiskon3").val(nominaldiskon);
	}
	
function hitung_total_netto3() 
	{
     var harga=$('#totalharga3').val();
	 var nominaldiskon=$('#totalnominaldiskon3').val();
     
	 var netto=harga-nominaldiskon;
     $("#totalharganetto3").val(netto);
	}
function hitung_total_persen_diskon3() 
	{
     var harga=$('#totalharga3').val();
	 var nominaldiskon=$('#totalnominaldiskon3').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#totaldiskon3").val(diskon);
	}

function hitung_total_nominal_diskon3() 
	{
     var harga=$('#totalharga3').val();
	 var diskon=$('#totaldiskon3').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#totalnominaldiskon3").val(nominaldiskon);
	}

$(document).ready(function(){

var data = [<?php echo $testing; ?>];
var data2 = [<?php echo $testing2; ?>];
var data3 = [<?php echo $testing3; ?>];

$('#inputID').typeahead({
    source: function (query, process) {
        states = [];
		map = {};
		
		var source = [];
		$.each(data, function (i, state) {
			map[state.stateName] = state;
			states.push(state.stateName);
		});
	 
		process(states);
		
    },
    updater: function (item) {
        
		selectedState = map[item].stateCode;
		selectedState2 = map[item].stateDisplay;
		$("#inputID2").val(selectedState2);
		return selectedState;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
    },
});

$('#referensi').typeahead({
    source: function (query, process) {
        states2 = [];
		map2 = {};
		
		var source = [];
		$.each(data2, function (i, state) {
			map2[state.stateName] = state;
			states2.push(state.stateName);
		});
	 
		process(states2);
		
    },
    updater: function (item) {
        
		selectedState = map2[item].stateCode;
		selectedState2 = map2[item].stateDisplay;
		$("#idreferensi").val(selectedState);
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#dokter').typeahead({
    source: function (query, process) {
        states3 = [];
		map3 = {};
		
		var source = [];
		$.each(data2, function (i, state) {
			map3[state.stateName] = state;
			states3.push(state.stateName);
		});
	 
		process(states3);
		
    },
    updater: function (item) {
        
		selectedState = map3[item].stateCode;
		selectedState2 = map3[item].stateDisplay;
		$("#iddokter").val(selectedState);
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#dokter2').typeahead({
    source: function (query, process) {
        states3 = [];
		map3 = {};
		
		var source = [];
		$.each(data2, function (i, state) {
			map3[state.stateName] = state;
			states3.push(state.stateName);
		});
	 
		process(states3);
		
    },
    updater: function (item) {
        
		selectedState = map3[item].stateCode;
		selectedState2 = map3[item].stateDisplay;
		$("#iddokter2").val(selectedState);
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#dokter3').typeahead({
    source: function (query, process) {
        states3 = [];
		map3 = {};
		
		var source = [];
		$.each(data2, function (i, state) {
			map3[state.stateName] = state;
			states3.push(state.stateName);
		});
	 
		process(states3);
		
    },
    updater: function (item) {
        
		selectedState = map3[item].stateCode;
		selectedState2 = map3[item].stateDisplay;
		$("#iddokter3").val(selectedState);
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#namapemeriksaan').typeahead({
    source: function (query, process) {
        states4 = [];
		map4 = {};
		
		var source = [];
		$.each(data3, function (i, state) {
			map4[state.stateName] = state;
			states4.push(state.stateName);
		});
	 
		process(states4);
		
    },
    updater: function (item) {
        
		selectedState = map4[item].stateCode;
		selectedState2 = map4[item].stateDisplay;
		harga = map4[item].harga;
		diskon = map4[item].diskon;
		fee1 = map4[item].fee1;
		fee2 = map4[item].fee2;
		$("#pemeriksaan").val(selectedState);
		$("#harga").val(harga);
		$("#diskon").val(diskon);
		$("#fee1").val(fee1);
		$("#fee2").val(fee2);
		hitung_nominal_diskon();
		hitung_netto();
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#namapemeriksaan2').typeahead({
    source: function (query, process) {
        states4 = [];
		map4 = {};
		
		var source = [];
		$.each(data3, function (i, state) {
			map4[state.stateName] = state;
			states4.push(state.stateName);
		});
	 
		process(states4);
		
    },
    updater: function (item) {
        
		selectedState = map4[item].stateCode;
		selectedState2 = map4[item].stateDisplay;
		harga = map4[item].harga;
		diskon = map4[item].diskon;
		fee1 = map4[item].fee1;
		fee2 = map4[item].fee2;
		$("#pemeriksaan2").val(selectedState);
		$("#harga2").val(harga);
		$("#diskon2").val(diskon);
		$("#fee12").val(fee1);
		$("#fee22").val(fee2);
		hitung_nominal_diskon2();
		hitung_netto2();
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#namapemeriksaan3').typeahead({
    source: function (query, process) {
        states4 = [];
		map4 = {};
		
		var source = [];
		$.each(data3, function (i, state) {
			map4[state.stateName] = state;
			states4.push(state.stateName);
		});
	 
		process(states4);
		
    },
    updater: function (item) {
        
		selectedState = map4[item].stateCode;
		selectedState2 = map4[item].stateDisplay;
		harga = map4[item].harga;
		diskon = map4[item].diskon;
		fee1 = map4[item].fee1;
		fee2 = map4[item].fee2;
		$("#pemeriksaan3").val(selectedState);
		$("#harga3").val(harga);
		$("#diskon3").val(diskon);
		$("#fee13").val(fee1);
		$("#fee23").val(fee2);
		hitung_nominal_diskon3();
		hitung_netto3();
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#diskon').keyup(function() {
			hitung_nominal_diskon();
			hitung_netto();
});

$('#nominaldiskon').keyup(function() {
			hitung_persen_diskon();
			hitung_netto();
});

$('#totaldiskon').keyup(function() {
			hitung_total_nominal_diskon();
			hitung_total_netto();
});

$('#totalnominaldiskon').keyup(function() {
			hitung_total_persen_diskon();
			hitung_total_netto();
});


$('#diskon2').keyup(function() {
			hitung_nominal_diskon2();
			hitung_netto2();
});

$('#nominaldiskon2').keyup(function() {
			hitung_persen_diskon2();
			hitung_netto2();
});

$('#totaldiskon2').keyup(function() {
			hitung_total_nominal_diskon2();
			hitung_total_netto2();
});

$('#totalnominaldiskon2').keyup(function() {
			hitung_total_persen_diskon2();
			hitung_total_netto2();
});

$('#diskon3').keyup(function() {
			hitung_nominal_diskon3();
			hitung_netto3();
});

$('#nominaldiskon3').keyup(function() {
			hitung_persen_diskon3();
			hitung_netto3();
});

$('#totaldiskon3').keyup(function() {
			hitung_total_nominal_diskon3();
			hitung_total_netto3();
});

$('#totalnominaldiskon3').keyup(function() {
			hitung_total_persen_diskon3();
			hitung_total_netto3();
});

});

var controller = 'transaksi';
var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';

function load_data_ajax(){
var reg='nono';
var id_layanan=$('#pemeriksaan').val();
var id_dokter=$('#iddokter').val();
var harga=$('#harga').val();
var diskon=$('#diskon').val();
var nominal_diskon=$('#nominaldiskon').val();
var netto=$('#harganetto').val();
var fee1=$('#fee1').val();
var fee2=$('#fee2').val();

var id_layanan2=$('#pemeriksaan2').val();
var id_dokter2=$('#iddokter2').val();
var harga2=$('#harga2').val();
var diskon2=$('#diskon2').val();
var nominal_diskon2=$('#nominaldiskon2').val();
var netto2=$('#harganetto2').val();
var fee12=$('#fee12').val();
var fee22=$('#fee22').val();

var id_layanan3=$('#pemeriksaan3').val();
var id_dokter3=$('#iddokter3').val();
var harga3=$('#harga3').val();
var diskon3=$('#diskon3').val();
var nominal_diskon3=$('#nominaldiskon3').val();
var netto3=$('#harganetto3').val();
var fee13=$('#fee13').val();
var fee23=$('#fee23').val();


	$.ajax({
		'url' : base_url + controller + '/insert_layanan',
		'type' : 'POST', //the way you want to send data to your URL
		'data' : {'reg' : reg, 'id_layanan' : id_layanan, 'id_dokter': id_dokter, 'harga' : harga, 'diskon' : diskon, 'nominal_diskon' : nominal_diskon, 'netto' : netto, 'fee1' : fee1, 'fee2' : fee2, 'id_layanan2' : id_layanan2, 'id_dokter2': id_dokter2, 'harga2' : harga2, 'diskon2' : diskon2, 'nominal_diskon2' : nominal_diskon2, 'netto2' : netto2, 'fee12' : fee12, 'fee22' : fee22, 'id_layanan3' : id_layanan3, 'id_dokter3': id_dokter3, 'harga3' : harga3, 'diskon3' : diskon3, 'nominal_diskon3' : nominal_diskon3, 'netto3' : netto3, 'fee13' : fee13, 'fee23' : fee23},
		'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
			var container = $('#tabel_layanan'); //jquery selector (get element by id)
			if(data){
				$('#pemeriksaan').val(0);
				$('#iddokter').val(0);
				$('#harga').val("0");
				$('#diskon').val("0");
				$('#nominaldiskon').val("0");
				$('#harganetto').val("0");
				$('#namapemeriksaan').val("");
				$('#dokter').val("");
				$('#fee1').val(0);
				$('#fee2').val(0);
				
				$('#pemeriksaan3').val(0);
				$('#iddokter3').val(0);
				$('#harga3').val("0");
				$('#diskon3').val("0");
				$('#nominaldiskon3').val("0");
				$('#harganetto3').val("0");
				$('#namapemeriksaan3').val("");
				$('#dokter3').val("");
				$('#fee12').val(0);
				$('#fee22').val(0);
				
				$('#pemeriksaan2').val(0);
				$('#iddokter2').val(0);
				$('#harga2').val("0");
				$('#diskon2').val("0");
				$('#nominaldiskon2').val("0");
				$('#harganetto2').val("0");
				$('#namapemeriksaan2').val("");
				$('#dokter2').val("");
				$('#fee13').val(0);
				$('#fee23').val(0);
				
				container.html(data);
				var temp = $('#temptotalharga').val();
				$('#totalharga').val(temp);
				hitung_total_netto();
			}
		}
	});
}

function pasienbaru(){
	window.location.href = base_url + '/pasien/pasien_baru/1';
}

function delete_layanan( nomor_register, id_layanan, id_dokter,	harga, diskon, nominal_diskon, netto	){

	$.ajax({
		'url' : base_url + controller + '/delete_layanan',
		'type' : 'POST', //the way you want to send data to your URL
		'data' : {'reg' : nomor_register, 'id_layanan' : id_layanan, 'id_dokter': id_dokter, 'harga' : harga, 'diskon' : diskon, 'nominal_diskon' : nominal_diskon, 'netto' : netto},
		'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
			var container = $('#tabel_layanan'); //jquery selector (get element by id)
			if(data){
				$('#pemeriksaan').val(0);
				$('#iddokter').val(0);
				$('#harga').val("0");
				$('#diskon').val("0");
				$('#nominaldiskon').val("0");
				$('#harganetto').val("0");
				$('#namapemeriksaan').val("");
				$('#dokter').val("");
				
				$('#pemeriksaan3').val(0);
				$('#iddokter3').val(0);
				$('#harga3').val("0");
				$('#diskon3').val("0");
				$('#nominaldiskon3').val("0");
				$('#harganetto3').val("0");
				$('#namapemeriksaan3').val("");
				$('#dokter3').val("");
				
				$('#pemeriksaan2').val(0);
				$('#iddokter2').val(0);
				$('#harga2').val("0");
				$('#diskon2').val("0");
				$('#nominaldiskon2').val("0");
				$('#harganetto2').val("0");
				$('#namapemeriksaan2').val("");
				$('#dokter2').val("");
				
				container.html(data);
				var temp = $('#temptotalharga').val();
				$('#totalharga').val(temp);
				hitung_total_netto();
			}
		}
	});
}


</script>