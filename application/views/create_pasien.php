<div class="container">
   
      <h2>Tambah Pasien</h2>
      <form class="form-horizontal" action="<?php echo site_url("pasien/create_pasien"); ?>" method="POST">
		  <div class="control-group">
			<label class="control-label" for="inputID">Nomor ID Pasien</label>
			<div class="controls control-row">
			  <input type="hidden" id="dari" name="dari" value="<?php echo $dari; ?>">
			  
			  <input type="text" id="inputID" name="inputID" placeholder="Nomor ID Pasien">
			  
				<span id="peringatan" class="add-on alert alert-info">Pastikan Nomor ID Pasien sesuai dengan Nomor KTP Pasien </span>
				
			  
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputID">Med Rec</label>
			<div class="controls control-row">
			  
			  <div class="input-prepend">
			  <span class="add-on"><i class="icon-tasks"></i></span>
			  <span class="add-on">Med Rec </span>
			  <input type="text" id="inputmedrec" name="inputmedrec" placeholder="Med Rec" disabled>
			  </div>
			  &nbsp;
			  
			  <label class="checkbox inline">
				<input type="checkbox" id="member" name="member" value="1"> Member
			  </label>
			</div>
			
		  </div>
		    
		  <div class="control-group">
			<label class="control-label" for="inputNama">Nama Pasien</label>
			<div class="controls">
				<select name="initial" id="initial" class="input-small">
					<option value="0">Tn.</option>
					<option value="1">Ny.</option>
					<option value="2">Nona</option>
					<option value="3">Anak</option>
					<option value="4">Bayi</option>
				</select>
			  <input type="text" id="inputNama" name="inputNama" class="span5" placeholder="Nama Pasien">
			</div>
		  </div>
		<?php
				
				$date=date("d-m-Y");
			?>
		<div class="control-group">
			<label class="control-label" for="tanggallahir">Tanggal Lahir</label>
			<div class="controls control-row">
			
				<div class="input-append date" id="dp3" data-date="01-01-1989" data-date-format="dd-mm-yyyy">
					<input class="input-small" size="16" type="text" value="01-01-1989" name="tanggallahir" id="tanggallahir">
					
					<span class="add-on"><i class="icon-calendar"></i></span>
				</div>
				<input type="hidden" value="<?php echo $date; ?>" name="now" id="now">
				<div class="input-prepend input-append">
				  <span class="add-on">Umur </span>
				  <input type="text" class="span1" id="umur" name="umur" value="0" readonly>
				  <span class="add-on">Tahun </span>
				  <input type="text" class="span1" id="umur_bulan" name="umur_bulan" value="0" readonly>
				  <span class="add-on">Bulan </span>
				  <input type="text" class="span1" id="umur_hari" name="umur_hari" value="0" readonly>
				  <span class="add-on">Hari </span>
				</div>
				
				
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="goldar">Golongan Darah</label>
			<div class="controls control-row">
				<select name="goldar" id="goldar" class="input-small">
					<option value="0">A</option>
					<option value="1">B</option>
					<option value="2">O</option>
					<option value="3">AB</option>
				</select>
					
				<div class="input-prepend">
				  <span class="add-on">Rhesus </span>
					<select name="resus" id="resus" class="input-medium">
					  <option value="0">Positive</option>
					  <option value="1">Negative</option>
					</select>
					
				</div>
				
				&nbsp;
				<div class="input-prepend input-append">
				  <span class="add-on">Jenis Kelamin </span>
					<select name="jeniskelamin" id="jeniskelamin" class="input-medium">
					  <option value="0">Laki-laki</option>
					  <option value="1">Perempuan</option>
					</select>
					
				</div>
				
			</div>
		  </div>
		  
		  <div class="control-group">
			
			<label class="control-label" for="alamat">Alamat</label>
			<div class="controls">
			<textarea name="alamat" id="alamat" rows="4" class="span6"></textarea>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputKota">Kota</label>
			<div class="controls control-row">
			  <input type="text" id="inputKota" name="inputKota" class="input-xlarge" placeholder="Kota" autocomplete="off">
			  <input type="hidden" id="idkota" name="idkota" value="0" >
			  <div class="input-prepend">
					<span class="add-on">Kode Pos </span>
					<input type="text" id="inputKodepos" name="inputKodepos" class="span2" placeholder="Kode Pos">
					
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputEmail">Email</label>
			<div class="controls control-row">
			  <input type="text" id="inputEmail" name="inputEmail" class="input-large" placeholder="Email">
			  <div class="input-prepend">
					<span class="add-on">Pekerjaan </span>
					<select name="pekerjaan" id="pekerjaan" class="input-large">
					  <option value="Pegawai Negeri">Pegawai Negeri</option>
					  <option value="Wiraswasta">Wiraswasta</option>
					  <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
					  <option value="Karyawan Swasta">Karyawan Swasta</option>
					</select>
					
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputTelepon">Telepon</label>
			<div class="controls control-row">
			  <input type="text" id="inputTelepon" name="inputTelepon" class="input-large" placeholder="Telepon">
			  <div class="input-prepend">
					<span class="add-on">Hp / Flexi </span>
					<input type="text" id="hp" name="hp" class="span3" placeholder="Nomor HP">
				</div>
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="asuransi">Perusahaan/asuransi/CC</label>
			<div class="controls control-row">		  
				<select name="asuransi" id="asuransi" class="span6">
				<?php foreach ($perusahaan as $perusahaan_item): ?>
					<option value="<?php echo $perusahaan_item['id_perusahaan'] ?>"><?php echo $perusahaan_item['nama_perusahaan'] ?></option>
				<?php endforeach; ?>
				  
				</select>
			
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="keterangan">Keterangan</label>
			<div class="controls control-row">
			  <input type="text" id="keterangan" name="keterangan" class="span6" placeholder="Keterangan">
			</div>
		  </div>
		  
		  <div class="control-group">
			<div class="controls">
			  <button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Simpan</button>
			</div>
		  </div>
		</form>
   
</div>

<script type="text/javascript">
function ubah_tanggallahir() {
    var str=$('#inputID').val();
			
	var hari=str.substring(6,8);
	
	if(hari>31)
	{
		hari=hari-40;
		$("#jeniskelamin").val(1);
		$("#jeniskelamin").val(1);
		$("#initial").val(1);
		if(hari<10)hari="0"+hari;
		
	}
	var tgl=hari+"-"+str.substring(8,10)+"-19"+str.substring(10,12);
	
	$('#tanggallahir').val(tgl);
	$('#inputmedrec').val(str);
	var tanggallahir=$('#tanggallahir').val();
	var now=$('#now').val();
	
	var diff=datediff(tanggallahir,now);
	humanise(diff);
	}
function dstrToUTC(ds) {
    var dsarr = ds.split("-");
     var dd = parseInt(dsarr[0],10);
     var mm = parseInt(dsarr[1],10);
     var yy = parseInt(dsarr[2],10);
     return Date.UTC(yy,mm-1,dd,0,0,0); 
	}
function datediff(ds1,ds2) 
	{
     var d1 = dstrToUTC(ds1);
     var d2 = dstrToUTC(ds2);
     var oneday = 86400000;
     return (d2-d1) / oneday;    
	}
function humanise (diff) {
  // The string we're working with to create the representation
  var str = '';
  // Map lengths of `diff` to different time periods
  var values = {
    'umur': 365, 
    'umur_bulan': 30, 
    'umur_hari': 1
  };

  // Iterate over the values...
  for (var x in values) {
    var amount = Math.floor(diff / values[x]);

    // ... and find the largest time value that fits into the diff
    if (amount >= 1) {
       // If we match, add to the string ('s' is for pluralization)
       str += amount + x + (amount > 1 ? 's' : '') + ' ';
	   $('#'+x).val(amount);
	   

       // and subtract from the diff
       diff -= amount * values[x];
    }
	else
	{
		$('#'+x).val(0);
	}
  }

  return str;
}
	
$(document).ready(function(){

var data2 = [<?php echo $testing2; ?>];

$('#inputKota').typeahead({
    source: function (query, process) {
        states2 = [];
		map2 = {};
		
		var source = [];
		$.each(data2, function (i, state) {
			map2[state.stateName] = state;
			states2.push(state.stateName);
		});
	 
		process(states2);
		
    },
    updater: function (item) {
        
		selectedState = map2[item].stateCode;
		selectedState2 = map2[item].stateDisplay;
		$("#idkota").val(selectedState);
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});



$('#dp3').datepicker()
		  .on('changeDate', function(ev){
			var tanggallahir=$('#tanggallahir').val();
			var now=$('#now').val();
			
			var diff=datediff(tanggallahir,now);
			humanise(diff);
		  });
});

$('#tanggallahir').keyup(function() {
			var tanggallahir=$('#tanggallahir').val();
			var now=$('#now').val();
			
			var diff=datediff(tanggallahir,now);
			humanise(diff);
});

$('#inputID').keyup(function() {
			var inputID=$('#inputID').val();
			$('#inputmedrec').val(inputID);	
			var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
			$.ajax({
				'url' : base_url + '/pasien/cek_ada',
				'type' : 'POST', //the way you want to send data to your URL
				'data' : {'input' : inputID},
				'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					
					if(data>0){
						$('#peringatan').addClass("alert-error");
						$("#peringatan").removeClass("alert-info")
						$("#peringatan").html("Nomor KTP ini pernah digunakan, tidak bisa didaftarkan lagi!!!")
					}
					else
					{
						$('#peringatan').addClass("alert-info");
						$("#peringatan").removeClass("alert-error")
						$("#peringatan").html("Pastikan Nomor ID Pasien sesuai dengan Nomor KTP")
					}
				}
			});
});

$('#inputID').change(function() {
			
			ubah_tanggallahir();
			
});

$( "#initial" )
  .change(function () {
    var str = 0;
    $( "#initial option:selected" ).each(function() {
      str = $( this ).val();
    });
    if(str==0)
	{
		$("#jeniskelamin").val(0);
	}
	else if(str==1 || str==2)
	{
		$("#jeniskelamin").val(1);
	}
	
  })
  .change();



</script>