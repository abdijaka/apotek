<form class="form-horizontal" method="POST" action="<?php echo site_url('transaksi/update_pembayaran') ?>">
<div class="modal-dialog">
    <div class="modal-content modal-content-no-radius">
      <div class="modal-header modal-header-blue">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title modal-title-center" id="registerModalLabel">Edit Pembayaran</h4>
      </div>
      <div class="modal-body">
			<?php
			$date=date("d-m-Y");
			$tanggal =date('j-m-Y', strtotime($pembayaran[0]['tanggal']));
			$jam =date('G:i:s', strtotime($pembayaran[0]['tanggal']));
			?>
			
			<div class="row">
			  <div class="col-md-12">
				<div class="col-md-6">
				<label class="control-label" for="eb_tanggal">Tanggal</label>
					<input type="text" value="<?php echo $tanggal; ?>" readonly>
					<input type="hidden" value="<?php echo $pembayaran[0]['tanggal']; ?>" name="eb_tanggal" id="eb_tanggal" >
				</div>
				
				<div class="col-md-6">
					<label class="control-label" for="nota">Nomor Registrasi</label>
					<input type="text" id="eb_nota" name="eb_nota" value="<?php echo $transaksi['nomor_registrasi']; ?>" readonly >
				</div>
				</div>
			</div>
			  
			  <div class="row">
			  <div class="col-md-12">
				<div class="col-md-6">
				<label class="control-label" for="eb_carabayar">Cara Bayar</label>
				
				  <select name="eb_carabayar" id="eb_carabayar" >
									<?php foreach ($carabayar as $carabayar_item): ?>
										<option <?php if ($carabayar_item['id']==$pembayaran[0]['carabayar']) echo 'selected'; ?> value="<?php echo $carabayar_item['id'] ?>"><?php echo $carabayar_item['nama'] ?></option>
									<?php endforeach; ?>
									  
									</select>
				
				</div>
				
				<div class="col-md-6">
					<label class="control-label" for="jumlahbayar">Jumlah Yang Dibayarkan</label>
					<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">Rp</span>
				  <input type="text" id="eb_jumlahbayar2" name="eb_jumlahbayar2"  value="<?php echo $pembayaran[0]['jumlah']; ?>" >
				  <input type="hidden" id="eb_totalharganetto" name="eb_totalharganetto"  value="<?php echo $transaksi['harga_netto']; ?>" >
				  <input type="hidden" id="eb_totalbayar" name="eb_totalbayar"  value="<?php echo $transaksi['bayar']; ?>" >
				  <input type="hidden" id="eb_temp_bayar" name="eb_temp_bayar"  value="<?php echo ($transaksi['bayar']-$pembayaran[0]['jumlah']); ?>" >
				</div>
				</div>
			  </div>
			  </div>
			  <div class="row">
			  <div class="col-md-12">
				<div class="col-md-6">
				<label class="control-label" for="eb_carabayar">Charge Bank</label>
					<div class="input-group margin-bottom-sm">
					<?php
						$persen=$pembayaran[0]['chargebank']*100/$pembayaran[0]['jumlah'];
					?>
					<input type="text" id="eb_persencharge" name="eb_persencharge"  value="<?php echo $persen; ?>" >
					<span class="input-group-addon">%</span><span class="input-group-addon">Rp</span>
					
					<input type="text" id="eb_chargebank" name="eb_chargebank"  value="<?php echo $pembayaran[0]['chargebank']; ?>" readonly>
				</div>
				</div>
				
				<div class="col-md-6">
					<label class="control-label" for="jumlahbayar">Total+Charge</label>
					<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">Rp</span>
				  <input type="text" id="eb_bayarpluscharge" name="eb_bayarpluscharge"  value="<?php echo $pembayaran[0]['jumlah']+$pembayaran[0]['chargebank']; ?>" readonly>
				</div>
				</div>
				
			  </div>
			  </div>
			  
			  <div class="row">
				<div class="col-md-12">
				<div class="col-md-12">
				<label class="control-label" for="sisa">Sisa</label>
					<div class="input-group margin-bottom-sm">
					<span class="input-group-addon">Rp</span>
					<input type="text" id="eb_sisa2" name="eb_sisa2"  value="<?php echo $transaksi['sisa']; ?>" readonly>
					
					</div>
				</div>
				</div>
			  </div>
			  <div class="row">
				<div class="col-md-12">
				<div class="col-md-12">
				<label class="control-label" for="eb_memo">Memo</label>
				
				  <textarea id="eb_memo" name="eb_memo"><?php echo $pembayaran[0]['memo'] ?></textarea>
				</div>
				</div>
			  </div>
			  
		<script type="text/javascript">
		function eb_hitung_sisa()
			{
			 var harga=$('#eb_totalharganetto').val();
			 var bayar=$('#eb_jumlahbayar2').val();
			 var eb_temp_bayar=$('#eb_temp_bayar').val();
			 
			 var sisa=harga-bayar-eb_temp_bayar;
			 
			 $("#eb_sisa2").val(sisa);
			}
		function eb_hitung_charge()
			{
			 var persencharge=$('#eb_persencharge').val();
			 var bayar=$('#eb_jumlahbayar2').val();
			 var charge=Math.ceil(persencharge*bayar/100);
			 $("#eb_chargebank").val(charge);
			}
		function eb_hitung_totalcharge()
			{
			 var chargebank=$('#eb_chargebank').val();
			 var bayar=$('#eb_jumlahbayar2').val();
			 var total_charge=chargebank*1+bayar*1;
			 $("#eb_bayarpluscharge").val(total_charge);
			}

		$(document).ready(function(){
		eb_hitung_sisa();
		$('#eb_jumlahbayar2').keyup(function() {
					eb_hitung_charge();
					eb_hitung_totalcharge();
					eb_hitung_sisa();
		});
		
		$('#eb_persencharge').keyup(function() {
					eb_hitung_charge();
					eb_hitung_totalcharge();
		});
		
		$('#eb_carabayar').change(function() {
				var carabayar=$("#eb_carabayar").val();
				if(carabayar==3)
				{
					$("#eb_persencharge").val(2.5);
					eb_hitung_charge();
					eb_hitung_totalcharge();
				}
				else
				{
					$("#eb_persencharge").val(0);
					eb_hitung_charge();
					eb_hitung_totalcharge();
				}
		});
		
		});
		</script>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Update Pembayaran</button>
      </div>
    </div>
  </div>
</form>