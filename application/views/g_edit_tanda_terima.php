<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head -->
<head>
	<!-- Meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>MUSI APPS</title>
	<meta content="MUSI Apps" name="description">
	<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
	<meta content="MUSI" name="author">
	<meta content="yes" name="apple-mobile-web-app-capable">
	<meta content="black" name="apple-mobile-web-app-status-bar-style">
	<!-- Style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
	<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
	<link href="<?php echo base_url('assets/css/DT_bootstrap.css') ?>" rel="stylesheet">
	<!-- Icon -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>
<body style="">
	<!-- Header -->
	<header>
		<?php
			$this->load->view('templates/menubar_kiri');
		?>
	</header>
	<!-- Content -->
	<section id="wrap">
		<section class="content content-white">
			<div class="container container-content">
				<div class="row">
					<div class="col-md-12">
						<legend style="text-align:center;">EDIT TANDA TERIMA</legend>
						<form class="form-horizontal" id="form1" action="<?php echo site_url("g_tanda_terima_yunan/update_tanda_terima"); ?>" method="POST">
							<?php
							$date = $tanda_terima['tanggal'];
							$tgl_tanda_terima = date('d-m-Y',strtotime($date));
							?>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-3">
										<label class="control-label" for="inputTanggal">Tanggal Tanda Terima</label>
										<input type="text" value="<?php echo $tgl_tanda_terima; ?>" name="tgl_tanda_terima" id="tgl_tanda_terima" data-date-format="dd-mm-yyyy">
									</div>	
									<div class="col-md-3">
										<label class="control-label" for="inputReg">No. Tanda Terima</label>
										<input type="hidden" value="<?php echo $tanda_terima['id']; ?>" id="id_tanda_terima" name="id_tanda_terima" autocomplete="off">
										<input type="text" value="<?php echo $tanda_terima['no_tanda_terima']; ?>" id="no_tanda_terima" name="no_tanda_terima" autocomplete="off">
										<div id="check_tanda_terima" value="0">
										</div>
									</div>		
									<div class="col-md-3">
										<label class="control-label" for="inputID">Supplier</label>
										<select name="supplier" id="supplier" class="input-large">
										<?php foreach ($supplier as $supplier_item): ?>
										  <option value="<?php echo $supplier_item['id_supplier']; ?>" <?php if($tanda_terima['supplier']==$supplier_item['id_supplier']) echo "selected"; ?>><?php echo $supplier_item['nama_supplier']; ?></option>
										<?php endforeach ?>
										</select>
									</div>	
									<div class="col-md-1">
										<label class="control-label" for="pasien_baru">&nbsp;</label>
										<br>
										<a style="vertical-align:bottom;" type="button" href="<?php echo site_url('g_supplier/supplier_baru/1'); ?>" class="btn btn-info">Suppl Baru</a>
									</div>	
								</div>
							</div>
							<hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-info">
										<div class="container-fluid" style="margin-top:-15px; ">
											<div class="row-fluid">
						              			<div class="col-md-4" style="background-color:#90CA77; padding-bottom:20px; padding-top:20px;">
									  				<div class="row">
														<div class="col-md-13">
															<div class="col-md-12">
																<label class="control-label" style="color:#fff;" for="pemeriksaan">No. Faktur</label>
																<input type="hidden" id="nomor_faktur_h" name="nomor_faktur_h" >
																<input type="text" id="nomor_faktur" name="nomor_faktur" placeholder="Nomor faktur" autocomplete="off">
																<div id="checkfaktur" value="0">
																</div>
															</div>	
														</div>	
									 				</div>	
									 				<div class="row">
														<div class="col-md-13">
															<div class="col-md-12">
																<label class="control-label" for="diskon" style="color:#fff;">Tanggal Faktur</label>
																<input type="text" placeholder="Tanggal Faktur" name="tgl_faktur" id="tgl_faktur" data-date-format="dd-mm-yyyy" readonly>
															</div>	
														</div>	
									 				</div>	
									 				<div class="row">
														<div class="col-md-13">
															<div class="col-md-12">
																<label class="control-label" for="diskon" style="color:#fff;">Tanggal Jatuh Tempo</label>
																<input type="text" placeholder="Tanggal Jatuh Tempo" name="jatuh_tempo" id="jatuh_tempo" data-date-format="dd-mm-yyyy" readonly>
															</div>	
														</div>	
									 				</div>
													 <div class="row">
														<div class="col-md-13">
															<div class="col-md-12">
																<label class="control-label" for="harganetto" style="color:#fff;">Netto</label>
																<div class="input-group margin-bottom-sm">
																	<span class="input-group-addon">Rp</span>
																	<input type="text" id="netto" name="netto"  autocomplete="off" readonly>
																</div>
																<div id="label_harganetto" class="alert-danger">
																</div>
															</div>	
														</div>	
													 </div>	
													 <div class="row">
														<div class="col-md-13">
															<div class="col-md-6">
																<input type="checkbox" id="sp" name="sp" value="1">
																<label class="control-label" for="diskon" style="color:#fff;">SP</label>
															</div>	
															<div class="col-md-6">
																<input type="checkbox" id="fak_pajak" name="fak_pajak" value="1">
																<label class="control-label" for="diskon" style="color:#fff;">Fak. Pajak</label>
															</div>	
														</div>	
													 </div>
													 <div class="row">
														<div class="col-md-13">
															<div class="col-md-6">
																<input type="checkbox" id="sj" name="sj" value="1">
																<label class="control-label" for="diskon" style="color:#fff;">SJ</label>
															</div>	
															<div class="col-md-6">
																<input type="checkbox" id="fak_beli" name="fak_beli" value="1">
																<label class="control-label" for="diskon" style="color:#fff;">Fak. Beli</label>
															</div>
														</div>	
													 </div>
													 <!--<div class="row">
														<div class="col-md-13">
															<div class="col-md-6">
																<input type="checkbox" id="kwitansi" name="kwitansi" value="1">
																<label class="control-label" for="diskon" style="color:#fff;">Kwitansi</label>
															</div>		
														</div>	
													 </div>-->
													 <div class="row">
														<div class="col-md-13">
															<div class="col-md-12">
																<label class="control-label" for="diskon" style="color:#fff;">Keterangan</label>
																<textarea id="keterangan" name="keterangan" value="" placeholder="Keterangan"></textarea>
															</div>	
														</div>	
													 </div>
													 <div class="row">
														<div class="col-md-13">
															<div class="col-md-12">
																<input class="verified" type="checkbox" id="verified" name="fak_beli" value="1">
																<label class="control-label" for="diskon" style="color:#0000FF;">Verified</label>
															</div>	
														</div>	
													 </div>			
													 <div class="row">
														<div class="col-md-13">
															<div class="col-md-12">
																<br>
																<button type="button" onclick="load_data_ajax(); return false;" id="tambah_faktur" class="btn btn-danger">Tambahkan <i class="fa fa-chevron-right"></i></button>
															</div>	
														</div>	
													 </div>	
												</div>
						            			<div class="col-md-8">
											   		<div class="row">
											   			<div class="col-md-12">
											   				<div class="col-md-12">
											   					<div id="tabel_layanan">
											   					</div>
											  				</div>
											   			</div>
											   		</div>
											   		
												 	<div class="row">
														<div class="col-md-12">
															<div class="col-md-12">
																<label class="control-label" for="totalharganetto">Total Bayar</label>
																<p id="rp_total_bayar" name="rp_total_bayar" style="color:#CC0000; font-size:32px; font-weight:bold;"> Rp 0 </p>
																<input type="hidden" id="total_bayar" name="total_bayar"  class="input-large" value="0" >
															</div>	
														</div>	
												 	</div>
												 	<div class="row">
												 		<div class="col-md-12">
												 			<table class="table table-bordered table-condensed " >
												 				<thead>
																<tr style="background-color: #90CA77; color:#fff; font-size:14px;">
																  <th>Tanggal</th>
																  <th>Cara Bayar</th>
																  <th>Pembayaran</th>
																</tr>
															  	</thead>
															  	<tbody style="font-size:14px;">
																<?php
																$i=0;
																$total_bayar=0;
																
																	foreach ($pembayaran as $pembayaran_tanda_terima): 
																	$tanggal_bayar =date('j-m-Y', strtotime($pembayaran_tanda_terima['tanggal']));
																	$jam_bayar =date('G:i', strtotime($pembayaran_tanda_terima['tanggal']));
																	$total_bayar=$total_bayar+$pembayaran_tanda_terima['jumlah'];
																	?>
																	
																	<tr style="background-color: #fff;">
																	  
																	  <td><?php echo $tanggal_bayar; ?></td>
																	  <td><?php echo $pembayaran_tanda_terima['namacarabayar']; ?></td>
																	  <td style="text-align:right; "><p class="text-right"><?php echo formatRupiah($pembayaran_tanda_terima['jumlah']); ?></p> </td>
																	  
																	  <!--
																	  <td><a href="<?php echo site_url('transaksi/cetak_pembayaran/'.$transaksi['nomor_registrasi'].'/'.$pembayaran_item['tanggal']); ?>" class="btn btn-success"><i class="fa fa-print"></i> Print</a></td>
																	  -->
																	</tr>
																<?php 
																endforeach ?>	
																	<tr style="">
																	  <td colspan="1" style="text-align:center; color:#CC0000; font-weight:bold; font-size:16px;">Total Pembayaran</td>
																	  <td colspan="4" style="text-align:right;  font-size:20px; color:#CC0000; font-weight:bold;">
																	  	<p id="rp_total_pembayaran" name="rp_total_pembayaran" class="text-right"><?php echo formatRupiah($total_bayar); ?></p>
																	  	<input type="hidden" id="total_pembayaran" name="total_pembayaran"  class="input-large" value="<?php echo $total_bayar?>" >
																	  </td>
																	  
																	</tr>
																  </tbody>
												 			</table>
												 		</div>
												 	</div>
												 	<div class="row" >
														<div class="col-md-12" style="z-index:1;">
															<div  class="col-md-4">
																<label class="control-label" for="totalharga">Cara Bayar</label>
																<select name="carabayar" id="carabayar" >
																<?php foreach ($carabayar as $carabayar_item): ?>
																	<option value="<?php echo $carabayar_item['id'] ?>"><?php echo $carabayar_item['nama'] ?></option>
																<?php endforeach; ?>
																</select>
															</div>
															<div class="col-md-4">
																<label class="control-label" for="totalharga">Bayar (Rp)</label>
																<div class="input-group margin-bottom-sm">
																	<span class="input-group-addon">Rp</span>
																	<input type="text" id="bayar" name="bayar"  value="0" autocomplete="off" >
																</div>
																<div id="label_bayar" class="alert-danger" >

																</div>
															</div>
															<div class="col-md-4">
																<label class="control-label" for="totalharga">Kurang (Rp)</label>
																<div class="input-group margin-bottom-sm">
																	<span class="input-group-addon">Rp</span>
																	<input type="hidden" id="kurang" name="kurang"  value="0"  readonly>
																	<input type="text" id="rp_kurang" name="rp_kurang"  value="0"  readonly>
																</div>
															</div>	
														</div>
													</div>
													<div class="row">
												 		<div class="col-md-12">
												 			<div class="col-md-12">
												 				<hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
												 			</div>
												 		</div>
												 	</div>
												 	
												 	<div class="row">
												 		</br>
												 		<div class="col-md-12">
												 			<div class="col-md-12">
												  				<button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Update Tanda Terima</button>
											  				</div>
											  			</div>
												 	</div>	
												</div> 
						            		</div>
										</div>
									</div>
								</div>	
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	</section>

	<!-- Footer -->
	<footer class="mini-footer">
	    <div class="container container-footer">
	    	<div class="row">
	        	<div class="col-md-6 col-sm-6">
	            <div class="bptik-copy hide-mini-footer">
	            Musi Heart Clinic
	            </div>
	            <div class="bptik-reserved  hide-mini-footer">
	            Surabaya
	            </div>
	            </div>  
	        </div>
	    </div>
	</footer>

	<!-- Script -->
	<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	<script>
		$('.navbar-toggle-side').click(function(e){
			toggleSide(e,false)
			});
		$('.navbar-side-to-search').click(function(e){
			toggleSide(e,true)
		});
		function toggleSide(action,search){
			action.preventDefault();
			$('.navbar-side').toggleClass('mini-side');
			$('footer').toggleClass('mini-footer');
			$('#wrap').toggleClass('mini-side-open');
			if(search)$('.side-search-input').focus();
		}
		function ShowMenuNavJadwal(nama){
			if(nama != "-1"){
				$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
				$(nama).toggleClass("in").toggleClass("fadeInRight");
			}else{
				$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
			}		
		}
	</script>
	<script type="text/javascript">
		var controller = 'g_tanda_terima_yunan';
		var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
		var id_tanda_terima = $("#id_tanda_terima").val();

		$.ajax({
			'url' : base_url + controller + '/show_table',
			'type' : 'POST', //the way you want to send data to your URL
			'data' : {'id_tanda_terima' : id_tanda_terima},
			'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
				var container = $('#tabel_layanan'); //jquery selector (get element by id)
				if(data){
					container.html(data);
					var temp = $('#temptotalharga').val();
					var total_pembayaran = $('#total_pembayaran').val();
					$('#total_bayar').val(temp-total_pembayaran);
					$("#rp_total_bayar").html('Rp '+formatRupiah(temp-total_pembayaran));
					$('#kurang').val(temp-total_pembayaran);
					$('#rp_kurang').val(formatRupiah(temp-total_pembayaran));
					hitung_kurangbayar();
					
					$('#nomor_faktur').val("");
					$('#tgl_faktur').val("");
					$('#jatuh_tempo').val("");
					$('#netto').val("");
				}
			}
		});

		function formatRupiah(nilaiUang2)
		{
			var nilaiUang=nilaiUang2+"";
			var nilaiRupiah 	= "";
			var jumlahAngka 	= nilaiUang.length;

			while(jumlahAngka > 3)
			{

			sisaNilai = jumlahAngka-3;
			nilaiRupiah = "."+nilaiUang.substr(sisaNilai,3)+""+nilaiRupiah;

			nilaiUang = nilaiUang.substr(0,sisaNilai)+"";
			jumlahAngka = nilaiUang.length;
			}

			nilaiRupiah = nilaiUang+""+nilaiRupiah+",-";
			return nilaiRupiah;
		}

		function hitung_kurangbayar()
		{
			var bayar = $('#bayar').val();
			var total_bayar = $('#total_bayar').val();
			var kurangbayar = total_bayar - bayar;
			$('#kurang').val(kurangbayar);
			$('#rp_kurang').val(formatRupiah(kurangbayar));
		}
		function label_bayar() 
		{
		 var bayar=$('#bayar').val();
	     $("#label_bayar").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp'+formatRupiah(bayar));
		}

		$(document).ready(function()
		{
			label_bayar();

			$( "#form1" ).submit(function( event ) {
  
				var tgl_tanda_terima=$('#tgl_tanda_terima').val();
				var no_tanda_terima=$('#no_tanda_terima').val();
				var check_tanda_terima=$('#check_tanda_terima').val();

				if(no_tanda_terima==0 || no_tanda_terima=="" || no_tanda_terima=="0")
				{
					alert("Nomor Tanda Terima Belum Terisi");
					event.preventDefault();
				}
				else if(tgl_tanda_terima==0 || tgl_tanda_terima=="" || tgl_tanda_terima=="0")
				{
					alert("Tanggal Tanda Terima Belum terisi");
					event.preventDefault();
				}
				else if(check_tanda_terima==1 || check_tanda_terima=="1")
				{
					alert("Nomor Tanda Terima ini Sudah Pernah Didaftarkan!!");
					event.preventDefault();
				}
			});

			var data3;

			$('#nomor_faktur').keyup(function()
			{
				if($(this).val()!="")
				{
					$.ajax({
						type: "POST",
						url: "<?php echo base_url('g_tanda_terima_yunan/ajaxTypeheadFaktur') ;?>",
						data: { src_param: $(this).val() },
						cache: true,
						success:
							function(result)
							{
								data3 = JSON.parse(result);
							}
						});
				}
			});

			$('#nomor_faktur').typeahead({
			    source: function (query, process) {
			        states4 = [];
					map4 = {};
					
					var source = [];
					$.each(data3, function (i, state) {
						map4[state.stateName] = state;
						states4.push(state.stateName);
					});
				 
					process(states4);
			    },
			    updater: function (item) {
			        
					selectedState = map4[item].stateCode;
					selectedState2 = map4[item].stateDisplay;
					tgl_faktur = map4[item].tgl_faktur;
					jatuh_tempo = map4[item].jatuh_tempo;
					netto = map4[item].total;
					
					$("#nomor_faktur").val(selectedState);
					$("#tgl_faktur").val(tgl_faktur);
					$("#jatuh_tempo").val(jatuh_tempo);
					$("#netto").val(formatRupiah(netto));
					return selectedState2;
			    },
			    matcher: function (item) {
			        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
						return true;
					}
			    },
			    sorter: function (items) {
			        return items.sort();
			    },
			    highlighter: function (item) {
					var regex = new RegExp( '(' + this.query + ')', 'gi' );
					return item.replace( regex, "<strong>$1</strong>" );
					
			    },
			});
	
			$('#no_tanda_terima').keyup(function() {
				var reg=$('#no_tanda_terima').val();
				
				var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
				$.ajax({
					'url' : base_url + 'g_tanda_terima_yunan/check_tanda_terima',
					'type' : 'POST', //the way you want to send data to your URL
					'data' : {'reg' : reg},
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
						if(data>0){
							$('#check_tanda_terima').addClass("alert-error");
							$("#check_tanda_terima").removeClass("alert-info");
							$("#check_tanda_terima").html("Nomor ini sudah didaftarkan");
							$("#check_tanda_terima").val(1);
							$('#no_tanda_terima').focus();
						}
						else
						{
							$('#check_tanda_terima').addClass("alert-info");
							$("#check_tanda_terima").removeClass("alert-error");
							$("#check_tanda_terima").html("");
							$("#check_tanda_terima").val(0);
						}
					}
				});
			});

			$('#tgl_tanda_terima').datepicker()
			.on('changeDate', function(ev){

			});

			$('#bayar').keyup(function() {
				label_bayar();
				hitung_kurangbayar();
			});
	
		});
		
		var controller = 'g_tanda_terima_yunan';
		var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';

		function load_data_ajax()
		{
			var reg='0';
			var nomor_faktur=$('#nomor_faktur').val();
			var tgl_faktur=$('#tgl_faktur').val();
			var jatuh_tempo=$('#jatuh_tempo').val();
			var netto=$('#netto').val();
			var keterangan=$('#keterangan').val();

			var sp = 0;
			var fak_pajak = 0;
			var sj = 0;
			var fak_beli = 0;
			var kwitansi = 0;
			var verified = 0;

			if (nomor_faktur==0 || nomor_faktur=="")
			{
				alert('Masukkan Nomor Faktur');
			}
			else if(document.getElementById("verified").checked == false)
			{
				alert('Verified belum dicentang');
			}
			
			else
			{	
				if(document.getElementById("sp").checked == true)
				{
					var sp = 1;
				}
				if(document.getElementById("fak_pajak").checked == true)
				{
					var fak_pajak = 1;
				}
				if(document.getElementById("sj").checked == true)
				{
					var sj = 1;
				}
				if(document.getElementById("fak_beli").checked == true)
				{
					var fak_beli = 1;
				}
				/*
				if(document.getElementById("kwitansi").checked == true)
				{
					var kwitansi = 1;
				}
				*/
				if(document.getElementById("verified").checked == true)
				{
					var verified = 1;
				}

				$.ajax({
					'url' : base_url + controller + '/insert_faktur',
					'type' : 'POST', //the way you want to send data to your URL
					'data' : {'reg' : reg, 'nomor_faktur' : nomor_faktur, 'tgl_faktur': tgl_faktur, 'jatuh_tempo' : jatuh_tempo, 'netto' : netto, 'sp' : sp, 'fak_pajak' : fak_pajak, 'sj' : sj, 'fak_beli' : fak_beli, 'kwitansi' : kwitansi, 'verified' : verified, 'keterangan' : keterangan},
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
						var container = $('#tabel_layanan'); //jquery selector (get element by id)
						if(data){
							container.html(data);
							var temp = $('#temptotalharga').val();
							$('#total_bayar').val(temp);
							$("#rp_total_bayar").html('Rp '+formatRupiah(temp));
							$('#kurang').val(temp);
							$('#rp_kurang').val(formatRupiah(temp));
							hitung_kurangbayar();
							
							$('#nomor_faktur').val("");
							$('#tgl_faktur').val("");
							$('#jatuh_tempo').val("");
							$('#netto').val("");
						}
					}
				});
			}
		}
		function delete_faktur(nomor_faktur, tgl_faktur, total)
		{
			$.ajax({
				'url' : base_url + controller + '/delete_faktur',
				'type' : 'POST', //the way you want to send data to your URL
				'data' : {'nomor_faktur' : nomor_faktur, 'tgl_faktur' : tgl_faktur, 'total':total},
				'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					var container = $('#tabel_layanan'); //jquery selector (get element by id)
					if(data){
						container.html(data);
						var temp = $('#temptotalharga').val();
						$('#total_bayar').val(temp);
						$("#rp_total_bayar").html('Rp '+formatRupiah(temp));
						hitung_kurangbayar();
					}
				}
			});
		}

		function edit_faktur()
		{
			var id_tanda_terima = $('#id_tanda_terima').val();
			var id_pembelian = $('#id_pembelian_modal').val();
			var keterangan = $('#keterangan_modal').val();
			var sp = 0;
			var sj = 0;
			var faktur_pajak = 0;
			var faktur_beli = 0;
			var verified = 0;

			if(document.getElementById("verified_modal").checked == false)
			{
				alert('Verified belum dicentang');
			}
			else
			{
				if(document.getElementById("sp_modal").checked == true )
				{
					sp = 1;
				}
				if(document.getElementById("sj_modal").checked == true )
				{
					sj = 1;
				}
				if(document.getElementById("fak_pajak_modal").checked == true )
				{
					faktur_pajak = 1;
				}
				if(document.getElementById("fak_beli_modal").checked == true )
				{
					faktur_beli = 1;
				}
				if(document.getElementById("verified_modal").checked == true )
				{
					verified = 1;
				}

				$.ajax({
					'url' : base_url + controller + '/edit_faktur',
					'type' : 'POST', //the way you want to send data to your URL
					'data' : {'id_tanda_terima' : id_tanda_terima, 'id_pembelian' : id_pembelian, 'sp' : sp, 'sj': sj, 'faktur_pajak' : faktur_pajak, 'faktur_beli' : faktur_beli, 'verified' : verified, 'keterangan' : keterangan},
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
						var container = $('#tabel_layanan'); //jquery selector (get element by id)
						if(data){
							container.html(data);
							var temp = $('#temptotalharga').val();
							$('#total_bayar').val(temp);
							$("#rp_total_bayar").html('Rp '+formatRupiah(temp));
							$('#kurang').val(temp);
							$('#rp_kurang').val(formatRupiah(temp));
							hitung_kurangbayar();
							$('#nomor_faktur').val("");
							$('#tgl_faktur').val("");
							$('#jatuh_tempo').val("");
							$('#netto').val("");
							//$('#modal').modal('hide');
						}
					}
				});

			}
		}

	</script>
</body>
</html>