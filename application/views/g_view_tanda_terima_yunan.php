<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

function get_month($month)
{
	$labelmonth="Januari";

	if($month==2)
	$labelmonth="Februari";
	else if($month==3)
	$labelmonth="Maret";
	else if($month==4)
	$labelmonth="April";
	else if($month==5)
	$labelmonth="Mei";
	else if($month==6)
	$labelmonth="Juni";
	else if($month==7)
	$labelmonth="Juli";
	else if($month==8)
	$labelmonth="Agustus";
	else if($month==9)
	$labelmonth="September";
	else if($month==10)
	$labelmonth="Oktober";
	else if($month==11)
	$labelmonth="November";
	else if($month==12)
	$labelmonth="Desember";

	return $labelmonth;

}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head -->
<head>
	<!-- Meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>MUSI APPS</title>
	<meta content="MUSI Apps" name="description">
	<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
	<meta content="MUSI" name="author">
	<meta content="yes" name="apple-mobile-web-app-capable">
	<meta content="black" name="apple-mobile-web-app-status-bar-style">
	<!-- Style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
	<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
	<link href="<?php echo base_url('assets/css/DT_bootstrap.css') ?>" rel="stylesheet">
	<!-- Icon -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>

<body style="">
	<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<section id="wrap">
	<section class="content content-white">
		<div class="container container-content"> 	
			    <div class="row">
				    <div class="col-md-12">
						<legend style="text-align:center;">DAFTAR TANDA TERIMA</legend>
						<h2>
							<a class="btn btn-primary btn-success" href="<?php echo site_url("g_tanda_terima_yunan/create_tanda_terima"); ?>">Buat Tanda Terima Baru</a>
						</h2>
						<?php if($status==1) 
						{
						?>
						<div class="alert alert-success">
						Data Berhasil Dimasukkan!!
						</div>
						<?php } ?>
						
						<?php if($status==2) 
						{
						?>
						<div class="alert alert-info">
						Data Berhasil Diupdate!!
						</div>
						<?php } ?>
						
						<?php if($status==3) 
						{
						?>
						<div class="alert alert-danger">
						Data Berhasil Dihapus!!
						</div>
						<?php } ?>
						<?php
							// echo "<pre>"; 
							// var_dump($pembelian_sort);
							// echo "</pre>";
						?>
							
						<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
							<thead>
								<tr>
									<th>No</th>
									<th>No Tanda Terima</th>
									<th>Tanggal</th>
									<th>Supplier</th>
									<th>Total</th>
									<th>Kekurangan</th>
									<th>Status</th>
									<th>Edit</th>
									<th>Delete</th>
									<th>Print</th>
								</tr>
							</thead>
							<tbody>
							<?php
							foreach ($tanda_terima as $a): ?>
							<?php
								$tgl = explode("-", $a['tanggal']);
								$no = 1;
							?>
							<tr class="odd gradeX">
								<td><?php echo $no ?></td>
								<td><?php echo $a['no_tanda_terima'] ?></td>
								<td><?php echo $tgl[2].' '.get_month($tgl[1]).' '.$tgl[0]; ?></td>
								<td><?php echo $a['nama_supplier'] ?></td>
								<td><?php echo formatRupiah($a['total']) ?></td>
								<td><?php echo formatRupiah($a['kurangbayar']); ?></td>
								<td>
									<?php 
										if($a['status'] == 2)
										{
											echo '<span class="label label-success">Lunas</span>';
										} 
										else
										{
											echo '<span class="label label-warning">Belum Lunas</span>';
										}
									?>
								</td>
								<td class="center"> <a class="btn btn-primary" href="<?php echo site_url("g_tanda_terima_yunan/edit_tanda_terima/".$a['id']); ?>"><i class="fa fa-edit"></i>Edit</a> </td>
								<td class="center"><a class="btn btn-danger" href="<?php echo site_url("g_tanda_terima_yunan/delete_tanda_terima/".$a['id']); ?>"><i class="fa fa-trash-o"></i>Delete</a></td>
								<?php
								if($a['status'] == 2)
								{ ?>
									<td class="center"><a class="btn btn-success" href="<?php echo site_url("g_tanda_terima_yunan/cetak_tanda_terima/".$a['id']); ?>"><i class="fa fa-print"></i>Print</a></td>
								<?php }
								else
								{?>
									<td></td>
								<?php }
								?>
							</tr>
						
							<?php 
							$no++;
							endforeach ?>	
							</tbody>
						</table>    	
				</div>
			</div>
		</div>
	</section>
</section>
<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/js/DT_bootstrap.js') ?>"></script>

<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}
$(document).ready(function(){
	$('#example').dataTable(
	{
	  "bSort" : false,
	  "bDestroy": true
	});
});
</script>
</body>
</html>