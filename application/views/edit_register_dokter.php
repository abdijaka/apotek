<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">REGISTER DOKTER</legend>
		<form class="form-horizontal" action="<?php echo site_url("dokter/update_dokter"); ?>" method="POST">
		    <input type="hidden" id="inputID" name="inputID" placeholder="Nomor ID dokter" value="<?php echo $dokter['id_dokter'] ?>">
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">NAMA DOKTER :</label>
					<div class="col-lg-10">
					<div class="col-lg-2">
					<select name="initial" id="initial" class="input-small">
					<option value="0" <?php if ($dokter['initial']==0) echo "selected";  ?>>Tn.</option>
					<option value="1" <?php if ($dokter['initial']==0) echo "selected";  ?>>Ny.</option>
					
					</select>
					</div>
					<div class="col-lg-5">
					  <input type="text" id="inputNama" name="inputNama" placeholder="Nama dokter" value="<?php echo $dokter['nama_dokter'] ?>">
					</div>  
					<div class="col-lg-2">
					  <label class="checkbox inline">
						<input type="checkbox" id="member" name="member" value="1" <?php if ($dokter['internal']==1) echo "checked";  ?> > Internal
					  </label>
					</div>
					</div>
			</div>

			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">NOMOR SIP :</label>
					<div class="col-lg-10">
					<div class="col-lg-7">
						<input type="text" id="nomorsip" name="nomorsip" placeholder="Nomor SIP" value="<?php echo $dokter['nomor_sip'] ?>" >
					</div>
					
					</div>
			</div>
			
			<?php
				
				$date=date("d-m-Y");
				$tgl = new DateTime($dokter['tanggal_lahir']);
				
			?>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">TANGGAL LAHIR :</label>
					<div class="col-lg-10">
					<div class="col-lg-2">
						<input type="text" value="<?php echo $tgl->format('d-m-Y') ?>" name="tanggallahir" id="tanggallahir" data-date-format="dd-mm-yyyy">
					</div>
					<div class="col-lg-5">
					  <select name="jeniskelamin" id="jeniskelamin" class="input-medium">
					  <option value="0" <?php if ($dokter['jenis_kelamin']==0) echo "selected";  ?>>Laki-laki</option>
					  <option value="1" <?php if ($dokter['jenis_kelamin']==1) echo "selected";  ?>>Perempuan</option>
					</select>
					</div>  
					<div class="col-lg-2">
					  
					</div>
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">ALAMAT :</label>
					<div class="col-lg-10">
					<div class="col-lg-7">
						<textarea name="alamat" id="alamat" rows="4" ><?php echo $dokter['alamat'] ?></textarea>
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">KOTA :</label>
					<div class="col-lg-10">
					<div class="col-lg-4">
						<input type="text" id="inputKota" name="inputKota" class="input-xlarge" placeholder="Kota" value="<?php echo $dokter['kota'] ?>" >
					</div>
					<div class="col-lg-3">
						<input type="text" id="inputKodepos" name="inputKodepos"  placeholder="Kode Pos" value="<?php echo $dokter['kodepos'] ?>" >
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">KONTAK :</label>
					<div class="col-lg-10">
					<div class="col-lg-3">
						<input type="text" id="inputEmail" name="inputEmail" placeholder="Email" value="<?php echo $dokter['email'] ?>" >
					</div>
					<div class="col-lg-2">
						<input type="text" id="inputTelepon" name="inputTelepon"  placeholder="Telepon" value="<?php echo $dokter['telepon'] ?>" >
					</div>
					<div class="col-lg-2">
						<input type="text" id="hp" name="hp"  placeholder="Nomor HP" value="<?php echo $dokter['hp'] ?>" >
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">PERUSAHAAN :</label>
					<div class="col-lg-10">
					<div class="col-lg-7">
						<input type="text" id="inputPerusahaan" name="inputPerusahaan"  placeholder="Perusahaan" value="<?php echo $dokter['perusahaan'] ?>" >
					</div>
					
					</div>
			</div>
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">KETERANGAN :</label>
					<div class="col-lg-10">
					<div class="col-lg-7">
						<input type="text" id="keterangan" name="keterangan" placeholder="Keterangan" value="<?php echo $dokter['keterangan'] ?>" >
					</div>
					
					</div>
			</div>
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">&nbsp;</label>
					<div class="col-lg-10">
					<div class="col-lg-7">
						<button type="submit" class="btn btn-info "><i class="fa fa-save  "></i> Simpan</button>
					</div>
					
					</div>
			</div>
		  
		  
		</form>
   
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>



<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>
<script type="text/javascript">
function dstrToUTC(ds) {
    var dsarr = ds.split("-");
     var dd = parseInt(dsarr[0],10);
     var mm = parseInt(dsarr[1],10);
     var yy = parseInt(dsarr[2],10);
     return Date.UTC(yy,mm-1,dd,0,0,0); 
	}
function datediff(ds1,ds2) 
	{
     var d1 = dstrToUTC(ds1);
     var d2 = dstrToUTC(ds2);
     var oneday = 86400000;
     return (d2-d1) / oneday;    
	}
function humanise (diff) {
  // The string we're working with to create the representation
  var str = '';
  // Map lengths of `diff` to different time periods
  var values = {
    'umur': 365, 
    'umur_bulan': 30, 
    'umur_hari': 1
  };

  // Iterate over the values...
  for (var x in values) {
    var amount = Math.floor(diff / values[x]);

    // ... and find the largest time value that fits into the diff
    if (amount >= 1) {
       // If we match, add to the string ('s' is for pluralization)
       str += amount + x + (amount > 1 ? 's' : '') + ' ';
	   $('#'+x).val(amount);
	   

       // and subtract from the diff
       diff -= amount * values[x];
    }
	else
	{
		$('#'+x).val(0);
	}
  }

  return str;
}
	
$(document).ready(function(){
$('#tanggallahir').datepicker()
		  .on('changeDate', function(ev){
			var tanggallahir=$('#tanggallahir').val();
			var now=$('#now').val();
			
			var diff=datediff(tanggallahir,now);
			humanise(diff);
		  });
});

$('#tanggallahir').keyup(function() {
			var tanggallahir=$('#tanggallahir').val();
			var now=$('#now').val();
			
			var diff=datediff(tanggallahir,now);
			humanise(diff);
});

$( "#initial" )
  .change(function () {
    var str = 0;
    $( "#initial option:selected" ).each(function() {
      str = $( this ).val();
    });
    if(str==0)
	{
		$("#jeniskelamin").val(0);
	}
	else if(str==1)
	{
		$("#jeniskelamin").val(1);
	}
  })
  .change();


</script>
</body></html>