<?php 
function terbilang($x)
{
$abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
if ($x < 12)
return " " . $abil[$x];
elseif ($x < 20)
return Terbilang($x - 10) . " Belas";
elseif ($x < 100)
return Terbilang($x / 10) . " Puluh" . Terbilang($x % 10);
elseif ($x < 200)
return " seratus" . Terbilang($x - 100);
elseif ($x < 1000)
return Terbilang($x / 100) . " Ratus" . Terbilang($x % 100);
elseif ($x < 2000)
return " seribu" . Terbilang($x - 1000);
elseif ($x < 1000000)
return Terbilang($x / 1000) . " Ribu" . Terbilang($x % 1000);
elseif ($x < 1000000000)
return Terbilang($x / 1000000) . " Juta" . Terbilang($x % 1000000);
}

function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

function get_month($month)
{
	$labelmonth="Januari";

	if($month==2)
	$labelmonth="Februari";
	else if($month==3)
	$labelmonth="Maret";
	else if($month==4)
	$labelmonth="April";
	else if($month==5)
	$labelmonth="Mei";
	else if($month==6)
	$labelmonth="Juni";
	else if($month==7)
	$labelmonth="Juli";
	else if($month==8)
	$labelmonth="Agustus";
	else if($month==9)
	$labelmonth="September";
	else if($month==10)
	$labelmonth="Oktober";
	else if($month==11)
	$labelmonth="November";
	else if($month==12)
	$labelmonth="Desember";

	return $labelmonth;

}

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once 'PHPExcel/Classes/PHPExcel.php';

require_once 'PHPExcel/Classes/PHPExcel/IOFactory.php';

$fileName = 'PHPExcel/Examples/templates/Kwitansi Apotek.xlsx';
try {
    $objPHPExcel = PHPExcel_IOFactory::load($fileName);
} catch (Exception $e) {
    die("Error loading file: ".$e->getMessage()."<br />\n");
}


//$tgl = new DateTime($penjualan['tgl_faktur']);
$pieces = explode("-", $penjualan['tgl_faktur']);


$objPHPExcel->getActiveSheet()->setCellValue('A8', "No : ".$penjualan['nomor_faktur']);
$objPHPExcel->getActiveSheet()->setCellValue('A10', "Tanggal : ".$pieces[2].' '.get_month($pieces[1]).' '.$pieces[0]."");
$objPHPExcel->getActiveSheet()->setCellValue('A11', "Nama Pasien : ".$penjualan['nama_pasien']);

$nomor=1;
$baseRow = 12;
$i=0;
$netto=0;
foreach ($list as $list_item): 
	$i++;
	$netto=$netto+$list_item['total'];
	
		$row = $baseRow + $nomor;
		$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);

		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $list_item['jumlah'])
								  ->setCellValue('B'.$row, $list_item['nama_item'])
								  ->setCellValue('C'.$row, formatRupiah($list_item['total']));

	

	

$nomor++;

endforeach;

$objPHPExcel->getActiveSheet()->setCellValue('C'.($row+1), formatRupiah($netto));
$objPHPExcel->getActiveSheet()->setCellValue('C'.($row+2), formatRupiah($penjualan['nominal_diskon']));
$objPHPExcel->getActiveSheet()->setCellValue('C'.($row+3), formatRupiah($penjualan['total']));

$objPHPExcel->getActiveSheet()->setCellValue('A'.($row+6), "Terbilang : ".terbilang($penjualan['total'])." Rupiah");

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Kwitansi Apotek');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Kwitansi_Penjualan_'.$penjualan['nomor_faktur'].'.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>