<?php

function html2ascii($s){
 // convert links
 $s = preg_replace('/<a\s+.*?href="?([^\" >]*)"?[^>]*>(.*?)<\/a>/i','$2 ($1)',$s);
 
 // convert p, br and hr tags
 $s = preg_replace('@<(b|h)r[^>]*>@i',"\n",$s);
 $s = preg_replace('@<p[^>]*>@i',"\n\n",$s);
 $s = preg_replace('@<div[^>]*>(.*)</div>@i',"\n".'$1'."\n",$s);  
  
 // convert bold and italic tags
 $s = preg_replace('@<b[^>]*>(.*?)</b>@i','*$1*',$s);
 $s = preg_replace('@<strong[^>]*>(.*?)</strong>@i','*$1*',$s);
 $s = preg_replace('@<i[^>]*>(.*?)</i>@i','_$1_',$s);
 $s = preg_replace('@<em[^>]*>(.*?)</em>@i','_$1_',$s);
   
 // decode any entities
 $s = strtr($s,array_flip(get_html_translation_table(HTML_ENTITIES)));
 
 // decode numbered entities
 $s = preg_replace('/&#(\d+);/e','chr(str_replace(";","",str_replace("&#","","$0")))',$s);
 
 // strip any remaining HTML tags
 $s = strip_tags($s);
 
 // return the string
 return $s;
}

function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

if("".$transaksi['baru']==0)
$label_baru="L";
else
$label_baru="B";

if($transaksi['tipebayar']==1)
$carabayar="TUNAI";
else if($transaksi['tipebayar']==2)
$carabayar="KREDIT";
else
$carabayar="DEBIT";

if($transaksi['initial']==0)
$initial="TN.";
else if($transaksi['initial']==1)
$initial="NY.";
else if($transaksi['initial']==2)
$initial="NN.";
else if($transaksi['initial']==3)
$initial="ANAK";
else
$initial="BAYI";

$tanggal =date('j-m-Y', strtotime($transaksi['tanggal']));
$jam =date('G:i:s', strtotime($transaksi['tanggal']));
?>
<div class="container">

      <form class="form-horizontal" id="form1" >
			<?php
			$date=date("d-m-Y");
			?>
			
		<div class="control-group">
			<label class="control-label" for="inputTanggal">Tanggal</label>
			<div class="controls control-row">
				
				<input class="span2" size="16" type="text" value="<?php echo $tanggal; ?>" readonly>
				
				<div class="input-prepend" >
					<span class="add-on">Kwitansi Nomor : </span>
					<input type="text" value="<?php echo "No: ".$transaksi['nomor_registrasi']." (".$label_baru.")"; ?>" readonly>
					
				</div>
				
			</div>
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputID">Nomor ID Pasien</label>
			<div class="controls control-row">
			  <input type="text" value="<?php echo $transaksi['pasien']; ?>" readonly>
			  <input type="text"  readonly value="<?php echo $initial." ".strtoupper($transaksi['nama_pasien']); ?>">
			  
			</div>
			
		  </div>
		  
		  <div class="control-group">
			<label class="control-label" for="inputID">Dokter Pengirim</label>
			<div class="controls control-row">
			  
					<input type="text" value="<?php echo $transaksi['nama_dokter']; ?>" readonly>
					
					
			</div>
			
		  </div>
		  
		  <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
		  
		  <div class="alert alert-info">
			<table class="table table-bordered table-condensed " >
			<thead>
			<tr style="background-color: #CC6600; color:#fff; font-size:16px;">
			  <th>No. </th>
			  <th>Pemeriksaan</th>
			  <th>Dokter</th>
			  <th>Harga</th>
			  <th>Diskon</th>
			  <th>Netto</th>
			</tr>
		  </thead>
			<tbody>
			<?php
			$i=0;
			$netto=0;
				foreach ($detail as $list_item): 
				$i++;
				$netto=$netto+$list_item['netto'];
				?>
				
				<tr style="background-color: #fff;">
				  <td style="text-align:center;"><?php echo $i; ?></td>
				  <td><?php echo $list_item['nama_layanan']; ?></td>
				  <td><?php echo $list_item['nama_dokter']; ?></td>
				  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($list_item['harga']); ?></p> </td>
				  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($list_item['nominal_diskon']); ?></p></td>
				  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($list_item['netto']); ?></p></td>
				  
				  
				  
				</tr>
			<?php 
			endforeach ?>	
				<tr style="">
				  <td colspan="2" style="text-align:center; color:#CC0000; font-weight:bold; font-size:16px;">Sub Total</td>
				  <td colspan="4" style="text-align:right;  font-size:20px; color:#CC0000; font-weight:bold;"><p class="text-right"><?php echo formatRupiah($netto); ?></p></td>
				  
				</tr>
			  </tbody>
		</table>
		
		<div class="control-group">
						<label class="control-label" for="totalharga">Sub Total</label>
						<div class="controls control-row">
						  <input type="text" class="input-small" readonly value="<?php echo formatRupiah($netto); ?>">
							<div class="input-prepend input-apppend" >
								<span class="add-on">Diskon</span>
								
								<input readonly type="text" id="totalnominaldiskon" name="totalnominaldiskon"  class="input-small" value="<?php echo formatRupiah($transaksi['nominal_diskon']); ?>">
								
							</div>
							
						</div>
						
					  </div>
		<div class="control-group" >
						<label class="control-label" for="totalharganetto">Harga Netto</label>
						<div class="controls">
						  
								<p id="labelnetto" style="color:#CC0000; font-size:32px; font-weight:bold;"> <?php echo formatRupiah($transaksi['harga_netto']); ?> </p>
								
						</div>
						
					  </div>
					  
		<div class="control-group" >
			<table class="table table-bordered table-condensed " >
			<thead>
			<tr style="background-color: #CC6600; color:#fff; font-size:16px;">
			  <th>Tanggal</th>
			  <th>Jumlah Pembayaran</th>
			  <th>Cara Bayar</th>
			  
			</tr>
		  </thead>
			<tbody>
			<?php
			$i=0;
			$netto=0;
			
				foreach ($pembayaran as $pembayaran_item): 
				$tanggal_bayar =date('j-m-Y', strtotime($pembayaran_item['tanggal']));
				$jam_bayar =date('G:i', strtotime($pembayaran_item['tanggal']));
				?>
				
				<tr style="background-color: #fff;">
				  
				  <td><?php echo $tanggal_bayar.' '.$jam_bayar; ?></td>
				  <td><?php echo $pembayaran_item['namacarabayar']; ?></td>
				  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($pembayaran_item['jumlah']); ?></p> </td>
				  
				</tr>
			<?php 
			endforeach ?>	
				<tr style="">
				  <td colspan="1" style="text-align:center; color:#CC0000; font-weight:bold; font-size:16px;">Total Pembayaran</td>
				  <td colspan="2" style="text-align:right;  font-size:20px; color:#CC0000; font-weight:bold;"><p class="text-right"><?php echo formatRupiah($transaksi['bayar']); ?></p></td>
				  
				</tr>
			  </tbody>
		</table>
		
		</div>
		<div class="control-group" >
			<label class="control-label" for="totalharganetto">Sisa</label>
				<div class="controls">
				  
						<p id="labelnetto" style="color:#CC0000; font-size:32px; font-weight:bold;">Rp <?php echo strtoupper($transaksi['sisa']); ?>,- </p>
						
				</div>			
						
		</div> 
		  </div>
		  <div class="control-group" >
		  <div class="controls">
		  <a href="<?php echo site_url('transaksi/create_transaksi'); ?>" class="btn btn-large btn-info "><i class="icon-chevron-left icon-white"></i> Kembali</a>
		  <a href="<?php echo site_url('transaksi/cetak_kwitansi/'.$transaksi['nomor_registrasi']); ?>" target="_blank" class="btn btn-large btn-info "><i class="icon-print icon-white"></i> Cetak</a>
		  
		  <button onClick="print()" value="Print" class="btn btn-large btn-info ">Print</button>
   <input type=button onClick="findPrinter()" value="Detect Printer"><br />
		  <applet id="qz" name="QZ Print Plugin" code="qz.PrintApplet.class" width="55" height="55">
	  <param name="jnlp_href" value="<?php echo base_url('assets/qz/dist/qz-print_jnlp.jnlp') ?>">
          <param name="cache_option" value="plugin">
   </applet>
		</applet>
		  
		  </div>
		  </div>
		  
		</form>
		
		<table  id="simple-html-table" border="1" style="display:none">
		<tr><th>NO</th><th>TINDAKAN</th><th>BIAYA(RP)</th><th>DISC(RP)</th><th>NETTO(RP)</th></tr>
		<tbody>
			
			<?php
			$i=0;
			$netto=0;
			$harga=0;
			$diskon=0;
				foreach ($detail as $list_item): 
				$i++;
				$netto=$netto+$list_item['netto'];
				$harga=$harga+$list_item['harga'];
				$diskon=$diskon+$list_item['nominal_diskon'];
				?>
				
				<tr >
				  <td ><?php echo $i; ?></td>
				  <td><?php echo $list_item['nama_layanan']; ?></td>
				  
				  <td ><?php echo formatRupiah($list_item['harga']); ?> </td>
				  <td ><?php echo formatRupiah($list_item['nominal_diskon']); ?></td>
				  <td ><?php echo formatRupiah($list_item['netto']); ?></td>
				  
				</tr>
				<tr>
				<td  ></td>
				  <td  ><?php echo $list_item['nama_dokter']; ?></td>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
				</tr>  
			<?php 
			endforeach ?>	
				
		</tbody>
			<tr>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
			</tr>
			<tr>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
				  <td  ></td>
			</tr>
			<tr>
				  <td  ></td>
				  <td  >Sub Total</td>
				  <td  ><?php echo formatRupiah($harga); ?></td>
				  <td  ><?php echo formatRupiah($diskon); ?></td>
				  <td  ><?php echo formatRupiah($netto); ?></td>
				  
			
			</tr>
			<tr>
				  <td  ></td>
				  <td  >Total</td>
				  <td  ></td>
				  <td  ><?php echo formatRupiah($transaksi['nominal_diskon']) ?></td>
				  <td  ><?php echo formatRupiah($transaksi['harga_netto']); ?></td>
				  
			
			</tr>
	</table>
   <div id="tes" style="font-family: monospace; font-size: 12px;"></div>

</div>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.ascii.min.js') ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
			$("#tes").html($("#simple-html-table").ascii("raw"));
		});      
	var qz;   // Our main applet
      function findPrinter() {
         
         if (qz != null) {
            // Searches for locally installed printer with "zebra" in the name
            qz.findPrinter("Zebra");
         }
         
         // *Note:  monitorFinding() still works but is too complicated and
         // outdated.  Instead create a JavaScript  function called 
         // "jzebraDoneFinding()" and handle your next steps there.
         monitorFinding();
      }

      function findPrinters() {
         
         if (qz != null) {
            // Searches for locally installed printer with "zebra" in the name
            qz.findPrinter("\\{dummy printer name for listing\\}");
         }

         monitorFinding2();
      }

      function print() {
         
         if (qz != null) {
            // Send characters/raw commands to qz using "append"
            // This example is for EPL.  Please adapt to your printer language
            // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
            qz.append("\nN\n");            
            qz.append("q609\n");

            qz.append("Q203,26\n");
            
            qz.print(); // send commands to printer
	 }
	 
         // *Note:  monitorPrinting() still works but is too complicated and
         // outdated.  Instead create a JavaScript  function called 
         // "jzebraDonePrinting()" and handle your next steps there.
	 monitorPrinting();
         
         /**
           *  PHP PRINTING:
           *  // Uses the php `"echo"` function in conjunction with qz-print `"append"` function
           *  // This assumes you have already assigned a value to `"$commands"` with php
           *  qz.append(<?php echo $commands; ?>);
           */
           
         /**
           *  SPECIAL ASCII ENCODING
           *  //qz.setEncoding("UTF-8");
           *  qz.setEncoding("Cp1252"); 
           *  qz.append("\xDA");
           *  qz.append(String.fromCharCode(218));
           *  qz.append(chr(218));
           */
         
      }
      
      
      function chr(i) {
         return String.fromCharCode(i);
      }
      
      // *Note:  monitorPrinting() still works but is too complicated and
      // outdated.  Instead create a JavaScript  function called 
      // "jzebraDonePrinting()" and handle your next steps there.
      function monitorPrinting() {
	
	if (qz != null) {
	   if (!qz.isDonePrinting()) {
	      window.setTimeout('monitorPrinting()', 100);
	   } else {
	      var e = qz.getException();
	      alert(e == null ? "Printed Successfully" : "Exception occured: " + e.getLocalizedMessage());
              qz.clearException();
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
      
      function monitorFinding() {
	
	if (qz != null) {
	   if (!qz.isDoneFinding()) {
	      window.setTimeout('monitorFinding()', 100);
	   } else {
	      var printer = qz.getPrinter();
              alert(printer == null ? "Printer not found" : "Printer \"" + printer + "\" found");
	   }
	} else {
            alert("Applet not loaded!");
        }
      }

      function monitorFinding2() {
	
	if (qz != null) {
	   if (!qz.isDoneFinding()) {
	      window.setTimeout('monitorFinding2()', 100);
	   } else {
              var printersCSV = qz.getPrinters();
              var printers = printersCSV.split(",");
              for (p in printers) {
                  alert(printers[p]);
              }
              
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
      
      // *Note:  monitorAppending() still works but is too complicated and
      // outdated.  Instead create a JavaScript  function called 
      // "jzebraDoneAppending()" and handle your next steps there.
      function monitorAppending() {
	
	if (qz != null) {
	   if (!qz.isDoneAppending()) {
	      window.setTimeout('monitorAppending()', 100);
	   } else {
	      qz.print(); // Don't print until all of the data has been appended
              
              // *Note:  monitorPrinting() still works but is too complicated and
              // outdated.  Instead create a JavaScript  function called 
              // "jzebraDonePrinting()" and handle your next steps there.
              monitorPrinting();
	   }
	} else {
            alert("Applet not loaded!");
        }
      }

      // *Note:  monitorAppending2() still works but is too complicated and
      // outdated.  Instead create a JavaScript  function called 
      // "jzebraDoneAppending()" and handle your next steps there.
      function monitorAppending2() {
	
	if (qz != null) {
	   if (!qz.isDoneAppending()) {
	      window.setTimeout('monitorAppending2()', 100);
	   } else {
	      qz.printPS(); // Don't print until all of the image data has been appended
              
              // *Note:  monitorPrinting() still works but is too complicated and
              // outdated.  Instead create a JavaScript  function called 
              // "jzebraDonePrinting()" and handle your next steps there.
              monitorPrinting();
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
      
      // *Note:  monitorAppending3() still works but is too complicated and
      // outdated.  Instead create a JavaScript  function called 
      // "jzebraDoneAppending()" and handle your next steps there.
      function monitorAppending3() {
	
	if (qz != null) {
	   if (!qz.isDoneAppending()) {
	      window.setTimeout('monitorAppending3()', 100);
	   } else {
	      qz.printHTML(); // Don't print until all of the image data has been appended
              
              
              // *Note:  monitorPrinting() still works but is too complicated and
              // outdated.  Instead create a JavaScript  function called 
              // "jzebraDonePrinting()" and handle your next steps there.
              monitorPrinting();
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
      
      function useDefaultPrinter() {
         
         if (qz != null) {
            // Searches for default printer
            qz.findPrinter();
         }
         
         monitorFinding();
      }
      
      function jzebraReady() {
          // Change title to reflect version
          qz = document.getElementById('qz');
          
      }
      
      /**
       * By default, jZebra prevents multiple instances of the applet's main 
       * JavaScript listener thread to start up.  This can cause problems if
       * you have jZebra loaded on multiple pages at once. 
       * 
       * The downside to this is Internet Explorer has a tendency to initilize the
       * applet multiple times, so use this setting with care.
       */
      function allowMultiple() {
          
          if (qz != null) {
              var multiple = qz.getAllowMultipleInstances();
              qz.allowMultipleInstances(!multiple);
              alert('Allowing of multiple applet instances set to "' + !multiple + '"');
          }
      }
      
      function printPage() {
           $("#content").html2canvas({ 
                canvas: hidden_screenshot,
                onrendered: function() {printBase64Image($("canvas")[0].toDataURL('image/png'));}
           });
      }
      
      function printBase64Image(base64data) {
         
      	 if (qz != null) {
               qz.findPrinter("\\{dummy printer name for listing\\}");
               while (!qz.isDoneFinding()) {
                    // Note, endless while loops are bad practice.
               }

               var printers = qz.getPrinters().split(",");
               for (i in printers) {
		    if (printers[i].indexOf("Microsoft XPS") != -1 || 
			printers[i].indexOf("PDF") != -1) {
			   qz.setPrinter(i);      
		    }	       
               }
               
               // No suitable printer found, exit
               if (qz.getPrinter() == null) {
                   alert("Could not find a suitable printer for printing an image.");
                   return;
               }

               // Optional, set up custom page size.  These only work for PostScript printing.
               // setPaperSize() must be called before setAutoSize(), setOrientation(), etc.
               qz.setPaperSize("8.5in", "11.0in");  // US Letter
               qz.setAutoSize(true);
               qz.appendImage(base64data);
	    }

            // Very important for images, uses printPS() insetad of print()
            // *Note:  monitorAppending2() still works but is too complicated and
            // outdated.  Instead create a JavaScript  function called 
            // "jzebraDoneAppending()" and handle your next steps there.
	    monitorAppending2();
      }

      function logFeatures() {
          if (document.jzebra != null) {
              
              var logging = qz.getLogPostScriptFeatures();
              qz.setLogPostScriptFeatures(!logging);
              alert('Logging of PostScript printer capabilities to console set to "' + !logging + '"');
          }
      }
   
      function useAlternatePrinting() {
          
          if (qz != null) {
              var alternate = qz.isAlternatePrinting();
              qz.useAlternatePrinting(!alternate);
              alert('Alternate CUPS printing set to "' + !alternate + '"');
          }
      }
	  
	  function listSerialPorts() {
		
                if (qz != null) {
			qz.findPorts();
            while (!qz.isDoneFindingPorts()) {} // wait
			var ports = qz.getPorts().split(",");
			for (p in ports) {
				if (p == 0) {
					document.getElementById("port_name").value = ports[p];
				}
				alert(ports[p]);
			}
		}
	  }
	  
	  function openSerialPort() {
		
                if (qz != null) {
                    qz.openPort(document.getElementById("port_name").value);
		}
	  }
          
          function closeSerialPort() {
		
                if (qz != null) {
                    qz.closePort(document.getElementById("port_name").value);
		}
	  }
          
          // Automatically gets fired with the port is finished opening (even if it fails to open)
          function jzebraDoneOpeningPort(portName) {
              
              if (qz != null) {
                  var e = qz.getException();
                  if (e != null) {
                      alert("Could not open port [" + portName + "] \n\t" + e.getLocalizedMessage());
                      qz.clearException();
                  } else {
                      alert("Port [" + portName +  "] is open!");
                  }
              }
          }
          
          // Automatically gets fired with the port is finished closing (even if it fails to close)
          function jzebraDoneClosingPort(portName) {
              
              if (qz != null) {
                  var e = qz.getException();
                  if (e != null) {
                      alert("Could not close port [" + portName + "] \n\t" + e.getLocalizedMessage());
                      qz.clearException();
                  } else {
                      alert("Port [" + portName +  "] closed!");
                  }
              }
          }
          
          function sendSerialData() {
		
                if (qz != null) {
                    // Beggining and ending patterns that signify port has responded
                    // chr(2) and chr(13) surround data on a Mettler Toledo Scale
                    qz.setSerialBegin(chr(2));
                    qz.setSerialEnd(chr(13));
                    // Baud rate, data bits, stop bits, parity, flow control
                    // "9600", "7", "1", "even", "none" = Default for Mettler Toledo Scale
                    qz.setSerialProperties("9600", "7", "1", "even", "none");
                    // Send raw commands to the specified port.
                    // W = weight on Mettler Toledo Scale
                    qz.send(document.getElementById("port_name").value, "\nW\n");
                    
                    var e = qz.getException();
                    if (e != null) {
                        alert("Could not send data:\n\t" + e.getLocalizedMessage());
                        qz.clearException();  
                    }
		}
	  }
          
          // Automatically gets called when the serial port responds with data
          function jzebraSerialReturned(portName, data) {
            if (data == null || data == "") {       // Test for blank data
                alert("No data was returned.")
            } else if (data.indexOf("?") !=-1) {    // Test for bad data
                alert("Device not ready.  Please wait.")
            } else {                                // Display good data
                alert("Port [" + portName + "] returned data:\n\t" + data);
            }
          }

   </script>
