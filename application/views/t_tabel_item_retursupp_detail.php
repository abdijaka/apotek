<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}
?>
<br>
<table class="table table-bordered table-condensed " >
	<thead>
	<tr style="background-color: #90CA77; color:#fff; font-size:14px;">
	  <th>No. </th>
	  <th>Item</th>
	  <th>Jumlah</th>
	  <th>Satuan</th>
	</tr>
  </thead>
	<tbody style="font-size:13px;">
	<?php
	$i=0;
	$netto=0;
		foreach ($list as $list_item): 
		$i++;
		?>
		
		<tr style="background-color: #fff;">
		  <td style="text-align:center;"><?php echo $i; ?></td>
		  <td><?php echo $list_item['nama_item']; ?></td>
		  <td><?php echo $list_item['jumlah']; ?></td>
		  <td><?php echo $list_item['satuan']; ?></td>
		  
		</tr>
	<?php 		
	endforeach ?>	
	  </tbody>
</table>