<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head -->
<head>
	<!-- Meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>MUSI APPS</title>
	<meta content="MUSI Apps" name="description">
	<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
	<meta content="MUSI" name="author">
	<meta content="yes" name="apple-mobile-web-app-capable">
	<meta content="black" name="apple-mobile-web-app-status-bar-style">
	<!-- Style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
	<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
	<!-- Icon -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>
<!-- Body -->
<body style="">
	<header>
		<?php
			$this->load->view('templates/menubar_kiri');
		?>
	</header>
	<!-- Content -->
	<section id="wrap">
		<section class="content content-white">
			<div class="container container-content">
				<div class="row">
					<div class="col-md-12">
						<legend style="text-align:center;">EDIT PEMBELIAN</legend>
						<form class="form-horizontal" id="form1" action="<?php echo site_url("g_pembelian/update_pembelian"); ?>" method="POST">
							<?php
								$date=date("d-m-Y");
								$tgl = new DateTime($pembelian['tgl_faktur']);
								$jatuh_tempo = new DateTime($pembelian['jatuh_tempo']);
							?>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-3">
										<label class="control-label" for="inputTanggal">Tanggal Faktur</label>
											<input type="text" value="<?php echo $tgl->format('d-m-Y'); ?>" name="tanggalfaktur" id="tanggalfaktur" data-date-format="dd-mm-yyyy">
									</div>	
									<div class="col-md-3">
											<label class="control-label" for="inputReg">No. Faktur</label>
											<input type="hidden" id="id_pembelian" name="id_pembelian" value="<?php echo $pembelian['id_pembelian'] ?>">
											<input type="text" id="nomorfaktur" name="nomorfaktur" value="<?php echo $pembelian['nomor_faktur'] ?>">
											<input type="hidden" id="tempreg" name="tempreg" value="<?php echo $pembelian['nomor_faktur'] ?>">
											<div id="peringatan" value="0">	
											</div>
									</div>
									<div class="col-md-3">
											<label class="control-label" for="inputReg">No. Delivery</label>
											<input type="hidden" id="no_delivery" name="no_delivery" value="<?php echo $pembelian['no_delivery'] ?>">
											<input type="text" id="no_delivery" name="no_delivery" value="<?php echo $pembelian['no_delivery'] ?>">
											<input type="hidden" id="tempdev" name="tempdev" value="<?php echo $pembelian['no_delivery'] ?>">
											<div id="peringatan_no_delivery" value="0">	
											</div>
									</div>		
									<div class="col-md-3">
										<label class="control-label" for="inputJatuhTempo">Jatuh Tempo</label>
										<input type="text" value="<?php echo date('d-m-Y', strtotime($pembelian['jatuh_tempo'])); ?>" name="inputJatuhTempo" id="inputJatuhTempo" data-date-format="dd-mm-yyyy">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-3">
									</div>
									<div class="col-md-3">
									</div>
									<div class="col-md-3">
									</div>
									<div class="col-md-3">
										<label class="control-label" for="inputID">Supplier</label>
										<select name="supplier" id="supplier" class="input-large">
											<?php foreach ($supplier as $supplier_item): ?>
											<option value="<?php echo $supplier_item['id_supplier']; ?>" <?php if($pembelian['supplier']==$supplier_item['id_supplier']) echo "selected"; ?>><?php echo $supplier_item['nama_supplier']; ?></option>
											<?php endforeach ?>
										</select>
									</div>	
								</div>
							</div>
							
							<hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-info">
										<div class="container-fluid" style="margin-top:-15px; ">
											<div class="row-fluid">
												<div class="col-md-4" style="background-color:#90CA77; padding-bottom:20px; padding-top:20px;">
													<div class="row">
														<div class="col-md-13">
															<div class="col-md-12">
																<label class="control-label" style="color:#fff;" for="pemeriksaan">Nama Item</label>
																<input type="hidden" id="id_item" name="id_item" >
																<input type="text" id="namaitem" name="namaitem" placeholder="Nama Item" autocomplete="off">
															</div>	
														</div>	
													</div>
													<div class="row">
														<div class="col-md-13">
														<div class="col-md-5">
														<label class="control-label" for="diskon" style="color:#fff;">Jumlah</label>
														<div class="input-group margin-bottom-sm">
														<input type="number" id="jumlah" name="jumlah" value="0">
														</div>
														</div>	
														<div class="col-md-7">
														<label class="control-label" for="diskon" style="color:#fff;">Satuan</label>
														<div class="input-group margin-bottom-sm">
														<input type="text" id="satuan" name="satuan" >
														</div>
														</div>	
														</div>	
													</div>
													<div class="row">
														<div class="col-md-13">
															<div class="col-md-6">
																<label class="control-label" for="harga" style="color:#fff;">Harga Satuan</label>
																<div class="input-group margin-bottom-sm">
																<span class="input-group-addon">Rp</span>
																<input type="text" id="harga_satuan" name="harga_satuan"  autocomplete="off" value="0">
																</div>
																<div id="rp_harga_satuan" class="alert-danger"></div>
															</div>		
															<div class="col-md-6">
																<label class="control-label" for="harga" style="color:#fff;">Sub Total</label>
																<div class="input-group margin-bottom-sm">
																	<span class="input-group-addon">Rp</span>
																	<input type="text" id="subtotal" name="subtotal"  autocomplete="off" value="0">
																</div>
																<div id="rp_subtotal" class="alert-danger">
																</div>
															</div>	
														</div>	
													</div>
													<div class="row">
														<div class="col-md-13">
															<div class="col-md-5">
															<label class="control-label" for="diskon" style="color:#fff;">Diskon</label>
															<div class="input-group margin-bottom-sm">
															<input type="text" id="diskon" name="diskon" autocomplete="off" value="0">
															<span class="input-group-addon">%</span>
															</div>
															</div>
														</div>	
														<div class="col-md-7">
															<label class="control-label" for="diskon">&nbsp;</label>
															<div class="input-group margin-bottom-sm">
																<span class="input-group-addon">Rp</span>
																<input type="text" id="nominal_diskon" name="nominal_diskon"  autocomplete="off" value="0">
															</div>
															<div id="rp_nominal_diskon" class="alert-danger">
															</div>
														</div>	
													</div>
													<div class="row">
														<div class="col-md-13">
															<div class="col-md-12">
																<label class="control-label" for="harganetto" style="color:#fff;">Netto</label>
																<div class="input-group margin-bottom-sm">
																	<span class="input-group-addon">Rp</span>
																	<input type="text" id="netto" name="netto"  autocomplete="off">
																</div>
																<div id="rp_netto" class="alert-danger">
																</div>
															</div>	
														</div>	
													</div>
													<div class="row">
														<div class="col-md-13">
														<div class="col-md-6">
														<label class="control-label" for="diskon" style="color:#fff;">Tanggal Masuk</label>
														<input type="text" value="<?php echo $date; ?>" name="tanggalmasuk" id="tanggalmasuk" data-date-format="dd-mm-yyyy">
														</div>	
														<div class="col-md-6">
														<label class="control-label" for="diskon" style="color:#fff;">Expired Date</label>
														<input type="text" value="<?php echo $date; ?>" name="expdate" id="expdate" data-date-format="dd-mm-yyyy">
														</div>	
														</div>	
													</div>
													<div class="row">
														<div class="col-md-13">
														<div class="col-md-12">
														<br>
														<button type="button" onclick="load_data_ajax(); return false;" id="tambah_layanan" class="btn btn-danger">Tambahkan <i class="fa fa-chevron-right"></i></button>
													  
														</div>	
														</div>	
													</div>
												</div>
												<div class="col-md-8">
													<div class="row">
													    <div class="col-md-12">
													    <div class="col-md-12">
													    <div id="tabel_layanan"></div>
													    </div>
													    </div>
												    </div>
												    <?php //var_dump($pembelian)?>
												    <div class="row">
														<div class="col-md-12">
														<div class="col-md-4">
														<label class="control-label" for="totalharga">Total</label>
														<div class="input-group margin-bottom-sm">
														<span class="input-group-addon">Rp</span>
														<input type="hidden" id="totalharga" name="totalharga" class="input-small" readonly value="<?php echo $pembelian['subtotal']; ?>">
														<input type="text" id="label_totalharga" name="label_totalharga" class="input-small" readonly value="<?php echo $pembelian['subtotal']; ?>">
														</div>
														</div>	
														
														<div class="col-md-4">
															<label class="control-label" for="totalharga">Diskon (%)</label>
															<div class="input-group margin-bottom-sm">
																<input type="text" id="diskon_total" name="diskon_total"  value="<?php echo $pembelian['diskon']; ?>" autocomplete="off">
																<span class="input-group-addon">%</span>
															</div>	
														</div>	
														
														<div class="col-md-4">
															<label class="control-label" for="totalharga">Nominal Diskon (Rp)</label>
															<div class="input-group margin-bottom-sm">
																<span class="input-group-addon">Rp</span>
																<input type="text" id="nominal_diskon_total" name="nominal_diskon_total"  class="input-small" value="<?php echo $pembelian['nominal_diskon']; ?>" autocomplete="off">
															</div>
													  		<div id="rp_nominal_diskon_total" class="alert-danger">
															</div>
														</div>	
														</div>	
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="col-md-4">
																<label class="control-label" for="totalharga">Total Setelah Didiskon</label>
																<div class="input-group margin-bottom-sm">
																	<span class="input-group-addon">Rp</span>
																	<input type="hidden" id="total_diskon" name="total_diskon" class="input-small" readonly value="0">
																	<input type="text" id="rp_total_diskon" name="rp_total_diskon" class="input-small" readonly value="0">
																</div>
															</div>	
															<div class="col-md-4">
																<label class="control-label" for="totalharga">PPN (%)</label>
																<div class="input-group margin-bottom-sm">
																	<input type="text" id="ppn_total" name="ppn_total"  value="<?php echo $pembelian['ppn']; ?>" autocomplete="off">
																	<span class="input-group-addon">%</span>
																</div>
															</div>	
															<div class="col-md-4">
																<label class="control-label" for="totalharga">Nominal PPN (Rp)</label>
																<div class="input-group margin-bottom-sm">
																	<span class="input-group-addon">Rp</span>
																	<input type="text" id="nominal_ppn_total" name="nominal_ppn_total"  class="input-small" value="<?php echo $pembelian['nominal_ppn']; ?>" autocomplete="off">
																</div>
																<div id="rp_nominal_ppn_total" class="alert-danger">
																</div>
															</div>	
														</div>	
												 	</div>
													<div class="row">
														<div class="col-md-12">
														<div class="col-md-12">
														<label class="control-label" for="netto_total">Harga Netto Total</label>
														<p id="rp_netto_total" style="color:#CC0000; font-size:32px; font-weight:bold;"> Rp 0 </p>
															<input type="hidden" id="netto_total" name="netto_total"  class="input-large" value="0" > 
														</div>	
														
														</div>	
													</div>
													<div class="row">
														<div class="col-md-12">
														<div class="col-md-12">
														<hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
														</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
														<div class="col-md-12">
														<label class="control-label" for="totalharga">&nbsp;</label><br>
														<button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Update Pembelian</button>
														</div>
														</div>
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			<div>
		</section>
	</section>
</body>
<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">    
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
        </div>
    </div>
</footer>
</html>

<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>

<script>
$('.navbar-toggle-side').click(function(e)
{
	toggleSide(e,false)
});
$('.navbar-side-to-search').click(function(e)
{
	toggleSide(e,true)
});
function toggleSide(action,search)
{
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}
function ShowMenuNavJadwal(nama)
{
	if(nama != "-1")
	{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}
	else
	{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}	
}
</script>

<script type="text/javascript">

var controller = 'g_pembelian';
var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';

function formatRupiah(nilaiUang2)
{
	var nilaiUang=nilaiUang2+"";
	var nilaiRupiah 	= "";
	var jumlahAngka 	= nilaiUang.length;

	while(jumlahAngka > 3)
	{

	sisaNilai = jumlahAngka-3;
	nilaiRupiah = "."+nilaiUang.substr(sisaNilai,3)+""+nilaiRupiah;

	nilaiUang = nilaiUang.substr(0,sisaNilai)+"";
	jumlahAngka = nilaiUang.length;
	}

	nilaiRupiah = nilaiUang+""+nilaiRupiah+",-";
	return nilaiRupiah;
}

function hitung_subtotal()
{
	var harga=$("#harga_satuan").val();
	var jumlah=$("#jumlah").val();
	var subtotal=harga*jumlah;
	$("#subtotal").val(subtotal);
	rp_subtotal();
	hitung_nominal_diskon();
	hitung_netto();
}
function hitung_netto()
{
	var subtotal=$("#subtotal").val();
	var nominal_diskon=$("#nominal_diskon").val();
	var netto=subtotal-nominal_diskon;
	$("#netto").val(netto);
	rp_netto();
}
function hitung_nominal_diskon()
{
	var diskon=$("#diskon").val();
	var subtotal=$("#subtotal").val();
	var nominal_diskon=subtotal*diskon/100;
	$("#nominal_diskon").val(nominal_diskon);
	rp_nominal_diskon();
	hitung_netto();
}
function hitung_diskon()
{
	var nominal_diskon=$("#nominal_diskon").val();
	var subtotal=$("#subtotal").val();
	var diskon=nominal_diskon*100/subtotal;
	$("#diskon").val(diskon);
}
function hitung_diskon_total()
{
	var nominal_diskon_total=$("#nominal_diskon_total").val();
	var totalharga=$("#totalharga").val();
	var diskon_total=nominal_diskon_total*100/totalharga;
	$("#diskon_total").val(diskon_total);
	
}
function hitung_nominal_diskon_total()
{
	var totalharga=$("#totalharga").val();
	var diskon_total=$("#diskon_total").val();
	var nominal_diskon_total=totalharga*diskon_total/100;
	$("#nominal_diskon_total").val(nominal_diskon_total);
	rp_nominal_diskon_total();
	hitung_total_diskon();
}
function hitung_total_diskon()
{
	var totalharga=$("#totalharga").val();
	var nominal_diskon_total=$("#nominal_diskon_total").val();
	var total_diskon=totalharga-nominal_diskon_total;
	$("#total_diskon").val(total_diskon);
	$("#rp_total_diskon").val(formatRupiah(total_diskon));
}
function hitung_ppn_total()
{
	var total_diskon=$("#total_diskon").val();
	var nominal_ppn_total=$("#nominal_ppn_total").val();
	var ppn_total=nominal_ppn_total*100/total_diskon;
	$("#ppn_total").val(ppn_total);
}
function hitung_nominal_ppn_total()
{
	var total_diskon=$("#total_diskon").val();
	var ppn_total=$("#ppn_total").val();
	var nominal_ppn_total=ppn_total*total_diskon/100;
	$("#nominal_ppn_total").val(nominal_ppn_total);
	rp_nominal_ppn_total();
}
function hitung_netto_total()
{
	var total_diskon=$("#total_diskon").val();
	var nominal_ppn_total=$("#nominal_ppn_total").val();
	var netto_total=total_diskon*1 + nominal_ppn_total*1;
	console.log(total_diskon);
	console.log(nominal_ppn_total);
	$("#netto_total").val(netto_total);
	$("#rp_netto_total").html('Rp '+formatRupiah(netto_total));
}

function rp_subtotal() 
{
	var rp_subtotal=$('#subtotal').val();
	$("#rp_subtotal").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp'+formatRupiah(Math.floor(rp_subtotal)));
}
function rp_netto() 
{
	var rp_netto=$('#netto').val();
	$("#rp_netto").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp'+formatRupiah(Math.floor(rp_netto)));
}
function rp_nominal_diskon()
{
	var rp_nominal_diskon=$("#nominal_diskon").val();
	$("#rp_nominal_diskon").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp'+formatRupiah(Math.floor(rp_nominal_diskon)));
}
function rp_nominal_diskon_total()
{
	console.log("rp_nominal_diskon_total");
	var rp_nominal_diskon_total=$("#nominal_diskon_total").val();
	$("#rp_nominal_diskon_total").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp'+formatRupiah(Math.floor(rp_nominal_diskon_total)));
}
function rp_nominal_ppn_total()
{
	var rp_nominal_ppn_total=$("#nominal_ppn_total").val();
	$("#rp_nominal_ppn_total").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp'+formatRupiah(Math.floor(rp_nominal_ppn_total)));
}

function load_data_ajax()
{
	var reg=$('#id_pembelian').val();
	var id_item=$('#id_item').val();
	var jumlah=$('#jumlah').val();
	var harga=$('#harga_satuan').val();
	var harga_subtotal=$('#subtotal').val();
	var diskon=$('#diskon').val();
	var nominal_diskon=$('#nominal_diskon').val();
	var netto=$('#netto').val();
	var satuan=$('#satuan').val();
	var tanggalmasuk=$('#tanggalmasuk').val();
	var expdate=$('#expdate').val();


	if (id_item==0 || id_item=="")
	{
		alert('Masukkan Item');
		action.preventDefault();
	}
	else
	{
		if(jumlah==0 || jumlah=="")
		{
			alert('Masukkan Jumlah');
			action.preventDefault();
		}
		else
		{
		var controller = 'g_pembelian';
		var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
			$.ajax({
				'url' : base_url + controller + '/insert_item_edit',
				'type' : 'POST', //the way you want to send data to your URL
				'data' : {'reg' : reg, 'id_item' : id_item, 'jumlah': jumlah, 'harga' : harga, 'diskon' : diskon, 'nominal_diskon' : nominal_diskon, 'netto' : netto, 'harga_subtotal' : harga_subtotal, 'satuan' : satuan, 'tanggalmasuk' : tanggalmasuk, 'expdate' : expdate},
				'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					var container = $('#tabel_layanan'); //jquery selector (get element by id)
					if(data){
						container.html(data);
						var temp = $('#temptotalharga').val();
						$('#totalharga').val(temp);
						$('#label_totalharga').val(formatRupiah(temp));
						hitung_nominal_diskon_total();
						hitung_nominal_ppn_total();
						hitung_netto_total();
						
						$('#id_item').val(0);
						$('#jumlah').val(0);
						$('#harga').val("0");
						$('#harga_subtotal').val("0");
						$('#label_harga_subtotal').val("0");
						$('#diskon').val("0");
						$('#nominaldiskon').val("0");
						$('#harganetto').val("0");
						$('#label_harganetto').val("0");
						$('#namaitem').val("");
						$('#satuan').val("");
					}
				}
			});
		}
	}
}

$(document).ready(function()
{
	var controller = 'g_pembelian';
	var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
	var reg2=$("#id_pembelian").val();

	$.ajax({
		'url' : base_url + controller + '/show_table',
		'type' : 'POST', //the way you want to send data to your URL
		'data' : {'reg' : reg2},
		'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
			var container = $('#tabel_layanan'); //jquery selector (get element by id)
			if(data){
				container.html(data);
				var temp = $('#temptotalharga').val();
				$('#totalharga').val(temp);
				$('#label_totalharga').val(formatRupiah(temp));
				hitung_total_diskon();
				hitung_netto_total();
			}
		}
	});

	$( "#form1" ).submit(function( event ) 
	{
		var nomorfaktur=$('#nomorfaktur').val();
		var tanggalfaktur=$('#tanggalfaktur').val();
		var unik=$('#peringatan').val();
		if(nomorfaktur=="" && no_delivery=="")
		{
			alert("Isi Nomor Faktur atau Nomor Delivery");
			event.preventDefault();
		}
		else if(tanggalfaktur==0 || tanggalfaktur=="" || tanggalfaktur=="0")
		{
			alert("Tanggal Belum terisi");
			event.preventDefault();
		}
		else if(unik==1 || unik=="1")
		{
			alert("Nomor Faktur ini Sudah Pernah Didaftarkan!!");
			event.preventDefault();
		}
	});

	var data3;
	$('#namaitem').keyup(function()
	{
		if($(this).val()!="")
		{
			$.ajax({
					type: "POST",
					url: "<?php echo base_url('g_penjualan/ajaxTypeheadItem') ;?>",
					data: { src_param: $(this).val() },
					cache: true,
					success:
						function(result)
						{
							data3 = JSON.parse(result);
						}
					});
		}
	});

	$('#namaitem').typeahead({
	    source: function (query, process) 
	    {
	        states4 = [];
			map4 = {};
			var source = [];
			$.each(data3, function (i, state) {
				map4[state.stateName] = state;
				states4.push(state.stateName);
			});
			process(states4);
	    },
	    updater: function (item) 
	    {
			selectedState = map4[item].stateCode;
			selectedState2 = map4[item].stateDisplay;
			harga = map4[item].harga;
			satuan = map4[item].satuanbeli;
			$("#id_item").val(selectedState);
			$("#harga").val(harga);
			$("#satuan").val(satuan);
			return selectedState2;
	    },
	    matcher: function (item) 
	    {
	        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) 
	        {
				return true;
			}
	    },
	    sorter: function (items) 
	    {
	        return items.sort();
	    },
	    highlighter: function (item) 
	    {
			var regex = new RegExp( '(' + this.query + ')', 'gi' );
			return item.replace( regex, "<strong>$1</strong>" );
			
	    },
	});
	
	$('#jumlah').keyup(function() 
	{
		hitung_subtotal();
	});
	$('#harga_satuan').keyup(function() 
	{
		hitung_subtotal();
		hitung_nominal_diskon();
	});
	$('#subtotal').keyup(function() 
	{
		rp_subtotal();
		hitung_nominal_diskon();
		hitung_netto();
	});
	$('#diskon').keyup(function() 
	{
		hitung_nominal_diskon();
		hitung_netto();
	});
	$('#nominal_diskon').keyup(function() 
	{
		hitung_diskon();
		hitung_netto();
		rp_nominal_diskon();
	});
	$('#diskon_total').keyup(function() 
	{
		hitung_nominal_diskon_total();
		hitung_total_diskon();
		hitung_nominal_ppn_total();
		hitung_netto_total();
	});
	$('#nominal_diskon_total').keyup(function() 
	{
		hitung_diskon_total();
		rp_nominal_diskon_total();
		hitung_total_diskon();
		hitung_nominal_ppn_total();
		hitung_netto_total();
	});
	$('#ppn_total').keyup(function() 
	{
		hitung_nominal_ppn_total();
		hitung_netto_total();
	});
	$('#nominal_ppn_total').keyup(function() 
	{
		hitung_ppn_total();
		rp_nominal_ppn_total
		hitung_netto_total();
	});
	
	$('#nomorfaktur').keyup(function() 
	{
		var reg=$('#nomorfaktur').val();
		var tempreg=$('#tempreg').val();
		if(reg!=tempreg)
		{
		var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
		$.ajax({
			'url' : base_url + '/g_pembelian/checkfaktur',
			'type' : 'POST', //the way you want to send data to your URL
			'data' : {'reg' : reg},
			'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
				
				if(data>0){
					$('#peringatan').addClass("alert-error");
					$("#peringatan").removeClass("alert-info");
					$("#peringatan").html("Nomor Faktur ini sudah didaftarkan");
					$("#peringatan").val(1);
					
					$('#nomorfaktur').focus();
				}
				else
				{
					$('#peringatan').addClass("alert-info");
					$("#peringatan").removeClass("alert-error");
					$("#peringatan").html("");
					$("#peringatan").val(0);
				}
			}
		});
		}
	});
	$('#tanggalfaktur').datepicker().on('changeDate', function(ev){});
	$('#inputJatuhTempo').datepicker().on('changeDate', function(ev){});
	$('#tanggalmasuk').datepicker().on('changeDate', function(ev){});
	$('#expdate').datepicker().on('changeDate', function(ev){});
});

function function_jumlah_modal()
{
	hitung_subtotal_modal();
	hitung_total_modal();
}
function function_harga_satuan_modal()
{
	var harga_satuan_modal = $('#harga_satuan_modal').val();
	$('#rp_harga_satuan_modal').val(formatIDR(harga_satuan_modal));
	hitung_subtotal_modal();
	hitung_total_modal();
}
function function_sub_total_modal()
{
	var sub_total_modal = $('#sub_total_modal').val();
	$('#rp_sub_total_modal').val(formatIDR(sub_total_modal));
	hitung_total_modal();
}
function function_diskon_modal()
{
	var diskon_modal = $('#diskon_modal').val();
	$('#persen_diskon_modal').val(formatPersen(diskon_modal));
	hitung_nominal_diskon_modal();
	hitung_total_modal();
}
function function_nominal_diskon_modal()
{
	var nominal_diskon_modal = $('#nominal_diskon_modal').val();
	$('#rp_nominal_diskon_modal').val(formatIDR(nominal_diskon_modal));
	hitung_diskon_modal();
	hitung_total_modal();
}
function function_total_modal()
{
	var total_modal = $('#total_modal').val();
	$('#rp_total_modal').val(formatIDR(total_modal));
}
function hitung_subtotal_modal()
{
	var jumlah_modal = $('#jumlah_item_modal').val();
	var harga_satuan_modal = $('#harga_satuan_modal').val();
	var subtotal_modal = jumlah_modal*harga_satuan_modal;
	$('#sub_total_modal').val(subtotal_modal);
	$('#rp_sub_total_modal').val(formatIDR(subtotal_modal));
}
function hitung_total_modal()
{
	var subtotal_modal = $('#sub_total_modal').val();
	var nominal_diskon_modal = $('#nominal_diskon_modal').val();
	var total_modal = subtotal_modal - nominal_diskon_modal;
	$('#total_modal').val(total_modal);
	$('#rp_total_modal').val(formatIDR(total_modal));
}
function hitung_nominal_diskon_modal()
{
	var diskon_modal = $('#diskon_modal').val();
	var subtotal_modal = $('#sub_total_modal').val();
	var nominal_diskon_modal = subtotal_modal*diskon_modal/100;
	$('#nominal_diskon_modal').val(nominal_diskon_modal);
	$('#rp_nominal_diskon_modal').val(formatIDR(nominal_diskon_modal));
}
function hitung_diskon_modal()
{
	var nominal_diskon_modal = $('#nominal_diskon_modal').val();
	var subtotal_modal = $('#sub_total_modal').val();
	var diskon_modal = (nominal_diskon_modal/subtotal_modal) * 100;
	$('#diskon_modal').val(diskon_modal);
	$('#persen_diskon_modal').val(formatPersen(diskon_modal));
}

function formatIDR(nilaiUang2)
{
	var nilaiUang=nilaiUang2+"";
  var nilaiRupiah 	= "";
  var jumlahAngka 	= nilaiUang.length;
  
  while(jumlahAngka > 3)
  {
	
	sisaNilai = jumlahAngka-3;
    nilaiRupiah = "."+nilaiUang.substr(sisaNilai,3)+""+nilaiRupiah;
    
    nilaiUang = nilaiUang.substr(0,sisaNilai)+"";
    jumlahAngka = nilaiUang.length;
  }
 
  nilaiRupiah = "Rp "+nilaiUang+""+nilaiRupiah+",-";
  return nilaiRupiah;
}
function formatPersen(nilai)
{
	var persen = nilai + " %";
	return persen;
}
function edit_item()
{
	var id_pembelian = $('#id_pembelian').val();
	var id_item_beli = $('#id_item_beli_modal').val();
	var jumlah = $('#jumlah_item_modal').val();
	var harga_satuan = $('#harga_satuan_modal').val();
	var subtotal = $('#sub_total_modal').val();
	var diskon = $('#diskon_modal').val();
	var nominal_diskon = $('#nominal_diskon_modal').val();
	var total = $('#total_modal').val();

	if(jumlah == "" || jumlah == 0)
	{
		alert('Masukkan jumlah item');
	}
	else
	{
		$.ajax({
			'url' : base_url + controller + '/edit_item',
			'type' : 'POST', //the way you want to send data to your URL
					'data' : {'id_pembelian' : id_pembelian,'id_item_beli' : id_item_beli, 'jumlah' : jumlah, 'harga_satuan' : harga_satuan, 'subtotal' : subtotal, 'diskon' : diskon, 'nominal_diskon' : nominal_diskon, 'total' : total},
					'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
						var container = $('#tabel_layanan'); //jquery selector (get element by id)
						if(data){
							container.html(data);
							var temp = $('#temptotalharga').val();
							$('#totalharga').val(temp);
							$('#label_totalharga').val(formatRupiah(temp));
							hitung_total_diskon();
							hitung_nominal_diskon_total();
							hitung_nominal_ppn_total();
							hitung_netto_total();
							
							$('#id_item').val(0);
							$('#jumlah').val(0);
							$('#harga').val("0");
							$('#harga_subtotal').val("0");
							$('#label_harga_subtotal').val("0");
							$('#diskon').val("0");
							$('#nominaldiskon').val("0");
							$('#harganetto').val("0");
							$('#label_harganetto').val("0");
							$('#namaitem').val("");
							$('#satuan').val("");
						}
					}
		});
	}
}

function delete_layanan(reg, netto, pembelian){
	var controller = 'g_pembelian';
	var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
	$.ajax({
		'url' : base_url + controller + '/delete_item',
		'type' : 'POST', //the way you want to send data to your URL
		'data' : {'reg' : reg, 'netto' : netto, 'pembelian':pembelian},
		'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
			var container = $('#tabel_layanan'); //jquery selector (get element by id)
			if(data){
				container.html(data);
				var temp = $('#temptotalharga').val();
				$('#totalharga').val(temp);
				$('#label_totalharga').val(formatRupiah(temp));
				hitung_total_diskon();
				hitung_nominal_diskon_total();
				hitung_nominal_ppn_total();
				hitung_netto_total();
				
			}
		}
	});
}
</script>

