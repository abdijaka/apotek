<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">	
<link href="<?php echo base_url('assets/select2-3.5.0/select2.css') ?>" rel="stylesheet">	
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>

	<?php
		$this->load->view('templates/menubar_kiri');
	?>

</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		
		<legend style="text-align:center;">LAPORAN TRANSAKSI PER ITEM 
		
		<?php foreach ($item as $list_item): ?>
			  <?php if($id_item==$list_item['id_item']) echo $list_item['nama_item']; ?>
			  <?php endforeach ?>
		
		</legend>
		
		
		<form id="form1" action="<?php echo site_url("g_laporan/laporan_item"); ?>" method="POST">
		<div class="row">
				<div class="col-md-12">
				<div class="col-md-1">
				<a class="btn" ><b>Pilih Item : </b></a>
				</div>
				<div class="col-md-3">
					<select name="id_item" id="id_item" class="input-large" style="width:250px;" >
						  <?php foreach ($item as $list_item): ?>
						  <option <?php if($id_item==$list_item['id_item']) echo "selected"; ?> value="<?php echo $list_item['id_item']; ?>"><?php echo $list_item['nama_item']; ?></option>
						  <?php endforeach ?>
					</select>
				</div>
				
				<div class="col-md-1">
				<a class="btn">Dari Tanggal :</a>
				</div>
				<div class="col-md-2">
				<input  type="text" value="<?php echo $mulai;?>" name="mulai" id="mulai" data-date-format="dd-mm-yyyy">
				</div>
				
				<div class="col-md-1">
				<a class="btn">Sampai :</a>
				</div>
				<div class="col-md-2">
				<input  type="text" value="<?php echo $selesai;?>" name="selesai" id="selesai" data-date-format="dd-mm-yyyy">
				</div>
				
				<div class="col-md-1">
				<button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Tampilkan</button>
				</div>
				</div>
		</div>
		</form>
		
		
		
		<br>
		
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" >
			<thead>
				<tr style="background-color: #90CA77; color:#fff;">
					
					<th>Tanggal Faktur</th>
					<th>Nomor Faktur</th>
					<th>Jumlah Pembelian</th>
					<th>Jumlah Penjualan</th>
					<th>Jumlah Retur</th>
					<th>Jumlah Pemusnahan</th>
					
					
				</tr>
			</thead>
			<tbody>
			<?php

			$nomor=1;
			$jml_beli=0;
			$jml_jual=0;
			$jml_retur=0;
			$jml_musnah=0;
			
			foreach ($detail as $detail_item): 
			//$totaljumlah=$totaljumlah+$detail_item['jumlah'];
			//$totalnetto=$totalnetto+$detail_item['total'];
			$tgl = new DateTime($detail_item['tgl']);
			//$tgl2 = new DateTime($detail_item['tgl_masuk']);
			$jml_beli=$jml_beli+$detail_item['beli'];
			$jml_jual=$jml_jual+$detail_item['jual'];
			$jml_retur=$jml_retur+$detail_item['retur'];
			$jml_musnah=$jml_musnah+$detail_item['musnah'];
			?>
			
			<tr class="odd gradeX">
					<td><?php echo $tgl->format('d-m-Y'); ?></td>
					<td><?php echo $detail_item['nomor_faktur'] ?></td>
					<td style="text-align:right;"><?php echo $detail_item['beli'] ?></td>
					<td style="text-align:right;"><?php echo $detail_item['jual'] ?></td>
					<td style="text-align:right;"><?php echo $detail_item['retur'] ?></td>
					<td style="text-align:right;"><?php echo $detail_item['musnah'] ?></td>
					
					
			</tr>
			
		
			<?php 
			$nomor++;
			
			endforeach ?>
			<tr >		
					<th colspan="2">Total</th>
					<th><?php echo $jml_beli; ?></th>
					<th><?php echo $jml_jual; ?></th>
					<th><?php echo $jml_retur; ?></th>
					<th><?php echo $jml_musnah; ?></th>
			</tr>
			<tr >
					
			</tr>
			</tbody>
		</table>
      
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>


<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	<script src="<?php echo base_url('assets/select2-3.5.0/select2.js') ?>"></script>
	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

$(document).ready(function(){

	$("#id_item").select2();
	$('#mulai').datepicker(
			{
			
			}
			)
		  .on('changeDate', function(ev){
			
		});
		$('#selesai').datepicker(
			{
			
			}
			)
		  .on('changeDate', function(ev){
			
		});
});

</script>

</body></html>