<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
	<!-- Header -->
	<header>
		<?php
			$this->load->view('templates/menubar_kiri');
		?>
	</header>
	<!-- Content -->
	<section id="wrap">
		<section class="content content-white">
		    <div class="container container-content"> 	
			    <div class="row">
				    <div class="col-md-12">
						<legend style="text-align:center;">EDIT KARTU HUTANG</legend>
						<form class="form-horizontal" id="form1" action="<?php echo site_url("g_keuangan_yunan/update_hutang"); ?>" method="POST">
							<?php
								$date=date("d-m-Y");
								$tgl = new DateTime($pembelian['tgl_faktur']);
								$jatuh_tempo = new DateTime($pembelian['jatuh_tempo']);
							?>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-3">
										<label class="control-label" for="inputTanggal">Tanggal Faktur</label>
										<input type="text" value="<?php echo $tgl->format('d-m-Y'); ?>" name="tanggalfaktur" id="tanggalfaktur" data-date-format="dd-mm-yyyy">
									</div>	
									<div class="col-md-3">
										<label class="control-label" for="inputReg">No. Faktur</label>
										<input type="hidden" id="id_pembelian" name="id_pembelian" value="<?php echo $pembelian['id_pembelian'] ?>">
										<input type="text" id="nomorfaktur" name="nomorfaktur" value="<?php echo $pembelian['nomor_faktur'] ?>">
										<input type="hidden" id="tempreg" name="tempreg" value="<?php echo $pembelian['nomor_faktur'] ?>">
										<div id="peringatan" value="0">
										</div>
									</div>		
									<div class="col-md-3">
										<label class="control-label" for="inputJatuhTempo">Jatuh Tempo</label>
										<input type="text" value="<?php echo date('d-m-Y', strtotime($pembelian['jatuh_tempo'])); ?>" name="inputJatuhTempo" id="inputJatuhTempo" data-date-format="dd-mm-yyyy">
									</div>
									<div class="col-md-3">
										<label class="control-label" for="inputID">Supplier</label>
										<select name="supplier" id="supplier" class="input-large">
										  	<?php foreach ($supplier as $supplier_item): ?>
										  	<option value="<?php echo $supplier_item['id_supplier']; ?>" <?php if($pembelian['supplier']==$supplier_item['id_supplier']) echo "selected"; ?>><?php echo $supplier_item['nama_supplier']; ?></option>
										  	<?php endforeach ?>
										</select>
									</div>	
								</div>
							</div>
							<?php 
								// echo "<pre>";
								// var_dump($pembayaran);
								// echo "</pre>";
							?>
						  	<hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-info">
										<div class="container-fluid" style="margin-top:-15px; ">
											<div class="row-fluid">
										        <div class="col-md-12">
												   	<div class="row">
													   <div class="col-md-12">
														   	<div class="col-md-12">
														   		<div id="tabel_layanan">
														   		</div>
														   	</div>
													   </div>
												   	</div>
												   	<div class="row">
														<div class="col-md-12">
															<div class="col-md-4">
																<label class="control-label" for="totalharga">Total</label>
																<div class="input-group margin-bottom-sm">
																	<span class="input-group-addon">Rp</span>
																	<input type="hidden" id="totalharga" name="totalharga" class="input-small" readonly value="<?php echo $pembelian['subtotal']; ?>">
																	<input type="text" id="label_totalharga" name="label_totalharga" class="input-small" readonly value="<?php echo $pembelian['subtotal']; ?>">
																</div>
															</div>	
															<div class="col-md-4">
																<label class="control-label" for="totalharga">Diskon (%)</label>
																<div class="input-group margin-bottom-sm">
																	<input type="text" id="totaldiskon" name="totaldiskon"  value="<?php echo $pembelian['diskon']; ?>" autocomplete="off" readonly>
																	<span class="input-group-addon">%</span>
																</div>
															</div>	
															<div class="col-md-4">
																<label class="control-label" for="totalharga">Nominal Diskon (Rp)</label>
																<div class="input-group margin-bottom-sm">
																	<span class="input-group-addon">Rp</span>
																	<input type="hidden" id="totalnominaldiskon" name="totalnominaldiskon"  class="input-small" value="<?php echo $pembelian['nominal_diskon']; ?>" autocomplete="off" readonly>
																	<input type="text" id="rp_totalnominaldiskon" name="rp_totalnominaldiskon"  class="input-small" value="<?php echo number_format($pembelian['nominal_diskon'], 0 , '' , '.'). ',-'; ?>" autocomplete="off" readonly>
																</div>
															</div>	
														</div>	
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="col-md-4">
																<label class="control-label" for="totalharga">Total Setelah Didiskon</label>
																<div class="input-group margin-bottom-sm">
																	<span class="input-group-addon">Rp</span>
																	<input type="hidden" id="total_diskon" name="total_diskon" class="input-small" readonly value="0">
																	<input type="text" id="rp_total_diskon" name="rp_total_diskon" class="input-small" readonly value="0">
																</div>
															</div>	
															<div class="col-md-4">
																<label class="control-label" for="totalharga">PPN (%)</label>
																<div class="input-group margin-bottom-sm">
																	<input type="text" id="ppn_total" name="ppn_total"  value="<?php echo $pembelian['ppn']; ?>" autocomplete="off" readonly>
																	<span class="input-group-addon">%</span>
																</div>
															</div>	
															<div class="col-md-4">
																<label class="control-label" for="totalharga">Nominal PPN (Rp)</label>
																<div class="input-group margin-bottom-sm">
																	<span class="input-group-addon">Rp</span>
																	<input type="hidden" id="nominal_ppn_total" name="nominal_ppn_total"  class="input-small" value="<?php echo $pembelian['nominal_ppn']; ?>" autocomplete="off" readonly>
																	<input type="text" id="rp_nominal_ppn_total" name="rp_nominal_ppn_total"  class="input-small" value="<?php echo number_format($pembelian['nominal_ppn'], 0 , '' , '.'). ',-'; ?>" autocomplete="off" readonly>
																</div>
																<div id="rp_nominal_ppn_total" class="alert-danger">
																</div>
															</div>	
														</div>	
												 	</div>
													<div class="row">
														<div class="col-md-12">
															<div class="col-md-12">
																<label class="control-label" for="totalharganetto">Harga Netto</label>
																<p id="labelnetto" style="color:#CC0000; font-size:32px; font-weight:bold;"> Rp 0 </p>
																<input type="hidden" id="totalharganetto" name="totalharganetto"  class="input-large" value="0" >
															</div>	
														</div>	
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="col-md-12">
																<hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
														<div class="col-md-12">
															<table class="table table-bordered table-condensed " >
																<thead>
																<tr style="background-color: #90CA77; color:#fff; font-size:14px;">
																  <th>Tanggal</th>
																  <th>Cara Bayar</th>
																  <th>Pembayaran</th>
																  <!-- <th>Action</th> -->
																</tr>
															  	</thead>
																<tbody style="font-size:14px;">
																<?php
																$i=0;
																$total_bayar=0;
																
																	foreach ($pembayaran as $pembayaran_item): 
																	$tanggal_bayar =date('j-m-Y', strtotime($pembayaran_item['tanggal']));
																	$jam_bayar =date('G:i', strtotime($pembayaran_item['tanggal']));
																	$total_bayar=$total_bayar+$pembayaran_item['jumlah'];
																	?>
																	
																	<tr style="background-color: #fff;">
																	  
																	  <td><?php echo $tanggal_bayar.' '.$jam_bayar; ?></td>
																	  <td><?php echo $pembayaran_item['namacarabayar']; ?></td>
																	  <td style="text-align:right; "><p class="text-right"><?php echo formatRupiah($pembayaran_item['jumlah']); ?></p> </td>
																	  
																	  <!--
																	  <td><a href="<?php echo site_url('transaksi/cetak_pembayaran/'.$transaksi['nomor_registrasi'].'/'.$pembayaran_item['tanggal']); ?>" class="btn btn-success"><i class="fa fa-print"></i> Print</a></td>
																	  -->
																	</tr>
																<?php 
																endforeach ?>	
																	<tr style="">
																	  <td colspan="1" style="text-align:center; color:#CC0000; font-weight:bold; font-size:16px;">Total Pembayaran</td>
																	  <td colspan="4" style="text-align:right;  font-size:20px; color:#CC0000; font-weight:bold;"><p class="text-right"><?php echo formatRupiah($total_bayar); ?></p></td>
																	  
																	</tr>
																  </tbody>
															</table>
														</div>	
														</div>	
													 </div>
												    <div class="row" >

														<div class="col-md-12" style="z-index:1;">
															<div  class="col-md-4">
																<label class="control-label" for="totalharga">Cara Bayar</label>
																<select name="carabayar" id="carabayar" >
																<?php foreach ($carabayar as $carabayar_item): ?>
																	<option value="<?php echo $carabayar_item['id'] ?>"><?php echo $carabayar_item['nama'] ?></option>
																<?php endforeach; ?>
																</select>
															</div>
															<?php //var_dump($pembayaran)?>	
															<div class="col-md-4">
																<label class="control-label" for="totalharga">Bayar (Rp)</label>
																<div class="input-group margin-bottom-sm">
																	<span class="input-group-addon">Rp</span>
																	<input type="text" id="jumlahbayar" name="jumlahbayar"  value="0" autocomplete="off" >
																</div>
																<div id="label_bayar" class="alert-danger" >

																</div>
															</div>
															<?php //echo $pembelian['kurangbayar'];?>	
															<div class="col-md-4">
																<label class="control-label" for="totalharga">Kurang (Rp)</label>
																<div class="input-group margin-bottom-sm">
																	<span class="input-group-addon">Rp</span>
																	<input type="hidden" id="kurangbayar" name="kurangbayar"  value="<?php echo $pembelian['kurangbayar'];?>"  readonly>
																	<input type="hidden" id="sisa" name="sisa"  value="<?php echo $pembelian['kurangbayar'];?>"  readonly>
																	<input type="text" id="rp_sisa" name="rp_sisa"  value="<?php echo number_format($pembelian['kurangbayar'], 0 , '' , '.'). ',-';?>"  readonly>
																</div>
															</div>	
														</div>	
													</div>
												  	<div class="row">
														<div class="col-md-12">
															<div class="col-md-12">
															  	<label class="control-label" for="totalharga">&nbsp;</label><br>
															  	<button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Simpan</button>
															</div>
														</div>
												  	</div>		
												</div>
							            	</div>
										</div>
									</div>
								</div>
						  	</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	</section>
</body>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>


<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>
<script type="text/javascript">

function hitung_netto()
{
	var harga=$("#harga_subtotal").val();
	var nominal_diskon=$("#nominaldiskon").val();
	var netto=harga-nominal_diskon;
	$("#harganetto").val(Math.floor(netto));
	$("#label_harganetto").val(formatRupiah(Math.floor(netto)));
	label_harga_satuan();
}

function hitung_harga_subtotal()
{
	var harga=$("#harga").val();
	var jumlah=$("#jumlah").val();
	var subtotal=harga*jumlah;
	$("#harga_subtotal").val(subtotal);
	$("#label_harga_subtotal").val(formatRupiah(subtotal));
	hitung_netto();
}

function hitung_nominal_diskon()
{
	var subtotal=$("#harga_subtotal").val();
	var diskon=$("#diskon").val();
	var nominaldiskon=subtotal*diskon/100;
	$("#nominaldiskon").val(Math.floor(nominaldiskon));
	hitung_netto();
}

function hitung_persen_diskon()
{
	var subtotal=$("#harga_subtotal").val();
	var nominaldiskon=$("#nominaldiskon").val();
	var diskon=nominaldiskon*100/subtotal;
	$("#diskon").val(Math.ceil(diskon));
	hitung_netto();
}

function hitung_total_netto() 
	{
	 var hargadiskon=$('#total_diskon').val();
	 var nominalppn=$('#nominal_ppn_total').val();
     
	 var netto=hargadiskon*1+nominalppn*1;
     $("#totalharganetto").val(Math.floor(netto));
	 $("#labelnetto").html('Rp '+formatRupiah(Math.floor(netto)));
	hitung_kurang_bayar();
	
	}

function hitung_total_persen_diskon() 
	{
     var harga=$('#totalharga').val();
	 var nominaldiskon=$('#totalnominaldiskon').val();
     
	 var diskon=nominaldiskon*100/harga;
     $("#totaldiskon").val(Math.ceil(diskon));
	 hitung_total_netto();
	}

function hitung_total_nominal_diskon() 
	{
     var harga=$('#totalharga').val();
	 var diskon=$('#totaldiskon').val();
     
	 var nominaldiskon=harga*diskon/100;
     $("#totalnominaldiskon").val(Math.floor(nominaldiskon));
	 hitung_total_netto();
	}
function hitung_total_diskon()
	{
		var totalharga=$("#totalharga").val();
		var nominal_diskon_total=$("#totalnominaldiskon").val();
		var total_diskon=totalharga-nominal_diskon_total;
		$("#total_diskon").val(total_diskon);
		$("#rp_total_diskon").val(formatRupiah(total_diskon));
	}

function hitung_kurang_bayar() 
	{
     var kurangbayar=$('#kurangbayar').val();
	 var jumlahbayar=$('#jumlahbayar').val();
     console.log(totalharganetto);
     console.log(kurangbayar);
     console.log(jumlahbayar);
	 var kurang=kurangbayar-jumlahbayar;
     $("#sisa").val(kurang);
     $("#rp_sisa").val(formatRupiah(Math.floor(kurang)));
	 label_bayar();
	}

function label_bayar() 
	{
	 var jumlahbayar=$('#jumlahbayar').val();
     $("#label_bayar").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp'+formatRupiah(Math.floor(jumlahbayar)));
	}
function label_harga_satuan() 
	{
	 var hargasatuan=$('#harga').val();
     $("#label_harga_satuan").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp'+formatRupiah(Math.floor(hargasatuan)));
	}



$(document).ready(function(){


var controller = 'g_keuangan_yunan';
var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
var reg2=$("#id_pembelian").val();
$.ajax({
		'url' : base_url + controller + '/show_table',
		'type' : 'POST', //the way you want to send data to your URL
		'data' : {'reg' : reg2},
		'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
			var container = $('#tabel_layanan'); //jquery selector (get element by id)
			if(data){
				container.html(data);
				var temp = $('#temptotalharga').val();
				$('#totalharga').val(temp);
				$('#label_totalharga').val(formatRupiah(temp));
				hitung_total_diskon();
				hitung_total_netto();
			}
		}
	});

$( "#form1" ).submit(function( event ) {
  
  var nomorfaktur=$('#nomorfaktur').val();
  var tanggalfaktur=$('#tanggalfaktur').val();
  var unik=$('#peringatan').val();
  
	if(nomorfaktur==0 || nomorfaktur=="" || nomorfaktur=="0")
	{
		alert("Nomor Faktur Belum Terisi");
		event.preventDefault();
	}
	else if(tanggalfaktur==0 || tanggalfaktur=="" || tanggalfaktur=="0")
	{
		alert("Tanggal Belum terisi");
		event.preventDefault();
	}
	else if(unik==1 || unik=="1")
	{
		alert("Nomor Faktur ini Sudah Pernah Didaftarkan!!");
		event.preventDefault();
	}
	
});

var data3;

$('#namaitem').keyup(function(){
	if($(this).val()!="")
	{
		$.ajax({
				type: "POST",
				url: "<?php echo base_url('g_penjualan/ajaxTypeheadItem') ;?>",
				data: { src_param: $(this).val() },
				cache: true,
				success:
					function(result)
					{
						data3 = JSON.parse(result);
					}
				});
	}
});

$('#namaitem').typeahead({
    source: function (query, process) {
        states4 = [];
		map4 = {};
		
		var source = [];
		$.each(data3, function (i, state) {
			map4[state.stateName] = state;
			states4.push(state.stateName);
		});
	 
		process(states4);
		
    },
    updater: function (item) {
        
		selectedState = map4[item].stateCode;
		selectedState2 = map4[item].stateDisplay;
		harga = map4[item].harga;
		satuan = map4[item].satuanbeli;
		
		$("#id_item").val(selectedState);
		$("#harga").val(harga);
		$("#satuan").val(satuan);
		hitung_harga_subtotal();
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#jumlah').keyup(function() {
	hitung_harga_subtotal();
});

$('#harga').keyup(function() {
	hitung_harga_subtotal();
});


$('#diskon').keyup(function() {
	hitung_nominal_diskon();
});

$('#nominaldiskon').keyup(function() {
	hitung_persen_diskon();
});

$('#totaldiskon').keyup(function() {
	hitung_total_nominal_diskon();
});

$('#totalnominaldiskon').keyup(function() {
	hitung_total_persen_diskon();
});

$('#jumlahbayar').keyup(function() {
	hitung_kurang_bayar();
});

$('#nomorfaktur').keyup(function() {
			var reg=$('#nomorfaktur').val();
			var tempreg=$('#tempreg').val();
			if(reg!=tempreg)
			{
			var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
			$.ajax({
				'url' : base_url + '/g_keuangan_yunan/checkfaktur',
				'type' : 'POST', //the way you want to send data to your URL
				'data' : {'reg' : reg},
				'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					
					if(data>0){
						$('#peringatan').addClass("alert-error");
						$("#peringatan").removeClass("alert-info");
						$("#peringatan").html("Nomor Faktur ini sudah didaftarkan");
						$("#peringatan").val(1);
						
						$('#nomorfaktur').focus();
						
					}
					else
					{
						$('#peringatan').addClass("alert-info");
						$("#peringatan").removeClass("alert-error");
						$("#peringatan").html("");
						$("#peringatan").val(0);
					}
				}
			});
			}
});


$('#tanggalfaktur').datepicker()
		  .on('changeDate', function(ev){
			
		  });
$('#inputJatuhTempo').datepicker()
		  .on('changeDate', function(ev){
			
		  });
$('#tanggalmasuk').datepicker()
		  .on('changeDate', function(ev){
			
		  });
$('#expdate').datepicker()
		  .on('changeDate', function(ev){
			
		  });
		  

});

function formatRupiah(nilaiUang2)
{
	var nilaiUang=nilaiUang2+"";
  var nilaiRupiah 	= "";
  var jumlahAngka 	= nilaiUang.length;
  
  while(jumlahAngka > 3)
  {
	
	sisaNilai = jumlahAngka-3;
    nilaiRupiah = "."+nilaiUang.substr(sisaNilai,3)+""+nilaiRupiah;
    
    nilaiUang = nilaiUang.substr(0,sisaNilai)+"";
    jumlahAngka = nilaiUang.length;
  }
 
  nilaiRupiah = nilaiUang+""+nilaiRupiah+",-";
  return nilaiRupiah;
}


function load_data_ajax(){
var reg=$('#id_pembelian').val();
var id_item=$('#id_item').val();
var jumlah=$('#jumlah').val();
var harga=$('#harga').val();
var harga_subtotal=$('#harga_subtotal').val();
var diskon=$('#diskon').val();
var nominal_diskon=$('#nominaldiskon').val();
var netto=$('#harganetto').val();
var satuan=$('#satuan').val();
var tanggalmasuk=$('#tanggalmasuk').val();
var expdate=$('#expdate').val();


if (id_item==0 || id_item=="")
{
	alert('Masukkan Item');
	action.preventDefault();
}
else
{
	if(jumlah==0 || jumlah=="")
	{
		alert('Masukkan Jumlah');
		action.preventDefault();
	}
	else
	{
	var controller = 'g_keuangan_yunan';
	var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
		$.ajax({
			'url' : base_url + controller + '/insert_item',
			'type' : 'POST', //the way you want to send data to your URL
			'data' : {'reg' : reg, 'id_item' : id_item, 'jumlah': jumlah, 'harga' : harga, 'diskon' : diskon, 'nominal_diskon' : nominal_diskon, 'netto' : netto, 'harga_subtotal' : harga_subtotal, 'satuan' : satuan, 'tanggalmasuk' : tanggalmasuk, 'expdate' : expdate},
			'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
				var container = $('#tabel_layanan'); //jquery selector (get element by id)
				if(data){
					container.html(data);
					var temp = $('#temptotalharga').val();
					$('#totalharga').val(temp);
					$('#label_totalharga').val(formatRupiah(temp));
					
					$('#id_item').val(0);
					$('#jumlah').val(0);
					$('#harga').val("0");
					$('#harga_subtotal').val("0");
					$('#label_harga_subtotal').val("0");
					$('#diskon').val("0");
					$('#nominaldiskon').val("0");
					$('#harganetto').val("0");
					$('#label_harganetto').val("0");
					$('#namaitem').val("");
					$('#satuan').val("");
				}
			}
		});
	}
}
}

function pasienbaru(){
	window.location.href = base_url + '/pasien/pasien_baru/1';
}

function delete_layanan(reg, netto, pembelian){
	var controller = 'g_keuangan_yunan';
	var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
	$.ajax({
		'url' : base_url + controller + '/delete_item',
		'type' : 'POST', //the way you want to send data to your URL
		'data' : {'reg' : reg, 'netto' : netto, 'pembelian':pembelian},
		'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
			var container = $('#tabel_layanan'); //jquery selector (get element by id)
			if(data){
				container.html(data);
				var temp = $('#temptotalharga').val();
				$('#totalharga').val(temp);
				$('#label_totalharga').val(formatRupiah(temp));
				hitung_total_nominal_diskon();
				hitung_total_netto();
				
			}
		}
	});
}


</script>
</body></html>