<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">EDIT PASIEN</legend>
	
		<form class="form-horizontal" action="<?php echo site_url("pasien/update_pasien"); ?>" method="POST">
		
		<input type="hidden" id="inputID_id" name="inputID_id" value="<?php echo $pasien['id_pasien'] ?>">
		
		<div class="alert alert-info">
		  
		  <div class="row">
			<div class="col-md-12">
			<div class="col-md-2">
			<p style="text-align:right;  font-weight:bold; color:#d9534f; margin-top:10px;">NAMA PASIEN :</p>
			</div>
			<div class="col-md-2">
			
				<select name="initial" id="initial" class="input-small">
					<option value="0" <?php if ($pasien['initial']==0) echo "selected";  ?> >Tn.</option>
					<option value="1" <?php if ($pasien['initial']==1) echo "selected";  ?> >Ny.</option>
					<option value="2" <?php if ($pasien['initial']==2) echo "selected";  ?> >Nona</option>
					<option value="3" <?php if ($pasien['initial']==3) echo "selected";  ?> >Anak</option>
					<option value="4" <?php if ($pasien['initial']==4) echo "selected";  ?> >Bayi</option>
				</select>
			  
			</div>
			<div class="col-md-4">
				
				<input type="text" id="inputNama" name="inputNama"  placeholder="Nama Pasien" value="<?php echo $pasien['nama_pasien'] ?>">
			</div>
			
			</div>
		  </div>
		  
		  <div class="row">
		  
			<div class="col-md-12">
			<div class="col-md-2">
			<p style="text-align:right;  font-weight:bold; color:#d9534f; margin-top:40px;">KARAKTERISTIK :</p>
			</div>
			
			<div class="col-md-3">
				<label class="control-label" for="inputNama">Jenis Kelamin</label>
				<select name="jeniskelamin" id="jeniskelamin" class="input-medium">
					  <option value="0" <?php if($pasien['jenis_kelamin']==0) echo 'selected'; ?> >Laki-laki</option>
					  <option value="1" <?php if($pasien['jenis_kelamin']==1) echo 'selected'; ?>  >Perempuan</option>
					</select>
			</div>
			<div class="col-md-3">
				<label class="control-label" for="inputNama">Umur</label>
					<div class="input-group margin-bottom-sm">
							<input type="number" id="umur" name="umur" value="<?php echo $pasien['umur']; ?>" >
							<span class="input-group-addon">Tahun</span>  
						</div>
				
			</div>
			</div>
		  </div>
		
		  
		  <div class="row">
			<div class="col-md-12">
			<div class="col-md-2">
			</div>
			<div class="col-md-10">
			  <br>
			  <button type="submit" class="btn btn-info btn-large"><i class="fa fa-save"></i> Update</button>
			</div>
			</div>
		  </div>
		</div>
		</form>
   
		
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>

<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>
<script type="text/javascript">
function ubah_tanggallahir() {
    var str=$('#inputID').val();
			
	var hari=str.substring(6,8);
	
	if(hari>31)
	{
		hari=hari-40;
		$("#jeniskelamin").val(1);
		$("#jeniskelamin").val(1);
		$("#initial").val(1);
		if(hari<10)hari="0"+hari;
		
	}
	var tgl=hari+"-"+str.substring(8,10)+"-19"+str.substring(10,12);
	
	$('#tanggallahir').val(tgl);
	$('#inputmedrec').val(str);
	var tanggallahir=$('#tanggallahir').val();
	var now=$('#now').val();
	
	var diff=datediff(tanggallahir,now);
	humanise(diff);
	}
function dstrToUTC(ds) {
    var dsarr = ds.split("-");
     var dd = parseInt(dsarr[0],10);
     var mm = parseInt(dsarr[1],10);
     var yy = parseInt(dsarr[2],10);
     return Date.UTC(yy,mm-1,dd,0,0,0); 
	}
function datediff(ds1,ds2) 
	{
     var d1 = dstrToUTC(ds1);
     var d2 = dstrToUTC(ds2);
     var oneday = 86400000;
     return (d2-d1) / oneday;    
	}
function humanise (diff) {
  // The string we're working with to create the representation
  var str = '';
  // Map lengths of `diff` to different time periods
  var values = {
    'umur': 365, 
    'umur_bulan': 30, 
    'umur_hari': 1
  };

  // Iterate over the values...
  for (var x in values) {
    var amount = Math.floor(diff / values[x]);

    // ... and find the largest time value that fits into the diff
    if (amount >= 1) {
       // If we match, add to the string ('s' is for pluralization)
       str += amount + x + (amount > 1 ? 's' : '') + ' ';
	   $('#'+x).val(amount);
	   

       // and subtract from the diff
       diff -= amount * values[x];
    }
	else
	{
		$('#'+x).val(0);
	}
  }

  return str;
}
	
$(document).ready(function(){

var data2 = [<?php echo $testing2; ?>];

$('#inputKota').typeahead({
    source: function (query, process) {
        states2 = [];
		map2 = {};
		
		var source = [];
		$.each(data2, function (i, state) {
			map2[state.stateName] = state;
			states2.push(state.stateName);
		});
	 
		process(states2);
		
    },
    updater: function (item) {
        
		selectedState = map2[item].stateCode;
		selectedState2 = map2[item].stateDisplay;
		$("#idkota").val(selectedState);
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});



$('#tanggallahir').datepicker()
		  .on('changeDate', function(ev){
			var tanggallahir=$('#tanggallahir').val();
			var now=$('#now').val();
			
			var diff=datediff(tanggallahir,now);
			humanise(diff);
		  });
});

$('#tanggallahir').keyup(function() {
			var tanggallahir=$('#tanggallahir').val();
			var now=$('#now').val();
			
			var diff=datediff(tanggallahir,now);
			humanise(diff);
});

$('#inputID').keyup(function() {
			var inputID=$('#inputID').val();
			$('#inputmedrec').val(inputID);	
			var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
			$.ajax({
				'url' : base_url + '/pasien/cek_ada',
				'type' : 'POST', //the way you want to send data to your URL
				'data' : {'input' : inputID},
				'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					
					if(data>0){
						alert("Nomor KTP ini sudah pernah didaftarkan!!!")
						$('#inputID').focus();
					}
					else
					{
						$('#peringatan').addClass("alert-info");
						$("#peringatan").removeClass("alert-error")
						$("#peringatan").html("Pastikan Nomor ID Pasien sesuai dengan Nomor KTP")
					}
				}
			});
});

$('#inputID').change(function() {
			
			ubah_tanggallahir();
			
});

$( "#initial" )
  .change(function () {
    var str = 0;
    $( "#initial option:selected" ).each(function() {
      str = $( this ).val();
    });
    if(str==0)
	{
		$("#jeniskelamin").val(0);
	}
	else if(str==1 || str==2)
	{
		$("#jeniskelamin").val(1);
	}
	
  })
  .change();



</script>
</body></html>