<div class="container">
   
      <h2>Transaksi</h2>
	  
	  
	  
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
			<thead>
				<tr>
					<th>Nomor Registrasi</th>
					<th>Tanggal</th>
					<th>Nama Pasien</th>
					<th>Nama Dokter</th>
					<th>Action</th>
				
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ($transaksi as $transaksi_item): ?>
			
			<tr class="odd gradeX">
					<td><?php echo $transaksi_item['nomor_registrasi'] ?></td>
					<td><?php echo $transaksi_item['tanggal'] ?></td>
					<td><?php echo $transaksi_item['nama_pasien'] ?></td>
					<td><?php echo $transaksi_item['nama_dokter'] ?></td>
					
					<td class="center">  <div class="btn-group">
                <button class="btn">Action</button>
                <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url("transaksi/edit_transaksi/".$transaksi_item['nomor_registrasi']) ?>">Edit Transaksi</a></li>
                  <li><a href="<?php echo site_url("transaksi/tambah_pembayaran/".$transaksi_item['nomor_registrasi']); ?>"  data-target="#myModal" data-toggle="modal" >Tambah Pembayaran</a></li>
                  <li><a href="<?php echo site_url("transaksi/delete_transaksi/".$transaksi_item['nomor_registrasi']); ?>">Delete</a></li>
                  <li class="divider"></li>
                  <li><a href="<?php echo site_url("transaksi/view_detail/".$transaksi_item['nomor_registrasi']); ?>">Lihat</a></li>
				  <li><a target="_blank" href="<?php echo site_url("transaksi/cetak_kwitansi/".$transaksi_item['nomor_registrasi']); ?>">Print</a></li>
                </ul>
              </div>
			</td>
				</tr>
		
			<?php 
			
			endforeach ?>
		
				
				
			</tbody>
		</table>
      <input type="hidden" id="flag0" name="flag0"  value="0" >
   
</div>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
    <h3 id="myModalLabel">Tambah Pembayaran</h3>
  </div>
  <div class="modal-body">
  
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    
  </div>
</div>