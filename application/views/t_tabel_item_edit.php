<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}
function formatPersen($nilai)
{
	$nilaipersen = $nilai.' %';
	return $nilaipersen;

}
?>
<br>
<table class="table table-bordered table-condensed " >
	<thead>
	<tr style="background-color: #90CA77; color:#fff; font-size:14px;">
	  <th>No. </th>
	  <th>Item</th>
	  <th>Jumlah</th>
	  <th>Subtotal</th>
	  <th>Diskon</th>
	  <th>Netto</th>
	  <th>Edit</th>
	  <th>Delete</th>
	</tr>
  </thead>
	<tbody style="font-size:13px;">
	<?php
	$i=0;
	$netto=0;
		foreach ($list as $list_item): 
		$i++;
		$netto=$netto+$list_item['total'];
		?>
		
		<tr style="background-color: #fff;">
		  <td style="text-align:center;"><?php echo $i; ?></td>
		  <td><?php echo $list_item['nama_item']; ?></td>
		  <td><?php echo $list_item['jumlah']; ?></td>
		  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($list_item['subtotal']); ?></p> </td>
		  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($list_item['nominal_diskon']); ?></p></td>
		  <td style="text-align:right; width:100px;"><p class="text-right"><?php echo formatRupiah($list_item['total']); ?></p></td>
		  
		  <?php
		  $temp_fun="delete_layanan('".$list_item['id_item_beli']."',".$list_item['total'].",".$list_item['pembelian'].")";
		  ?>
		  
		  <td><p class="text-right"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal"><i class="fa fa-edit "></i> </button></p></td>
		  <td><p class="text-right"><button type="button" onclick="<?php echo $temp_fun; ?>; return false;" class="btn btn-danger"><i class="fa fa-trash-o "></i> </button></p></td>
		  
		</tr>
		<div id="modal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<a class="close" data-dismiss="modal">×</a>
						<h3 class="text-center">Edit Item</h3>
					</div>
					<div class="modal-body">
						<form class="feedback" name="feedback">
							<strong>Nama Item</strong>
							<br />
							<input type="text" id="no_faktur_modal" name="no_faktur" value="<?php echo $list_item['nama_item']; ?>" readonly>
							<input type="hidden" id="id_item_beli_modal" name="id_item_beli_modal" value="<?php echo $list_item['id_item_beli']; ?>" readonly>
							<br />
							<strong>Jumlah</strong>
							<br />
							<input type="text" id="jumlah_item_modal" name="jumlah_item_modal" value="<?php echo $list_item['jumlah']; ?>" onkeyup="function_jumlah_modal()">
							<br />
							<strong>Harga Satuan</strong>
							<br />
							<input type="text" id="harga_satuan_modal" name="harga_satuan_modal" value="<?php echo $list_item['harga']; ?>" onkeyup="function_harga_satuan_modal()">
							<input type="text" id="rp_harga_satuan_modal" name="rp_harga_satuan_modal" value="<?php echo formatRupiah($list_item['harga']); ?>" readonly>
							<br />
							<strong>Sub Total</strong>
							<br />
							<input type="text" id="sub_total_modal" name="sub_total_modal" value="<?php echo $list_item['subtotal']; ?>" onkeyup="function_sub_total_modal()">
							<input type="text" id="rp_sub_total_modal" name="rp_sub_total_modal" value="<?php echo formatRupiah($list_item['subtotal']); ?>" readonly>
							<br />
							<strong>Diskon</strong>
							<br />
							<input type="text" id="diskon_modal" name="diskon_modal" value="<?php echo $list_item['diskon']; ?>" onkeyup="function_diskon_modal()">
							<input type="text" id="persen_diskon_modal" name="persen_diskon_modal" value="<?php echo formatPersen($list_item['diskon']); ?>" readonly>
							<br />
							<strong>Nominal Diskon</strong>
							<br />
							<input type="text" id="nominal_diskon_modal" name="nominal_diskon_modal" value="<?php echo $list_item['nominal_diskon']; ?>" onkeyup="function_nominal_diskon_modal()">
							<input type="text" id="rp_nominal_diskon_modal" name="rp_nominal_diskon_modal" value="<?php echo formatRupiah($list_item['nominal_diskon']); ?>" readonly>
							<br />
							<strong>Netto</strong>
							<br />
							<input type="text" id="total_modal" name="total_modal" value="<?php echo $list_item['total']; ?>" onkeyup="function_total_modal()">
							<input type="text" id="rp_total_modal" name="rp_total_modal" value="<?php echo formatRupiah($list_item['total']); ?>" readonly>
							<br />
						</form>
					</div>
					<div class="modal-footer">
						<button class="btn btn-success" id="submit" onclick="edit_item(); return false; " data-dismiss="modal" data-backdrop="false">Update</button>
						<a href="#" class="btn" data-dismiss="modal">Close</a>
					</div>
				</div>
			</div>		
		</div>
	<?php 		
	endforeach ?>	
		<!---
		<tr style="">
		  <td colspan="2" style="text-align:center; color:#CC0000; font-weight:bold; font-size:16px;">Sub Total</td>
		  <td colspan="4" style="text-align:right;  font-size:20px; color:#CC0000; font-weight:bold;"><p class="text-right"><?php echo formatRupiah($netto); ?></p></td>
		  <td >&nbsp;</td>
		  
		</tr>
		-->
	  </tbody>
</table>
<input type="hidden" id="temptotalharga" name="temptotalharga" class="input-small" value="<?php echo $netto; ?>" >
