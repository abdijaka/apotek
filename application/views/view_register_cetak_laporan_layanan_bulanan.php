<?php
require('fpdf17/mc_table.php');

function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

function get_month($month)
{
	$labelmonth="Januari";

	if($month==2)
	$labelmonth="Februari";
	else if($month==3)
	$labelmonth="Maret";
	else if($month==4)
	$labelmonth="April";
	else if($month==5)
	$labelmonth="Mei";
	else if($month==6)
	$labelmonth="Juni";
	else if($month==7)
	$labelmonth="Juli";
	else if($month==8)
	$labelmonth="Agustus";
	else if($month==9)
	$labelmonth="September";
	else if($month==10)
	$labelmonth="Oktober";
	else if($month==11)
	$labelmonth="November";
	else if($month==12)
	$labelmonth="Desember";

	return $labelmonth;

}

$pdf=new PDF_MC_Table('P','mm','A4');
$pdf->AddPage();

$pdf->SetFont('Arial','B',12);

$pdf->SetY(5);

$pdf->Cell(210, 2.7, "Laporan Jumlah Layanan Bulanan", 0, 0, 'C');

$pdf->SetY(10);

$pdf->Cell(210, 2.7, "Musi Heart Clinic\n", 0, 0, 'C');

$pdf->SetY(15);

$pieces = explode("-", $mulai);

$pdf->Cell(210, 2.7, "Bulan : ".get_month($pieces[0])." ".$pieces[1]."", 0, 0, 'C');

$pdf->SetFont('Arial','B',9);
//Table with 20 rows and 4 columns
$pdf->SetWidths(array(10,50,20));
$pdf->SetY(25);

$pdf->SetX(75);

$pdf->Row(array("No", "Nama Layanan", "Jumlah Layanan"));
$pdf->SetFont('Arial','',8);

$nomor=1;
$total_pendapatan=0;
foreach ($laporan as $laporan_item):
$pdf->SetX(75);
$pdf->Row(array($nomor, $laporan_item['nama_layanan'], $laporan_item['jumlah_layanan']));
$nomor++;

endforeach;

$pdf->SetFont('Arial','B',8);



$pdf->Output("Laporan Layanan_".get_month($pieces[0])." ".$pieces[1].".pdf", "D");
//$pdf->Output();
?>