<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">RETUR ITEM</legend>

		<form class="form-horizontal" id="form1" action="<?php echo site_url("g_retur/insert_retur"); ?>" method="POST">
			<?php
			$date=date("d-m-Y");
			?>
			
		
			<div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
					<label class="control-label" for="inputTanggal">Tanggal Retur</label>
						<input type="text" value="<?php echo $date; ?>" name="tgl_retur" id="tgl_retur" data-date-format="dd-mm-yyyy">
				</div>	
				<div class="col-md-4">
						<label class="control-label" for="inputReg">No. Retur</label>
						<input type="text" id="nomorretur" name="nomorretur" autocomplete="off" value="<?php echo $nomor_baru;?>">
						
						<div id="peringatan" value="0"></div>
				</div>		
				<div class="col-md-3">
					<label class="control-label" for="inputID">Ruangan</label>
						<select name="ruangan" id="ruangan" class="input-large" readonly>
							  <?php foreach ($ruangan as $ruangan_item): ?>
							  <option value="<?php echo $ruangan_item['id_ruangan']; ?>"><?php echo $ruangan_item['nama_ruangan']; ?></option>
							  <?php endforeach ?>
						</select>
				</div>	
				
			</div>
			</div>
			
		  <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
		  
		  
			
			<div class="row">
			<div class="col-md-12">
			<div class="alert alert-info">
				<div class="container-fluid" style="margin-top:-15px; ">
				<div class="row-fluid">
			
              <div class="col-md-4" style="background-color:#90CA77; padding-bottom:20px; padding-top:20px;">
			  
			  <div class="row">
				<div class="col-md-13">
				<div class="col-md-12">
				<label class="control-label" style="color:#fff;" for="pemeriksaan">Nama Item</label>
			
				<input type="hidden" id="id_item" name="id_item" >
				<input type="text" id="namaitem" name="namaitem" placeholder="Nama Item" autocomplete="off">
			  
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-5">
				<label class="control-label" for="diskon" style="color:#fff;">Jumlah</label>
				<div class="input-group margin-bottom-sm">
				<input type="number" id="jumlah" name="jumlah" value="0">
				
				</div>
				
				</div>	
				
				
				<div class="col-md-7">
				<label class="control-label" for="diskon" style="color:#fff;">Satuan</label>
				<div class="input-group margin-bottom-sm">
				<input type="text" id="satuan" name="satuan" >
				</div>
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-5">
				<label class="control-label" style="color:#fff;" for="pemeriksaan">Sisa Stock PHC</label>
			
				<input type="text" id="stock_gudang" name="stock_gudang" value="0" autocomplete="off" readonly>
				<input type="hidden" id="temp_stock_gudang" name="temp_stock_gudang" value="0" autocomplete="off" readonly>
			  
				</div>	
				<div class="col-md-7">
				<label class="control-label" for="diskon" style="color:#fff;">&nbsp;</label>
				<div class="input-group margin-bottom-sm">
				<input type="text" id="satuan2" name="satuan2" readonly>
				</div>
				</div>	
				</div>	
			 </div>	
			 
			 <div class="row">
				<div class="col-md-13">
				<div class="col-md-12">
				<br>
				<button type="button" onclick="load_data_ajax(); return false;" id="tambah_layanan" class="btn btn-danger">Tambahkan <i class="fa fa-chevron-right"></i></button>
			  
				</div>	
				</div>	
			 </div>	
			
		</div>
            <div class="col-md-8">
				  
					
					   <div class="row">
					   <div class="col-md-12">
					   <div class="col-md-12">
					   <div id="tabel_layanan">
						<br>
						<table class="table table-bordered table-condensed " >
						<thead>
						<tr style="background-color: #90CA77; color:#fff; font-size:14px;">
						  <th>No. </th>
						  <th>Item</th>
						  <th>Jumlah</th>
						  <th>Satuan</th>
						  <th>Delete</th>
						</tr>
						</thead>
						</table>
					   </div>
					   </div>
					   </div>
					   </div>
					   
					  <div class="row">
						<div class="col-md-12">
						<div class="col-md-12">
						  <label class="control-label" for="totalharga">&nbsp;</label><br>
						  <button type="submit" class="btn btn-info btn-large"><i class="icon-ok-sign icon-white"></i> Retur Item</button>
						</div>
						</div>
					  </div>
					
				</div>
            </div>
			</div>
			</div>
			</div>
			
		  </div>
		  </div>
		  
		  
		</form>
   		
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>


<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>
<script type="text/javascript">

$(document).ready(function(){

$( "#form1" ).submit(function( event ) {
  
  var nomorfaktur=$('#nomorretur').val();
  var tanggalfaktur=$('#tgl_retur').val();
  var unik=$('#peringatan').val();
  
	if(nomorfaktur==0 || nomorfaktur=="" || nomorfaktur=="0")
	{
		alert("Nomor Pengiriman Belum Terisi");
		event.preventDefault();
	}
	else if(tanggalfaktur==0 || tanggalfaktur=="" || tanggalfaktur=="0")
	{
		alert("Tanggal Belum terisi");
		event.preventDefault();
	}
	else if(unik==1 || unik=="1")
	{
		alert("Nomor Pengiriman ini Sudah Pernah Didaftarkan!!");
		event.preventDefault();
	}
	
});

var data3 = [<?php echo $testing3; ?>];

$('#namaitem').typeahead({
    source: function (query, process) {
        states4 = [];
		map4 = {};
		
		var source = [];
		$.each(data3, function (i, state) {
			map4[state.stateName] = state;
			states4.push(state.stateName);
		});
	 
		process(states4);
		
    },
    updater: function (item) {
        
		selectedState = map4[item].stateCode;
		selectedState2 = map4[item].stateDisplay;
		stock_gudang = map4[item].stock_gudang;
		satuan = map4[item].satuanbeli;
		
		$("#id_item").val(selectedState);
		$("#satuan").val(satuan);
		$("#satuan2").val(satuan);
		$("#stock_gudang").val(stock_gudang);
		$("#temp_stock_gudang").val(stock_gudang);
		
		return selectedState2;
    },
    matcher: function (item) {
        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
			return true;
		}
    },
    sorter: function (items) {
        return items.sort();
    },
    highlighter: function (item) {
		var regex = new RegExp( '(' + this.query + ')', 'gi' );
		return item.replace( regex, "<strong>$1</strong>" );
		
    },
});

$('#jumlah').keyup(function() {
	var jumlah = $("#jumlah").val();
	var temp_stock_gudang = $("#temp_stock_gudang").val();
	var sisa=temp_stock_gudang-jumlah;
	$("#stock_gudang").val(sisa);
});


$('#nomorretur').keyup(function() {
			var reg=$('#nomorretur').val();
			
			var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
			$.ajax({
				'url' : base_url + '/g_retur/checkfaktur',
				'type' : 'POST', //the way you want to send data to your URL
				'data' : {'reg' : reg},
				'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					
					if(data>0){
						$('#peringatan').addClass("alert-error");
						$("#peringatan").removeClass("alert-info");
						$("#peringatan").html("Nomor Pengiriman ini sudah didaftarkan");
						$("#peringatan").val(1);
						
						$('#nomorretur').focus();
						
					}
					else
					{
						$('#peringatan').addClass("alert-info");
						$("#peringatan").removeClass("alert-error");
						$("#peringatan").html("");
						$("#peringatan").val(0);
					}
				}
			});
});

$('#tgl_retur').datepicker()
		  .on('changeDate', function(ev){
			
		  });
		  

});

function formatRupiah(nilaiUang2)
{
	var nilaiUang=nilaiUang2+"";
  var nilaiRupiah 	= "";
  var jumlahAngka 	= nilaiUang.length;
  
  while(jumlahAngka > 3)
  {
	
	sisaNilai = jumlahAngka-3;
    nilaiRupiah = "."+nilaiUang.substr(sisaNilai,3)+""+nilaiRupiah;
    
    nilaiUang = nilaiUang.substr(0,sisaNilai)+"";
    jumlahAngka = nilaiUang.length;
  }
 
  nilaiRupiah = nilaiUang+""+nilaiRupiah+",-";
  return nilaiRupiah;
}

var controller = 'g_retur';
var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';

function load_data_ajax(){
var reg='0';
var id_item=$('#id_item').val();
var jumlah=$('#jumlah').val();
var satuan=$('#satuan').val();
var stock_gudang=$('#stock_gudang').val();

if (id_item==0 || id_item=="")
{
	alert('Masukkan Item');
	action.preventDefault();
}
else
{
	if(jumlah==0 || jumlah=="")
	{
		alert('Masukkan Jumlah');
		action.preventDefault();
	}
	else if(stock_gudang<0)
	{
		alert('Tidak Boleh melebihi Stock');
		action.preventDefault();
	}
	
	if(id_item==0 || id_item=="")
	{
		alert('Masukkan Item');
		action.preventDefault();
	}
	else if(stock_gudang<0)
	{
		alert('Tidak Boleh melebihi Stock');
		action.preventDefault();
	}
	
	else
	{
		$.ajax({
			'url' : base_url + controller + '/insert_item',
			'type' : 'POST', //the way you want to send data to your URL
			'data' : {'reg' : reg, 'id_item' : id_item, 'jumlah': jumlah, 'satuan' : satuan},
			'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
				var container = $('#tabel_layanan'); //jquery selector (get element by id)
				if(data){
					container.html(data);
					
					$('#id_item').val(0);
					$('#namaitem').val("");
					$('#jumlah').val(0);
					$('#stock_gudang').val(0);
					$('#temp_stock_gudang').val(0);
					$('#satuan').val("");
					$('#satuan2').val("");
				}
			}
		});
	}
}
}

function supplierbaru(){
	window.location.href = base_url + 'g_supplier/supplier_baru/1';
}

function delete_layanan(reg, retur){

	$.ajax({
		'url' : base_url + controller + '/delete_item',
		'type' : 'POST', //the way you want to send data to your URL
		'data' : {'reg' : reg, 'retur':retur},
		'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
			var container = $('#tabel_layanan'); //jquery selector (get element by id)
			if(data){
				container.html(data);
				
			}
		}
	});
}



</script>
</body></html>