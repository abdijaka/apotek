<?php 
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

function get_month($month)
{
	$labelmonth="Januari";

	if($month==2)
	$labelmonth="Februari";
	else if($month==3)
	$labelmonth="Maret";
	else if($month==4)
	$labelmonth="April";
	else if($month==5)
	$labelmonth="Mei";
	else if($month==6)
	$labelmonth="Juni";
	else if($month==7)
	$labelmonth="Juli";
	else if($month==8)
	$labelmonth="Agustus";
	else if($month==9)
	$labelmonth="September";
	else if($month==10)
	$labelmonth="Oktober";
	else if($month==11)
	$labelmonth="November";
	else if($month==12)
	$labelmonth="Desember";

	return $labelmonth;

}

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once 'PHPExcel/Classes/PHPExcel.php';

require_once 'PHPExcel/Classes/PHPExcel/IOFactory.php';

$fileName = 'PHPExcel/Examples/templates/template_pemakaian_item.xlsx';
try {
    $objPHPExcel = PHPExcel_IOFactory::load($fileName);
} catch (Exception $e) {
    die("Error loading file: ".$e->getMessage()."<br />\n");
}

$pieces = explode("-", $pemakaian['tgl_pakai']);



$objPHPExcel->getActiveSheet()->setCellValue('B2', "".$pemakaian['nomor_registrasi']);
$objPHPExcel->getActiveSheet()->setCellValue('B3', "".$pemakaian['no_rekam_medis']);

$objPHPExcel->getActiveSheet()->setCellValue('B4', "".$pemakaian['nama_pasien']);

$objPHPExcel->getActiveSheet()->setCellValue('B5', "Kamar ");
$objPHPExcel->getActiveSheet()->setCellValue('B7', "TANGGAL PENGIRIMAN : ".$pieces[2].' '.get_month($pieces[1]).' '.$pieces[0]."");

$objPHPExcel->getActiveSheet()->setCellValue('D4', "NOMOR PENGIRIMAN : ".$pemakaian['nomor_pakai']);

$objPHPExcel->getActiveSheet()->setCellValue('D5', "Tindakan : ".$pemakaian['tindakan']);

$d_p="";
$d_o="";
$cb="Tunai";

foreach ($dokter as $dokter_item):
if($pemakaian['dokter_pengirim']==$dokter_item['id_dokter'])
{
	$d_p=$dokter_item['nama_dokter'];
	
}
if($pemakaian['dokter_operator']==$dokter_item['id_dokter'])
{
	$d_o=$dokter_item['nama_dokter'];
}
endforeach;

foreach ($penjamin as $penjamin_item):
if($pemakaian['penjamin']==$penjamin_item['id_penjamin'] )
{
	$cb=$penjamin_item['nama_penjamin'];
}

endforeach;

$objPHPExcel->getActiveSheet()->setCellValue('D6', "Dokter Pengirim : ".$d_p);
$objPHPExcel->getActiveSheet()->setCellValue('D7', "Dokter Pelaksana : ".$d_o);
$objPHPExcel->getActiveSheet()->setCellValue('D8', "Status : ".$cb." ".formatRupiah($pemakaian['tarif']));

$nomor=1;
$baseRow = 9;
foreach ($list as $list_item):
if($nomor==1)
{
	$row=10;
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $nomor)
							  ->setCellValue('C'.$row, $list_item['nama_item'])
							  ->setCellValue('D'.$row, $list_item['jumlah']);
}
else
{
	$row = $baseRow + $nomor;
	$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);

	$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $nomor)
							  ->setCellValue('C'.$row, $list_item['nama_item'])
							  ->setCellValue('D'.$row, $list_item['jumlah']);

}


$nomor++;

endforeach;

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Laporan Pengiriman');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Laporan_Pemakaian_'.$pemakaian['nomor_pakai'].'.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>