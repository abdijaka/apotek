<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>

	<?php
		$this->load->view('templates/menubar_kiri');
	?>

</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">EDIT ITEM</legend>
		<form class="form-horizontal" action="<?php echo site_url("g_item/update_item"); ?>" method="POST">
		    
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">NAMA ITEM :</label>
					<div class="col-lg-10">
					
					<!-- dihidden -->
					  <input type="hidden" id="inputMerk" name="inputMerk" placeholder="Merk" value="<?php echo $item['merk'] ?>">
					
					<div class="col-lg-5">
					  <input type="hidden" id="inputID" name="inputID" value="<?php echo $item['id_item'] ?>">
					  <input type="text" id="inputNama" name="inputNama" placeholder="Nama Item" value="<?php echo $item['nama_item'] ?>">
					</div>  
					
					<div class="col-lg-3">
						<div class="input-group margin-bottom-sm">
							<span class="input-group-addon">Satuan : </span>  
							<input type="text" id="inputSatuanJual" name="inputSatuanJual" placeholder="Satuan" value="<?php echo $item['satuan_jual'] ?>">	
								
						</div>  
					  
					</div>  
					
					<!--
					<div class="col-md-1">
		    
					  <label class="checkbox inline">
						<input type="checkbox" id="generik" name="generik" value="1" <?php if($item['isgenerik']==1 || $item['isgenerik']=="1") echo 'checked'; ?>> Generik
					  </label>
					  
					</div>
					-->
					</div>
			</div>
			
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">Kategori :</label>
					<div class="col-lg-10">
					<div class="col-lg-3">
						<select name="kategori" id="kategori" class="input-large">
						  <?php foreach ($kategori as $kategori_item): ?>
						  <option value="<?php echo $kategori_item['id_kategori']; ?>" <?php if($item['kategori']==$kategori_item['id_kategori']) echo 'selected'; ?>><?php echo $kategori_item['nama_kategori']; ?></option>
						  <?php endforeach ?>
						</select>
					</div>
					<!--
					<div class="col-lg-5">
						<div class="input-group margin-bottom-sm">
							<span class="input-group-addon">Supplier Utama : </span>
							<select name="supplier" id="supplier" class="input-large">
							  <option value="0" <?php if($item['supplier']==0 || $item['supplier']=="0") echo 'selected'; ?>>Tidak Ada</option>
							  <?php foreach ($supplier as $supplier_item): ?>
							  <option value="<?php echo $supplier_item['id_supplier']; ?>" <?php if($item['supplier']==$supplier_item['id_supplier']) echo 'selected'; ?>><?php echo $supplier_item['nama_supplier']; ?></option>
							  <?php endforeach ?>
							</select>
						</div>	
					</div>
					-->
					</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-lg-2" for="inputPersenMargin">PERSEN MARGIN :</label>
				<div class="col-lg-10">
					<div class="col-lg-4">
						<div class="input-group margin-bottom-sm">
							<input type="number" id="inputPersenMargin" name="inputPersenMargin" value="<?php echo $item['persen_margin'] ?>">
							<span class="input-group-addon sj">%</span>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">	
					<label class="control-label col-lg-2" for="inputNama">HARGA BELI/JUAL :</label>
					<div class="col-lg-10">					
						<div class="col-lg-4">
							<div class="input-group margin-bottom-sm">
								<span class="input-group-addon">Harga Beli : Rp</span>  
								<input type="text" id="inputHargaBeli" name="inputHargaBeli" placeholder="Harga Beli Terakhir" value="<?php echo $item['harga_beli'] ?>">
							</div>  
							<div class="alert-info" id="formathargabeli"> </div>
						</div>  
						<div class="col-lg-4">
							<div class="input-group margin-bottom-sm">
								<span class="input-group-addon">Harga Jual : Rp</span>  
								<input type="text" id="inputHargaJual" name="inputHargaJual" placeholder="Harga Jual Terakhir" value="<?php echo $item['harga_jual'] ?>">
							</div>
							<div class="alert-info" id="formathargajual"> </div>
						</div>  
					</div>
			</div>
			
			<!--
			<div class="form-group">	
					<label class="control-label col-lg-2" for="inputNama">SATUAN BELI/JUAL :</label>
					<div class="col-lg-10">					
						<div class="col-lg-4">
							<div class="input-group margin-bottom-sm">
								<span class="input-group-addon">Satuan Beli : </span>  
								<input type="text" id="inputSatuanBeli" name="inputSatuanBeli" placeholder="Satuan Beli" value="<?php echo $item['satuan_beli'] ?>">
							</div>	
						</div>  
						<div class="col-lg-4">
							<div class="input-group margin-bottom-sm">
								<span class="input-group-addon">Satuan Jual : </span>  
								<input type="text" id="inputSatuanJual" name="inputSatuanJual" placeholder="Satuan Jual" value="<?php echo $item['satuan_jual'] ?>">
							</div>
						</div>  
					</div>
			</div>
			
			<div class="form-group">	
					<label class="control-label col-lg-2" for="inputNama">KONVERSI SATUAN :</label>
					<div class="col-lg-10">					
						<div class="col-lg-8">
							<div class="input-group margin-bottom-sm">
								<span class="input-group-addon">1 </span>  
								<span class="input-group-addon sb">satuan beli</span>  
								<span class="input-group-addon"> = </span>
								<input type="number" id="sb_ke_sj" name="sb_ke_sj" value="<?php echo $item['jumlah_sb_ke_sj'] ?>">
								<span class="input-group-addon sj">satuan jual</span>
							</div>	
						</div>  
					</div>
			</div>
			-->
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">STOCK :</label>
					<div class="col-lg-10">
					
					<div class="col-lg-4">
						<div class="input-group margin-bottom-sm">
							<span class="input-group-addon">Stock Awal : </span>  
							<input type="number" id="inputStockAwal" name="inputStockAwal" placeholder="Stock Awal" value="<?php echo $item['stock_awal'] ?>">
							<span class="input-group-addon sj">satuan</span>
						</div>
					</div>  
					<div class="col-lg-4">
						<div class="input-group margin-bottom-sm">
							<span class="input-group-addon">Minimum Stock : </span>  
							<input type="number" id="inputStockMin" name="inputStockMin" placeholder="Minimum Stock" value="<?php echo $item['stock_min'] ?>">
							<span class="input-group-addon sj">satuan</span>
						</div>
					</div>  
					
					
					</div>
			</div>
			
			<div class="form-group">	
					<label class="control-label col-lg-2" for="inputNama">STOCK AWAL PHC :</label>
					<div class="col-lg-10">					
						<div class="col-lg-8">
							<input type="text" id="inputStockAwalPhc" name="inputStockAwalPhc" placeholder="Satuan" value="<?php echo $item['stock_awal_phc'] ?>">	
						</div>  
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">LOKASI ITEM :</label>
					<div class="col-lg-10">
					<div class="col-lg-8">
						<input type="text" id="lokasi" name="lokasi" placeholder="Lokasi Item" value="<?php echo $item['lokasi'] ?>">
					</div>
					
					</div>
			</div>
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">KETERANGAN :</label>
					<div class="col-lg-10">
					<div class="col-lg-8">
						<input type="text" id="keterangan" name="keterangan" placeholder="Keterangan" value="<?php echo $item['keterangan'] ?>">
					</div>
					
					</div>
			</div>
			
			
			<div class="form-group">
					
					<label class="control-label col-lg-2" for="inputNama">&nbsp;</label>
					<div class="col-lg-10">
					<div class="col-lg-7">
						<button type="submit" class="btn btn-info "><i class="fa fa-save  "></i> Update</button>
					</div>
					
					</div>
			</div>
		  
		  
		</form>
   
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>


<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>

<script type="text/javascript">
function rubah_satuanjual() 
	{
     var inputSatuanJual=$('#inputSatuanJual').val();
     $(".sj").text(inputSatuanJual);
	}
	
function rubah_satuanbeli() 
	{
     var inputSatuanBeli=$('#inputSatuanBeli').val();
     $(".sb").text(inputSatuanBeli);
	}

function formatRupiah(nilaiUang2)
{
	var nilaiUang=nilaiUang2+"";
  var nilaiRupiah 	= "";
  var jumlahAngka 	= nilaiUang.length;
  
  while(jumlahAngka > 3)
  {
	
	sisaNilai = jumlahAngka-3;
    nilaiRupiah = "."+nilaiUang.substr(sisaNilai,3)+""+nilaiRupiah;
    
    nilaiUang = nilaiUang.substr(0,sisaNilai)+"";
    jumlahAngka = nilaiUang.length;
  }
 
  nilaiRupiah = nilaiUang+""+nilaiRupiah+",-";
  return nilaiRupiah;
}

$(document).ready(function(){

var nilai=$("#inputHargaBeli").val();
			$("#formathargabeli").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp "+formatRupiah(nilai));
			
var nilai=$("#inputHargaJual").val();
			$("#formathargajual").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp "+formatRupiah(nilai));

$('#inputSatuanJual').keyup(function() {
			rubah_satuanjual();
});

$('#inputSatuanBeli').keyup(function() {
			rubah_satuanbeli();
});

$('#inputPersenMargin').keyup(function(){
	calculateHargaJual();
});

$('#inputHargaBeli').keyup(function() {
	var nilai=$("#inputHargaBeli").val();
	calculateHargaJual();
	$("#formathargabeli").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp "+formatRupiah(nilai));
});

});

function calculateHargaJual(){
	var persenMargin 	= parseInt($('#inputPersenMargin').val());
	var hargaBeli 		= parseInt($('#inputHargaBeli').val());
	var hargaJual 		= (1.1 * hargaBeli) + (1.1 * hargaBeli * persenMargin / 100);
	$('#inputHargaJual').val(hargaJual.toFixed(2));
	$("#formathargajual").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp "+formatRupiah(hargaJual.toFixed(0)));
}


</script>
</body></html>