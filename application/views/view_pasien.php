<div class="container">
   
      <h2>Pasien</h2>
	  <h2>
	  <a class="btn btn-primary btn-large" href="<?php echo site_url("pasien/pasien_baru"); ?>">Daftarkan Pasien Baru</a>
	  </h2>
		<?php if($status==1) 
		{
		?>
		<div class="alert alert-success">
		Data Berhasil Dimasukkan!!
		</div>
		<?php } ?>
		
		<?php if($status==2) 
		{
		?>
		<div class="alert alert-info">
		Data Berhasil Diupdate!!
		</div>
		<?php } ?>
		
		<?php if($status==3) 
		{
		?>
		<div class="alert alert-danger">
		Data Berhasil Dihapus!!
		</div>
		<?php } ?>
		
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
			<thead>
				<tr>
					<th>Nama Pasien</th>
					<th>Jenis Kelamin</th>
					<th>Umur</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ($pasien as $pasien_item): ?>
			
			<tr class="odd gradeX">
					<td><?php echo $pasien_item['nama_pasien'] ?></td>
					<td><?php echo if($pasien_item['jenis_kelamin']==0) echo "Laki-laki"; else echo "Perempuan"; ?></td>
					<td class="center"> <a class="btn btn-primary" href="<?php echo site_url("pasien/edit_pasien/".$pasien_item['id_pasien']); ?>">Edit</a> </td>
					<td class="center"><a class="btn btn-danger" href="<?php echo site_url("pasien/delete_pasien/".$pasien_item['id_pasien']); ?>">Delete</a></td>
				</tr>
		
			<?php 
			
			endforeach ?>
		
				
				
			</tbody>
		</table>
      
   
</div>
