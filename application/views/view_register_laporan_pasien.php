<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}
?>	
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
<link href="<?php echo base_url('assets/css/DT_bootstrap.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/legends/css/demo.css') ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/css/styles.css') ?>" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>	
<script src="<?php echo base_url('assets/js/Chart.js') ?>"></script>
<script src="<?php echo base_url('assets/legends/src/legend.js') ?>"></script>
</head>


<!-- Body -->
<body class="light">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->

		<div class="layout">
            <div class="header-text">Laporan Klinik Jantung MUSI</div>
            <div class="img-theme" data-bind="click:toggleTheme"></div>
            <div class="content helth">
                
                <div class="row">
                    <div class="legend-row">
                        <div class="legend">Laporan Kunjungan Pasien </div>
                        <div class="hr"></div>
                    </div>
                    <div class="clear"></div>
					<?php
						$total=($laporanharianpasien==NULL ? 0:$laporanharianpasien['jumlah']);
						$baru=($laporanharianpasien==NULL ? 0:$laporanharianpasien['jumlahbaru']);
						
						$month=date("n");
						$year=date("Y");

						$labelmonth="Januari";

						if($month==2)
						$labelmonth="Februari";
						else if($month==3)
						$labelmonth="Maret";
						else if($month==4)
						$labelmonth="April";
						else if($month==5)
						$labelmonth="Mei";
						else if($month==6)
						$labelmonth="Juni";
						else if($month==7)
						$labelmonth="Juli";
						else if($month==8)
						$labelmonth="Agustus";
						else if($month==9)
						$labelmonth="September";
						else if($month==10)
						$labelmonth="Oktober";
						else if($month==11)
						$labelmonth="November";
						else if($month==12)
						$labelmonth="Desember";
						
						?>
                    <div class="col1">
                        <div class="text">Laporan Jumlah Kunjungan Pasien <?php echo $labelmonth.' '.$year; ?>.</div>
                        <label id="requestNumber" class="label-value" ><?php echo $jumlahbulanini; ?></label>
                    </div>
                    <div class="col1" >
						<div class="text">Laporan Kunjungan Pasien Hari ini (Pasien Baru/Total)</div>
						
                        <label id="requestNumber" class="label-value" ><?php echo $baru;  ?>/<?php echo $total; ?></label>
					</div>
                    <div class="col3" >
					<div style="position: relative;">
					<canvas id="canvas" height="160" width="600"></canvas>
					<div id="tooltip"></div>
					
					</div>
					<script>
					var labels=<?php echo $labels; ?>;
					var values=<?php echo $values; ?>;
					var values2=<?php echo $values2; ?>;
					
					var lineChartData = {
						labels : labels,
						datasets : [
							{
								fillColor : "rgba(220,220,220,0.5)",
								strokeColor : "rgba(220,220,220,1)",
								pointColor : "rgba(220,220,220,1)",
								pointStrokeColor : "#fff",
								data : values,
								mouseover: function(data) {
									// data returns details about the point hovered, such as x and y position and index, as
									// well as details about the hover event in data.event
									// You can do whatever you like here, but here is a sample implementation of a tooltip
									var active_value = values[data.point.dataPointIndex];
									var active_date = labels[data.point.dataPointIndex];
									// For details about where this tooltip came from, read below
									//alert(active_value);
									var base_url = "<?php echo base_url(''); //you have to load the "url_helper" to use this function ?>";
									$("#tooltip").html("<img class='arrow' src='"+base_url+"/assets/css/arrow.png' /><table style='height:200%;'><tr ><td style='background-color:#75c0e0; color:#fff;'><strong style='font-size: 1.8em; font-weight: bold;'>"+active_value +"</strong></td><td><font style='font-size:1em;'>Total Pasien<hr style='margin: 0px;'>"+active_date+" </font></td></tr></table>").css("position", "absolute").css("left", data.point.x+17).css("top", data.point.y).css("display", 'block').css("background-color", '#FFF');
								  },
								  mouseout: function (data) {
									// Hide the tooltip
									$("#tooltip").css("display","none");
								  }
							},
							{
								fillColor : "rgba(151,187,205,0.5)",
								strokeColor : "rgba(151,187,205,1)",
								pointColor : "rgba(151,187,205,1)",
								pointStrokeColor : "#fff",
								data : values2,
								mouseover: function(data) {
									// data returns details about the point hovered, such as x and y position and index, as
									// well as details about the hover event in data.event
									// You can do whatever you like here, but here is a sample implementation of a tooltip
									var active_value = values2[data.point.dataPointIndex];
									var active_date = labels[data.point.dataPointIndex];
									// For details about where this tooltip came from, read below
									//alert(active_value);
									var base_url = "<?php echo base_url(''); //you have to load the "url_helper" to use this function ?>";
									$("#tooltip").html("<img class='arrow' src='"+base_url+"/assets/css/arrow.png' /><table style='height:200%;'><tr ><td style='background-color:#75c0e0; color:#fff;'><strong style='font-size: 1.8em; font-weight: bold;'>"+active_value +"</strong></td><td><font style='font-size:1em;'>Pasien Baru<hr style='margin: 0px;'>"+active_date+" </font></td></tr></table>").css("position", "absolute").css("left", data.point.x+17).css("top", data.point.y).css("display", 'block').css("background-color", '#FFF');
								  },
								  mouseout: function (data) {
									// Hide the tooltip
									$("#tooltip").css("display","none");
								  }
							}
						]
						
					}
					var maxi=Math.max.apply(Math,values);
					
					var options={
						scaleOverlay : true,
						scaleOverride : true,
				
						//** Required if scaleOverride is true **
						//Number - The number of steps in a hard coded scale
						scaleSteps : 5,
						//Number - The value jump in the hard coded scale
						scaleStepWidth : maxi/5,
						//Number - The scale starting value
						scaleStartValue : 0,
					}

				var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Line(lineChartData, options);
				
				</script>
				
					</div>
					
                    
                </div>
                <div class="row" style="height:500px;">
                    <div class="legend-row">
                        <div class="legend">10 Besar Kota Asal Pasien Bulan <?php echo $mulai;?> s/d <?php echo $sampai;?></div>
                        <div class="hr"></div>
                    </div>
                    <div class="clear"></div>
					
                    <div class="col1"  >
					<div class="text">Laporan Kunjungan Pasien Bulan</div>
					<div class="text">Dari Bulan:</div>
					<form action="<?php echo site_url("laporan/detailpasien"); ?>" method="POST">
					
					<input class="input-small" size="16" type="text" value="<?php echo $mulai;?>" name="mulai" id="mulai" data-date-format="mm-yyyy">
					
					
					
					<div class="text">Sampai Bulan :</div>
					
					<input class="input-small" size="16" type="text" value="<?php echo $sampai;?>" name="sampai" id="sampai" data-date-format="mm-yyyy">
					
					<div class="text"><button type="submit" class="btn btn-info"><i class="icon-ok-sign icon-white"></i> Tampilkan</button></a></div>
					
					</form>
					<form action="<?php echo site_url("laporan/detailpasien2"); ?>" method="POST">
					<input type="hidden" value="<?php echo $mulai;?>" name="mulai2" id="mulai2" >
					<input type="hidden" value="<?php echo $sampai;?>" name="sampai2" id="sampai2">
					<div class="text"><button type="submit" class="btn btn-danger"><i class="icon-ok-sign icon-white"></i> Download</button></a></div>
					</form>
					</div>
					
                    <div class="col5"  >
					<div id="pieLegend"></div>
					</div>
					<div class="col4"  >
					<div class="text">Laporan Total Pasien Per Kota</div>
					<br>
					<div class="clear"></div>
					<div style="position: relative;">
						<canvas id="canvas2" height="300" width="300"></canvas>
					</div>
					<script>
					var pieData=<?php echo $pieData; ?>;
					
					var myPie = new Chart(document.getElementById("canvas2").getContext("2d")).Pie(pieData);
					legend(document.getElementById("pieLegend"), pieData);
					</script>
					</div>
					
					<div class="col5"  >
					<div id="donLegend"></div>
					</div>
					
					<div class="col4"  >
					<div class="text">Laporan Jumlah Pasien Baru Per Kota</div>
					
					<div style="position: relative;">
					<div class="clear"></div>
					<br>
					<div style="position: relative;">
						<canvas id="canvas3" height="300" width="300"></canvas>
						
					</div>
					<script>
						var doughnutData=<?php echo $donData; ?>;
						
						var myDoughnut = new Chart(document.getElementById("canvas3").getContext("2d")).Doughnut(doughnutData);
						legend(document.getElementById("donLegend"), doughnutData);
					</script>
					</div>
					
					</div>
					
                    
                </div>
                
            </div>
        </div>
<script type="text/javascript">
$(document).ready(function(){
		$('#mulai').datepicker(
			{
			format: "mm-yyyy",
			viewMode: "months", 
			minViewMode: "months"
			}
			)
		  .on('changeDate', function(ev){
			
		});
		$('#sampai').datepicker(
			{
			format: "mm-yyyy",
			viewMode: "months", 
			minViewMode: "months"
			}
			)
		  .on('changeDate', function(ev){
			
		});
});
</script>
		

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>

<!-- Script -->

	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>

</body></html>