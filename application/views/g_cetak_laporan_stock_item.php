<?php 
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

function get_month($month)
{
	$labelmonth="Januari";

	if($month==2)
	$labelmonth="Februari";
	else if($month==3)
	$labelmonth="Maret";
	else if($month==4)
	$labelmonth="April";
	else if($month==5)
	$labelmonth="Mei";
	else if($month==6)
	$labelmonth="Juni";
	else if($month==7)
	$labelmonth="Juli";
	else if($month==8)
	$labelmonth="Agustus";
	else if($month==9)
	$labelmonth="September";
	else if($month==10)
	$labelmonth="Oktober";
	else if($month==11)
	$labelmonth="November";
	else if($month==12)
	$labelmonth="Desember";

	return $labelmonth;

}

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once 'PHPExcel/Classes/PHPExcel.php';

require_once 'PHPExcel/Classes/PHPExcel/IOFactory.php';

$fileName = 'PHPExcel/Examples/templates/1. Template Print Home.xlsx';
try {
    $objPHPExcel = PHPExcel_IOFactory::load($fileName);
} catch (Exception $e) {
    die("Error loading file: ".$e->getMessage()."<br />\n");
}
$date=date("d-m-Y");
$pieces = explode("-", $date);

$objPHPExcel->getActiveSheet()->setCellValue('A2', "Tanggal : ".$pieces[0].' '.get_month($pieces[1]).' '.$pieces[2]."");

$nomor=1;
$baseRow = 6;
foreach ($stock as $stock_item): 
	$stock_total=$stock_item['stock_gudang']+$stock_item['stock_awal'];
	
	$flag1="V";
	
	if($stock_total<=$stock_item['stock_min']) $flag1="X";
	
	if($nomor==1)
	{
		$row=7;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $nomor)
								  ->setCellValue('B'.$row, $stock_item['nama_item'])
								  ->setCellValue('C'.$row, $stock_item['stock_min'])
								  ->setCellValue('D'.$row, $stock_total)
								  ->setCellValue('E'.$row, $flag1);
	}
	else
	{
		$row = $baseRow + $nomor;
		$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);

		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $nomor)
								  ->setCellValue('B'.$row, $stock_item['nama_item'])
								  ->setCellValue('C'.$row, $stock_item['stock_min'])
								  ->setCellValue('D'.$row, $stock_total)
								  ->setCellValue('E'.$row, $flag1);

	}


$nomor++;

endforeach;

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Laporan Stock');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Laporan_Stock_'.$date.'.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>