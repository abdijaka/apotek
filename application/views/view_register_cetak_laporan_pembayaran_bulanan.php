<?php
require('fpdf17/mc_table.php');

function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

function get_month($month)
{
	$labelmonth="Januari";

	if($month==2)
	$labelmonth="Februari";
	else if($month==3)
	$labelmonth="Maret";
	else if($month==4)
	$labelmonth="April";
	else if($month==5)
	$labelmonth="Mei";
	else if($month==6)
	$labelmonth="Juni";
	else if($month==7)
	$labelmonth="Juli";
	else if($month==8)
	$labelmonth="Agustus";
	else if($month==9)
	$labelmonth="September";
	else if($month==10)
	$labelmonth="Oktober";
	else if($month==11)
	$labelmonth="November";
	else if($month==12)
	$labelmonth="Desember";

	return $labelmonth;

}

$pdf=new PDF_MC_Table('P','mm','A4');
$pdf->AddPage();

$pdf->SetFont('Arial','B',12);

$pdf->SetY(5);

$pdf->Cell(210, 2.7, "LAPORAN PENDAPATAN BULANAN", 0, 0, 'C');

$pdf->SetY(10);

$pdf->Cell(210, 2.7, "Musi Heart Clinic\n", 0, 0, 'C');

$pdf->SetY(15);

$pieces = explode("-", $mulai);

$pdf->Cell(210, 2.7, "Bulan : ".get_month($pieces[0])." ".$pieces[1]."", 0, 0, 'C');

$pdf->SetFont('Arial','B',9);
//Table with 20 rows and 4 columns
$pdf->SetWidths(array(10,20,40, 30, 50));
$pdf->SetY(25);

$pdf->SetX(25);

$pdf->Row(array("No", "Tanggal", "Pendapatan Netto", "Charge Bank", "Pendapatan Netto+Charge"));
$pdf->SetFont('Arial','',8);

$nomor=1;
$total_pendapatan=0;
$total_charge=0;

foreach ($laporan as $laporan_item):
$pdf->SetX(25);
$pdf->Row(array($nomor, $laporan_item['tanggal'], formatRupiah($laporan_item['jumlah']), formatRupiah($laporan_item['charge']), formatRupiah($laporan_item['jumlah']+$laporan_item['charge'])));
$nomor++;
$total_pendapatan=$total_pendapatan+$laporan_item['jumlah'];
$total_charge=$total_charge+$laporan_item['charge'];

endforeach;

$pdf->SetFont('Arial','B',8);
$pdf->SetX(25);
$pdf->Row(array('', 'Total', formatRupiah($total_pendapatan), formatRupiah($total_charge), formatRupiah($total_pendapatan+$total_charge)));


$pdf->Output("Laporan Pendapatan_".get_month($pieces[0])." ".$pieces[1].".pdf", "D");
//$pdf->Output();
?>