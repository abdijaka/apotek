<!DOCTYPE html>
<!-- saved from url=(0044)https://apps.ptiik.ub.ac.id/apps/info/agenda -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head -->
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="css/style.min.css">
		
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="img/favicon.png">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
<nav class="navbar navbar-default navbar-fixed-top navbar-no-margin-bottom" role="navigation">
    <button type="button" class="navbar-toggle navbar-toggle-side">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
<div class="container">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href=""><img class="logo-ptiik-apps" src="img/musi.png" alt="PTIIK APPS logo"></a>
  </div>

  </div>
</nav>

</header>

<!-- Content -->
<section id="wrap" style="background-image:url('img/fort_awesome.jpg');">
<section class="content content-white" style="background-image:url('img/fort_awesome.jpg'); height:585px; ">
	
    <div class="container container-content" > 	
		
		<form action="https://apps.ptiik.ub.ac.id/apps/registrasi" id="form-save-user" class="form-horizontal" role="form" method="POST">
		
		  <div class="modal-dialog" >
			<div class="modal-content modal-content-no-radius" style="background: #fff;
    filter:alpha(opacity=90);
    -moz-opacity:0.9; 
    opacity: 0.9;
    ">
			  <div class="modal-header modal-header-blue">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title modal-title-center" id="registerModalLabel">LOGIN</h4>
			  </div>
			  <div class="modal-body" 	>
			  <div class="alert alert-block alert-info"><p>
			  Silahkan Masukkan Username dan Password Anda!.
			  </p></div>
				<div class="form-group">
					<label for="inputEmail1" class="col-lg-2 control-label">Username</label>
					<div class="col-lg-10">
					<input type="email" name="email" class="form-control" id="inputEmail1" placeholder="Username">
					
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail1" class="col-lg-2 control-label">Password</label>
					<div class="col-lg-10">
					<input type="email" name="email" class="form-control" id="inputEmail1" placeholder="Password">
					
					</div>
				</div>
			  </div>
			  <div class="modal-footer">
				<button type="submit" name="b_reg" class="btn btn-content-ready btn-no-radius">Login</button>
			  </div>
			</div>
		  </div>
		
		</form>
	</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>


<!-- Script -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/collapse.min.js"></script>
<script type="text/javascript" src="js/dropdown.js"></script>
<script type="text/javascript" src="js/modal.min.js"></script>
<script type="text/javascript" src="js/tab.js"></script>
<script type="text/javascript" src="js/tooltip.min.js"></script>
<script type="text/javascript" src="js/moment.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/bootsrap-datatables.js"></script>
<script type="text/javascript" src="js/transition.min.js"></script>

</body></html>