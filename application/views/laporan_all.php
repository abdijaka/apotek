<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}
?>	
<body class="light">
        <div class="layout">
            <div class="header-text">Laporan Klinik Jantung MUSI</div>
            <div class="img-theme" data-bind="click:toggleTheme"></div>
            <div class="content helth">
                <a href="<?php echo site_url('laporan/detailpasien'); ?>"><div class="slide-button right-button" ></div></a>
				<a href="<?php echo site_url('laporan/detailkeuangan'); ?>"><div class="slide-button2 right-button" ></div></a>
                <div class="row">
                    <div class="legend-row">
                        <div class="legend">Laporan Kunjungan Pasien </div>
                        <div class="hr"></div>
                    </div>
                    <div class="clear"></div>
					<?php
						$total=($laporanharianpasien==NULL ? 0:$laporanharianpasien['jumlah']);
						$baru=($laporanharianpasien==NULL ? 0:$laporanharianpasien['jumlahbaru']);
						
						$month=date("n");
						$year=date("Y");

						$labelmonth="Januari";

						if($month==2)
						$labelmonth="Februari";
						else if($month==3)
						$labelmonth="Maret";
						else if($month==4)
						$labelmonth="April";
						else if($month==5)
						$labelmonth="Mei";
						else if($month==6)
						$labelmonth="Juni";
						else if($month==7)
						$labelmonth="Juli";
						else if($month==8)
						$labelmonth="Agustus";
						else if($month==9)
						$labelmonth="September";
						else if($month==10)
						$labelmonth="Oktober";
						else if($month==11)
						$labelmonth="November";
						else if($month==12)
						$labelmonth="Desember";
						
						?>
                    <div class="col1">
                        <div class="text">Laporan Jumlah Kunjungan Pasien <?php echo $labelmonth.' '.$year; ?>.</div>
                        <label id="requestNumber" class="label-value" ><?php echo $jumlahbulanini; ?></label>
                    </div>
                    <div class="col1" >
						<div class="text">Laporan Kunjungan Pasien Hari ini (Pasien Baru/Total)</div>
						
                        <label id="requestNumber" class="label-value" ><?php echo $baru;  ?>/<?php echo $total; ?></label>
					</div>
                    <div class="col3" >
					<div style="position: relative;">
					<canvas id="canvas" height="160" width="600"></canvas>
					<div id="tooltip"></div>
					
					</div>
					<script>
					var labels=<?php echo $labels; ?>;
					var values=<?php echo $values; ?>;
					var values2=<?php echo $values2; ?>;
					
					var lineChartData = {
						labels : labels,
						datasets : [
							{
								fillColor : "rgba(220,220,220,0.5)",
								strokeColor : "rgba(220,220,220,1)",
								pointColor : "rgba(220,220,220,1)",
								pointStrokeColor : "#fff",
								data : values,
								mouseover: function(data) {
									// data returns details about the point hovered, such as x and y position and index, as
									// well as details about the hover event in data.event
									// You can do whatever you like here, but here is a sample implementation of a tooltip
									var active_value = values[data.point.dataPointIndex];
									var active_date = labels[data.point.dataPointIndex];
									// For details about where this tooltip came from, read below
									//alert(active_value);
									var base_url = "<?php echo base_url(''); //you have to load the "url_helper" to use this function ?>";
									$("#tooltip").html("<img class='arrow' src='"+base_url+"/assets/css/arrow.png' /><table style='height:200%;'><tr ><td style='background-color:#75c0e0; color:#fff;'><strong style='font-size: 1.8em; font-weight: bold;'>"+active_value +"</strong></td><td><font style='font-size:1em;'>Total Pasien<hr style='margin: 0px;'>"+active_date+" </font></td></tr></table>").css("position", "absolute").css("left", data.point.x+17).css("top", data.point.y).css("display", 'block').css("background-color", '#FFF');
								  },
								  mouseout: function (data) {
									// Hide the tooltip
									$("#tooltip").css("display","none");
								  }
							},
							{
								fillColor : "rgba(151,187,205,0.5)",
								strokeColor : "rgba(151,187,205,1)",
								pointColor : "rgba(151,187,205,1)",
								pointStrokeColor : "#fff",
								data : values2,
								mouseover: function(data) {
									// data returns details about the point hovered, such as x and y position and index, as
									// well as details about the hover event in data.event
									// You can do whatever you like here, but here is a sample implementation of a tooltip
									var active_value = values2[data.point.dataPointIndex];
									var active_date = labels[data.point.dataPointIndex];
									// For details about where this tooltip came from, read below
									//alert(active_value);
									var base_url = "<?php echo base_url(''); //you have to load the "url_helper" to use this function ?>";
									$("#tooltip").html("<img class='arrow' src='"+base_url+"/assets/css/arrow.png' /><table style='height:200%;'><tr ><td style='background-color:#75c0e0; color:#fff;'><strong style='font-size: 1.8em; font-weight: bold;'>"+active_value +"</strong></td><td><font style='font-size:1em;'>Pasien Baru<hr style='margin: 0px;'>"+active_date+" </font></td></tr></table>").css("position", "absolute").css("left", data.point.x+17).css("top", data.point.y).css("display", 'block').css("background-color", '#FFF');
								  },
								  mouseout: function (data) {
									// Hide the tooltip
									$("#tooltip").css("display","none");
								  }
							}
						]
						
					}
					var maxi=Math.max.apply(Math,values);
					
					var options={
						scaleOverlay : true,
						scaleOverride : true,
				
						//** Required if scaleOverride is true **
						//Number - The number of steps in a hard coded scale
						scaleSteps : 5,
						//Number - The value jump in the hard coded scale
						scaleStepWidth : maxi/5,
						//Number - The scale starting value
						scaleStartValue : 0,
					}

				var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Line(lineChartData, options);
				
				</script>
				
					</div>
					
                    
                </div>
                <div class="row">
                    <div class="legend-row">
                        <div class="legend">Laporan Bisnis Klinik</div>
                        <div class="hr"></div>
                    </div>
                    <div class="clear"></div>
					<?php
						$total=($laporanharianpendapatan==NULL ? 0:$laporanharianpendapatan['jumlah']);
						
						?>
                    <div class="col1">
                        <div class="text">Laporan Omzet Bisnis <?php echo $labelmonth.' '.$year; ?>.</div>
                        <label id="CPU" class="label-value" ><?php echo $pendapatanbulanini/1000000; ?><font style=" font-family:Arial; font-weight:lighter;"> juta</font></label>
                    </div>
                    <div class="col1" >
						<div class="text">Laporan Omzet Bisnis Hari ini </div>
						
                        <label id="CPU" class="label-value" ><?php echo formatRupiah($total); ?></label>
					</div>
                    <div class="col3" >
					<div style="position: relative;">
					<canvas id="canvas2" height="160" width="600"></canvas>
					<div id="tooltip2"></div>
					
					</div>
					<script>
					var pendlabels=<?php echo $pendlabels; ?>;
					var pendvalues=<?php echo $pendvalues; ?>;
					
					var lineChartData = {
						labels : pendlabels,
						datasets : [
							{
								fillColor : "rgba(151,187,205,0.5)",
								strokeColor : "rgba(151,187,205,1)",
								pointColor : "rgba(151,187,205,1)",
								pointStrokeColor : "#fff",
								data : pendvalues,
								mouseover: function(data) {
									// data returns details about the point hovered, such as x and y position and index, as
									// well as details about the hover event in data.event
									// You can do whatever you like here, but here is a sample implementation of a tooltip
									var active_value = pendvalues[data.point.dataPointIndex];
									var active_date = pendlabels[data.point.dataPointIndex];
									var temp=active_value/1000000;
									// For details about where this tooltip came from, read below
									//alert(active_value);
									var base_url = "<?php echo base_url(''); //you have to load the "url_helper" to use this function ?>";
									$("#tooltip2").html("<img class='arrow' src='"+base_url+"/assets/css/arrow.png' /><table style='height:200%;'><tr ><td style='background-color:#75c0e0; color:#fff;'><strong style='font-size: 1.5em; font-weight: bold;'>"+temp +"jt</strong></td><td><font style='font-size:1em;'>Omzet<hr style='margin: 0px;'>"+active_date+" </font></td></tr></table>").css("position", "absolute").css("left", data.point.x+17).css("top", data.point.y).css("display", 'block').css("background-color", '#FFF');
								  },
								  mouseout: function (data) {
									// Hide the tooltip
									$("#tooltip2").css("display","none");
								  }
							}
						]
						
					}
					var maxi=Math.max.apply(Math,pendvalues);
					
					var options={
						scaleOverlay : true,
						scaleOverride : true,
				
						//** Required if scaleOverride is true **
						//Number - The number of steps in a hard coded scale
						scaleSteps : 5,
						//Number - The value jump in the hard coded scale
						scaleStepWidth : maxi/5,
						//Number - The scale starting value
						scaleStartValue : 0,
					}

				var myLine = new Chart(document.getElementById("canvas2").getContext("2d")).Line(lineChartData, options);
				
				</script>
	
					</div>
                </div>
                
            </div>
        </div>
    