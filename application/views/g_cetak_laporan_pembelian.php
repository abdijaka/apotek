<?php 
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "" . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

function get_month($month)
{
	$labelmonth="Januari";

	if($month==2)
	$labelmonth="Februari";
	else if($month==3)
	$labelmonth="Maret";
	else if($month==4)
	$labelmonth="April";
	else if($month==5)
	$labelmonth="Mei";
	else if($month==6)
	$labelmonth="Juni";
	else if($month==7)
	$labelmonth="Juli";
	else if($month==8)
	$labelmonth="Agustus";
	else if($month==9)
	$labelmonth="September";
	else if($month==10)
	$labelmonth="Oktober";
	else if($month==11)
	$labelmonth="November";
	else if($month==12)
	$labelmonth="Desember";

	return $labelmonth;

}

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once 'PHPExcel/Classes/PHPExcel.php';

require_once 'PHPExcel/Classes/PHPExcel/IOFactory.php';

$fileName = 'PHPExcel/Examples/templates/6. Nota Pembelian.xlsx';
try {
    $objPHPExcel = PHPExcel_IOFactory::load($fileName);
} catch (Exception $e) {
    die("Error loading file: ".$e->getMessage()."<br />\n");
}

$pieces = explode("-", $pembelian['tgl_faktur']);

$objPHPExcel->getActiveSheet()->setCellValue('B3', "Faktur Pembelian    : ".$pembelian['nomor_faktur']);
$objPHPExcel->getActiveSheet()->setCellValue('B4', "Tanggal Pembelian  : ".$pieces[2].' '.get_month($pieces[1]).' '.$pieces[0]."");
$objPHPExcel->getActiveSheet()->setCellValue('B5', "Supplier                  : ".$pembelian['nama_supplier']);

$nomor=1;
$baseRow = 7;
$i=0;
$netto=0;
foreach ($list as $list_item): 
	$i++;
	$netto=$netto+$list_item['total'];
	if($nomor==1)
	{
		$row=8;
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $nomor)
								  ->setCellValue('C'.$row, $list_item['nama_item'])
								  ->setCellValue('D'.$row, $list_item['jumlah'])
								  ->setCellValue('E'.$row, formatRupiah($list_item['harga']))
								  ->setCellValue('F'.$row, formatRupiah($list_item['total']));
	}
	else
	{
		$row = $baseRow + $nomor;
		$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);

		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $nomor)
								  ->setCellValue('C'.$row, $list_item['nama_item'])
								  ->setCellValue('D'.$row, $list_item['jumlah'])
								  ->setCellValue('E'.$row, formatRupiah($list_item['harga']))
								  ->setCellValue('F'.$row, formatRupiah($list_item['total']));

	}


$nomor++;

endforeach;

$objPHPExcel->getActiveSheet()->setCellValue('F'.($row+2), "Rp ".formatRupiah($netto));
$objPHPExcel->getActiveSheet()->setCellValue('F'.($row+3), "Rp ".formatRupiah($pembayaran[0]['chargebank']));
$objPHPExcel->getActiveSheet()->setCellValue('F'.($row+4), "Rp ".formatRupiah($pembelian['total']));
$objPHPExcel->getActiveSheet()->setCellValue('E'.($row+7), "".$pembelian['operator']);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Kwitansi Pembelian');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Kwitansi_Pembelian_'.$pembelian['nomor_faktur'].'.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>