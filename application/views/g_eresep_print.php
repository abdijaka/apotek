<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id">
<!-- Head -->
<head>
	<!-- Meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>MUSI APPS</title>
	<meta content="MUSI Apps" name="description">
	<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
	<meta content="MUSI" name="author">
	<meta content="yes" name="apple-mobile-web-app-capable">
	<meta content="black" name="apple-mobile-web-app-status-bar-style">
	<!-- Style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
	<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<![endif]-->
	<!-- Icon -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>
<!-- Body -->
<body>
	<!-- Content -->
	<div class="container container-content">
		<div class="row non-print_area">
		    <div class="col-md-12">
				<legend style="text-align:center;">PREVIEW E-RESEP</legend>
			</div>
		</div>

		<div class="row">
			<div align="center">
				<div id='preview_eresep' style='width: 500px;'>
					<table border='0' width='100%'>
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>
						<tr>
							<td colspan='3'><center><strong>E-RESEP</strong></center></td>
						</tr>
						<tr>
							<td colspan='3'><center><strong>APOTEK KARDIA MEDIKA</strong></center></td>
						</tr>
						<tr>
							<td colspan='3'><center><small><strong>Jl. Raya Kertajaya Indah No. 66 Blok H No. 109 Surabaya</strong></small></center></td>
						</tr>
						<tr>
							<td colspan='3'><center><small><strong>Telp. (031) 5947825</strong></small></center></td>
						</tr>
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>
						<tr>
							<td>Dokter</td>
							<td>:</td>
							<td><?php echo $eresep['nama_dokter']; ?></td>
						</tr>
						<tr>
							<td colspan='3'><hr /></td>
						</tr>
						<tr>
							<td colspan='3' align='right'>Surabaya, <?php echo $eresep['tgl_eresep']; ?></td>
						</tr>
						<tr>
							<td colspan='3'>
								<table border='0' width="100%">
									<tbody>
										<?php foreach ($detail as $d) { ?>
											<tr>
												<td colspan='4'>R/</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td><?php echo $d['nama_item']; ?></td>
												<td align='right'><?php echo $d['jumlah']; ?></td>
												<td align='right'><?php echo $d['aturan_pakai']; ?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan='3'><hr /></td>
						</tr>
						<tr>
							<td>Nama Pasien</td>
							<td>:</td>
							<td><?php echo $eresep['nama_pasien']; ?></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td>:</td>
							<td><?php echo $eresep['alamat'] ?></td>
						</tr>
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		
		<div class="row non-print_area">
		    <div class="col-md-12">
				<hr />
			</div>
		</div>
		<div class="row non-print_area">
		    <div class="col-md-12">
				<div class="col-md-10">&nbsp;</div>
				<div class="col-md-1">
					<a href="<?php echo site_url("g_eresep/view_eresep"); ?>" id='btn_back' class="btn btn-large btn-primary "><i class="fa fa-chevron-left fa-lg "></i> Back</a>
				</div>
				<div class="col-md-1">
					<a href="#" id='btn_print' class="btn btn-large btn-success "><i class="fa fa-print fa-lg "></i> Print</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Script -->
	<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>	
	<script>
		$('.navbar-toggle-side').click(function(e) {
			toggleSide(e,false)
			});
		$('.navbar-side-to-search').click(function(e) {
			toggleSide(e,true)
		});
			
		function toggleSide(action,search) {
			action.preventDefault();
			$('.navbar-side').toggleClass('mini-side');
			$('footer').toggleClass('mini-footer');
			$('#wrap').toggleClass('mini-side-open');
			if(search)$('.side-search-input').focus();
		}

		function ShowMenuNavJadwal(nama) {
			if(nama != "-1"){
				$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
				$(nama).toggleClass("in").toggleClass("fadeInRight");
			}else{
				$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
			}
				
		}
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#btn_print").on("click", function() {
				window.print();
			});
		});
	</script>
	<style type="text/css">
		@media print {
			.non-print_area { display: none !important; }
			#preview_resep { 
				display: block !important;
			}
		}
	</style>
</body>
</html>