<?php
function formatRupiah($nilaiUang)
{
  $nilaiRupiah 	= "";
  $jumlahAngka 	= strlen($nilaiUang);
  while($jumlahAngka > 3)
  {
    $nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
    $sisaNilai = strlen($nilaiUang) - 3;
    $nilaiUang = substr($nilaiUang,0,$sisaNilai);
    $jumlahAngka = strlen($nilaiUang);
  }
 
  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
  return $nilaiRupiah;
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
		
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">DETAIL PEMAKAIAN</legend>

			<?php
			$tanggal =date('j-m-Y', strtotime($pemakaian['tgl_pakai']));
			$jam =date('G:i:s', strtotime($pemakaian['tgl_pakai']));
			$date=date("d-m-Y");
			?>
			
		
			<div class="row">
			<div class="col-md-12">
				<div class="col-md-3">
					<label class="control-label" for="inputTanggal">Tanggal</label>
					<div class="input-append date" id="dp3" data-date="<?php echo $date; ?>" data-date-format="dd-mm-yyyy">
						<input type="text" value="<?php echo $tanggal; ?>" name="tanggalawal" id="tanggalawal" readonly>
					</div>	
				</div>	
				<div class="col-md-3">
						<label class="control-label" for="inputReg">No. Pemakaian</label>
						<input type="text" id="inputReg" name="inputReg" value="<?php echo $pemakaian['nomor_pakai']; ?>" readonly>
				</div>		
				<div class="col-md-2">
					<label class="control-label" for="inputID">Ruangan</label>
					
					<input type="text" id="ruangan" name="ruangan" value="<?php echo $pemakaian['nama_ruangan']; ?>" readonly>
					
				</div>	
				
				<div class="col-md-3">
					<label class="control-label" for="inputID">Pasien</label>
					
					<input type="text" id="pasien" name="pasien" value="<?php echo $pemakaian['nama_pasien']; ?>" readonly>
					
				</div>	
			
			</div>
			</div>
			<div class="row">
			<div class="col-md-12">
				<div class="col-md-3">
					<label class="control-label" for="inputTanggal">Nomor Registrasi</label>
						<input type="text" value="<?php echo $pemakaian['nomor_registrasi']; ?>" name="nomor_registrasi" id="nomor_registrasi" placeholder="Nomor Registrasi" readonly>
				</div>	
				
				<div class="col-md-3">
					<label class="control-label" for="inputTanggal">Nomor Rekam Medis</label>
						<input type="text" value="<?php echo $pemakaian['no_rekam_medis']; ?>" name="no_rekam_medis" id="no_rekam_medis" placeholder="Nomor Rekam Medis" readonly>
				</div>	
				
				<div class="col-md-2">
					<label class="control-label" for="inputID">Penjamin</label>
						<select name="penjamin" id="penjamin" class="input-large" readonly>
							  <?php foreach ($penjamin as $penjamin_item): ?>
							  <option <?php if($pemakaian['penjamin']==$penjamin_item['id_penjamin'] ) echo 'selected' ?> value="<?php echo $penjamin_item['id_penjamin']; ?>"><?php echo $penjamin_item['nama_penjamin']; ?></option>
							  <?php endforeach ?>
						</select>
				</div>	
				<div class="col-md-3">
					<label class="control-label" for="inputID">Dokter Pengirim</label>
						<select name="pengirim" id="pengirim" class="input-large" readonly>
							  <?php foreach ($dokter as $dokter_item): ?>
							  <option <?php if($pemakaian['dokter_pengirim']==$dokter_item['id_dokter'] ) echo 'selected' ?> value="<?php echo $dokter_item['id_dokter']; ?>"><?php echo $dokter_item['nama_dokter']; ?></option>
							  <?php endforeach ?>
						</select>
				</div>	
				
			</div>
			</div>
			
			<div class="row" id="tabel_tindakan">
				
			</div>
			
			<div class="row">
			<div class="col-md-12">
				
				<div class="col-md-5">
					<label class="control-label" for="inputTanggal">Tarif</label>
						<input type="text" value="<?php echo formatRupiah($pemakaian['tarif']); ?>" name="tarif" id="tarif" placeholder="Tarif" readonly>
				</div>	
				
			</div>
			</div>
			
			<div class="row" id="tabel_dokter">
				
			</div>
			
			<div class="row">
			<div class="col-md-12">
				
				<div class="col-md-11">
					<label class="control-label" for="inputTanggal">Catatan</label>
						<input type="text" name="catatan" id="catatan" placeholder="Catatan" value="<?php echo $pemakaian['catatan'] ?>" readonly>
				</div>	
				
				
			</div>
			</div>
			
		  <hr style="color: #0099FF; background-color: #0099FF; height: 1px;">
		  
			<div class="row">
			<div class="col-md-12">
			<div class="alert alert-info">
				<div class="container-fluid" style="margin-top:-15px; ">
				<div class="row-fluid">
			
              <div class="col-md-12">
				  
					   <div class="row">
					   <div class="col-md-12">
					   <div class="col-md-12">
					   <div id="tabel_layanan"></div>
					   </div>
					   </div>
					   </div>
					   
						 
						 
					  <div class="row">
						<div class="col-md-12">
						<div class="col-md-8">
						  &nbsp;
						</div>
						<div class="col-md-1">
						  <a href="<?php echo site_url('g_pemakaian/view_detail2/'.$pemakaian['id_pemakaian']); ?>" class="btn btn-large btn-success "><i class="fa fa-print "></i> Print</a>
						</div>
						<div class="col-md-3">
						  <a href="<?php echo site_url('g_pemakaian/create_pemakaian'); ?>" class="btn btn-large btn-info "><i class="fa fa-chevron-left "></i> Kembali ke Pemakaian</a>
						</div>
						</div>
					  </div>
					
				</div>
            </div>
			</div>
			</div>
			</div>
			
		  </div>
		  </div>
		  
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>

<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/transition.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/modal.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>

<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}


function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>
	
<script type="text/javascript">


function formatRupiah(nilaiUang2)
{
	var nilaiUang=nilaiUang2+"";
  var nilaiRupiah 	= "";
  var jumlahAngka 	= nilaiUang.length;
  
  while(jumlahAngka > 3)
  {
	
	sisaNilai = jumlahAngka-3;
    nilaiRupiah = "."+nilaiUang.substr(sisaNilai,3)+""+nilaiRupiah;
    
    nilaiUang = nilaiUang.substr(0,sisaNilai)+"";
    jumlahAngka = nilaiUang.length;
  }
 
  nilaiRupiah = nilaiUang+""+nilaiRupiah+",-";
  return nilaiRupiah;
}
	var controller = 'g_pemakaian';
	var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';

$(document).ready(function(){

var reg='<?php echo $pemakaian["id_pemakaian"]; ?>';
$.ajax({
	'url' : base_url + controller + '/show_detail_item',
	'type' : 'POST', //the way you want to send data to your URL
	'data' : {'reg' : reg},
	'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
		var container = $('#tabel_layanan'); //jquery selector (get element by id)
		if(data){
			
			container.html(data);
		}
	}
});

$.ajax({
	'url' : base_url + controller + '/show_detail_dokter',
	'type' : 'POST', //the way you want to send data to your URL
	'data' : {'reg' : reg},
	'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
		var container = $('#tabel_dokter'); //jquery selector (get element by id)
		if(data){
			container.html(data);
		}
	}
});

$.ajax({
	'url' : base_url + controller + '/show_detail_tindakan',
	'type' : 'POST', //the way you want to send data to your URL
	'data' : {'reg' : reg},
	'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
		var container = $('#tabel_tindakan'); //jquery selector (get element by id)
		if(data){
			container.html(data);
		}
	}
});

});


</script>


</body></html>