<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id"><!-- Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUSI APPS</title>
<meta content="MUSI Apps" name="description">
<meta content="MUSI, MUSI Application, MUSI APPS, MUSI" name="keywords">
<meta content="MUSI" name="author">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.min.css')?>">
<link href="<?php echo base_url('assets/css/datepicker.css') ?>" rel="stylesheet">		
<link href="<?php echo base_url('assets/css/DT_bootstrap.css') ?>" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<![endif]-->
<!-- Icon -->
<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>">
</head>


<!-- Body -->
<body style="">
<!-- Header -->
<header>
	<?php
		$this->load->view('templates/menubar_kiri');
	?>
</header>
<!-- Content -->
<section id="wrap">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend style="text-align:center;">DAFTAR ITEM</legend>
		<h2><a class="btn btn-primary btn-success" href="<?php echo site_url("g_item/item_baru"); ?>">Daftarkan Item Baru</a>
		</h2>
		<?php if($status==1) 
		{
		?>
		<div class="alert alert-success">
		Data Berhasil Dimasukkan!!
		</div>
		<?php } ?>
		
		<?php if($status==2) 
		{
		?>
		<div class="alert alert-info">
		Data Berhasil Diupdate!!
		</div>
		<?php } ?>
		
		<?php if($status==3) 
		{
		?>
		<div class="alert alert-danger">
		Data Berhasil Dihapus!!
		</div>
		<?php } ?>
		
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example_no_pagination">
			<thead>
				<tr>
					<th>Nama Item</th>
					<th>Stock Sisa</th>
					<th>Stock Gudang</th>
					<th>Satuan</th>
					<th>Edit</th>
					<!-- <th>Delete</th> -->
				</tr>
			</thead>
			<tbody id="displayResult">
			<?php
			foreach ($item as $item_item): ?>
			
			<tr class="odd gradeX">
					
					<td><?php echo $item_item['nama_item'] ?></td>
					<td><?php echo $item_item['stock_sisa']+$item_item['stock_awal']+$item_item['stock_awal_phc']; ?></td>
					<td><?php echo $item_item['stock_gudang']+$item_item['stock_awal']; ?></td>
					<td><?php echo $item_item['satuan_jual'] ?></td>
					
					<td class="center"> <a class="btn btn-primary" href="<?php echo site_url("g_item/edit_item/".$item_item['id_item']); ?>"><i class="fa fa-edit"></i> Edit</a> </td>
					<!-- <td class="center"><a class="btn btn-danger" href="<?php echo site_url("g_item/delete_item/".$item_item['id_item']); ?>"><i class="fa fa-trash-o"></i> Delete</a></td> -->
				</tr>
		
			<?php 
			
			endforeach ?>
		
				
				
			</tbody>
		</table>
      	<div class="row">
      		<div class="col-md-12">
				<div class="pull-left"><?php echo $benchmark;?> seconds, <?php echo $memory;?></div>
	      		<div class="pull-right">
	      			<?php echo (isset($pagination_link)) ? $pagination_link : '' ;?>
	      		</div>
      		</div>
      	</div>
		
</div>
</div>
</div>
</section>
</section>

<!-- Footer -->
<footer class="mini-footer">
    <div class="container container-footer">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            
            <div class="bptik-copy hide-mini-footer">
            Musi Heart Clinic
            </div>
            <div class="bptik-reserved  hide-mini-footer">
            Surabaya
            </div>
            </div>
            
        </div>
    </div>

</footer>




<!-- Script -->
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	
    <script src="<?php echo base_url('assets/js/js/bootstrap-transition.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-alert.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-modal.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-dropdown.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-scrollspy.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tab.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-popover.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-button.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-collapse.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/js/bootstrap-carousel.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/js/bootstrap-typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.dataTables.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/DT_bootstrap.js') ?>"></script>
	
<script>
$('.navbar-toggle-side').click(function(e){
	toggleSide(e,false)
	});
$('.navbar-side-to-search').click(function(e){
	toggleSide(e,true)
});
	
$(document).ready(function() {
    $('#example_no_pagination').dataTable( {
        "bPaginate":   false
    } );
} );

function toggleSide(action,search){
	action.preventDefault();
	$('.navbar-side').toggleClass('mini-side');
	$('footer').toggleClass('mini-footer');
	$('#wrap').toggleClass('mini-side-open');
	if(search)$('.side-search-input').focus();
}

$(document).ready(function(){
	var src_input = $('#example_filter').find("input");
	src_input.attr("id","input_src");
});

$(document).ready(function(){
	var src_input = $('#example_no_pagination_filter').find("input");
	src_input.attr("id","input_src");
	src_input.attr("style","display:inline-block !important");
	//$('<button class="btn btn default" id="srcButton" style="display:inline-block !important">Cari</button>').insertAfter(src_input);

	$("#input_src").keyup(function(){
		if($(this).val()!="")
		{
			$.ajax({
					type: "POST",
					url: "<?php echo base_url('g_item/view_item_ajax') ;?>",
					data: { src_param: $(this).val() },
					cache: false,
					success:
						function(result)
						{
							$('#displayResult').html(result);
						}
					});
		}
	});

});

function ShowMenuNavJadwal(nama){
	if(nama != "-1"){
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
		$(nama).toggleClass("in").toggleClass("fadeInRight");
	}else{
		$(".menu-jadwal-navigation .in").removeClass("in").removeClass("fadeInRight");
	}
		
}

</script>

</body></html>