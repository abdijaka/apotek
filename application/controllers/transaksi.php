<?php if (!defined('BASEPATH')) die();
class Transaksi extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('transaksi_model');
		$this->load->model('pasien_model');
		$this->load->model('dokter_model');
		$this->load->model('layanan_model');
	}
	
   public function index()
	{
      
	  $this->load->view('include/header');
	  $this->load->view('templates/menubar');
      $this->load->view('view_transaksi');
      $this->load->view('include/footer');
	}
	
	public function view_transaksi($tanggal=FALSE)
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi2($tanggal);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_transaksi', $data);
	}
	
	public function view_all_transaksi()
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi2(0);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_transaksi', $data);
	}
	
	public function view_transaksi_hutang()
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi_hutang();
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_transaksi_cicil', $data);
	}
	
	public function delete_transaksi($reg)
	{
		$sql="delete from transaksi where nomor_registrasi='$reg'";
		$this->db->query($sql);
		
		$sql="delete from detail_transaksi where nomor_register='$reg'";
		$this->db->query($sql);
		
		$sql="delete from pembayaran where nomor_registrasi='$reg'";
		$this->db->query($sql);
		
		
		$this->view_transaksi();
	}
	
	public function insert_layanan(){
 
        $data['title'] = 'Lorem ipsum';
        //$data['list'] = $this->test_model->get_data();
		
		$reg = $this->input->post('reg');
		$id_layanan = $this->input->post('id_layanan');
		$id_dokter = $this->input->post('id_dokter');
		$harga = $this->input->post('harga');
		$diskon = $this->input->post('diskon');
		$nominal_diskon = $this->input->post('nominal_diskon');
		$netto = $this->input->post('netto');
		$fee1 = $this->input->post('fee1');
		$fee2 = $this->input->post('fee2');
		
		/*
		
		$id_layanan2 = $this->input->post('id_layanan2');
		$id_dokter2 = $this->input->post('id_dokter2');
		$harga2 = $this->input->post('harga2');
		$diskon2 = $this->input->post('diskon2');
		$nominal_diskon2 = $this->input->post('nominal_diskon2');
		$netto2 = $this->input->post('netto2');
		$fee12 = $this->input->post('fee12');
		$fee22 = $this->input->post('fee22');
		
		$id_layanan3 = $this->input->post('id_layanan3');
		$id_dokter3 = $this->input->post('id_dokter3');
		$harga3 = $this->input->post('harga3');
		$diskon3 = $this->input->post('diskon3');
		$nominal_diskon3 = $this->input->post('nominal_diskon3');
		$netto3 = $this->input->post('netto3');
		$fee13 = $this->input->post('fee13');
		$fee23 = $this->input->post('fee23');
		
		*/
		
		$sql="INSERT INTO `detail_transaksi` (`nomor_register`, `id_layanan`, `id_dokter`, `harga`, `diskon`, `nominal_diskon`, `netto`, fee1, fee2, fee3, fee4, `status`) VALUES ('$reg', $id_layanan, $id_dokter, '$harga', '$diskon', '$nominal_diskon', '$netto', '$fee1','$fee2','0','0', '1');";
		$this->db->query($sql);
		
		
		$sql="update transaksi set total_harga=total_harga+$netto, nominal_diskon=diskon*total_harga/100, harga_netto=total_harga-nominal_diskon, sisa=harga_netto-bayar  where nomor_registrasi='$reg'";
		$this->db->query($sql);
		
		/*
		
		if($id_layanan2!=0)
		{
			$sql="INSERT INTO `detail_transaksi` (`nomor_register`, `id_layanan`, `id_dokter`, `harga`, `diskon`, `nominal_diskon`, `netto`, fee1, fee2, fee3, fee4, `status`) VALUES ('$reg', $id_layanan2, $id_dokter2, '$harga2', '$diskon2', '$nominal_diskon2', '$netto2', '$fee1','$fee2','0','0', '1');";
			$this->db->query($sql);
		}
		if($id_layanan3!=0)
		{
			$sql="INSERT INTO `detail_transaksi` (`nomor_register`, `id_layanan`, `id_dokter`, `harga`, `diskon`, `nominal_diskon`, `netto`, fee1, fee2, fee3, fee4, `status`) VALUES ('$reg', $id_layanan3, $id_dokter3, '$harga3', '$diskon3', '$nominal_diskon3', '$netto3', '$fee1','$fee2','0','0', '1');";
			$this->db->query($sql);
			
		}
		
		*/
		
		$data['list'] = $this->transaksi_model->get_detail($reg);
        $this->load->view('sample_table', $data);
		
    }
	
	public function show_table()
	{
		$reg = $this->input->post('reg');
		$data['list'] = $this->transaksi_model->get_detail($reg);
        $this->load->view('sample_table', $data);
	}
	
	public function show_detail_pemeriksaan()
	{
		$reg = $this->input->post('reg');
		$data['list'] = $this->transaksi_model->get_detail($reg);
        $this->load->view('detail_pemeriksaan', $data);
	}
	
	public function tambah_pembayaran($nomor)
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		$data['carabayar'] = $this->transaksi_model->get_carabayar();
        $this->load->view('tambah_pembayaran', $data);
	}
	
	public function edit_pembayaran($inputReg, $tanggal, $carabayar, $jumlahbayar)
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		$data['carabayar'] = $this->transaksi_model->get_carabayar();
		
		$tanggal=urldecode($tanggal);
		$sql="select * FROM `pembayaran` where nomor_registrasi='$inputReg' AND tanggal='$tanggal' AND carabayar=$carabayar AND jumlah=$jumlahbayar ;";
		$query = $this->db->query($sql);
		
		$data['pembayaran']=$query->result_array();
		
        $this->load->view('edit_pembayaran', $data);
	}
	
	public function insert_pembayaran()
	{
		$tanggal = $this->input->post('pb_tanggal');
		$inputReg = $this->input->post('pb_nota');
		
		$flag = $this->input->post('flag');
		
		$netto = $this->input->post('pb_totalharganetto');
		$carabayar = $this->input->post('pb_carabayar');
		$jumlahbayar = $this->input->post('pb_jumlahbayar2');
		$totalbayar = $this->input->post('pb_totalbayar')+$this->input->post('pb_jumlahbayar2');
		$sisa = $this->input->post('pb_sisa2');		
		$chargebank = $this->input->post('pb_chargebank');		
		$memo = $this->input->post('memo');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		

		$sql="INSERT INTO `pembayaran` (`nomor_registrasi`, `tanggal`, carabayar, jumlah, memo, chargebank, operator) VALUES ('$inputReg', NOW(), $carabayar, 			'$jumlahbayar', '$memo', '$chargebank','$operator');";

		$this->db->query($sql);
		
		$sql="UPDATE `transaksi` set bayar='$totalbayar', sisa='$sisa' where nomor_registrasi='$inputReg';";
		$this->db->query($sql);
		if($flag==0)
		redirect('transaksi/view_detail/'.$inputReg);
		else
		redirect('transaksi/edit_transaksi/'.$inputReg);
	}
	
	public function update_pembayaran()
	{
		$tanggal = $this->input->post('eb_tanggal');
		$inputReg = $this->input->post('eb_nota');
		
		$netto = $this->input->post('eb_totalharganetto');
		$carabayar = $this->input->post('eb_carabayar');
		$jumlahbayar = $this->input->post('eb_jumlahbayar2');
		$totalbayar = $this->input->post('eb_temp_bayar')+$this->input->post('eb_jumlahbayar2');
		$sisa = $this->input->post('eb_sisa2');
		$eb_chargebank = $this->input->post('eb_chargebank');
		
		$memo = $this->input->post('eb_memo');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		

		$sql="UPDATE `pembayaran` set carabayar=$carabayar, jumlah='$jumlahbayar', memo='$memo', operator='$operator', chargebank='$eb_chargebank' where `nomor_registrasi`='$inputReg' AND tanggal='$tanggal';";
		
		$this->db->query($sql);
		
		$sql="UPDATE `transaksi` set bayar='$totalbayar', sisa='$sisa' where nomor_registrasi='$inputReg';";
		$this->db->query($sql);
		
		redirect('transaksi/edit_transaksi/'.$inputReg);
		
	}
	
	public function delete_pembayaran($inputReg, $tanggal, $carabayar, $jumlahbayar)
	{
		$tanggal=urldecode($tanggal);
		$sql="DELETE FROM `pembayaran` where nomor_registrasi='$inputReg' AND tanggal='$tanggal' AND carabayar=$carabayar AND jumlah=$jumlahbayar ;";
		
		$this->db->query($sql);
		
		$sql="update transaksi set sisa=sisa+$jumlahbayar, 	bayar=bayar-$jumlahbayar where nomor_registrasi='$inputReg'";
		$this->db->query($sql);
		
		redirect('transaksi/edit_transaksi/'.$inputReg);
	}
	
	public function delete_layanan(){
        //$data['title'] = 'Lorem ipsum';
        //$data['list'] = $this->test_model->get_data();
		
		$reg = $this->input->post('reg');
		$id_layanan = $this->input->post('id_layanan');
		$id_dokter = $this->input->post('id_dokter');
		$harga = $this->input->post('harga');
		$diskon = $this->input->post('diskon');
		$nominal_diskon = $this->input->post('nominal_diskon');
		$netto = $this->input->post('netto');
		
		
		$sql="delete from `detail_transaksi` where `nomor_register`='$reg' AND `id_layanan`=$id_layanan AND `id_dokter`=$id_dokter AND `harga`=$harga AND `diskon`=$diskon AND `nominal_diskon`=$nominal_diskon AND `netto`=$netto;";
		$this->db->query($sql);
		
		$sql="update transaksi set total_harga=total_harga-$netto, nominal_diskon=diskon*total_harga/100, harga_netto=total_harga-nominal_diskon, sisa=harga_netto-bayar where nomor_registrasi='$reg'";
		$this->db->query($sql);
		
		$data['list'] = $this->transaksi_model->get_detail($reg);
        $this->load->view('sample_table', $data);
 
    }
	
	
	public function create_transaksi($id_pasien="")
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			
			$data['id_pasien']=$id_pasien;
			if($id_pasien!="")
			{
				$temp_nama=$this->pasien_model->get_pasien($id_pasien);
				$data['nama']=$temp_nama['nama_pasien'];
			}
			else
			{
				$data['nama']="";
			}
			$sql="delete from detail_transaksi where nomor_register like 'nono';";
			$this->db->query($sql);
		  
		  $last = $this->transaksi_model->get_last_transaksi();
		  $dokter = $this->dokter_model->get_dokter();
		  $pasien = $this->pasien_model->get_pasien();
		  $layanan = $this->layanan_model->get_layanan();
		  $data['carabayar'] = $this->transaksi_model->get_carabayar();
		  
		  $testing='';
			foreach ($pasien as $pasien_item):
			$alamat=trim ( $pasien_item["alamat"] );
			$testing = $testing.'{"stateCode": "'.$pasien_item["id_pasien"].'", "stateDisplay": "'.$pasien_item["nama_pasien"].'", "stateName": "'.$pasien_item["id_pasien"].' | '.$pasien_item["nama_pasien"].' | '.$alamat.'"},';
					
			endforeach;
			 
			$testing=substr_replace($testing ,"",-1);
			$testing=trim(preg_replace('/\s+/', ' ', $testing));
			$data['testing']=$testing;
			
			$testing2='';
			foreach ($dokter as $pasien_item):
			$testing2 = $testing2.'{"stateCode": "'.$pasien_item["id_dokter"].'", "stateDisplay": "'.$pasien_item["nama_dokter"].'", "stateName": "'.$pasien_item["nama_dokter"].'"},';
					
			endforeach;
			
			$testing2=substr_replace($testing2 ,"",-1);
			$testing2=trim(preg_replace('/\s+/', ' ', $testing2));
			$data['testing2']=$testing2;
			
			$testing3='';
			foreach ($layanan as $pasien_item):
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_layanan"].'", "harga": "'.$pasien_item["harga"].'", "fee1": "'.$pasien_item["share1"].'", "fee2": "'.$pasien_item["share2"].'", "diskon": "'.$pasien_item["diskon"].'", "stateDisplay": "'.$pasien_item["nama_layanan"].'", "stateName": "'.$pasien_item["nama_layanan"].' | '.$this->formatRupiah($pasien_item["harga"]).'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;

		  if($last==NULL)
		  {
			$date=date("md");
			$year=date("Y");
			$data['nomor_baru']= substr($year, -2).''.$date.'001';
			
		  }
		  else
		  {
			  $last_number=substr($last['nomor_registrasi'], -3);
			
			  $current_number=($last_number*1)+(1*1);
			  
			  
			  $date=date("md");
			  $year=date("Y");
			  
			  $nomor_baru= str_pad($current_number, 3, '0', STR_PAD_LEFT);
			  $data['nomor_baru']= substr($year, -2).''.$date.''.$nomor_baru;
			  
		  }
		  
		  /*
		  $this->load->view('include/header');
		  $this->load->view('templates/menubar');
		  $this->load->view('create_transaksi', $data);
		  $this->load->view('include/footer');
		  */
		  $this->load->view('register', $data);
		}
	}
	
	public function edit_transaksi($inputReg)
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
		  $dokter = $this->dokter_model->get_dokter();
		  $pasien = $this->pasien_model->get_pasien();
		  $layanan = $this->layanan_model->get_layanan();
		  $data['carabayar'] = $this->transaksi_model->get_carabayar();
		  
		  $testing='';
			foreach ($pasien as $pasien_item):
			$alamat=trim ( $pasien_item["alamat"] );
			$testing = $testing.'{"stateCode": "'.$pasien_item["id_pasien"].'", "stateDisplay": "'.$pasien_item["nama_pasien"].'", "stateName": "'.$pasien_item["id_pasien"].' | '.$pasien_item["nama_pasien"].' | '.$alamat.'"},';
					
			endforeach;
			 
			$testing=substr_replace($testing ,"",-1);
			$testing=trim(preg_replace('/\s+/', ' ', $testing));
			$data['testing']=$testing;
			
			$testing2='';
			foreach ($dokter as $pasien_item):
			$testing2 = $testing2.'{"stateCode": "'.$pasien_item["id_dokter"].'", "stateDisplay": "'.$pasien_item["nama_dokter"].'", "stateName": "'.$pasien_item["nama_dokter"].'"},';
					
			endforeach;
			
			$testing2=substr_replace($testing2 ,"",-1);
			$testing2=trim(preg_replace('/\s+/', ' ', $testing2));
			$data['testing2']=$testing2;
			
			$testing3='';
			foreach ($layanan as $pasien_item):
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_layanan"].'", "harga": "'.$pasien_item["harga"].'", "fee1": "'.$pasien_item["share1"].'", "fee2": "'.$pasien_item["share2"].'", "diskon": "'.$pasien_item["diskon"].'", "stateDisplay": "'.$pasien_item["nama_layanan"].'", "stateName": "'.$pasien_item["nama_layanan"].' | '.$this->formatRupiah($pasien_item["harga"]).'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;

		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran($inputReg);
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		/*  
		  $this->load->view('include/header');
		  $this->load->view('templates/menubar');
		  $this->load->view('edit_transaksi', $data);
		  $this->load->view('include/footer');
		*/  
		$this->load->view('register_edit', $data);
		
		}
	}
	
	public function insert_transaksi(){
 
		$inputReg = $this->input->post('inputReg');
		$baru = $this->input->post('baru');
		if($baru==NULL)
		{
			$baru=0;
		}
		
		$id_pasien = $this->input->post('inputID');
		$id_dokter = $this->input->post('idreferensi');
		$harga = $this->input->post('totalharga');
		$diskon = $this->input->post('totaldiskon');
		$nominal_diskon = $this->input->post('totalnominaldiskon');
		
		$netto = $this->input->post('totalharganetto');
		$carabayar = $this->input->post('carabayar');
		$jumlahbayar = $this->input->post('jumlahbayar');
		$sisa = $this->input->post('sisa');
		$chargebank = $this->input->post('chargebank');
		
		$sql="INSERT INTO `transaksi` (`nomor_registrasi`, `tanggal`, `referensi`, `pasien`, `total_harga`, `diskon`, `nominal_diskon`, `harga_netto`, `keterangan`, `status`, baru, bayar, sisa, carabayar) VALUES ('$inputReg', NOW(), $id_dokter, '$id_pasien', $harga, '$diskon', '$nominal_diskon', '$netto', '', '1', $baru, '$jumlahbayar', '$sisa', $carabayar);";
		$this->db->query($sql);
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;		

		$sql="INSERT INTO `pembayaran` (`nomor_registrasi`, `tanggal`, carabayar, chargebank, jumlah, memo, operator) VALUES ('$inputReg', NOW(), $carabayar, '$chargebank', '$jumlahbayar', '', '$operator');";
		$this->db->query($sql);
		
		$sql="update detail_transaksi set nomor_register='$inputReg' where nomor_register like 'nono'";
		$this->db->query($sql);
		
		//$this->cetak_kwitansi($inputReg);
		
		redirect('transaksi/view_detail/'.$inputReg);
		
    }
	
	public function update_transaksi(){
 
		$inputReg = $this->input->post('inputReg');
		$baru = $this->input->post('baru');
		if($baru==NULL)
		{
			$baru=0;
		}
		
		$id_pasien = $this->input->post('inputID');
		$id_dokter = $this->input->post('idreferensi');
		$harga = $this->input->post('totalharga');
		$diskon = $this->input->post('totaldiskon');
		$nominal_diskon = $this->input->post('totalnominaldiskon');
		
		$netto = $this->input->post('totalharganetto');
		$carabayar = $this->input->post('carabayar');
		$jumlahbayar = $this->input->post('jumlahbayar');
		$sisa = $this->input->post('sisa');
		
		$sql="UPDATE `transaksi` set `referensi`=$id_dokter, `pasien`='$id_pasien', `total_harga`=$harga, `diskon`='$diskon', `nominal_diskon`='$nominal_diskon', `harga_netto`='$netto', baru=$baru, bayar='$jumlahbayar', sisa='$sisa' where nomor_registrasi='$inputReg';";
		$this->db->query($sql);
		
		
		//$this->cetak_kwitansi($inputReg);
		
		redirect('transaksi/view_detail/'.$inputReg);
		
    }

	public function view_detail($inputReg=0){
		
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran($inputReg);
		
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('success', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('detail_register_transaksi', $data);
    }
	
	public function receipt($inputReg=0){
		
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran($inputReg);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function cetak_pembayaran($inputReg=0, $tanggal){
		$tanggal=urldecode($tanggal);
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran_kwitansi($inputReg, $tanggal);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function laporan_harian($tanggal=FALSE){
		
		$transaksihariini = $this->transaksi_model->get_transaksi2($tanggal);

		$detail=array();
		$pembayaran=array();
		foreach ($transaksihariini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg);
		endforeach;
		$data['transaksihariini']=$transaksihariini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		if($tanggal === FALSE)
		{
			$tanggal=date('j-m-Y');
		}
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function laporan_harian_baru($tanggal=FALSE){
		
		
		if($this->input->post('mulai')!=NULL)
		{
			$tanggal=$this->input->post('mulai');
		}
		else if($tanggal === FALSE)
		{
			$tanggal=date('Y-m-j');
		}
		$tanggal =date('Y-m-j', strtotime($tanggal));
		
		$pembayaran_hari_ini=$this->transaksi_model->get_pembayaran(FALSE, $tanggal);
		
		$detail=array();
		$pembayaran=array();
		foreach ($pembayaran_hari_ini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg, $tanggal);
		endforeach;
		$data['transaksihariini']=$pembayaran_hari_ini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function cetak_kwitansi($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('create_transaksi_success', $data);
    }
	
	public function cetak_kwitansi_besar(){
		$data['kwb_nomorkwitansi'] = $this->input->post('kwb_nomorkwitansi');
		$data['kwb_dari'] = $this->input->post('kwb_dari');
		$data['kwb_jumlah'] = $this->input->post('kwb_jumlah');
		$data['kwb_keperluan'] = $this->input->post('kwb_keperluan');
		$data['kwb_nominal'] = $this->input->post('kwb_nominal');
		$data['kwb_tanggal'] = $this->input->post('kwb_tanggal');
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	public function cetak_kwitansi_besar2($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	function formatRupiah($nilaiUang)
	{
	  $nilaiRupiah 	= "";
	  $jumlahAngka 	= strlen($nilaiUang);
	  while($jumlahAngka > 3)
	  {
		$nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
		$sisaNilai = strlen($nilaiUang) - 3;
		$nilaiUang = substr($nilaiUang,0,$sisaNilai);
		$jumlahAngka = strlen($nilaiUang);
	  }
	 
	  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
	  return $nilaiRupiah;
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
