<?php if (!defined('BASEPATH')) die();
class G_distribusi extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_distribusi_model');
		$this->load->model('g_item_model');
		$this->load->model('g_ruangan_model');
		$this->load->model('g_stock_model');
	}
	
   public function index()
	{
	  /*
	  $this->load->view('include/header');
	  $this->load->view('templates/menubar');
      $this->load->view('view_transaksi');
      $this->load->view('include/footer');
	  */
	  $this->view_distribusi();
	}
	
	public function view_distribusi($status=0, $id_distribusi=FALSE )
	{
		$data['distribusi'] = $this->g_distribusi_model->get_distribusi($id_distribusi);
		$data['status']=$status;
		
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('g_view_gudang_distribusi', $data);
	}
	
	public function view_all_transaksi()
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi2(0);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_transaksi', $data);
	}
	
	public function checkfaktur()
	{
		$reg = $this->input->post('reg');
		$jumlah = $this->g_distribusi_model->get_jumlah_faktur($reg);
		echo $jumlah['jumlah'];
	}
	
	
	public function delete_distribusi($reg)
	{
		$detail_distribusi = $this->g_distribusi_model->get_detail($reg);
		
		foreach($detail_distribusi as $item_detail):
			
			$ruangan=$item_detail['ruangan'];
			
			$count=$this->g_stock_model->check_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			}
			
			$sql="update t_item set stock_gudang=stock_gudang+".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir-".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir-".$item_detail['jumlah'].", stock_awal=stock_awal-".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$count2=$this->g_stock_model->check_stockharian2($item_detail['item'], $item_detail['tgl_masuk'] );
			
			if($count2['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2($item_detail['item'], $item_detail['tgl_masuk']);
			}
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$item_detail['jumlah'].", stock_awal=stock_awal+".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
		endforeach;
		
		$sql="delete from t_distribusi where id_distribusi=$reg";
		$this->db->query($sql);
		
		$sql="delete from t_item_distribusi where distribusi=$reg";
		$this->db->query($sql);
		
		$this->view_distribusi(3);
	}
	
	public function insert_item(){
 
        $data['title'] = 'Lorem ipsum';
        //$data['list'] = $this->test_model->get_data();
		
		$reg = $this->input->post('reg');
		$id_item = $this->input->post('id_item');
		$jumlah = $this->input->post('jumlah');
		$satuan = $this->input->post('satuan');
		$tgl_distribusi = $this->input->post('tgl_distribusi');
		
		$ruangan = $this->input->post('ruangan');
		
		$sql="INSERT INTO `t_item_distribusi` (`distribusi`, `item`, tgl_masuk, `jumlah`, `satuan`) VALUES ($reg, $id_item, STR_TO_DATE('$tgl_distribusi', '%d-%m-%Y'), $jumlah,'$satuan');";
		$this->db->query($sql);
		
		if($reg==0 || $reg=="0")
		{
			
		}
		else
		{
			$sql="update t_item set stock_gudang=stock_gudang-".$jumlah." where id_item=".$id_item;
			$this->db->query($sql);
			
			$count=$this->g_stock_model->check_stockharian3_ruangan($id_item, $tgl_distribusi, $ruangan);
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian3_ruangan($id_item, $tgl_distribusi, $ruangan);
			}
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir+".$jumlah." where item=".$id_item." AND ruangan=$ruangan AND DATE(tgl_stock)=STR_TO_DATE('$tgl_distribusi', '%d-%m-%Y');";	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir+".$jumlah.", stock_awal=stock_awal+".$jumlah." where item=".$id_item." AND ruangan=$ruangan AND DATE(tgl_stock)>STR_TO_DATE('$tgl_distribusi', '%d-%m-%Y');";	
			$this->db->query($sql);
			
			$count2=$this->g_stock_model->check_stockharian3($id_item, $tgl_distribusi );
			
			if($count2['ada']=='0')
			{
				$this->g_stock_model->create_stockharian3($item_detail['item'], $item_detail['tgl_masuk']);
			}
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$jumlah." where item=".$id_item." AND DATE(tgl_stock)=STR_TO_DATE('$tgl_distribusi', '%d-%m-%Y');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$jumlah.", stock_awal=stock_awal-".$jumlah." where item=".$id_item." AND DATE(tgl_stock)>STR_TO_DATE('$tgl_distribusi', '%d-%m-%Y');";	
			$this->db->query($sql);
			
		}
		
		$data['list'] = $this->g_distribusi_model->get_detail($reg);
		
        $this->load->view('t_tabel_item_distribusi', $data);
		
    }
	
	public function show_table()
	{
		$reg = $this->input->post('reg');
		$data['list'] = $this->g_distribusi_model->get_detail($reg);
        $this->load->view('t_tabel_item_distribusi', $data);
	}
	
	public function show_detail_item()
	{
		
		$reg = $this->input->post('reg');
		$data['list'] = $this->g_distribusi_model->get_detail($reg);
        $this->load->view('t_tabel_item_distribusi_detail', $data);
	}
	
	public function delete_item(){
        //$data['title'] = 'Lorem ipsum';
        //$data['list'] = $this->test_model->get_data();
		
		$reg = $this->input->post('reg');
		
		$distribusi = $this->input->post('distribusi');
		
		if($distribusi==0 || $distribusi=="0")
		{
			
		}
		else
		{
			$item_detail=$this->g_distribusi_model->get_detail_itemdistribusi($reg);
			
			$ruangan=$item_detail['ruangan'];
			
			$count=$this->g_stock_model->check_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			}
			
			$sql="update t_item set stock_gudang=stock_gudang+".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir-".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir-".$item_detail['jumlah'].", stock_awal=stock_awal-".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$count2=$this->g_stock_model->check_stockharian2($item_detail['item'], $item_detail['tgl_masuk'] );
			
			if($count2['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2($item_detail['item'], $item_detail['tgl_masuk']);
			}
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$item_detail['jumlah'].", stock_awal=stock_awal+".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
		}
		
		$sql="delete from `t_item_distribusi` where `id_item_distribusi`=$reg;";
		$this->db->query($sql);
		
		$data['list'] = $this->g_distribusi_model->get_detail($distribusi);
        $this->load->view('t_tabel_item_distribusi', $data);
 
    }
	
	
	public function create_distribusi()
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$sql="delete from t_item_distribusi where distribusi=0;";
			$this->db->query($sql);
		  
			$data['ruangan'] = $this->g_ruangan_model->get_ruangan();
		  
			$item = $this->g_item_model->get_item();
		  
			$testing3='';
			foreach ($item as $pasien_item):
			$stock_gudang_all=$pasien_item["stock_gudang"]+$pasien_item["stock_awal"];
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "stock_gudang": "'.$stock_gudang_all.'", "satuanbeli": "'.$pasien_item["satuan_beli"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$stock_gudang_all.'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;
			
			$last = $this->g_distribusi_model->get_last_distribusi_hari_ini();
			
			if($last==NULL)
			  {
				$date=date("md");
				$year=date("Y");
				$data['nomor_baru']= 'K'.substr($year, -2).''.$date.'001';
			  }
			  else
			  {
				  $last_number=substr($last['nomor_distribusi'], -3);
				
				  $current_number=($last_number*1)+(1*1);
				  
				  
				  $date=date("md");
				  $year=date("Y");
				  
				  $nomor_baru= str_pad($current_number, 3, '0', STR_PAD_LEFT);
				  $data['nomor_baru']= 'K'.substr($year, -2).''.$date.''.$nomor_baru;
				  
			  }
		  
			$this->load->view('g_register_distribusi', $data);
		}
	}
	
	public function edit_distribusi($inputReg)
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$data['ruangan'] = $this->g_ruangan_model->get_ruangan();
		  
			$item = $this->g_item_model->get_item();
		  
			$testing3='';
			foreach ($item as $pasien_item):
			$stock_gudang_all=$pasien_item["stock_gudang"]+$pasien_item["stock_awal"];
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "stock_gudang": "'.$stock_gudang_all.'", "satuanbeli": "'.$pasien_item["satuan_beli"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$stock_gudang_all.'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;
		  

			$data['distribusi'] = $this->g_distribusi_model->get_distribusi($inputReg);
			
			$this->load->view('g_edit_distribusi', $data);
		
		}
	}
	
	public function insert_distribusi(){
 
		$nomordistribusi = $this->input->post('nomordistribusi');
		$tgl_distribusi = $this->input->post('tgl_distribusi');
		
		$ruangan = $this->input->post('ruangan');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		
		$sql="INSERT INTO `t_distribusi` (`nomor_distribusi`, `tgl_isi`, `ruangan`, `tgl_distribusi`, `operator`) VALUES ('$nomordistribusi', NOW(), $ruangan, STR_TO_DATE('$tgl_distribusi', '%d-%m-%Y'), '$operator');";
		$this->db->query($sql);
		
		$last_distribusi = $this->g_distribusi_model->get_last_distribusi($nomordistribusi);
		
		$sql="update t_item_distribusi set distribusi=".$last_distribusi['id_distribusi'].", tgl_masuk=STR_TO_DATE('$tgl_distribusi', '%d-%m-%Y') where distribusi=0";
		$this->db->query($sql);
		
		$detail_distribusi = $this->g_distribusi_model->get_detail($last_distribusi['id_distribusi']);
		
		foreach($detail_distribusi as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			}
			
			$sql="update t_item set stock_gudang=stock_gudang-".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir+".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir+".$item_detail['jumlah'].", stock_awal=stock_awal+".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$count2=$this->g_stock_model->check_stockharian2($item_detail['item'], $item_detail['tgl_masuk'] );
			
			if($count2['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2($item_detail['item'], $item_detail['tgl_masuk']);
			}
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$item_detail['jumlah'].", stock_awal=stock_awal-".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			
		endforeach;
		
		
		redirect('g_distribusi/view_detail/'.$last_distribusi['id_distribusi']);
    }
	
	public function update_distribusi(){
 
		$inputReg = $this->input->post('id_distribusi');
		$nomordistribusi = $this->input->post('nomordistribusi');
		$tgl_distribusi = $this->input->post('tgl_distribusi');
		
		$ruangan = $this->input->post('ruangan');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		
		$sql="UPDATE `t_distribusi` set `nomor_distribusi`='$nomordistribusi',`ruangan`=$ruangan, `tgl_distribusi`=STR_TO_DATE('$tgl_distribusi', '%d-%m-%Y'), `operator`='$operator' where id_distribusi=$inputReg ;";
		$this->db->query($sql);
		
		redirect('g_distribusi/view_detail/'.$inputReg);
		
    }

	public function view_detail($inputReg=0){
		
		$data['distribusi'] = $this->g_distribusi_model->get_distribusi($inputReg);
		
		$this->load->view('g_detail_distribusi', $data);
    }
	
	public function view_detail2($inputReg=0){
		
		$data['distribusi'] = $this->g_distribusi_model->get_distribusi($inputReg);
		$data['list'] = $this->g_distribusi_model->get_detail($inputReg);
		
		$this->load->view('g_cetak_laporan_distribusi', $data);
    }
	
	public function receipt($inputReg=0){
		
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran($inputReg);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function cetak_pembayaran($inputReg=0, $tanggal){
		$tanggal=urldecode($tanggal);
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran_kwitansi($inputReg, $tanggal);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function laporan_harian($tanggal=FALSE){
		
		$transaksihariini = $this->transaksi_model->get_transaksi2($tanggal);

		$detail=array();
		$pembayaran=array();
		foreach ($transaksihariini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg);
		endforeach;
		$data['transaksihariini']=$transaksihariini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		if($tanggal === FALSE)
		{
			$tanggal=date('j-m-Y');
		}
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function laporan_harian_baru($tanggal=FALSE){
		
		
		if($this->input->post('mulai')!=NULL)
		{
			$tanggal=$this->input->post('mulai');
		}
		else if($tanggal === FALSE)
		{
			$tanggal=date('Y-m-j');
		}
		$tanggal =date('Y-m-j', strtotime($tanggal));
		
		$pembayaran_hari_ini=$this->transaksi_model->get_pembayaran(FALSE, $tanggal);
		
		$detail=array();
		$pembayaran=array();
		foreach ($pembayaran_hari_ini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg, $tanggal);
		endforeach;
		$data['transaksihariini']=$pembayaran_hari_ini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function cetak_kwitansi($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('create_transaksi_success', $data);
    }
	
	public function cetak_kwitansi_besar(){
		$data['kwb_nomorkwitansi'] = $this->input->post('kwb_nomorkwitansi');
		$data['kwb_dari'] = $this->input->post('kwb_dari');
		$data['kwb_jumlah'] = $this->input->post('kwb_jumlah');
		$data['kwb_keperluan'] = $this->input->post('kwb_keperluan');
		$data['kwb_nominal'] = $this->input->post('kwb_nominal');
		$data['kwb_tanggal'] = $this->input->post('kwb_tanggal');
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	public function cetak_kwitansi_besar2($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	function formatRupiah($nilaiUang)
	{
	  $nilaiRupiah 	= "";
	  $jumlahAngka 	= strlen($nilaiUang);
	  while($jumlahAngka > 3)
	  {
		$nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
		$sisaNilai = strlen($nilaiUang) - 3;
		$nilaiUang = substr($nilaiUang,0,$sisaNilai);
		$jumlahAngka = strlen($nilaiUang);
	  }
	 
	  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
	  return $nilaiRupiah;
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
