<?php if (!defined('BASEPATH')) die();
class G_stock extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_stock_model');
		$this->load->model('g_item_model');
		$this->load->model('g_ruangan_model');
	}
	
   public function index()
	{
		$this->view_stock_item(0);
	}
	
	public function create_stockharian($id_item = FALSE)
	{
		
		$item=$this->g_item_model->get_item();
		foreach($item as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian($item_detail['id_item']);
			if($count['ada']==0)
			{
				$this->g_stock_model->create_stockharian($item_detail['id_item']);
			}
			
		endforeach;
	}
	
	public function view_stock_item($status=0)
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
		$item=$this->g_item_model->get_item();
		$ruangan = $this->g_ruangan_model->get_ruangan();
		
		foreach($item as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian($item_detail['id_item']);
			if($count['ada']==0)
			{
				$this->g_stock_model->create_stockharian($item_detail['id_item']);
			}
			
			foreach($ruangan as $ruangan_detail):
			
				$count=$this->g_stock_model->check_stockharian_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				if($count['ada']==0)
				{
					$this->g_stock_model->create_stockharian_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				}
				
				$item_ruangan[$item_detail['id_item']][$ruangan_detail['id_ruangan']] = $this->g_item_model->get_item_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				
			endforeach;
			
		endforeach;
		$data['item_ruangan']=$item_ruangan;
		$data['stock'] = $this->g_stock_model->get_stock_item();
		$data['status']=$status;
		
		$this->load->view('g_view_gudang_stock_item', $data);
		}
	}
	
	public function print_stock_item($status=0)
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
		$item=$this->g_item_model->get_item();
		$ruangan = $this->g_ruangan_model->get_ruangan();
		
		foreach($item as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian($item_detail['id_item']);
			if($count['ada']==0)
			{
				$this->g_stock_model->create_stockharian($item_detail['id_item']);
			}
			
			foreach($ruangan as $ruangan_detail):
			
				$count=$this->g_stock_model->check_stockharian_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				if($count['ada']==0)
				{
					$this->g_stock_model->create_stockharian_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				}
				
				$item_ruangan[$item_detail['id_item']][$ruangan_detail['id_ruangan']] = $this->g_item_model->get_item_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				
			endforeach;
			
		endforeach;
		$data['item_ruangan']=$item_ruangan;
		$data['stock'] = $this->g_stock_model->get_stock_item();
		$data['status']=$status;
		
		$this->load->view('g_cetak_laporan_stock_item', $data);
		}
	}
	
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
