<?php if (!defined('BASEPATH')) die();
class G_stock extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_stock_model');
		$this->load->model('g_item_model');
		$this->load->model('g_ruangan_model');
		$this->load->helper('url');
		$this->load->library('pagination');
	}
	
   public function index()
	{
		$this->view_stock_item(0);
	}
	
	public function create_stockharian($id_item = FALSE)
	{
		
		$item=$this->g_item_model->get_item();
		foreach($item as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian($item_detail['id_item']);
			if($count['ada']==0)
			{
				$this->g_stock_model->create_stockharian($item_detail['id_item']);
			}
			
		endforeach;
	}

	public function view_stock_item_ajax()
	{
		if( !$this->input->post($this->input->post('src_param')) )
		{
			//redirect(base_url('g_stock'));
		}
		$item = $this->g_item_model->get_item_by_name($this->input->post('src_param'));
		$ruangan = $this->g_ruangan_model->get_ruangan();
		$item_ruangan = array();
		
		foreach($item as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian($item_detail['id_item']);
			if($count['ada']==0)
			{
				$this->g_stock_model->create_stockharian($item_detail['id_item']);
			}
			
			foreach($ruangan as $ruangan_detail):
			
				$count=$this->g_stock_model->check_stockharian_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				if($count['ada']==0)
				{
					$this->g_stock_model->create_stockharian_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				}
				
				$item_ruangan[$item_detail['id_item']][$ruangan_detail['id_ruangan']] = $this->g_item_model->get_item_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				
			endforeach;
			
		endforeach;

		$data['item_ruangan']=$item_ruangan;
		$data['stock'] = $this->g_item_model->get_item_by_name($this->input->post('src_param'));
		//$data['stock'] = $this->g_stock_model->get_stock_item();
		//echo json_encode($data);


		$nomor=1;
		foreach ($data['stock'] as $stock_item): 
		$stock_total=$stock_item['stock_gudang']+$stock_item['stock_awal'];
		
		$stock_total_phc=$data['item_ruangan'][$stock_item['id_item']][1]['stock_akhir']+$stock_item['stock_awal_phc'];
		
		$sisa_stock_total=$stock_item['stock_sisa']+$stock_item['stock_awal']+$stock_item['stock_awal_phc'];
		
		$flag1=0;
		//$flag2=0;
		if($stock_total<=$stock_item['stock_min']) $flag1=1;
		//if($stock_total_phc<=$stock_item['stock_min']) $flag2=1;
		echo '
		<tr >
				<td>'.$stock_item['nama_item'].'</td>
				<td>'.$stock_item['stock_min'].'</td>
				
				<td>'.$stock_total.'</td>
				
				<td>';
				if($flag1==1) echo '<div class="alert-danger"><span class="fa  fa-times-circle fa-lg"></span> Out of Stock</div>'; else echo '<div class="alert-success"><span class="fa fa-check fa-lg"></span> In Stock</div></td>';

		echo '</tr>';
		
		$nomor++;
		
		endforeach;

	}
	
	public function view_stock_item($status=0)
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{

		$this->benchmark->mark('code_start');

		//$item=$this->g_item_model->get_item();
		$ruangan = $this->g_ruangan_model->get_ruangan();
		
		$total_rows = $this->g_item_model->total_rows();
		
		//$data['total_rows'] = $total_rows;

		$config['base_url'] = base_url('/g_stock/view_stock_item/page/');
		$config['total_rows'] = $total_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 4;
		$item_ruangan = '';
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

		//Ajax call
		$item = ($this->input->post('src_param')) ? $this->g_item_model->get_item($this->input->post('src_param')) : $this->g_item_model->get_items_pagination($config['per_page'],$page);

		$this->pagination->initialize($config);

		$data['pagination_link'] = $this->pagination->create_links();

		foreach($item as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian($item_detail['id_item']);
			if($count['ada']==0)
			{
				$this->g_stock_model->create_stockharian($item_detail['id_item']);
			}
			
			foreach($ruangan as $ruangan_detail):
			
				$count=$this->g_stock_model->check_stockharian_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				if($count['ada']==0)
				{
					$this->g_stock_model->create_stockharian_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				}
				
				$item_ruangan[$item_detail['id_item']][$ruangan_detail['id_ruangan']] = $this->g_item_model->get_item_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				
			endforeach;
			
		endforeach;
		$data['item_ruangan']=$item_ruangan;
		$data['stock'] = $this->g_stock_model->get_stock_item_pagination($config['per_page'],$page);
		$data['status']=$status;


		$this->benchmark->mark('code_end');

		$data['memory'] = $this->benchmark->memory_usage();
		$data['benchmark'] = $this->benchmark->elapsed_time('code_start', 'code_end');
		
		$this->load->view('g_view_gudang_stock_item', $data);
		}
	}
	/*
	public function view_stock_item($status=0)
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
		$item=$this->g_item_model->get_item();
		$ruangan = $this->g_ruangan_model->get_ruangan();
		
		foreach($item as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian($item_detail['id_item']);
			if($count['ada']==0)
			{
				$this->g_stock_model->create_stockharian($item_detail['id_item']);
			}
			
			foreach($ruangan as $ruangan_detail):
			
				$count=$this->g_stock_model->check_stockharian_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				if($count['ada']==0)
				{
					$this->g_stock_model->create_stockharian_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				}
				
				$item_ruangan[$item_detail['id_item']][$ruangan_detail['id_ruangan']] = $this->g_item_model->get_item_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				
			endforeach;
			
		endforeach;
		$data['item_ruangan']=$item_ruangan;
		$data['stock'] = $this->g_stock_model->get_stock_item();
		$data['status']=$status;
		
		$this->load->view('g_view_gudang_stock_item', $data);
		}
	}*/
	
	public function print_stock_item($status=0)
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
		$item=$this->g_item_model->get_item();
		$ruangan = $this->g_ruangan_model->get_ruangan();
		
		foreach($item as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian($item_detail['id_item']);
			if($count['ada']==0)
			{
				$this->g_stock_model->create_stockharian($item_detail['id_item']);
			}
			
			foreach($ruangan as $ruangan_detail):
			
				$count=$this->g_stock_model->check_stockharian_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				if($count['ada']==0)
				{
					$this->g_stock_model->create_stockharian_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				}
				
				$item_ruangan[$item_detail['id_item']][$ruangan_detail['id_ruangan']] = $this->g_item_model->get_item_ruangan($item_detail['id_item'], $ruangan_detail['id_ruangan']);
				
			endforeach;
			
		endforeach;
		$data['item_ruangan']=$item_ruangan;
		$data['stock'] = $this->g_stock_model->get_stock_item();
		$data['status']=$status;
		
		$this->load->view('g_cetak_laporan_stock_item', $data);
		}
	}
	
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
