<?php if (!defined('BASEPATH')) die();
class G_eresep extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_eresep_model');
		$this->load->model('g_penjualan_model');
		$this->load->model('g_item_model');
		$this->load->model('pasien_model');
		$this->load->model('dokter_model');
		$this->load->model('g_stock_model');
		$this->load->model('g_carabayar_model');
	}
	
   public function index()
	{
	  $this->view_eresep();
	}
	
	public function view_eresep($id_eresep=FALSE, $warning_msg="")
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		$data['eresep'] = $this->g_eresep_model->get_eresep($id_eresep);
		$data['status'] = 0;
		$data['warning_msg'] = $warning_msg;
		$this->load->view('g_view_eresep', $data);
	}

	public function print_eresep($id_eresep)
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$data['eresep'] = $this->g_eresep_model->get_eresep($id_eresep);
			$detail = $this->g_eresep_model->get_detail($id_eresep);
			$f_detail = array();
			foreach ($detail as $d) 
			{
				$f_detail[] = array(
					"item" => $d['item'],
					"tgl_masuk" => $d['tgl_masuk'],
					"id_item_eresep" => $d['id_item_eresep'],
					"eresep" => $d['eresep'],
					"nama_item" => $d['nama_item'],
					"satuan" => $d['satuan'],
					"jumlah" => $this->romanicNumber($d['jumlah']),
					"aturan_pakai" => $d['aturan_pakai'],
				);
			}
			$data['detail'] = $f_detail;
			$this->load->view('g_eresep_print', $data);
		}
	}

	public function confirm_eresep($id_eresep)
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$this->db->delete("t_item_jual", "penjualan = 0");
			$item_eresep = $this->g_eresep_model->get_detail($id_eresep);
			foreach ($item_eresep as $item_item_eresep) {
				$item_info = $this->g_item_model->get_item($item_item_eresep['item']);
				$item_stock = $item_info['stock_awal'] + $item_info['stock_gudang'];
				$item_harga = $item_info['harga_jual'];
				// do insert t_item_penjualan with item_eresep info
				$t_item_jual_data = array(
					"item"				=> $item_item_eresep['item'],
					"penjualan"			=> 0,
					"item_eresep"		=> $item_item_eresep['id_item_eresep'],
					"tgl_masuk"			=> date("Y-m-d"),
					"exp_date"			=> date("Y-m-d"),
					"jumlah"			=> $item_item_eresep['jumlah'],
					"satuan"			=> $item_item_eresep['satuan'],
					"harga"				=> $item_harga,
					"subtotal"			=> $item_item_eresep['jumlah'] * $item_harga,
					"diskon"			=> 0,
					"nominal_diskon"	=> 0,
					"total"				=> $item_item_eresep['jumlah'] * $item_harga
				);
				if ($item_item_eresep['jumlah'] > $item_stock) {
					$t_item_jual_data['jumlah']	= $item_stock;
				}
				$this->db->insert("t_item_jual", $t_item_jual_data);
			}
			$data['eresep'] = $this->g_eresep_model->get_eresep($id_eresep);
			$data['pembayaran'] = $this->g_penjualan_model->get_pembayaran(0);
			$data['carabayar'] = $this->g_carabayar_model->get_carabayar();
			$data['pasien'] = $this->pasien_model->get_pasien();
			$data['dokter'] = $this->dokter_model->get_dokter();
			$item = $this->g_item_model->get_item();	  
			$items = '';
			foreach ($item as $item_item):
				$stock_gudang_all = $item_item["stock_awal"] + $item_item["stock_gudang"];
				$items .= '{"stateCode": "' . $item_item["id_item"] . '", "nama": "' . $item_item["nama_item"] . '", "harga": "' . $item_item["harga_jual"] . '", "satuanbeli": "' . $item_item["satuan_jual"] . '", "kode": "'.$item_item["kode"].'", "stateDisplay": "' . $item_item["nama_item"] . '", "stateName": "' . $item_item["nama_item"] . '", "stock_gudang": "' . $stock_gudang_all . '"},';
			endforeach;
			$items = substr_replace($items , "", -1);
			$items = trim(preg_replace('/\s+/', ' ', $items));
			$data['items'] = $items;

			$last = $this->g_penjualan_model->get_last_penjualan_hari_ini();
			if ($last == null)
			{
				$date = date("md");
				$year = date("Y");
				$data['nomor_baru'] = ''.substr($year, -2) . '' . $date . '001';
			}
			else
			{
				$last_number = substr($last['nomor_faktur'], -3);
				$current_number = ($last_number * 1) + (1 * 1);
				$date = date("md");
				$year = date("Y");
				$nomor_baru = str_pad($current_number, 3, '0', STR_PAD_LEFT);
				$data['nomor_baru'] = ''.substr($year, -2) . '' . $date . '' . $nomor_baru;  
			}

			$this->load->view('g_register_penjualan_eresep', $data);
		}
	}

	public function ajaxTypeheadItem(){
		$item = $this->g_item_model->get_item_by_name($this->input->post('src_param'));
		$items = '[';
		foreach ($item as $item_item):
			$stock_gudang_all = $item_item["stock_awal"] + $item_item["stock_gudang"];
			$items .= '{"stateCode": "'.$item_item["id_item"] . '", "nama": "' . $item_item["item_item"] . '", "stock_gudang": "' . $stock_gudang_all.'", "harga": "' . $item_item["harga_jual"].'", "satuanbeli": "'.$item_item["satuan_jual"] . '", "kode": "'.$item_item["kode"] . '", "stateDisplay": "' . $item_item["nama_item"].'", "stateName": "' . $item_item["nama_item"].' | ' . $this->formatRupiah($item_item["harga_jual"]) . '"},';
		endforeach;
		$items = substr_replace($items , "", -1);
		$items = $items.']';
		$items = trim(preg_replace('/\s+/', ' ', $items));
		echo $items;
	}

	private function formatRupiah($nilaiUang)
	{
		$nilaiRupiah 	= "";
	  	$jumlahAngka 	= strlen($nilaiUang);
	  	while($jumlahAngka > 3)
		{
			$nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
			$sisaNilai = strlen($nilaiUang) - 3;
			$nilaiUang = substr($nilaiUang,0,$sisaNilai);
			$jumlahAngka = strlen($nilaiUang);
	  	} 
	  	$nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
	  	return $nilaiRupiah;
	}

	private function romanicNumber($integer, $upcase = true) 
	{ 
	    $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1); 
	    $return = ''; 
	    while($integer > 0) 
	    { 
	        foreach($table as $rom=>$arb) 
	        { 
	            if($integer >= $arb) 
	            { 
	                $integer -= $arb; 
	                $return .= $rom; 
	                break; 
	            } 
	        } 
	    } 

	    return $return; 
	}
} 

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */