<?php if (!defined('BASEPATH')) die();
class G_distribusi extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_distribusi_model');
		$this->load->model('g_ruangan_model');
		$this->load->model('g_item_model');
	}
	
   public function index()
	{
		$this->view_distribusi(0);
	}
	
	public function view_distribusi($status=0)
	{
		$data['distribusi'] = $this->g_distribusi_model->get_distribusi();
		$data['status']=$status;
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_distribusi', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('g_view_gudang_distribusi', $data);
	}
	
	public function distribusi_baru()
	{
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('create_distribusi');
		$this->load->view('include/footer');
		*/
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$data['ruangan'] = $this->g_ruangan_model->get_ruangan();
		  
			$item = $this->g_item_model->get_item();
		  
			$testing3='';
			foreach ($item as $pasien_item):
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "stock_gudang": "'.$pasien_item["stock_gudang"].'", "satuanbeli": "'.$pasien_item["satuan_beli"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$pasien_item["stock_gudang"].'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;
		  
			$this->load->view('g_register_distribusi', $data);
		}
		
	}
	
	public function create_distribusi()
	{
		$tgl_distribusi = $this->input->post('tgl_distribusi');
		$id_item = $this->input->post('id_item');
		$ruangan = $this->input->post('ruangan');
		$jumlah = $this->input->post('jumlah');
		$stock_gudang = $this->input->post('stock_gudang');
		
		$sql="INSERT INTO  `t_distribusi` (`tgl_distribusi` ,`tgl_isi`, ruangan, item, jumlah)
		VALUES (STR_TO_DATE('$tgl_distribusi', '%d-%m-%Y'), NOW(), $ruangan, $id_item, $jumlah);";
		$this->db->query($sql);
		
		$this->view_distribusi(1);
	  
	}  
	  
	public function delete_distribusi($id_distribusi=FALSE)
	{
      
	  if($id_distribusi === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$sql="delete from t_distribusi where id_distribusi=".$id_distribusi;
		$this->db->query($sql);
		$this->view_distribusi(3);
	  }
	  
	}
	
	public function edit_distribusi($id_distribusi=FALSE)
	{
      
	  if($id_distribusi === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$data['distribusi'] = $this->g_distribusi_model->get_distribusi($id_distribusi);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('edit_distribusi', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('g_edit_distribusi', $data);
	  }
	  
	}
	
	public function update_distribusi()
	{
      
		$id_distribusi = $this->input->post('inputID');
		
		$inputNama = $this->input->post('inputNama');
		$keterangan = $this->input->post('keterangan');
		
		$sql="UPDATE `t_distribusi` set `nama_distribusi`='".$inputNama."',`keterangan`='".$keterangan."' WHERE id_distribusi=".$id_distribusi.";";
		
		$this->db->query($sql);
		
		$this->view_distribusi(2);
	  
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
