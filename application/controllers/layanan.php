<?php if (!defined('BASEPATH')) die();
class Layanan extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('layanan_model');
	}
	
   public function index()
	{
		$this->view_layanan(0);
	}
	
	public function view_layanan($status=0)
	{
		$data['layanan'] = $this->layanan_model->get_layanan();
		$data['status']=$status;
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_layanan', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_layanan', $data);
	}
	
	public function layanan_baru()
	{
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('create_layanan');
		$this->load->view('include/footer');
		*/
		$this->load->view('register_layanan');
	  
	}
	
	public function create_layanan()
	{
		
		$inputNama = $this->input->post('inputNama');
		$harga = $this->input->post('harga');
		$persendiskon = $this->input->post('persendiskon');
		$nominaldiskon = $this->input->post('nominaldiskon');
		$share1 = $this->input->post('share1');
		$share2 = $this->input->post('share2');
		$share3 = $this->input->post('share3');
		$share4 = $this->input->post('share4');
		
		$nominalshare1 = $this->input->post('nominalshare1');
		$nominalshare2 = $this->input->post('nominalshare2');
		$nominalshare3 = $this->input->post('nominalshare3');
		$nominalshare4 = $this->input->post('nominalshare4');
		
		$keterangan = $this->input->post('keterangan');
		
		
		$sql="INSERT INTO `layanan` (`id_layanan`, `nama_layanan`, `harga`, `diskon`, `nominal_diskon`, `share1`, `share2`, `share3`, `share4`, `persenshare1`, `persenshare2`, `persenshare3`, `persenshare4`, `status`, `keterangan`) VALUES (NULL, '$inputNama', '$harga', '$persendiskon', '$nominaldiskon', '$nominalshare1', '$nominalshare2', '$nominalshare3', '$nominalshare4', '$share1', '$share2', '$share3', '$share4', '1', '$keterangan');";
		$this->db->query($sql);
		
		$this->view_layanan(1);
	  
	}  
	  
	public function delete_layanan($id_layanan=FALSE)
	{
      
	  if($id_layanan === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$sql="update layanan set status=0 where id_layanan=".$id_layanan;
		$this->db->query($sql);
		$this->view_layanan(3);
	  }
	  
	}
	
	public function edit_layanan($id_layanan=FALSE)
	{
      
	  if($id_layanan === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$data['layanan'] = $this->layanan_model->get_layanan($id_layanan);
		
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('edit_layanan', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('edit_register_layanan', $data);
		
	  }
	  
	}
	
	public function update_layanan()
	{
      
		$id_layanan = $this->input->post('id_layanan');
		$inputNama = $this->input->post('inputNama');
		$harga = $this->input->post('harga');
		$persendiskon = $this->input->post('persendiskon');
		$nominaldiskon = $this->input->post('nominaldiskon');
		$share1 = $this->input->post('share1');
		$share2 = $this->input->post('share2');
		$share3 = $this->input->post('share3');
		$share4 = $this->input->post('share4');
		
		$nominalshare1 = $this->input->post('nominalshare1');
		$nominalshare2 = $this->input->post('nominalshare2');
		$nominalshare3 = $this->input->post('nominalshare3');
		$nominalshare4 = $this->input->post('nominalshare4');
		
		$keterangan = $this->input->post('keterangan');
		
		
		$sql="update `layanan` set `nama_layanan`='$inputNama', `harga`='$harga', `diskon`='$persendiskon', `nominal_diskon`='$nominaldiskon', `persenshare1`='$share1', `persenshare2`='$share2', `persenshare3`='$share3', `persenshare4`='$share4', `share1`='$nominalshare1', `share2`='$nominalshare2', `share3`='$nominalshare3', `share4`='$nominalshare4', `keterangan`='$keterangan' where id_layanan=$id_layanan;";
		$this->db->query($sql);
		
		$this->view_layanan(2);
	  
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
