<?php if (!defined('BASEPATH')) die();
class Laporan extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('laporan_model');
		$this->load->model('dokter_model');
		$this->load->model('layanan_model');
	}
	
   public function index()
	{
		$this->all();
	}
	
	public function all()
	{
		
		
		$data['laporanbulananpasien'] = array_reverse($this->laporan_model->laporanbulananpasien());
		//$data['laporanbulananpasienbaru'] = $this->laporan_model->laporanbulananpasienbaru();
		$data['laporanharianpasien'] = $this->laporan_model->laporanharianpasien();
		//$data['laporanharianpasienbaru'] = $this->laporan_model->laporanharianpasienbaru();
		$data['laporanbulananpendapatan'] = array_reverse($this->laporan_model->laporanbulananpendapatan());
		$data['laporanharianpendapatan'] = $this->laporan_model->laporanharianpendapatan();
		
		$month=date("n");
		$year=date("Y");
		
		$data['jumlahbulanini']=0;
		$labels='[';
		$values='[';
		$values2='[';
		
		foreach($data['laporanbulananpasien'] as $lbp):
			if($month==$lbp['bulan'] && $year=$lbp['tahun'])
			{
				$data['jumlahbulanini']=$lbp['jumlah'];
			}
			
			$labelmonth="Jan";
			if($lbp['bulan']==2)
			$labelmonth="Feb";
			else if($lbp['bulan']==3)
			$labelmonth="Mar";
			else if($lbp['bulan']==4)
			$labelmonth="Apr";
			else if($lbp['bulan']==5)
			$labelmonth="Mei";
			else if($lbp['bulan']==6)
			$labelmonth="Jun";
			else if($lbp['bulan']==7)
			$labelmonth="Jul";
			else if($lbp['bulan']==8)
			$labelmonth="Agust";
			else if($lbp['bulan']==9)
			$labelmonth="Sept";
			else if($lbp['bulan']==10)
			$labelmonth="Okt";
			else if($lbp['bulan']==11)
			$labelmonth="Nov";
			else if($lbp['bulan']==12)
			$labelmonth="Des";
			
			$labels=$labels.'"'.$labelmonth.' '.$lbp['tahun'].'",';
			$values=$values.''.$lbp['jumlah'].',';
			$values2=$values2.''.$lbp['jumlahbaru'].',';
		endforeach;
		
		$labels=substr_replace($labels ,"",-1);
		$labels=$labels.']';
		//$testing=trim(preg_replace('/\s+/', ' ', $testing));
		
		$values=substr_replace($values ,"",-1);
		$values=$values.']';
		//$testing=trim(preg_replace('/\s+/', ' ', $testing));
		
		$values2=substr_replace($values2 ,"",-1);
		$values2=$values2.']';
		//$testing=trim(preg_replace('/\s+/', ' ', $testing));
		
		$data['labels']=$labels;
		$data['values']=$values;
		$data['values2']=$values2;
		
		$data['pendapatanbulanini']=0;
		$pendlabels='[';
		$pendvalues='[';
		
		foreach($data['laporanbulananpendapatan'] as $lbp):
			if($month==$lbp['bulan'] && $year=$lbp['tahun'])
			{
				$data['pendapatanbulanini']=$lbp['jumlah'];
			}
			
			$labelmonth="Jan";
			if($lbp['bulan']==2)
			$labelmonth="Feb";
			else if($lbp['bulan']==3)
			$labelmonth="Mar";
			else if($lbp['bulan']==4)
			$labelmonth="Apr";
			else if($lbp['bulan']==5)
			$labelmonth="Mei";
			else if($lbp['bulan']==6)
			$labelmonth="Jun";
			else if($lbp['bulan']==7)
			$labelmonth="Jul";
			else if($lbp['bulan']==8)
			$labelmonth="Agust";
			else if($lbp['bulan']==9)
			$labelmonth="Sept";
			else if($lbp['bulan']==10)
			$labelmonth="Okt";
			else if($lbp['bulan']==11)
			$labelmonth="Nov";
			else if($lbp['bulan']==12)
			$labelmonth="Des";
			
			$pendlabels=$pendlabels.'"'.$labelmonth.' '.$lbp['tahun'].'",';
			$pendvalues=$pendvalues.''.$lbp['jumlah'].',';
		endforeach;
		
		$pendlabels=substr_replace($pendlabels ,"",-1);
		$pendlabels=$pendlabels.']';
		//$testing=trim(preg_replace('/\s+/', ' ', $testing));
		
		$pendvalues=substr_replace($pendvalues ,"",-1);
		$pendvalues=$pendvalues.']';
		//$testing=trim(preg_replace('/\s+/', ' ', $testing));
		
		
		$data['pendlabels']=$pendlabels;
		$data['pendvalues']=$pendvalues;
		
		/*
		$this->load->view('include/header2');
		$this->load->view('templates/menubar');
		$this->load->view('laporan_all', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('view_register_laporan_all', $data);
		
	}
	
	public function detailkeuangan()
	{
		$mulai = $this->input->post('mulai');
		$sampai = $this->input->post('sampai');
		$layanan = $this->input->post('layanan');
		
		$month=date("n");
		$year=date("Y");
		
		$flag=0;
		
		if($mulai==NULL)
		{	
			$mulai='12-2013';
			$data['laporanbulananpendapatan'] = array_reverse($this->laporan_model->laporanbulananpendapatan());
			$data['laporanbulananlayanan'] = array_reverse($this->laporan_model->laporanbulananlayanan());
			$flag=1;
		}
		else
		{
			$flag=1;
		}
		if($sampai==NULL)
		$sampai=$month.'-'.$year;
		if($layanan==NULL)
		$layanan=1;
		
		
		$pieces = explode("-", $mulai);
		
		$pieces2 = explode("-", $sampai);
		
		if($flag==1)
		{
			$data['laporanbulananpendapatan'] = array_reverse($this->laporan_model->laporanbulananpendapatan($pieces[0], $pieces[1], $pieces2[0], $pieces2[1]));
			$data['laporanbulananlayanan'] = array_reverse($this->laporan_model->laporanbulananlayanan($pieces[0], $pieces[1], $pieces2[0], $pieces2[1],$layanan));
		}
		
		$data['layanan'] = $this->layanan_model->get_layanan();
		$data['layananini'] = $this->layanan_model->get_layanan($layanan);
		$data['mulai']=$mulai;
		$data['sampai']=$sampai;
		$data['idlayanan']=$layanan;
		$data['laporanharianpendapatan'] = $this->laporan_model->laporanharianpendapatan();
		
		$data['pendapatanbulanini']=0;
		
		$pendlabels='[';
		$pendvalues='[';
		
		foreach($data['laporanbulananpendapatan'] as $lbp):
			if($month==$lbp['bulan'] && $year=$lbp['tahun'])
			{
				$data['pendapatanbulanini']=$lbp['jumlah'];
			}
			
			$labelmonth="Jan";
			if($lbp['bulan']==2)
			$labelmonth="Feb";
			else if($lbp['bulan']==3)
			$labelmonth="Mar";
			else if($lbp['bulan']==4)
			$labelmonth="Apr";
			else if($lbp['bulan']==5)
			$labelmonth="Mei";
			else if($lbp['bulan']==6)
			$labelmonth="Jun";
			else if($lbp['bulan']==7)
			$labelmonth="Jul";
			else if($lbp['bulan']==8)
			$labelmonth="Agust";
			else if($lbp['bulan']==9)
			$labelmonth="Sept";
			else if($lbp['bulan']==10)
			$labelmonth="Okt";
			else if($lbp['bulan']==11)
			$labelmonth="Nov";
			else if($lbp['bulan']==12)
			$labelmonth="Des";
			
			$pendlabels=$pendlabels.'"'.$labelmonth.' '.$lbp['tahun'].'",';
			$pendvalues=$pendvalues.''.$lbp['jumlah'].',';
		endforeach;
		
		$pendlabels=substr_replace($pendlabels ,"",-1);
		$pendlabels=$pendlabels.']';
		//$testing=trim(preg_replace('/\s+/', ' ', $testing));
		
		$pendvalues=substr_replace($pendvalues ,"",-1);
		$pendvalues=$pendvalues.']';
		//$testing=trim(preg_replace('/\s+/', ' ', $testing));
		
		
		$data['pendlabels']=$pendlabels;
		$data['pendvalues']=$pendvalues;
		
		$layananlabels='[';
		$layananvalues='[';
		$layananvalues2='[';
		
		foreach($data['laporanbulananlayanan'] as $lbp):
			
			$labelmonth="Jan";
			if($lbp['bulan']==2)
			$labelmonth="Feb";
			else if($lbp['bulan']==3)
			$labelmonth="Mar";
			else if($lbp['bulan']==4)
			$labelmonth="Apr";
			else if($lbp['bulan']==5)
			$labelmonth="Mei";
			else if($lbp['bulan']==6)
			$labelmonth="Jun";
			else if($lbp['bulan']==7)
			$labelmonth="Jul";
			else if($lbp['bulan']==8)
			$labelmonth="Agust";
			else if($lbp['bulan']==9)
			$labelmonth="Sept";
			else if($lbp['bulan']==10)
			$labelmonth="Okt";
			else if($lbp['bulan']==11)
			$labelmonth="Nov";
			else if($lbp['bulan']==12)
			$labelmonth="Des";
			
			$layananlabels=$layananlabels.'"'.$labelmonth.' '.$lbp['tahun'].'",';
			$layananvalues=$layananvalues.''.$lbp['jumlah'].',';
			$layananvalues2=$layananvalues2.''.$lbp['jmlpasien'].',';
		endforeach;
		
		$layananlabels=substr_replace($layananlabels ,"",-1);
		$layananlabels=$layananlabels.']';
		
		$layananvalues=substr_replace($layananvalues ,"",-1);
		$layananvalues=$layananvalues.']';
		
		$layananvalues2=substr_replace($layananvalues2 ,"",-1);
		$layananvalues2=$layananvalues2.']';
		
		
		$data['layananlabels']=$layananlabels;
		$data['layananvalues']=$layananvalues;
		$data['layananvalues2']=$layananvalues2;
		
		
		/*
		$this->load->view('include/header2');
		$this->load->view('templates/menubar');
		$this->load->view('detailpendapatan', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_laporan_keuangan', $data);
		
	}
	
	public function detail_layanan_bulanan()
	{
		$mulai = $this->input->post('mulai');
		
		
		$month=date("n");
		$year=date("Y");
		
		if($mulai==NULL)
		{	
			$mulai=$month.'-'.$year;
		}
		
		$pieces = explode("-", $mulai);
		
		$data['laporan'] = $this->laporan_model->laporan_semualayanan($pieces[0], $pieces[1]);
		$data['mulai']=$mulai;
		
		$this->load->view('view_register_laporan_layanan_bulanan', $data);
		
	}
	
	public function detail_layanan_pasien()
	{
		$mulai = $this->input->post('mulai');
		$id_layanan = $this->input->post('id_layanan');
		
		
		$month=date("n");
		$year=date("Y");
		
		if($mulai==NULL)
		{	
			$mulai=$month.'-'.$year;
			$id_layanan=1;
		}
		
		$pieces = explode("-", $mulai);
		
		$data['layanan'] = $this->layanan_model->get_layanan();
		
		$data['laporan'] = $this->laporan_model->laporan_pasien_layanan($pieces[0], $pieces[1], $id_layanan);
		$data['mulai']=$mulai;
		$data['id_layanan']=$id_layanan;
		$this->load->view('view_register_laporan_pasien_layanan', $data);
		
	}
	
	public function laporan_pembayaran_bulanan()
	{
		$mulai = $this->input->post('mulai');
		
		$month=date("n");
		$year=date("Y");
		
		if($mulai==NULL)
		{	
			$mulai=$month.'-'.$year;
		}
		
		$pieces = explode("-", $mulai);
		
		$data['laporan'] = $this->laporan_model->laporan_pembayaran_bulanan($pieces[0], $pieces[1]);
		$data['mulai']=$mulai;
		$this->load->view('view_register_laporan_pembayaran_bulanan', $data);
		
	}
	
	public function laporan_pembayaran_bulanan2()
	{
		$mulai = $this->input->post('mulai2');
		
		$month=date("n");
		$year=date("Y");
		
		if($mulai==NULL)
		{	
			$mulai=$month.'-'.$year;
		}
		
		$pieces = explode("-", $mulai);
		
		$data['laporan'] = $this->laporan_model->laporan_pembayaran_bulanan($pieces[0], $pieces[1]);
		$data['mulai']=$mulai;
		$this->load->view('view_register_cetak_laporan_pembayaran_bulanan', $data);
		
	}
	
	public function detail_layanan_bulanan2()
	{
		$mulai = $this->input->post('mulai2');
		
		
		$month=date("n");
		$year=date("Y");
		
		if($mulai==NULL)
		{	
			$mulai=$month.'-'.$year;
		}
		
		$pieces = explode("-", $mulai);
		
		$data['laporan'] = $this->laporan_model->laporan_semualayanan($pieces[0], $pieces[1]);
		$data['mulai']=$mulai;
		
		$this->load->view('view_register_cetak_laporan_layanan_bulanan', $data);
		
	}
	
	public function detailpasien()
	{
		$mulai = $this->input->post('mulai');
		$sampai = $this->input->post('sampai');
		
		$month=date("n");
		$year=date("Y");
		
		$flag=0;
		
		if($mulai==NULL)
		{
			$mulai=$month.'-'.$year;
			//$data['laporanbulananpasien'] = array_reverse($this->laporan_model->laporanbulananpasien());
			$flag=1;
		}
		else
		{
			$flag=1;
		}
		if($sampai==NULL)
		$sampai=$month.'-'.$year;
		
		
		$pieces = explode("-", $mulai);
		
		$pieces2 = explode("-", $sampai);
		if($flag==1)
		{
			$data['laporanbulananpasien'] = array_reverse($this->laporan_model->laporanbulananpasien($pieces[0], $pieces[1], $pieces2[0], $pieces2[1]));
		}
		$data['laporanharianpasien'] = $this->laporan_model->laporanharianpasien();
		$data['laporankotapasien'] = $this->laporan_model->laporankotapasien($pieces[0], $pieces[1], $pieces2[0], $pieces2[1]);
		
		$data['mulai']=$mulai;
		$data['sampai']=$sampai;
		
		$data['jumlahbulanini']=0;
		$labels='[';
		$values='[';
		$values2='[';
		
		foreach($data['laporanbulananpasien'] as $lbp):
			if($month==$lbp['bulan'] && $year=$lbp['tahun'])
			{
				$data['jumlahbulanini']=$lbp['jumlah'];
			}
			
			$labelmonth="Jan";
			if($lbp['bulan']==2)
			$labelmonth="Feb";
			else if($lbp['bulan']==3)
			$labelmonth="Mar";
			else if($lbp['bulan']==4)
			$labelmonth="Apr";
			else if($lbp['bulan']==5)
			$labelmonth="Mei";
			else if($lbp['bulan']==6)
			$labelmonth="Jun";
			else if($lbp['bulan']==7)
			$labelmonth="Jul";
			else if($lbp['bulan']==8)
			$labelmonth="Agust";
			else if($lbp['bulan']==9)
			$labelmonth="Sept";
			else if($lbp['bulan']==10)
			$labelmonth="Okt";
			else if($lbp['bulan']==11)
			$labelmonth="Nov";
			else if($lbp['bulan']==12)
			$labelmonth="Des";
			
			$labels=$labels.'"'.$labelmonth.' '.$lbp['tahun'].'",';
			$values=$values.''.$lbp['jumlah'].',';
			$values2=$values2.''.$lbp['jumlahbaru'].',';
		endforeach;
		
		$labels=substr_replace($labels ,"",-1);
		$labels=$labels.']';
		//$testing=trim(preg_replace('/\s+/', ' ', $testing));
		
		$values=substr_replace($values ,"",-1);
		$values=$values.']';
		//$testing=trim(preg_replace('/\s+/', ' ', $testing));
		
		$values2=substr_replace($values2 ,"",-1);
		$values2=$values2.']';
		//$testing=trim(preg_replace('/\s+/', ' ', $testing));
		
		$data['labels']=$labels;
		$data['values']=$values;
		$data['values2']=$values2;
		
		
		$pieData='[';
		$donData='[';
		
		$in=0;
		foreach($data['laporankotapasien'] as $lbp):
		if($in==0)
			$color="#F38630";
		else if($in==1)
			$color="#E0E4CC";
		else if($in==2)
			$color="#FF00FF";
		else if($in==3)
			$color="#69D2E7";
		else if($in==4)
			$color="#F7464A";
		else if($in==5)
			$color="#E2EAE9";
		else if($in==6)
			$color="#D4CCC5";
		else if($in==7)
			$color="#949FB1";
		else if($in==8)
			$color="#4D5360";
		else if($in==9)
			$color="#993333";
		else 
			$color="#CC00CC";
		
		if($lbp['kota']=="")
		$lbp['kota']="Undefined";
		$pieData=$pieData.'{value: '.$lbp['jumlah'].',color:"'.$color.'",title: "'.$lbp['kota'].' : '.$lbp['jumlah'].'"},';
		
		$donData=$donData.'{value: '.$lbp['jumlahbaru'].',color:"'.$color.'",title: "'.$lbp['kota'].' : '.$lbp['jumlahbaru'].'"},';
		$in++;
		endforeach;
		
		
		$pieData=substr_replace($pieData ,"",-1);
		$pieData=$pieData.']';
		
		$donData=substr_replace($donData ,"",-1);
		$donData=$donData.']';
		
		
		$data['pieData']=$pieData;
		$data['donData']=$donData;
		
		/*
		$this->load->view('include/header2');
		$this->load->view('templates/menubar');
		$this->load->view('detailpasien', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_laporan_pasien', $data);
	}
	
	
	public function view_laporan($bulan1=0, $bulan2=0, $tahun='2013')
	{
		
		$bulan1 = $this->input->post('bulan1');
		$bulan2 = $this->input->post('bulan2');
		$tahun = $this->input->post('tahun');
		
		if ($bulan1==NULL)
		{
			$bulan1=date('n');
		}
		if ($bulan2==NULL)
		{
			$bulan2=date('n');
		}
		if ($tahun==NULL)
		{
			$tahun='2013';
		}
		
		$data['laporan'] = $this->laporan_model->bulanan($bulan1, $bulan2, $tahun);
		$data['bulan1']=$bulan1;
		$data['bulan2']=$bulan2;
		$data['tahun']=$tahun;
		
		
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('laporan_bulanan', $data);
		$this->load->view('include/footer');
	}
	
	public function laporan_dokter()
	{
	
		$mulai = $this->input->post('mulai');
		$sampai = $this->input->post('sampai');
		$id_dokter = $this->input->post('id_dokter');
		
		$month=date("n");
		$year=date("Y");
		
		if($mulai==NULL)
		{
			$mulai=$month.'-'.$year;
			$sampai=$month.'-'.$year;
			$id_dokter=7;
		}
		
		$pieces = explode("-", $mulai);
		
		$pieces2 = explode("-", $sampai);
		
		if($pieces2[0]=="1" || $pieces2[0]==1)
		{
			$pieces3[0]=12;
			$pieces3[1]=$pieces2[1]-1;
		}
		else
		{
			$pieces3[0]=$pieces2[0]-1;
			$pieces3[1]=$pieces2[1];
		}
		
		
		//$data['laporanbulanandokter'] = array_reverse($this->laporan_model->laporanbulananpendapatan($pieces[0], $pieces[1], $pieces2[0], $pieces2[1]));
		
		//$data['laporan'] = $this->laporan_model->dokter_bulanan_pengirim($bulan1, $bulan2, $tahun, $id_dokter);
		
		//$data['laporan2'] = $this->laporan_model->dokter_bulanan_operator($bulan1, $bulan2, $tahun, $id_dokter);
		
		$data['laporan'] = $this->laporan_model->laporan_bulanan_dokter_pengirim($id_dokter, $pieces[0], $pieces[1], $pieces2[0], $pieces2[1]);
		
		$data['laporan2'] = $this->laporan_model->laporan_bulanan_dokter_operator($id_dokter, $pieces[0], $pieces[1], $pieces2[0], $pieces2[1]);
		
		$data['laporan3'] = $this->laporan_model->laporan_bulanan_dokter_pengirim_eecp_esmr($id_dokter, $pieces[0], $pieces[1], $pieces2[0], $pieces2[1]);
		
		$data['laporan4'] = $this->laporan_model->laporan_bulanan_dokter_pengirim_eecp_esmr($id_dokter, $pieces[0], $pieces[1], $pieces3[0], $pieces3[1]);
		
		$data['mulai']=$mulai;
		$data['sampai']=$sampai;
		$data['id_dokter']=$id_dokter;
		$data['dokter'] = $this->dokter_model->get_dokter();
		
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('dokter_bulanan', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('view_register_laporan_dokter', $data);
	}
	
	public function laporan_fee_dokter_operator_harian($mulai=NULL)
	{
	
		$mulai = $this->input->post('mulai');
		$id_dokter = $this->input->post('id_dokter');
		
		if($mulai==NULL)
		{
			$mulai=date('Y-m-j');
			$id_dokter=7;
		}
		
		$mulai =date('Y-m-j', strtotime($mulai));
		$data['laporan'] = $this->laporan_model->laporan_harian_dokter_pengirim($id_dokter, $mulai);
		$data['laporan2'] = $this->laporan_model->laporan_harian_dokter_operator($id_dokter, $mulai);
		$mulai =date('j-m-Y', strtotime($mulai));
		$data['mulai']=$mulai;
		$data['id_dokter']=$id_dokter;
		$data['dokter'] = $this->dokter_model->get_dokter();
		
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('dokter_bulanan', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_laporan_fee_dokter_harian', $data);
	}
	
	public function cetak_fee_dokter_bulanan()
	{
		$mulai = $this->input->post('mulai2');
		$sampai = $this->input->post('sampai2');
		$id_dokter = $this->input->post('id_dokter2');
		
		$month=date("n");
		$year=date("Y");
		
		if($mulai==NULL)
		{
			$mulai=$month.'-'.$year;
			$sampai=$month.'-'.$year;
			$id_dokter=7;
		}
		
		$pieces = explode("-", $mulai);
		
		$pieces2 = explode("-", $sampai);
		
		if($pieces2[0]=="1" || $pieces2[0]==1)
		{
			$pieces3[0]=12;
			$pieces3[1]=$pieces2[1]-1;
		}
		else
		{
			$pieces3[0]=$pieces2[0]-1;
			$pieces3[1]=$pieces2[1];
		}
		
		
		//$data['laporanbulanandokter'] = array_reverse($this->laporan_model->laporanbulananpendapatan($pieces[0], $pieces[1], $pieces2[0], $pieces2[1]));
		
		//$data['laporan'] = $this->laporan_model->dokter_bulanan_pengirim($bulan1, $bulan2, $tahun, $id_dokter);
		
		//$data['laporan2'] = $this->laporan_model->dokter_bulanan_operator($bulan1, $bulan2, $tahun, $id_dokter);
		
		$data['laporan'] = $this->laporan_model->laporan_bulanan_dokter_pengirim($id_dokter, $pieces[0], $pieces[1], $pieces2[0], $pieces2[1]);
		
		$data['laporan2'] = $this->laporan_model->laporan_bulanan_dokter_operator($id_dokter, $pieces[0], $pieces[1], $pieces2[0], $pieces2[1]);
		
		$data['laporan3'] = $this->laporan_model->laporan_bulanan_dokter_pengirim_eecp_esmr($id_dokter, $pieces[0], $pieces[1], $pieces2[0], $pieces2[1]);
		
		$data['laporan4'] = $this->laporan_model->laporan_bulanan_dokter_pengirim_eecp_esmr($id_dokter, $pieces[0], $pieces[1], $pieces3[0], $pieces3[1]);
		
		$data['mulai']=$mulai;
		$data['sampai']=$sampai;
		$data['id_dokter']=$id_dokter;
		$data['dokter'] = $this->dokter_model->get_dokter();
		
		$this->load->view('view_register_csv_laporan_fee_bulanan_dokter', $data);
	}
	
	public function cetak_fee_dokter_harian($mulai=NULL)
	{
	
		$mulai = $this->input->post('mulai2');
		$id_dokter = $this->input->post('id_dokter2');
		
		if($mulai==NULL)
		{
			$mulai=date('Y-m-j');
			$id_dokter=7;
		}
		
		$mulai =date('Y-m-j', strtotime($mulai));
		$data['laporan'] = $this->laporan_model->laporan_harian_dokter_pengirim($id_dokter, $mulai);
		$data['laporan2'] = $this->laporan_model->laporan_harian_dokter_operator($id_dokter, $mulai);
		$mulai =date('j-m-Y', strtotime($mulai));
		$data['mulai']=$mulai;
		$data['id_dokter']=$id_dokter;
		$data['dokter'] = $this->dokter_model->get_dokter();
		
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('dokter_bulanan', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_csv_laporan_fee_harian_dokter', $data);
	}
	
	public function detailpasien2()
	{
		$mulai = $this->input->post('mulai2');
		$sampai = $this->input->post('sampai2');
		
		$month=date("n");
		$year=date("Y");
		
		$flag=0;
		
		if($mulai==NULL)
		{
			$mulai=$month.'-'.$year;
			//$data['laporanbulananpasien'] = array_reverse($this->laporan_model->laporanbulananpasien());
			$flag=1;
		}
		else
		{
			$flag=1;
		}
		if($sampai==NULL)
		$sampai=$month.'-'.$year;
		
		
		$pieces = explode("-", $mulai);
		
		$pieces2 = explode("-", $sampai);
		if($flag==1)
		{
			$data['laporanbulananpasien'] = array_reverse($this->laporan_model->laporanbulananpasien($pieces[0], $pieces[1], $pieces2[0], $pieces2[1]));
		}
		
		$ind=1;
		foreach ($data['laporanbulananpasien'] as $laporan_item):
			$data_nama_pasien[$ind]=$this->laporan_model->laporan_nama_pasien($laporan_item['bulan'], $laporan_item['tahun']);
			$ind++;
		endforeach;
		
		$data['data_nama_pasien']=$data_nama_pasien;
		$data['laporankotapasien'] = $this->laporan_model->laporankotapasien($pieces[0], $pieces[1], $pieces2[0], $pieces2[1]);
		
		$data['mulai']=$mulai;
		$data['sampai']=$sampai;
		
		$this->load->view('view_register_cetak_laporan_pasien', $data);
	}
	   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
