<?php if (!defined('BASEPATH')) die();
class Pasien extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('pasien_model');
	}
	
   public function index()
	{
		$this->view_pasien(0);
	}
	
	public function view_pasien($status=0)
	{
		$data['pasien'] = $this->pasien_model->get_pasien();
		$data['status']=$status;
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_pasien', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_pasien', $data);
	}
	
	public function cek_ada()
	{
		$id = $this->input->post('input');
		$jumlah = $this->pasien_model->get_jumlah_pasien($id);
		echo $jumlah['jumlah'];
	}
	
	public function pasien_baru($dari=0)
	{
		$data['dari']=$dari;
		$kota = $this->pasien_model->get_kota();
		$data['perusahaan'] = $this->pasien_model->get_perusahaan();
		$testing2='';
	foreach ($kota as $pasien_item):
	
	$testing2 = $testing2.'{"stateCode": "'.$pasien_item["lokasi_ID"].'", "stateDisplay": "'.$pasien_item["lokasi_nama"].'", "stateName": "'.$pasien_item["lokasi_nama"].'"},';
	endforeach;
	
	$testing2=substr_replace($testing2 ,"",-1);
	$testing2=trim(preg_replace('/\s+/', ' ', $testing2));
	$data['testing2']=$testing2;
	/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('create_pasien', $data);
		$this->load->view('include/footer');
	*/
	
	$this->load->view('register_pasien', $data);
	
	}
	
	public function create_pasien()
	{
      $id_pasien = $this->input->post('inputID');
	  if($id_pasien==NULL)
	  {
		show_404();
	  }
	  else
	  {
		$dari = $this->input->post('dari');
		
		$id_pasien = $this->input->post('inputID');
		//$inputmedrec = $this->input->post('inputmedrec');
		$inputmedrec = $id_pasien;
		$member = $this->input->post('member');
		if($member==NULL)
		$member=0;
		
		$initial = $this->input->post('initial');
		$inputNama = $this->input->post('inputNama');
		$tanggallahir = $this->input->post('tanggallahir');
		$umur = $this->input->post('umur');
		$umur_bulan = $this->input->post('umur_bulan');
		$umur_hari = $this->input->post('umur_hari');
		$jeniskelamin = $this->input->post('jeniskelamin');
		$goldar = $this->input->post('goldar');
		$resus = $this->input->post('resus');
		$alamat = $this->input->post('alamat');
		$inputKota = $this->input->post('inputKota');
		$inputKodepos = $this->input->post('inputKodepos');
		$inputEmail = $this->input->post('inputEmail');
		$pekerjaan = $this->input->post('pekerjaan');
		$inputTelepon = $this->input->post('inputTelepon');
		$hp = $this->input->post('hp');
		//$inputPerusahaan = $this->input->post('inputPerusahaan');
		$asuransi = $this->input->post('asuransi');
		$keterangan = $this->input->post('keterangan');
		
		
		$sql="INSERT INTO `pasien` (`ktp`, `nama_pasien`, `med_rec`, `member`, `tanggal_lahir`, `umur`, `umur_bulan`, `umur_hari`, `jenis_kelamin`, `initial`, `goldar`, `rhesus`, `alamat`, `kota`, `kodepos`, `email`, `pekerjaan`, `telepon`, `hp`, `perusahaan`, `keterangan`)
		VALUES (
		'".$id_pasien."',  '".$inputNama."',  '".$inputmedrec."',  ".$member.",  STR_TO_DATE('$tanggallahir', '%d-%m-%Y'),  ".$umur.", ".$umur_bulan.", ".$umur_hari.",  ".$jeniskelamin.",  ".$initial.",  ".$goldar.",  ".$resus.",  '".$alamat."',  '".$inputKota."',  '".$inputKodepos."',  '".$inputEmail."',  '".$pekerjaan."',  '".$inputTelepon."',  '".$hp."',  ".$asuransi.",  '".$keterangan."');";
		$this->db->query($sql);
		if($dari==1)
		{
			header( 'Location: '.site_url("g_pemakaian/create_pemakaian/".$id_pasien."").'' );
		}
		else
		$this->view_pasien(1);
	  }
	}  
	  
	public function delete_pasien($id_pasien=FALSE)
	{
      
	  if($id_pasien === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$sql="delete from pasien where id_pasien=".$id_pasien;
		$this->db->query($sql);
		$this->view_pasien(3);
	  }
	  
	}
	
	public function edit_pasien($id_pasien=FALSE)
	{
      
	  if($id_pasien === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$kota = $this->pasien_model->get_kota();
		$data['perusahaan'] = $this->pasien_model->get_perusahaan();
		$testing2='';
	foreach ($kota as $pasien_item):
	
	$testing2 = $testing2.'{"stateCode": "'.$pasien_item["lokasi_ID"].'", "stateDisplay": "'.$pasien_item["lokasi_nama"].'", "stateName": "'.$pasien_item["lokasi_nama"].'"},';
	endforeach;
	
	
	
	$testing2=substr_replace($testing2 ,"",-1);
	$testing2=trim(preg_replace('/\s+/', ' ', $testing2));
	$data['testing2']=$testing2;
	
		
		$data['pasien'] = $this->pasien_model->get_pasien($id_pasien);
		
		$this->load->view('edit_register_pasien', $data);
	  }
	  
	}
	
	public function update_pasien()
	{
	  
		$inputID_id = $this->input->post('inputID_id');
		
		$id_pasien = $this->input->post('inputID');
		$inputmedrec = $this->input->post('inputmedrec');
		$member = $this->input->post('member');
		if($member==NULL)
		$member=0;
		
		$initial = $this->input->post('initial');
		$inputNama = $this->input->post('inputNama');
		$tanggallahir = $this->input->post('tanggallahir');
		$umur = $this->input->post('umur');
		$umur_bulan = $this->input->post('umur_bulan');
		$umur_hari = $this->input->post('umur_hari');
		$jeniskelamin = $this->input->post('jeniskelamin');
		$goldar = $this->input->post('goldar');
		$resus = $this->input->post('resus');
		$alamat = $this->input->post('alamat');
		$inputKota = $this->input->post('inputKota');
		$inputKodepos = $this->input->post('inputKodepos');
		$inputEmail = $this->input->post('inputEmail');
		$pekerjaan = $this->input->post('pekerjaan');
		$inputTelepon = $this->input->post('inputTelepon');
		$hp = $this->input->post('hp');
		$asuransi = $this->input->post('asuransi');
		//$inputPerusahaan = $this->input->post('inputPerusahaan');
		$keterangan = $this->input->post('keterangan');
		
		
		$sql="update `pasien` set `nama_pasien`='".$inputNama."', ktp='".$id_pasien."', `med_rec`='".$inputmedrec."', `member`=".$member.", `tanggal_lahir`=STR_TO_DATE('$tanggallahir', '%d-%m-%Y'), `umur`=".$umur.", `umur_bulan`=".$umur_bulan.", `umur_hari`=".$umur_hari.", `jenis_kelamin`=".$jeniskelamin.", `initial`=".$initial.", `goldar`=".$goldar.", `rhesus`=".$resus.", `alamat`='".$alamat."', `kota`='".$inputKota."', `kodepos`='".$inputKodepos."', `email`='".$inputEmail."', `pekerjaan`='".$pekerjaan."', `telepon`='".$inputTelepon."', `hp`='".$hp."', `perusahaan`=".$asuransi.", `keterangan`='".$keterangan."' where `id_pasien`='".$inputID_id."'";
		$this->db->query($sql);
		
		$this->view_pasien(2);
	  
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
