<?php if (!defined('BASEPATH')) die();
class G_kategori extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_kategori_model');
	}
	
   public function index()
	{
		$this->view_kategori(0);
	}
	
	public function view_kategori($status=0)
	{
		$data['kategori'] = $this->g_kategori_model->get_kategori();
		$data['status']=$status;
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_kategori', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('g_view_gudang_kategori', $data);
	}
	
	public function kategori_baru()
	{
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('create_kategori');
		$this->load->view('include/footer');
		*/
		$this->load->view('g_register_kategori');
	}
	
	public function create_kategori()
	{
		$inputNama = $this->input->post('inputNama');
		$keterangan = $this->input->post('keterangan');
		
		$sql="INSERT INTO  `t_kategori` (`nama_kategori` ,`keterangan`)
		VALUES (
		'".$inputNama."','".$keterangan."');";
		$this->db->query($sql);
		
		$this->view_kategori(1);
	  
	}  
	  
	public function delete_kategori($id_kategori=FALSE)
	{
      
	  if($id_kategori === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$sql="delete from t_kategori where id_kategori=".$id_kategori;
		$this->db->query($sql);
		$this->view_kategori(3);
	  }
	  
	}
	
	public function edit_kategori($id_kategori=FALSE)
	{
      
	  if($id_kategori === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$data['kategori'] = $this->g_kategori_model->get_kategori($id_kategori);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('edit_kategori', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('g_edit_kategori', $data);
	  }
	  
	}
	
	public function update_kategori()
	{
      
		$id_kategori = $this->input->post('inputID');
		
		$inputNama = $this->input->post('inputNama');
		$keterangan = $this->input->post('keterangan');
		
		$sql="UPDATE `t_kategori` set `nama_kategori`='".$inputNama."',`keterangan`='".$keterangan."' WHERE id_kategori=".$id_kategori.";";
		
		$this->db->query($sql);
		
		$this->view_kategori(2);
	  
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
