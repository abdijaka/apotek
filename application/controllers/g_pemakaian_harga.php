<?php if (!defined('BASEPATH')) die();
class G_pemakaian extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_pemakaian_model');
		$this->load->model('g_item_model');
		$this->load->model('pasien_model');
		$this->load->model('g_stock_model');
	}
	
   public function index()
	{
	  /*
	  $this->load->view('include/header');
	  $this->load->view('templates/menubar');
      $this->load->view('view_transaksi');
      $this->load->view('include/footer');
	  */
	  $this->view_pemakaian();
	}
	
	public function view_pemakaian($status=0, $id_pemakaian=FALSE )
	{
		$data['pemakaian'] = $this->g_pemakaian_model->get_pemakaian($id_pemakaian);
		$data['status']=$status;
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('g_view_gudang_pemakaian', $data);
	}
	
	public function view_all_transaksi()
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi2(0);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_transaksi', $data);
	}
	
	public function checkfaktur()
	{
		$reg = $this->input->post('reg');
		$jumlah = $this->g_pemakaian_model->get_jumlah_faktur($reg);
		echo $jumlah['jumlah'];
	}
	
	
	public function delete_pemakaian($reg)
	{
		$detail_beli = $this->g_pemakaian_model->get_detail($reg);
		foreach($detail_beli as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian2($item_detail['item'], $item_detail['tgl_masuk'] );
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2($item_detail['item'], $item_detail['tgl_masuk']);
			}
			
			$sql="update t_item set stock_gudang=stock_gudang-".$item_detail['jumlah'].", stock_sisa=stock_sisa-".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$item_detail['jumlah'].", stock_awal=stock_awal-".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
		endforeach;
		
		
		$sql="delete from t_pemakaian where id_pemakaian=$reg";
		$this->db->query($sql);
		
		$sql="delete from t_item_pakai where pemakaian=$reg";
		$this->db->query($sql);
		
		$this->view_pemakaian(3);
	}
	
	public function insert_item(){
 
        $data['title'] = 'Lorem ipsum';
        //$data['list'] = $this->test_model->get_data();
		
		$reg = $this->input->post('reg');
		$id_item = $this->input->post('id_item');
		$jumlah = $this->input->post('jumlah');
		$harga = $this->input->post('harga');
		$satuan = $this->input->post('satuan');
		$diskon = $this->input->post('diskon');
		$nominal_diskon = $this->input->post('nominal_diskon');
		$netto = $this->input->post('netto');
		$harga_subtotal = $this->input->post('harga_subtotal');
		
		$sql="INSERT INTO `t_item_pakai` (`pemakaian`, `item`, `jumlah`, `harga`, `diskon`, `nominal_diskon`, `total`, satuan, subtotal) VALUES ($reg, $id_item, $jumlah, '$harga', $diskon, '$nominal_diskon', '$netto', '$satuan','$harga_subtotal');";
		$this->db->query($sql);
		
		
		$sql="update t_pemakaian set subtotal=subtotal+$netto, nominal_diskon=diskon*subtotal/100, total=subtotal-nominal_diskon WHERE id_pemakaian=$reg";
		$this->db->query($sql);
		
		if($reg==0 || $reg=="0")
		{
			
		}
		else
		{
			$sql="update t_item set stock_gudang=stock_gudang+".$jumlah.", stock_sisa=stock_sisa+".$jumlah." where id_item=".$id_item;	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$jumlah." where item=".$id_item." AND DATE(tgl_stock)=STR_TO_DATE('$tanggalmasuk', '%d-%m-%Y');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$jumlah.", stock_awal=stock_awal+".$jumlah." where item=".$id_item." AND DATE(tgl_stock)>STR_TO_DATE('$tanggalmasuk', '%d-%m-%Y');";	
			$this->db->query($sql);
		}
		
		$data['list'] = $this->g_pemakaian_model->get_detail($reg);
		
        $this->load->view('t_tabel_item_pakai', $data);
		
    }
	
	public function show_table()
	{
		$reg = $this->input->post('reg');
		$data['list'] = $this->g_pemakaian_model->get_detail($reg);
        $this->load->view('t_tabel_item_pakai', $data);
	}
	
	public function show_detail_item()
	{
		$reg = $this->input->post('reg');
		$data['list'] = $this->g_pemakaian_model->get_detail($reg);
        $this->load->view('t_tabel_item_pakai', $data);
	}
	
	public function delete_item(){
        //$data['title'] = 'Lorem ipsum';
        //$data['list'] = $this->test_model->get_data();
		
		$reg = $this->input->post('reg');
		$netto = $this->input->post('netto');
		$pemakaian = $this->input->post('pemakaian');
		
		
		
		if($reg==0 || $reg=="0")
		{
			
		}
		else
		{
			$item_detail = $this->g_pemakaian_model->get_detail_itembeli($reg);
		
			$sql="update t_item set stock_gudang=stock_gudang-".$item_detail['jumlah'].", stock_sisa=stock_sisa-".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$item_detail['jumlah'].", stock_awal=stock_awal-".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
		}
		
		$sql="delete from `t_item_pakai` where `id_item_pakai`=$reg;";
		$this->db->query($sql);
		
		$sql="update t_pemakaian set subtotal=subtotal-$netto, nominal_diskon=diskon*subtotal/100, total=subtotal-nominal_diskon WHERE id_pemakaian=$reg";
		
		$this->db->query($sql);
		
		$data['list'] = $this->g_pemakaian_model->get_detail($pemakaian);
        $this->load->view('t_tabel_item_pakai', $data);
 
    }
	
	
	public function create_pemakaian($idsup=0)
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$data['pasien'] = $this->pasien_model->get_pasien();
			$data['idsup']=$idsup;
			
			$sql="delete from t_item_pakai where pemakaian=0;";
			$this->db->query($sql);
		  
			$item = $this->g_item_model->get_item();
		  
			$testing3='';
			foreach ($item as $pasien_item):
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "harga": "'.$pasien_item["harga_beli"].'", "satuanbeli": "'.$pasien_item["satuan_beli"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$this->formatRupiah($pasien_item["harga_beli"]).'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;
		  
			$this->load->view('g_register_pemakaian', $data);
		}
	}
	
	public function edit_pemakaian($inputReg)
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$data['supplier'] = $this->g_supplier_model->get_supplier();
		  
			$item = $this->g_item_model->get_item();
		  
			$testing3='';
			foreach ($item as $pasien_item):
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "harga": "'.$pasien_item["harga_beli"].'", "satuanbeli": "'.$pasien_item["satuan_beli"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$this->formatRupiah($pasien_item["harga_beli"]).'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;
		  

			$data['pemakaian'] = $this->g_pemakaian_model->get_pemakaian($inputReg);
			//$data['detail'] = $this->g_pemakaian_model->get_detail($inputReg);
			/*  
			  $this->load->view('include/header');
			  $this->load->view('templates/menubar');
			  $this->load->view('edit_transaksi', $data);
			  $this->load->view('include/footer');
			*/  
			$this->load->view('g_edit_pemakaian', $data);
		
		}
	}
	
	public function insert_pemakaian(){
 
		$nomorfaktur = $this->input->post('nomorfaktur');
		$tanggalfaktur = $this->input->post('tanggalfaktur');
		
		$pasien = $this->input->post('pasien');
		
		$harga = $this->input->post('totalharga');
		$diskon = $this->input->post('totaldiskon');
		$nominal_diskon = $this->input->post('totalnominaldiskon');
		
		$netto = $this->input->post('totalharganetto');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		
		$sql="INSERT INTO `t_pemakaian` (`nomor_faktur`, `tgl_isi`, `pasien`, `tgl_faktur`, `subtotal`, `diskon`, `nominal_diskon`, `total`, `operator`) VALUES ('$nomorfaktur', NOW(), $pasien, STR_TO_DATE('$tanggalfaktur', '%d-%m-%Y'), '$harga', $diskon, '$nominal_diskon', '$netto', '$operator');";
		$this->db->query($sql);
		
		$last_pemakaian = $this->g_pemakaian_model->get_last_pemakaian($nomorfaktur);
		
		$sql="update t_item_pakai set pemakaian=".$last_pemakaian['id_pemakaian'].", tgl_masuk=STR_TO_DATE('$tanggalfaktur', '%d-%m-%Y') where pemakaian=0";
		$this->db->query($sql);
		
		$detail_beli = $this->g_pemakaian_model->get_detail($last_pemakaian['id_pemakaian']);
		
		foreach($detail_beli as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian2($item_detail['item'], $item_detail['tgl_masuk'] );
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2($item_detail['item'], $item_detail['tgl_masuk']);
			}
			
			$sql="update t_item set stock_sisa=stock_sisa-".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$item_detail['jumlah'].", stock_awal=stock_awal+".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
		endforeach;
		
		//$this->db->query($sql);
		
		//$this->cetak_kwitansi($inputReg);
		
		redirect('g_pemakaian/view_detail/'.$last_pemakaian['id_pemakaian']);
		
    }
	
	public function update_pemakaian(){
 
		$inputReg = $this->input->post('id_pemakaian');
		
		$nomorfaktur = $this->input->post('nomorfaktur');
		$tanggalfaktur = $this->input->post('tanggalfaktur');
		
		$supplier = $this->input->post('supplier');
		
		$harga = $this->input->post('totalharga');
		$diskon = $this->input->post('totaldiskon');
		$nominal_diskon = $this->input->post('totalnominaldiskon');
		
		$netto = $this->input->post('totalharganetto');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		
		$sql="UPDATE `t_pemakaian` set `nomor_faktur`='$nomorfaktur',`supplier`=$supplier, `tgl_faktur`=STR_TO_DATE('$tanggalfaktur', '%d-%m-%Y'), `subtotal`='$harga', `diskon`=$diskon, `nominal_diskon`='$nominal_diskon', `total`='$netto', `operator`='$operator' where id_pemakaian=$inputReg";
		$this->db->query($sql);
		
		
		//$this->cetak_kwitansi($inputReg);
		
		redirect('g_pemakaian/view_detail/'.$inputReg);
		
    }

	public function view_detail($inputReg=0){
		
		$data['pemakaian'] = $this->g_pemakaian_model->get_pemakaian($inputReg);
		
		$data['detail'] = $this->g_pemakaian_model->get_detail($inputReg);
		
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('success', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('g_detail_pemakaian', $data);
    }
	
	public function receipt($inputReg=0){
		
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran($inputReg);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function cetak_pembayaran($inputReg=0, $tanggal){
		$tanggal=urldecode($tanggal);
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran_kwitansi($inputReg, $tanggal);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function laporan_harian($tanggal=FALSE){
		
		$transaksihariini = $this->transaksi_model->get_transaksi2($tanggal);

		$detail=array();
		$pembayaran=array();
		foreach ($transaksihariini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg);
		endforeach;
		$data['transaksihariini']=$transaksihariini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		if($tanggal === FALSE)
		{
			$tanggal=date('j-m-Y');
		}
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function laporan_harian_baru($tanggal=FALSE){
		
		
		if($this->input->post('mulai')!=NULL)
		{
			$tanggal=$this->input->post('mulai');
		}
		else if($tanggal === FALSE)
		{
			$tanggal=date('Y-m-j');
		}
		$tanggal =date('Y-m-j', strtotime($tanggal));
		
		$pembayaran_hari_ini=$this->transaksi_model->get_pembayaran(FALSE, $tanggal);
		
		$detail=array();
		$pembayaran=array();
		foreach ($pembayaran_hari_ini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg, $tanggal);
		endforeach;
		$data['transaksihariini']=$pembayaran_hari_ini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function cetak_kwitansi($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('create_transaksi_success', $data);
    }
	
	public function cetak_kwitansi_besar(){
		$data['kwb_nomorkwitansi'] = $this->input->post('kwb_nomorkwitansi');
		$data['kwb_dari'] = $this->input->post('kwb_dari');
		$data['kwb_jumlah'] = $this->input->post('kwb_jumlah');
		$data['kwb_keperluan'] = $this->input->post('kwb_keperluan');
		$data['kwb_nominal'] = $this->input->post('kwb_nominal');
		$data['kwb_tanggal'] = $this->input->post('kwb_tanggal');
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	public function cetak_kwitansi_besar2($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	function formatRupiah($nilaiUang)
	{
	  $nilaiRupiah 	= "";
	  $jumlahAngka 	= strlen($nilaiUang);
	  while($jumlahAngka > 3)
	  {
		$nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
		$sisaNilai = strlen($nilaiUang) - 3;
		$nilaiUang = substr($nilaiUang,0,$sisaNilai);
		$jumlahAngka = strlen($nilaiUang);
	  }
	 
	  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
	  return $nilaiRupiah;
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
