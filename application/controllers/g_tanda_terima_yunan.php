<?php if (!defined('BASEPATH')) die();
class G_tanda_terima_yunan extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_pembelian_model');
		$this->load->model('g_item_model');
		$this->load->model('g_supplier_model');
		$this->load->model('g_stock_model');
		$this->load->model('g_carabayar_model');
		$this->load->model('g_tanda_terima_model');
	}
	
   public function index()
	{
	  $this->view_tanda_terima();
	}

	private function diff($date1, $date2) {
        $diff = abs(strtotime($date2) - strtotime($date1));
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
        return $days;
    }
	
	public function view_tanda_terima($status=0, $id_pembelian=FALSE )
	{
		$arr_pembelian = $this->g_pembelian_model->get_pembelian($id_pembelian);
		$rentang = $this->input->post('rentang');

		foreach ($arr_pembelian as $key => $value) {
			$arr_pembelian[$key]['status_jatuh_tempo'] = array();
			$arr_pembelian[$key]['status_jatuh_tempo']['diff_date'] = new DateTime();
			if($value['total'] < $value['jumlah_bayar']){
				$arr_pembelian[$key]['status_jatuh_tempo']['status'] = TRUE;
				$tgl_jatuh_tempo = $value['jatuh_tempo'];
				$today = date("Y-m-d");
				if( $this->diff($tgl_jatuh_tempo, $today) > 0 ){

					$arr_pembelian[$key]['status_jatuh_tempo']['diff_date'] = $this->diff($tgl_jatuh_tempo, $today);
				}
				else $arr_pembelian[$key]['status_jatuh_tempo']['diff_date'] = NULL;

			}
			else $arr_pembelian[$key]['status_jatuh_tempo']['status'] = FALSE;
		}
		$data['tanda_terima'] = $this->g_tanda_terima_model->get_tanda_terima();
		$data['pembelian_sort'] = $this->g_pembelian_model->get_pembelian_sort($rentang);
		$data['pembayaran'] = $this->g_pembelian_model->get_pembayaran($id_pembelian);
		$data['pembelian'] = $arr_pembelian;
		$data['status']=$status;
		$this->load->view('g_view_tanda_terima_yunan', $data);
	}
	
	public function create_tanda_terima($idsup=0)
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$now = date('ymd');
			$count = $this->g_tanda_terima_model->count_tanda_terima_by_date($now);
			//var_dump($count);
			if($count == NULL)
			{
				$no_tanda_terima = $now.'001';
				$data['no_tanda_terima'] = $no_tanda_terima;
				$data['supplier'] = $this->g_supplier_model->get_supplier();
				$data['carabayar'] = $this->g_carabayar_model->get_carabayar();
				$data['idsup']=$idsup;
				//echo $no_tanda_terima;
			}
			else
			{
				$last_number = substr($count['no_tanda_terima'], -3);
				$current_number=($last_number*1)+(1*1);
				$no_baru = str_pad($current_number, 3, '0', STR_PAD_LEFT);
				$no_tanda_terima = $now.$no_baru;
				$data['no_tanda_terima'] = $no_tanda_terima;
				$data['supplier'] = $this->g_supplier_model->get_supplier();
				$data['carabayar'] = $this->g_carabayar_model->get_carabayar();
				$data['idsup']=$idsup;
				//echo $no_tanda_terima;
			}
			$this->load->view('g_register_tanda_terima_yunan', $data);
		}
	}

	public function ajaxTypeheadFaktur()
	{
		$faktur = $this->g_pembelian_model->get_pembelian_by_faktur($this->input->post('src_param'));
		$testing3='[';
		foreach ($faktur as $fak):
		$testing3 = $testing3.'{"stateCode": "'.$fak["nomor_faktur"].'", "tgl_faktur": "'.$fak["tgl_faktur"].'", "jatuh_tempo": "'.$fak["jatuh_tempo"].'", "total": "'.$fak["total"].'", "stateDisplay": "'.$fak["nomor_faktur"].'", "stateName": "'.$fak["nomor_faktur"].' | Total: '.$this->formatRupiah($fak["total"]).'"},';	
		endforeach;
		
		$testing3=substr_replace($testing3 ,"",-1);
		$testing3=$testing3.']';
		$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
		
		echo $testing3;

	}

	public function insert_faktur()
	{
		$reg = $this->input->post('reg');
		$nomor_faktur = $this->input->post('nomor_faktur');
		$jatuh_tempo = $this->input->post('jatuh_tempo');
		$total = $this->input->post('total');
		$sp = $this->input->post('sp');
		$fak_pajak = $this->input->post('fak_pajak');
		$sj = $this->input->post('sj');
		$fak_beli = $this->input->post('fak_beli');
		$kwitansi = $this->input->post('kwitansi');
		$verified = $this->input->post('verified');
		$keterangan = $this->input->post('keterangan');
		$cek_nomor_faktur = $this->g_tanda_terima_model->get_pembelian_belum_bayar($nomor_faktur);

		if($cek_nomor_faktur != NULL)
		{
			$sql = "UPDATE t_pembelian set id_tanda_terima=$reg, sp=$sp, faktur_pajak=$fak_pajak, sj=$sj, faktur_beli=$fak_beli, kwitansi=$kwitansi, verified=$verified, keterangan='$keterangan' where nomor_faktur='$nomor_faktur'";
			$this->db->query($sql);
		}

		$data['list'] = $this->g_tanda_terima_model->get_faktur_table_content($reg);

		$this->load->view('t_tabel_faktur', $data);
	}

	public function checkfaktur()
	{
		$nomor_faktur = $this->input->post('nomor_faktur');
		$jumlah = $this->g_tanda_terima_model->cek_faktur($nomor_faktur);
		//echo $jumlah->id_tanda_terima;
		//var_dump($jumlah);
	}

	public function insert_tanda_terima()
	{
		$tgl_tanda_terima = $this->input->post('tgl_tanda_terima');
		$tgl = date('Y-m-d',strtotime($tgl_tanda_terima));
		$no_tanda_terima = $this->input->post('no_tanda_terima');
		$nomor_faktur = $this->input->post('nomor_faktur');
		$supplier = $this->input->post('supplier');
		$total = $this->input->post('total_bayar');
		$kurangbayar = $this->input->post('kurang');
		$carabayar = $this->input->post('carabayar');
		$bayar = $this->input->post('bayar');

		$user = $this->ion_auth->user()->row();
		$operator=$user->username;

		if($kurangbayar > 0)
		{
			$sql = "INSERT INTO t_tanda_terima (`tanggal`, `no_tanda_terima`, `status`, `supplier`, `total`, `kurangbayar`, `operator`) VALUES ('$tgl', '$no_tanda_terima', 1, $supplier, $total, $kurangbayar, '$operator');";
			$this->db->query($sql);
		}
		elseif($kurangbayar == 0)
		{
			$sql = "INSERT INTO t_tanda_terima (`tanggal`, `no_tanda_terima`, `status`, `supplier`, `total`, `kurangbayar`, `operator`) VALUES (NOW(), '$no_tanda_terima', 2, $supplier, $total, $kurangbayar, '$operator');";
			$this->db->query($sql);
		}

		$last = $this->g_tanda_terima_model->get_last_tanda_terima();
		$id_last = $last[0]['id'];

		$sql1 = "INSERT INTO t_pembayaran_tanda_terima (`tanggal`, `id_tanda_terima`, `carabayar`, `jumlah`, `operator`) VALUES (NOW(), $id_last, $carabayar, $bayar, '$operator');";
		$this->db->query($sql1);

		$sql = "UPDATE t_pembelian set id_tanda_terima = $id_last, status = 3 WHERE id_tanda_terima = 0";
		$this->db->query($sql);

		redirect('g_tanda_terima_yunan/view_tanda_terima/1');
    }

	public function edit_tanda_terima($id_tanda_terima)
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$data['supplier'] = $this->g_supplier_model->get_supplier();

			$data['tanda_terima'] = $this->g_tanda_terima_model->get_tanda_terima($id_tanda_terima);
			
			$data['pembayaran'] = $this->g_tanda_terima_model->get_pembayaran($id_tanda_terima);
			$data['carabayar'] = $this->g_carabayar_model->get_carabayar();
			
			$this->load->view('g_edit_tanda_terima', $data);
		
		}
	}

	public function edit_faktur()
	{
		$reg = 0;
		$id_tanda_terima = $this->input->post('id_tanda_terima');
		$id_pembelian = $this->input->post('id_pembelian');
		$sp = $this->input->post('sp');
		$sj = $this->input->post('sj');
		$faktur_pajak = $this->input->post('faktur_pajak');
		$faktur_beli = $this->input->post('faktur_beli');
		$verified = $this->input->post('verified');
		$keterangan = $this->input->post('keterangan');

		$user = $this->ion_auth->user()->row();
		$operator=$user->username;

		$sql = "UPDATE t_pembelian set `sp` = $sp, `sj` = $sj, `faktur_pajak` = $faktur_pajak, `faktur_beli` = $faktur_beli, `verified` = $verified, `keterangan` = '$keterangan' WHERE id_pembelian = $id_pembelian";
		$this->db->query($sql);

		$data['list'] = $this->g_tanda_terima_model->get_faktur_table_content($id_tanda_terima);

		$this->load->view('t_tabel_faktur', $data);
		//redirect('g_tanda_terima/show_table/'.$id_tanda_terima);
	}

	public function update_tanda_terima(){
 
		$tgl_tanda_terima = $this->input->post('tgl_tanda_terima');
		$tgl = date('Y-m-d', strtotime($tgl_tanda_terima));
		$no_tanda_terima = $this->input->post('no_tanda_terima');
		$nomor_faktur = $this->input->post('nomor_faktur');
		$supplier = $this->input->post('supplier');
		$total = $this->input->post('total_bayar');
		$kurangbayar = $this->input->post('kurang');
		$carabayar = $this->input->post('carabayar');
		$bayar = $this->input->post('bayar');
		$id_tanda_terima = $this->input->post('id_tanda_terima');
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;

		if($kurangbayar > 0)
		{
			$sql = "UPDATE t_tanda_terima set tanggal='$tgl', no_tanda_terima='$no_tanda_terima', status=1, supplier=$supplier, kurangbayar=$kurangbayar, operator='$operator' WHERE id=$id_tanda_terima";
			$this->db->query($sql);
		}
		elseif($kurangbayar == 0)
		{
			$sql = "UPDATE t_tanda_terima set tanggal='$tgl', no_tanda_terima='$no_tanda_terima', status=2, supplier=$supplier, kurangbayar=$kurangbayar, operator='$operator' WHERE id=$id_tanda_terima";
			$this->db->query($sql);
		}

		$sql1 = "INSERT INTO t_pembayaran_tanda_terima (`tanggal`, `id_tanda_terima`, `carabayar`, `jumlah`, `operator`) VALUES (NOW(), $id_tanda_terima, $carabayar, $bayar, '$operator');";
		$this->db->query($sql1);

		$this->view_tanda_terima(2);
    }

    public function delete_faktur()
    {
    	$nomor_faktur = $this->input->post('nomor_faktur');
    	$tgl_faktur = $this->input->post('tgl_faktur');
    	$total = $this->input->post('total');

    	$sql = "UPDATE t_pembelian set id_tanda_terima = NULL, sp = NULL, faktur_pajak = NULL, sj = NULL, faktur_beli = NULL, kwitansi = NULL, verified = NULL, keterangan = NULL where nomor_faktur='$nomor_faktur'";
    	$this->db->query($sql);

    	$data['list'] = $this->g_tanda_terima_model->get_faktur_table_content('0');

		$this->load->view('t_tabel_faktur', $data);
    }
	
	public function delete_tanda_terima($id_tanda_terima)
	{
		$pembelian = $this->g_tanda_terima_model->get_tanda_terima($id_tanda_terima);
		foreach($pembelian as $p):
		{
			$sql = "UPDATE t_pembelian set `id_tanda_terima` = null, `status` = 0, `sp` = null, `sj` = null, `faktur_beli` = null, `faktur_pajak` = null, `kwitansi` = null, `keterangan` = null WHERE id_tanda_terima = $id_tanda_terima";
			$this->db->query($sql);
		}
		endforeach;

		$sql = "DELETE FROM t_tanda_terima where id = $id_tanda_terima";
		$this->db->query($sql);

		$sql = "DELETE FROM t_pembayaran_tanda_terima where id_tanda_terima = $id_tanda_terima";
		$this->db->query($sql);

		$this->view_tanda_terima(3);
	}
	
	public function show_table()
	{

		$id_tanda_terima = $this->input->post('id_tanda_terima');
		$data['list'] = $this->g_tanda_terima_model->get_faktur_table_content($id_tanda_terima);
        $this->load->view('t_tabel_faktur', $data);
	}
	
	public function show_detail_item()
	{
		$reg = $this->input->post('reg');
		$data['list'] = $this->g_pembelian_model->get_detail($reg);
        $this->load->view('t_tabel_item_detail', $data);
	}

	public function check_tanda_terima()
	{
		$reg = $this->input->post('reg');
		$jumlah = $this->g_tanda_terima_model->get_jumlah_tanda_terima($reg);
		echo $jumlah['jumlah'];
	}

	public function cetak_tanda_terima($id_tanda_terima = 0)
	{
		$data['tanda_terima'] = $this->g_tanda_terima_model->get_tanda_terima($id_tanda_terima);
		$data['pembayaran'] = $this->g_tanda_terima_model->get_pembayaran($id_tanda_terima);
		$data['list'] = $this->g_tanda_terima_model->get_faktur_table_content($id_tanda_terima);

		// echo "<pre>";
		// var_dump($data);
		// echo "</pre>";

		$this->load->view('g_cetak_tanda_terima', $data);
	}
	
	public function formatRupiah($nilaiUang)
	{
	  $nilaiRupiah 	= "";
	  $jumlahAngka 	= strlen($nilaiUang);
	  while($jumlahAngka > 3)
	  {
		$nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
		$sisaNilai = strlen($nilaiUang) - 3;
		$nilaiUang = substr($nilaiUang,0,$sisaNilai);
		$jumlahAngka = strlen($nilaiUang);
	  }
	 
	  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
	  return $nilaiRupiah;
	}
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
