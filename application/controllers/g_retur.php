<?php if (!defined('BASEPATH')) die();
class G_retur extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_retur_model');
		$this->load->model('g_item_model');
		$this->load->model('g_ruangan_model');
		$this->load->model('g_stock_model');
	}
	
   public function index()
	{
	  /*
	  $this->load->view('include/header');
	  $this->load->view('templates/menubar');
      $this->load->view('view_transaksi');
      $this->load->view('include/footer');
	  */
	  $this->view_retur();
	}
	
	public function view_retur($status=0, $id_retur=FALSE )
	{
		$data['retur'] = $this->g_retur_model->get_retur($id_retur);
		$data['status']=$status;
		
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('g_view_gudang_retur', $data);
	}
	
	public function view_all_transaksi()
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi2(0);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_transaksi', $data);
	}
	
	public function checkfaktur()
	{
		$reg = $this->input->post('reg');
		$jumlah = $this->g_retur_model->get_jumlah_faktur($reg);
		echo $jumlah['jumlah'];
	}
	
	
	public function delete_retur($reg)
	{
		$detail_retur = $this->g_retur_model->get_detail($reg);
		
		foreach($detail_retur as $item_detail):
			
			$ruangan=$item_detail['ruangan'];
			
			$count=$this->g_stock_model->check_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			}
			
			$sql="update t_item set stock_gudang=stock_gudang-".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir+".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir+".$item_detail['jumlah'].", stock_awal=stock_awal+".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$count2=$this->g_stock_model->check_stockharian2($item_detail['item'], $item_detail['tgl_masuk'] );
			
			if($count2['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2($item_detail['item'], $item_detail['tgl_masuk']);
			}
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$item_detail['jumlah'].", stock_awal=stock_awal-".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
		endforeach;
		
		$sql="delete from t_retur where id_retur=$reg";
		$this->db->query($sql);
		
		$sql="delete from t_item_retur where retur=$reg";
		$this->db->query($sql);
		
		$this->view_retur(3);
	}
	
	public function insert_item(){
 
        $data['title'] = 'Lorem ipsum';
        //$data['list'] = $this->test_model->get_data();
		
		$reg = $this->input->post('reg');
		$id_item = $this->input->post('id_item');
		$jumlah = $this->input->post('jumlah');
		$satuan = $this->input->post('satuan');
		$tgl_retur = $this->input->post('tgl_retur');
		
		$ruangan = $this->input->post('ruangan');
		
		$sql="INSERT INTO `t_item_retur` (`retur`, `item`, tgl_masuk, `jumlah`, `satuan`) VALUES ($reg, $id_item, STR_TO_DATE('$tgl_retur', '%d-%m-%Y'), $jumlah,'$satuan');";
		$this->db->query($sql);
		
		if($reg==0 || $reg=="0")
		{
			
		}
		else
		{
			$sql="update t_item set stock_gudang=stock_gudang+".$jumlah." where id_item=".$id_item;
			$this->db->query($sql);
			
			$count=$this->g_stock_model->check_stockharian3_ruangan($id_item, $tgl_retur, $ruangan);
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian3_ruangan($id_item, $tgl_retur, $ruangan);
			}
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir-".$jumlah." where item=".$id_item." AND ruangan=$ruangan AND DATE(tgl_stock)=STR_TO_DATE('$tgl_retur', '%d-%m-%Y');";	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir-".$jumlah.", stock_awal=stock_awal-".$jumlah." where item=".$id_item." AND ruangan=$ruangan AND DATE(tgl_stock)>STR_TO_DATE('$tgl_retur', '%d-%m-%Y');";	
			$this->db->query($sql);
			
			$count2=$this->g_stock_model->check_stockharian3($id_item, $tgl_retur );
			
			if($count2['ada']=='0')
			{
				$this->g_stock_model->create_stockharian3($item_detail['item'], $item_detail['tgl_masuk']);
			}
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$jumlah." where item=".$id_item." AND DATE(tgl_stock)=STR_TO_DATE('$tgl_retur', '%d-%m-%Y');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$jumlah.", stock_awal=stock_awal+".$jumlah." where item=".$id_item." AND DATE(tgl_stock)>STR_TO_DATE('$tgl_retur', '%d-%m-%Y');";	
			$this->db->query($sql);
			
		}
		
		$data['list'] = $this->g_retur_model->get_detail($reg);
		
        $this->load->view('t_tabel_item_retur', $data);
		
    }
	
	public function show_table()
	{
		$reg = $this->input->post('reg');
		$data['list'] = $this->g_retur_model->get_detail($reg);
        $this->load->view('t_tabel_item_retur', $data);
	}
	
	public function show_detail_item()
	{
		
		$reg = $this->input->post('reg');
		$data['list'] = $this->g_retur_model->get_detail($reg);
        $this->load->view('t_tabel_item_retur_detail', $data);
	}
	
	public function delete_item(){
        //$data['title'] = 'Lorem ipsum';
        //$data['list'] = $this->test_model->get_data();
		
		$reg = $this->input->post('reg');
		
		$retur = $this->input->post('retur');
		
		if($retur==0 || $retur=="0")
		{
			
		}
		else
		{
			$item_detail=$this->g_retur_model->get_detail_itemretur($reg);
			
			$ruangan=$item_detail['ruangan'];
			
			$count=$this->g_stock_model->check_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			}
			
			$sql="update t_item set stock_gudang=stock_gudang-".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir+".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir+".$item_detail['jumlah'].", stock_awal=stock_awal+".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$count2=$this->g_stock_model->check_stockharian2($item_detail['item'], $item_detail['tgl_masuk'] );
			
			if($count2['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2($item_detail['item'], $item_detail['tgl_masuk']);
			}
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$item_detail['jumlah'].", stock_awal=stock_awal-".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
		}
		
		$sql="delete from `t_item_retur` where `id_item_retur`=$reg;";
		$this->db->query($sql);
		
		$data['list'] = $this->g_retur_model->get_detail($retur);
        $this->load->view('t_tabel_item_retur', $data);
 
    }
	
	
	public function create_retur()
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$sql="delete from t_item_retur where retur=0;";
			$this->db->query($sql);
		  
			$data['ruangan'] = $this->g_ruangan_model->get_ruangan();
		  
			//$item = $this->g_item_model->get_item();
			
			$item = $this->g_item_model->get_item_ruangan(FALSE, 1);
			
			$testing3='';
			foreach ($item as $pasien_item):
			$stock_gudang_all=$pasien_item["stock_akhir"];
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "stock_gudang": "'.$stock_gudang_all.'", "satuanbeli": "'.$pasien_item["satuan_beli"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$stock_gudang_all.'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;
			
			$last = $this->g_retur_model->get_last_retur_hari_ini();
			
			if($last==NULL)
			  {
				$date=date("md");
				$year=date("Y");
				$data['nomor_baru']= 'R'.substr($year, -2).''.$date.'001';
			  }
			  else
			  {
				  $last_number=substr($last['nomor_retur'], -3);
				
				  $current_number=($last_number*1)+(1*1);
				  
				  
				  $date=date("md");
				  $year=date("Y");
				  
				  $nomor_baru= str_pad($current_number, 3, '0', STR_PAD_LEFT);
				  $data['nomor_baru']= 'R'.substr($year, -2).''.$date.''.$nomor_baru;
				  
			  }
		  
			$this->load->view('g_register_retur', $data);
		}
	}
	
	public function edit_retur($inputReg)
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$data['ruangan'] = $this->g_ruangan_model->get_ruangan();
		  
			//$item = $this->g_item_model->get_item();
			$item = $this->g_item_model->get_item_ruangan(FALSE, 1);
		  
			$testing3='';
			foreach ($item as $pasien_item):
			$stock_gudang_all=$pasien_item["stock_akhir"];
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "stock_gudang": "'.$stock_gudang_all.'", "satuanbeli": "'.$pasien_item["satuan_beli"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$stock_gudang_all.'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;
		  

			$data['retur'] = $this->g_retur_model->get_retur($inputReg);
			
			$this->load->view('g_edit_retur', $data);
		
		}
	}
	
	public function insert_retur(){
 
		$nomorretur = $this->input->post('nomorretur');
		$tgl_retur = $this->input->post('tgl_retur');
		
		$ruangan = $this->input->post('ruangan');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		
		$sql="INSERT INTO `t_retur` (`nomor_retur`, `tgl_isi`, `ruangan`, `tgl_retur`, `operator`) VALUES ('$nomorretur', NOW(), $ruangan, STR_TO_DATE('$tgl_retur', '%d-%m-%Y'), '$operator');";
		$this->db->query($sql);
		
		$last_retur = $this->g_retur_model->get_last_retur($nomorretur);
		
		$sql="update t_item_retur set retur=".$last_retur['id_retur'].", tgl_masuk=STR_TO_DATE('$tgl_retur', '%d-%m-%Y') where retur=0";
		$this->db->query($sql);
		
		$detail_retur = $this->g_retur_model->get_detail($last_retur['id_retur']);
		
		foreach($detail_retur as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			}
			
			$sql="update t_item set stock_gudang=stock_gudang+".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir-".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir-".$item_detail['jumlah'].", stock_awal=stock_awal-".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$count2=$this->g_stock_model->check_stockharian2($item_detail['item'], $item_detail['tgl_masuk'] );
			
			if($count2['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2($item_detail['item'], $item_detail['tgl_masuk']);
			}
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$item_detail['jumlah'].", stock_awal=stock_awal+".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			
		endforeach;
		
		
		redirect('g_retur/view_detail/'.$last_retur['id_retur']);
    }
	
	public function update_retur(){
 
		$inputReg = $this->input->post('id_retur');
		$nomorretur = $this->input->post('nomorretur');
		$tgl_retur = $this->input->post('tgl_retur');
		
		$ruangan = $this->input->post('ruangan');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		
		$sql="UPDATE `t_retur` set `nomor_retur`='$nomorretur',`ruangan`=$ruangan, `tgl_retur`=STR_TO_DATE('$tgl_retur', '%d-%m-%Y'), `operator`='$operator' where id_retur=$inputReg ;";
		$this->db->query($sql);
		
		redirect('g_retur/view_detail/'.$inputReg);
		
    }

	public function view_detail($inputReg=0){
		
		$data['retur'] = $this->g_retur_model->get_retur($inputReg);
		
		$this->load->view('g_detail_retur', $data);
    }
	
	public function view_detail2($inputReg=0){
		
		$data['retur'] = $this->g_retur_model->get_retur($inputReg);
		$data['list'] = $this->g_retur_model->get_detail($inputReg);
		$this->load->view('g_cetak_laporan_retur', $data);
    }
	
	public function receipt($inputReg=0){
		
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran($inputReg);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function cetak_pembayaran($inputReg=0, $tanggal){
		$tanggal=urldecode($tanggal);
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran_kwitansi($inputReg, $tanggal);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function laporan_harian($tanggal=FALSE){
		
		$transaksihariini = $this->transaksi_model->get_transaksi2($tanggal);

		$detail=array();
		$pembayaran=array();
		foreach ($transaksihariini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg);
		endforeach;
		$data['transaksihariini']=$transaksihariini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		if($tanggal === FALSE)
		{
			$tanggal=date('j-m-Y');
		}
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function laporan_harian_baru($tanggal=FALSE){
		
		
		if($this->input->post('mulai')!=NULL)
		{
			$tanggal=$this->input->post('mulai');
		}
		else if($tanggal === FALSE)
		{
			$tanggal=date('Y-m-j');
		}
		$tanggal =date('Y-m-j', strtotime($tanggal));
		
		$pembayaran_hari_ini=$this->transaksi_model->get_pembayaran(FALSE, $tanggal);
		
		$detail=array();
		$pembayaran=array();
		foreach ($pembayaran_hari_ini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg, $tanggal);
		endforeach;
		$data['transaksihariini']=$pembayaran_hari_ini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function cetak_kwitansi($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('create_transaksi_success', $data);
    }
	
	public function cetak_kwitansi_besar(){
		$data['kwb_nomorkwitansi'] = $this->input->post('kwb_nomorkwitansi');
		$data['kwb_dari'] = $this->input->post('kwb_dari');
		$data['kwb_jumlah'] = $this->input->post('kwb_jumlah');
		$data['kwb_keperluan'] = $this->input->post('kwb_keperluan');
		$data['kwb_nominal'] = $this->input->post('kwb_nominal');
		$data['kwb_tanggal'] = $this->input->post('kwb_tanggal');
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	public function cetak_kwitansi_besar2($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	function formatRupiah($nilaiUang)
	{
	  $nilaiRupiah 	= "";
	  $jumlahAngka 	= strlen($nilaiUang);
	  while($jumlahAngka > 3)
	  {
		$nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
		$sisaNilai = strlen($nilaiUang) - 3;
		$nilaiUang = substr($nilaiUang,0,$sisaNilai);
		$jumlahAngka = strlen($nilaiUang);
	  }
	 
	  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
	  return $nilaiRupiah;
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
