<?php if (!defined('BASEPATH')) die();
class Pasien extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('pasien_model');
	}
	
   public function index()
	{
		$this->view_pasien(0);
	}
	
	public function view_pasien($status=0)
	{
		$data['pasien'] = $this->pasien_model->get_pasien();
		$data['status']=$status;
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_pasien', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_pasien', $data);
	}
	
	public function cek_ada()
	{
		$id = $this->input->post('input');
		$jumlah = $this->pasien_model->get_jumlah_pasien($id);
		echo $jumlah['jumlah'];
	}
	
	public function pasien_baru($dari=0)
	{
		$data['dari']=$dari;
		$kota = $this->pasien_model->get_kota();
		$data['perusahaan'] = $this->pasien_model->get_perusahaan();
		$testing2='';
	foreach ($kota as $pasien_item):
	
	$testing2 = $testing2.'{"stateCode": "'.$pasien_item["lokasi_ID"].'", "stateDisplay": "'.$pasien_item["lokasi_nama"].'", "stateName": "'.$pasien_item["lokasi_nama"].'"},';
	endforeach;
	
	$testing2=substr_replace($testing2 ,"",-1);
	$testing2=trim(preg_replace('/\s+/', ' ', $testing2));
	$data['testing2']=$testing2;
	/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('create_pasien', $data);
		$this->load->view('include/footer');
	*/
	
	$this->load->view('register_pasien', $data);
	
	}
	
	public function create_pasien()
	{
      $inputNama = $this->input->post('inputNama');
	  if($inputNama==NULL)
	  {
		show_404();
	  }
	  else
	  {
		$dari = $this->input->post('dari');
		
		$initial = $this->input->post('initial');
		$inputNama = $this->input->post('inputNama');
		$umur = $this->input->post('umur');
		$jeniskelamin = $this->input->post('jeniskelamin');
		
		$sql="INSERT INTO `pasien` (`nama_pasien`, `umur`, `jenis_kelamin`, `initial`)
		VALUES (
		'".$inputNama."', ".$umur.", ".$jeniskelamin.",  ".$initial.");";
		$this->db->query($sql);
		if($dari==1)
		{
			header( 'Location: '.site_url("g_pemakaian/create_pemakaian/".$id_pasien."").'' );
		}
		else
		$this->view_pasien(1);
	  }
	}  
	  
	public function delete_pasien($id_pasien=FALSE)
	{
      
	  if($id_pasien === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$sql="delete from pasien where id_pasien=".$id_pasien;
		$this->db->query($sql);
		$this->view_pasien(3);
	  }
	  
	}
	
	public function edit_pasien($id_pasien=FALSE)
	{
      
	  if($id_pasien === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$kota = $this->pasien_model->get_kota();
		$data['perusahaan'] = $this->pasien_model->get_perusahaan();
		$testing2='';
	foreach ($kota as $pasien_item):
	
	$testing2 = $testing2.'{"stateCode": "'.$pasien_item["lokasi_ID"].'", "stateDisplay": "'.$pasien_item["lokasi_nama"].'", "stateName": "'.$pasien_item["lokasi_nama"].'"},';
	endforeach;
	
	
	
	$testing2=substr_replace($testing2 ,"",-1);
	$testing2=trim(preg_replace('/\s+/', ' ', $testing2));
	$data['testing2']=$testing2;
	
		
		$data['pasien'] = $this->pasien_model->get_pasien($id_pasien);
		
		$this->load->view('edit_register_pasien', $data);
	  }
	  
	}
	
	public function update_pasien()
	{
	  
		$inputID_id = $this->input->post('inputID_id');
		
		
		$initial = $this->input->post('initial');
		$inputNama = $this->input->post('inputNama');
		
		$umur = $this->input->post('umur');
		
		$jeniskelamin = $this->input->post('jeniskelamin');
		
		
		$sql="update `pasien` set `nama_pasien`='".$inputNama."', `umur`=".$umur.", `jenis_kelamin`=".$jeniskelamin.", `initial`=".$initial." where `id_pasien`='".$inputID_id."'";
		$this->db->query($sql);
		
		$this->view_pasien(2);
	  
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
