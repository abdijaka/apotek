<?php if (!defined('BASEPATH')) die();
class G_item extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_item_model');
		$this->load->model('g_supplier_model');
		$this->load->model('g_kategori_model');
		$this->load->model('g_stock_model');
	}
	
   public function index()
	{
		$this->view_item(0);
	}
	
	public function view_item($status=0)
	{
		$data['item'] = $this->g_item_model->get_item();
		$data['status']=$status;
		
		$this->load->view('g_view_gudang_item', $data);
	}
	
	public function item_baru()
	{
		$data['kategori'] = $this->g_kategori_model->get_kategori();
		$data['supplier'] = $this->g_supplier_model->get_supplier();
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('create_item');
		$this->load->view('include/footer');
		*/
		$this->load->view('g_register_item', $data);
	}
	
	public function create_item()
	{
		$generik=0;
		/*
		$generik = $this->input->post('generik');
		if($generik==NULL)
		$generik=0;
		*/
		
		$inputNama = $this->input->post('inputNama');
		
		$inputKode = '1';
		/*
		$inputKode = $this->input->post('inputKode');
		*/
		
		$inputMerk = $this->input->post('inputMerk');
		
		$kategori = 0;
		$supplier = 0;
		/*
		$kategori = $this->input->post('kategori');
		$supplier = $this->input->post('supplier');
		*/
		$inputPersenMargin = $this->input->post('inputPersenMargin');
		$inputHargaBeli = $this->input->post('inputHargaBeli');
		$inputHargaJual = $this->input->post('inputHargaJual');
		$inputSatuanJual = $this->input->post('inputSatuanJual');
		$inputSatuanBeli = $this->input->post('inputSatuanJual');
		//$sb_ke_sj = $this->input->post('sb_ke_sj');
		$sb_ke_sj = 1;
		$inputStockAwal = $this->input->post('inputStockAwal');
		$inputStockAwalPhc = $this->input->post('inputStockAwalPhc');
		$inputStockMin = $this->input->post('inputStockMin');
		$lokasi = $this->input->post('lokasi');
		$keterangan = $this->input->post('keterangan');
		
		$sql="INSERT INTO  `t_item` (
			`nama_item` ,
			`stock_awal` ,
			`stock_min` ,
			`persen_margin` ,
			`harga_beli` ,
			`harga_jual` ,
			`satuan_beli` ,
			`satuan_jual` ,
			`jumlah_sb_ke_sj` ,
			`kategori` ,
			`lokasi` ,
			`keterangan` ,
			`supplier` ,
			`isgenerik` ,
			`kode`,
			`merk`,
			`stock_awal_phc`,
			tgl_buat
			)
		VALUES (
		'".$inputNama."',".$inputStockAwal.",".$inputStockMin.",".$inputPersenMargin.",'".$inputHargaBeli."','".$inputHargaJual."','".$inputSatuanBeli."','".$inputSatuanJual."',".$sb_ke_sj.",".$kategori.",'".$lokasi."','".$keterangan."',".$supplier.",".$generik.",'".$inputKode."','".$inputMerk."', ".$inputStockAwalPhc.", NOW());";
		$this->db->query($sql);
		
		/*
		$last_item=$this->g_item_model->get_last_item($inputNama);
		$count=$this->g_stock_model->check_stockharian($last_item['id_item']);
		if($count['ada']==0)
		{
			$this->g_stock_model->create_stockharian($last_item['id_item']);
		}
		*/
		
		$item=$this->g_item_model->get_item();
		foreach($item as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian($item_detail['id_item']);
			if($count['ada']==0)
			{
				$this->g_stock_model->create_stockharian($item_detail['id_item']);
			}
			
		endforeach;
		
		redirect('g_item/view_item/1');
		//$this->view_item(1);
	  
	}  
	  
	public function delete_item($id_item=FALSE)
	{
		  if($id_item === FALSE)
		  {
			show_404();
		  }
		  else
		  {
			$sql="delete from t_item where id_item=".$id_item;
			$this->db->query($sql);
			
			$sql="delete from t_item_beli where item=".$id_item;
			$this->db->query($sql);
			
			$sql="delete from t_stockharian where item=".$id_item;
			$this->db->query($sql);
			
			$sql="delete from t_itemkeluar where item=".$id_item;
			$this->db->query($sql);
			
			$sql="delete from t_distribusi where item=".$id_item;
			$this->db->query($sql);
			
			//$this->view_item(3);
			redirect('g_item/view_item/3');
		  }
	  
	}
	
	public function edit_item($id_item=FALSE)
	{
      
	  if($id_item === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$data['item'] = $this->g_item_model->get_item($id_item);
		$data['kategori'] = $this->g_kategori_model->get_kategori();
		$data['supplier'] = $this->g_supplier_model->get_supplier();
		
		$this->load->view('g_edit_item', $data);
	  }
	  
	}
	
	public function update_item()
	{
      
		$id_item = $this->input->post('inputID');
		
		$generik=0;
		/*
		$generik = $this->input->post('generik');
		if($generik==NULL)
		$generik=0;
		*/
		
		$inputNama = $this->input->post('inputNama');
		$inputMerk = $this->input->post('inputMerk');
		
		$inputKode = '1';
		/*
		$inputKode = $this->input->post('inputKode');
		*/
		
		$kategori = 0;
		$supplier = 0;
		/*
		$kategori = $this->input->post('kategori');
		$supplier = $this->input->post('supplier');
		*/

		$inputPersenMargin = $this->input->post('inputPersenMargin');
		$inputHargaBeli = $this->input->post('inputHargaBeli');
		$inputHargaJual = $this->input->post('inputHargaJual');
		$inputSatuanJual = $this->input->post('inputSatuanJual');
		$inputSatuanBeli = $this->input->post('inputSatuanJual');
		//$sb_ke_sj = $this->input->post('sb_ke_sj');
		$sb_ke_sj = 1;
		$inputStockAwal = $this->input->post('inputStockAwal');
		$inputStockAwalPhc = $this->input->post('inputStockAwalPhc');
		$inputStockMin = $this->input->post('inputStockMin');
		$lokasi = $this->input->post('lokasi');
		$keterangan = $this->input->post('keterangan');
		
		$sql="UPDATE `t_item` set
			`nama_item`='".$inputNama."' ,
			`stock_awal`=".$inputStockAwal." ,
			`stock_awal_phc`=".$inputStockAwalPhc." ,
			`stock_min`=".$inputStockMin." ,
			`persen_margin`=".$inputPersenMargin.",
			`harga_beli`='".$inputHargaBeli."' ,
			`harga_jual`='".$inputHargaJual."' ,
			`satuan_beli`='".$inputSatuanBeli."' ,
			`satuan_jual`='".$inputSatuanJual."' ,
			`jumlah_sb_ke_sj`=".$sb_ke_sj." ,
			`kategori`=".$kategori." ,
			`lokasi`='".$lokasi."' ,
			`keterangan`='".$keterangan."' ,
			`supplier`=".$supplier." ,
			`isgenerik`=".$generik." ,
			`kode`='".$inputKode."',
			`merk`='".$inputMerk."'
			WHERE id_item=".$id_item.";";
		
		
		$this->db->query($sql);
		
		//$this->view_item(2);
		redirect('g_item/view_item/2');
	  
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
