<?php if (!defined('BASEPATH')) die();
class G_laporan extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_pembelian_model');
		$this->load->model('g_distribusi_model');
		$this->load->model('g_pemakaian_model');
		$this->load->model('g_penjualan_model');
		$this->load->model('g_retur_model');
		$this->load->model('g_retursupp_model');
		$this->load->model('g_item_model');
		$this->load->model('g_stock_model');
		$this->load->model('g_itemkeluar_model');
	}
	
   public function index()
	{
		$this->view_laporan(0);
	}
	
	public function view_laporan($status=0)
	{
		$data['laporan'] = $this->g_laporan_model->get_laporan();
		$data['status']=$status;
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_laporan', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('g_view_gudang_laporan', $data);
	}
	
	public function laporan_item($id_item=1){
		
		$data['detail'] = $this->g_pembelian_model->get_detail_peritem($id_item);
		$data['detail2'] = $this->g_itemkeluar_model->get_itemkeluar_item($id_item);	
		$this->load->view('g_view_laporan_item', $data);
    }
	
	public function laporan_pembelian_item($id_item=1){
		
		$data['detail'] = $this->g_pembelian_model->get_detail_peritem($id_item);		
		$this->load->view('g_view_laporan_pembelian_item', $data);
    }
	
	public function laporan_itemkeluar_item($id_item=3){
		
		$data['detail'] = $this->g_itemkeluar_model->get_itemkeluar_item($id_item);
		
		$this->load->view('g_view_laporan_itemkeluar_item', $data);
    }
	
	public function laporan_harian($tgl = FALSE){
		
		$data['pembelian'] = $this->g_pembelian_model->get_pembelian_harian($tgl);
		$data['distribusi'] = $this->g_distribusi_model->get_distribusi_harian($tgl);
		$data['penjualan'] = $this->g_penjualan_model->get_penjualan_harian($tgl);
		
		$this->load->view('g_view_laporan_harian', $data);
    }
	public function cetak_laporan_harian($tgl = FALSE){
		
		$data['pembelian'] = $this->g_pembelian_model->get_pembelian_harian($tgl);
		$data['distribusi'] = $this->g_distribusi_model->get_distribusi_harian($tgl);
		$data['penjualan'] = $this->g_penjualan_model->get_penjualan_harian($tgl);
		
		$this->load->view('g_cetak_laporan_harian', $data);
    }
	
	public function laporan_bulanan($bulan = FALSE){
		
		$data['pembelian'] = $this->g_pembelian_model->get_pembelian_bulanan($bulan);
		//$data['distribusi'] = $this->g_distribusi_model->get_distribusi_harian($bulan);
		$data['penjualan'] = $this->g_penjualan_model->get_penjualan_bulanan($bulan);
		
		$this->load->view('g_view_laporan_bulanan', $data);
    }
	public function cetak_laporan_bulanan($bulan = FALSE){
		
		$data['pembelian'] = $this->g_pembelian_model->get_pembelian_bulanan($bulan);
		//$data['distribusi'] = $this->g_distribusi_model->get_distribusi_harian($bulan);
		$data['penjualan'] = $this->g_penjualan_model->get_penjualan_bulanan($bulan);
		
		$this->load->view('g_cetak_laporan_bulanan', $data);
    }
	
	
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
