<?php if (!defined('BASEPATH')) die();
class G_ruangan extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_ruangan_model');
	}
	
   public function index()
	{
		$this->view_ruangan(0);
	}
	
	public function view_ruangan($status=0)
	{
		$data['ruangan'] = $this->g_ruangan_model->get_ruangan();
		$data['status']=$status;
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_ruangan', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('g_view_gudang_ruangan', $data);
	}
	
	public function ruangan_baru()
	{
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('create_ruangan');
		$this->load->view('include/footer');
		*/
		$this->load->view('g_register_ruangan');
	}
	
	public function create_ruangan()
	{
		$inputNama = $this->input->post('inputNama');
		$keterangan = $this->input->post('keterangan');
		
		$sql="INSERT INTO  `t_ruangan` (`nama_ruangan` ,`keterangan`)
		VALUES (
		'".$inputNama."','".$keterangan."');";
		$this->db->query($sql);
		
		$this->view_ruangan(1);
	  
	}  
	  
	public function delete_ruangan($id_ruangan=FALSE)
	{
      
	  if($id_ruangan === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$sql="delete from t_ruangan where id_ruangan=".$id_ruangan;
		$this->db->query($sql);
		$this->view_ruangan(3);
	  }
	  
	}
	
	public function edit_ruangan($id_ruangan=FALSE)
	{
      
	  if($id_ruangan === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$data['ruangan'] = $this->g_ruangan_model->get_ruangan($id_ruangan);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('edit_ruangan', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('g_edit_ruangan', $data);
	  }
	  
	}
	
	public function update_ruangan()
	{
      
		$id_ruangan = $this->input->post('inputID');
		
		$inputNama = $this->input->post('inputNama');
		$keterangan = $this->input->post('keterangan');
		
		$sql="UPDATE `t_ruangan` set `nama_ruangan`='".$inputNama."',`keterangan`='".$keterangan."' WHERE id_ruangan=".$id_ruangan.";";
		
		$this->db->query($sql);
		
		$this->view_ruangan(2);
	  
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
