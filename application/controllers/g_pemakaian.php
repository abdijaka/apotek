<?php if (!defined('BASEPATH')) die();
class G_pemakaian extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_pemakaian_model');
		$this->load->model('g_item_model');
		$this->load->model('g_ruangan_model');
		$this->load->model('pasien_model');
		$this->load->model('dokter_model');
		$this->load->model('g_stock_model');
		$this->load->model('g_tindakan_model');
	}
	
   public function index()
	{
	  /*
	  $this->load->view('include/header');
	  $this->load->view('templates/menubar');
      $this->load->view('view_transaksi');
      $this->load->view('include/footer');
	  */
	  $this->view_pemakaian();
	}
	
	public function view_pemakaian($status=0, $id_pemakaian=FALSE )
	{
		$data['pemakaian'] = $this->g_pemakaian_model->get_pemakaian($id_pemakaian);
		$data['status']=$status;
		
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('g_view_gudang_pemakaian', $data);
	}
	
	public function view_all_transaksi()
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi2(0);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_transaksi', $data);
	}
	
	public function checkfaktur()
	{
		$reg = $this->input->post('reg');
		$jumlah = $this->g_pemakaian_model->get_jumlah_faktur($reg);
		echo $jumlah['jumlah'];
	}
	
	
	public function delete_pemakaian($reg)
	{
		$detail_pemakaian = $this->g_pemakaian_model->get_detail($reg);
		
		foreach($detail_pemakaian as $item_detail):
			
			$ruangan=$item_detail['ruangan'];
			
			$count=$this->g_stock_model->check_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			}
			
			$sql="update t_item set stock_sisa=stock_sisa+".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir+".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir+".$item_detail['jumlah'].", stock_awal=stock_awal+".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
		endforeach;
		
		$sql="delete from t_pemakaian where id_pemakaian=$reg";
		$this->db->query($sql);
		
		$sql="delete from t_item_pakai where pemakaian=$reg";
		$this->db->query($sql);
		
		$this->view_pemakaian(3);
	}
	
	public function insert_item(){
 
		
		$reg = $this->input->post('reg');
		$id_item = $this->input->post('id_item');
		$jumlah = $this->input->post('jumlah');
		$satuan = $this->input->post('satuan');
		$tgl_pemakaian = $this->input->post('tgl_pemakaian');
		
		$ruangan = $this->input->post('ruangan');
		
		$sql="INSERT INTO `t_item_pakai` (`pemakaian`, `item`, tgl_masuk, `jumlah`, `satuan`) VALUES ($reg, $id_item, STR_TO_DATE('$tgl_pemakaian', '%d-%m-%Y'), $jumlah,'$satuan');";
		$this->db->query($sql);
		
		if($reg==0 || $reg=="0")
		{
			
		}
		else
		{
			$sql="update t_item set stock_sisa=stock_sisa-".$jumlah." where id_item=".$id_item;
			$this->db->query($sql);
			
			$count=$this->g_stock_model->check_stockharian3_ruangan($id_item, $tgl_pemakaian, $ruangan);
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian3_ruangan($id_item, $tgl_pemakaian, $ruangan);
			}
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir-".$jumlah." where item=".$id_item." AND ruangan=$ruangan AND DATE(tgl_stock)=STR_TO_DATE('$tgl_pemakaian', '%d-%m-%Y');";	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir-".$jumlah.", stock_awal=stock_awal-".$jumlah." where item=".$id_item." AND ruangan=$ruangan AND DATE(tgl_stock)>STR_TO_DATE('$tgl_pemakaian', '%d-%m-%Y');";	
			$this->db->query($sql);
			
		}
		
		$data['list'] = $this->g_pemakaian_model->get_detail($reg);
		
        $this->load->view('t_tabel_item_pakai', $data);
		
    }
	
	public function insert_tindakan(){
 
		$reg = $this->input->post('reg');
		$id_tindakan = $this->input->post('id_tindakan');
		
		$sql="INSERT INTO `t_tindakan_pakai` (`pemakaian`, `tindakan`) VALUES ($reg, $id_tindakan);";
		$this->db->query($sql);
		
		$data['tindakan2'] = $this->g_tindakan_model->get_tindakan();
		$data['list_tindakan'] = $this->g_pemakaian_model->get_detail_tindakan($reg);
		
        $this->load->view('t_tabel_tindakan_pakai', $data);
		
    }
	
	public function insert_dokter(){
 
		$reg = $this->input->post('reg');
		$id_dokter = $this->input->post('id_dokter');
		
		$sql="INSERT INTO `t_dokter_pakai` (`pemakaian`, `dokter`) VALUES ($reg, $id_dokter);";
		$this->db->query($sql);
		
		$data['dokter2'] = $this->dokter_model->get_dokter();
		$data['list_dokter'] = $this->g_pemakaian_model->get_detail_dokter($reg);
		
        $this->load->view('t_tabel_dokter_pakai', $data);
		
    }
	
	public function show_table()
	{
		$reg = $this->input->post('reg');
		$data['list'] = $this->g_pemakaian_model->get_detail($reg);
        $this->load->view('t_tabel_item_pakai', $data);
	}
	
	public function show_detail_item()
	{
		
		$reg = $this->input->post('reg');
		$data['list'] = $this->g_pemakaian_model->get_detail($reg);
        $this->load->view('t_tabel_item_pakai_detail', $data);
	}
	
	public function show_detail_dokter()
	{
		$reg = $this->input->post('reg');
		$data['list_dokter'] = $this->g_pemakaian_model->get_detail_dokter($reg);
        $this->load->view('t_tabel_dokter_pakai_detail', $data);
	}
	
	
	public function show_detail_tindakan()
	{
		$reg = $this->input->post('reg');
		$data['list_tindakan'] = $this->g_pemakaian_model->get_detail_tindakan($reg);
        $this->load->view('t_tabel_tindakan_pakai_detail', $data);
	}
	
	public function show_detail_dokter_edit()
	{
		$reg = $this->input->post('reg');
		$data['dokter2'] = $this->dokter_model->get_dokter();
		$data['list_dokter'] = $this->g_pemakaian_model->get_detail_dokter($reg);
        $this->load->view('t_tabel_dokter_pakai', $data);
	}
	
	
	public function show_detail_tindakan_edit()
	{
		$reg = $this->input->post('reg');
		$data['tindakan2'] = $this->g_tindakan_model->get_tindakan();
		$data['list_tindakan'] = $this->g_pemakaian_model->get_detail_tindakan($reg);
        $this->load->view('t_tabel_tindakan_pakai', $data);
	}
	
	public function delete_item(){
        //$data['title'] = 'Lorem ipsum';
        //$data['list'] = $this->test_model->get_data();
		
		$reg = $this->input->post('reg');
		
		$pemakaian = $this->input->post('pemakaian');
		
		if($pemakaian==0 || $pemakaian=="0")
		{
			
		}
		else
		{
			$item_detail=$this->g_pemakaian_model->get_detail_itempemakaian($reg);
			
			$ruangan=$item_detail['ruangan'];
			
			$count=$this->g_stock_model->check_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			}
			
			$sql="update t_item set stock_sisa=stock_sisa+".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir+".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir+".$item_detail['jumlah'].", stock_awal=stock_awal+".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
		}
		
		$sql="delete from `t_item_pakai` where `id_item_pakai`=$reg;";
		$this->db->query($sql);
		
		$data['list'] = $this->g_pemakaian_model->get_detail($pemakaian);
        $this->load->view('t_tabel_item_pakai', $data);
 
    }
	
	public function delete_tindakan(){
    	
		$reg = $this->input->post('reg');
		
		$pemakaian = $this->input->post('pemakaian');
		
		$sql="delete from `t_tindakan_pakai` where `id_tindakan_pakai`=$reg;";
		$this->db->query($sql);
		
		$data['tindakan2'] = $this->g_tindakan_model->get_tindakan();
		$data['list_tindakan'] = $this->g_pemakaian_model->get_detail_tindakan($pemakaian);
		
        $this->load->view('t_tabel_tindakan_pakai', $data);
 
    }
	
	public function delete_dokter(){
    	
		$reg = $this->input->post('reg');
		
		$pemakaian = $this->input->post('pemakaian');
		
		$sql="delete from `t_dokter_pakai` where `id_dokter_pakai`=$reg;";
		$this->db->query($sql);
		
		$data['dokter2'] = $this->dokter_model->get_dokter();
		$data['list_dokter'] = $this->g_pemakaian_model->get_detail_dokter($pemakaian);
		
        $this->load->view('t_tabel_dokter_pakai', $data);
 
    }
	
	public function create_pemakaian($idpas=0)
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$sql="delete from t_item_pakai where pemakaian=0;";
			$this->db->query($sql);
			
			$sql="delete from t_tindakan_pakai where pemakaian=0;";
			$this->db->query($sql);
			
			$sql="delete from t_dokter_pakai where pemakaian=0;";
			$this->db->query($sql);
		  
			$data['ruangan'] = $this->g_ruangan_model->get_ruangan();
			$data['dokter'] = $this->dokter_model->get_dokter();
			$data['penjamin'] = $this->g_pemakaian_model->get_penjamin();
			$data['tindakan'] = $this->g_tindakan_model->get_tindakan();
			
			$data['pasien'] = $this->pasien_model->get_pasien();
		  
			//$item = $this->g_item_model->get_item();
			
			$item=$this->g_item_model->get_item();
			foreach($item as $item_detail):
				
				$count=$this->g_stock_model->check_stockharian_ruangan($item_detail['id_item'], 1);
				if($count['ada']==0)
				{
					$this->g_stock_model->create_stockharian_ruangan($item_detail['id_item'], 1);
				}
				
			endforeach;
			
			$item = $this->g_item_model->get_item_ruangan(FALSE, 1);
			
			$testing3='';
			foreach ($item as $pasien_item):
			$stock_gudang_all=$pasien_item["stock_akhir"];
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "stock_gudang": "'.$stock_gudang_all.'", "satuanbeli": "'.$pasien_item["satuan_beli"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$stock_gudang_all.'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;
			$data['idpas']=$idpas;
			
			$last = $this->g_pemakaian_model->get_last_pemakaian_hari_ini();
			
			if($last==NULL)
			  {
				$date=date("md");
				$year=date("Y");
				$data['nomor_baru']= ''.substr($year, -2).''.$date.'001';
			  }
			  else
			  {
				  $last_number=substr($last['nomor_pakai'], -3);
				
				  $current_number=($last_number*1)+(1*1);
				  
				  
				  $date=date("md");
				  $year=date("Y");
				  
				  $nomor_baru= str_pad($current_number, 3, '0', STR_PAD_LEFT);
				  $data['nomor_baru']= ''.substr($year, -2).''.$date.''.$nomor_baru;
				  
			  }
			
			$this->load->view('g_register_pemakaian', $data);
		}
	}
	
	public function edit_pemakaian($inputReg)
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$data['ruangan'] = $this->g_ruangan_model->get_ruangan();
			$data['dokter'] = $this->dokter_model->get_dokter();
			$data['penjamin'] = $this->g_pemakaian_model->get_penjamin();
			$data['pasien'] = $this->pasien_model->get_pasien();
			
			$item=$this->g_item_model->get_item();
			foreach($item as $item_detail):
				
				$count=$this->g_stock_model->check_stockharian_ruangan($item_detail['id_item'], 1);
				if($count['ada']==0)
				{
					$this->g_stock_model->create_stockharian_ruangan($item_detail['id_item'], 1);
				}
				
			endforeach;
		  
			//$item = $this->g_item_model->get_item();
			$item = $this->g_item_model->get_item_ruangan(FALSE, 1);
			
			$testing3='';
			foreach ($item as $pasien_item):
			$stock_gudang_all=$pasien_item["stock_akhir"];
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "stock_gudang": "'.$stock_gudang_all.'", "satuanbeli": "'.$pasien_item["satuan_beli"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$stock_gudang_all.'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;
		  

			$data['pemakaian'] = $this->g_pemakaian_model->get_pemakaian($inputReg);
			
			$this->load->view('g_edit_pemakaian', $data);
		
		}
	}
	
	public function insert_pemakaian(){
 
		$nomorpemakaian = $this->input->post('nomorpemakaian');
		$tgl_pemakaian = $this->input->post('tgl_pemakaian');
		
		$ruangan = $this->input->post('ruangan');
		
		$pasien = $this->input->post('pasien');
		
		$nomor_registrasi = $this->input->post('nomor_registrasi');
		$no_rekam_medis = $this->input->post('no_rekam_medis');
		$penjamin = $this->input->post('penjamin');
		$tindakan = $this->input->post('tindakan');
		$dokter_pengirim = $this->input->post('pengirim');
		$dokter_operator = $this->input->post('operator');
		$tarif = $this->input->post('tarif');
		
		$catatan = $this->input->post('catatan');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		
		$sql="INSERT INTO `t_pemakaian` (`nomor_pakai`, `tgl_isi`, `ruangan`, pasien, `tgl_pakai`, `operator`, nomor_registrasi, no_rekam_medis, tindakan, tarif, dokter_pengirim, dokter_operator, penjamin, catatan) VALUES ('$nomorpemakaian', NOW(), $ruangan, $pasien, STR_TO_DATE('$tgl_pemakaian', '%d-%m-%Y'), '$operator', '$nomor_registrasi', '$no_rekam_medis', '$tindakan', '$tarif', $dokter_pengirim, $dokter_operator, $penjamin, '$catatan');";
		$this->db->query($sql);
		
		$last_pemakaian = $this->g_pemakaian_model->get_last_pemakaian($nomorpemakaian);
		
		$sql="update t_item_pakai set pemakaian=".$last_pemakaian['id_pemakaian'].", tgl_masuk=STR_TO_DATE('$tgl_pemakaian', '%d-%m-%Y') where pemakaian=0";
		$this->db->query($sql);
		
		$sql="update t_dokter_pakai set pemakaian=".$last_pemakaian['id_pemakaian']." where pemakaian=0";
		$this->db->query($sql);
		
		$sql="update t_tindakan_pakai set pemakaian=".$last_pemakaian['id_pemakaian']." where pemakaian=0";
		$this->db->query($sql);
		
		$detail_pemakaian = $this->g_pemakaian_model->get_detail($last_pemakaian['id_pemakaian']);
		
		foreach($detail_pemakaian as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2_ruangan($item_detail['item'], $item_detail['tgl_masuk'], $ruangan);
			}
			
			$sql="update t_item set stock_sisa=stock_sisa-".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir-".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stock_ruangan_harian set stock_akhir=stock_akhir-".$item_detail['jumlah'].", stock_awal=stock_awal-".$item_detail['jumlah']." where item=".$item_detail['item']." AND ruangan=$ruangan AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			
		endforeach;
		
		
		redirect('g_pemakaian/view_detail/'.$last_pemakaian['id_pemakaian']);
    }
	
	public function update_pemakaian(){
 
		$inputReg = $this->input->post('id_pemakaian');
		$nomorpemakaian = $this->input->post('nomorpemakaian');
		$tgl_pemakaian = $this->input->post('tgl_pemakaian');
		
		$ruangan = $this->input->post('ruangan');
		$pasien = $this->input->post('pasien');
		
		$nomor_registrasi = $this->input->post('nomor_registrasi');
		$no_rekam_medis = $this->input->post('no_rekam_medis');
		$penjamin = $this->input->post('penjamin');
		$tindakan = $this->input->post('tindakan');
		$dokter_pengirim = $this->input->post('pengirim');
		$dokter_operator = $this->input->post('operator');
		$tarif = $this->input->post('tarif');
		$catatan = $this->input->post('catatan');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		
		$sql="UPDATE `t_pemakaian` set `nomor_pakai`='$nomorpemakaian',`ruangan`=$ruangan, `pasien`=$pasien, `tgl_pakai`=STR_TO_DATE('$tgl_pemakaian', '%d-%m-%Y'), `operator`='$operator', nomor_registrasi='$nomor_registrasi', no_rekam_medis='$no_rekam_medis', tindakan='$tindakan', tarif='$tarif', dokter_pengirim=$dokter_pengirim, dokter_operator=$dokter_operator, penjamin=$penjamin, catatan='$catatan' where id_pemakaian=$inputReg ;";
		$this->db->query($sql);
		
		redirect('g_pemakaian/view_detail/'.$inputReg);
		
    }

	public function view_detail($inputReg=0){
		
		$data['pemakaian'] = $this->g_pemakaian_model->get_pemakaian($inputReg);
		$data['dokter'] = $this->dokter_model->get_dokter();
		$data['penjamin'] = $this->g_pemakaian_model->get_penjamin();
		
		$this->load->view('g_detail_pemakaian', $data);
    }
	
	public function view_detail2($inputReg=0){
		
		$data['pemakaian'] = $this->g_pemakaian_model->get_pemakaian($inputReg);
		$data['dokter'] = $this->dokter_model->get_dokter();
		$data['penjamin'] = $this->g_pemakaian_model->get_penjamin();
		
		$data['list'] = $this->g_pemakaian_model->get_detail($inputReg);
		
		$data['list_tindakan'] = $this->g_pemakaian_model->get_detail_tindakan($inputReg);
		$data['list_dokter'] = $this->g_pemakaian_model->get_detail_dokter($inputReg);
		
		$this->load->view('g_cetak_laporan_pemakaian', $data);
    }
	
	public function receipt($inputReg=0){
		
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran($inputReg);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function cetak_pembayaran($inputReg=0, $tanggal){
		$tanggal=urldecode($tanggal);
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran_kwitansi($inputReg, $tanggal);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function laporan_harian($tanggal=FALSE){
		
		$transaksihariini = $this->transaksi_model->get_transaksi2($tanggal);

		$detail=array();
		$pembayaran=array();
		foreach ($transaksihariini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg);
		endforeach;
		$data['transaksihariini']=$transaksihariini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		if($tanggal === FALSE)
		{
			$tanggal=date('j-m-Y');
		}
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function laporan_harian_baru($tanggal=FALSE){
		
		
		if($this->input->post('mulai')!=NULL)
		{
			$tanggal=$this->input->post('mulai');
		}
		else if($tanggal === FALSE)
		{
			$tanggal=date('Y-m-j');
		}
		$tanggal =date('Y-m-j', strtotime($tanggal));
		
		$pembayaran_hari_ini=$this->transaksi_model->get_pembayaran(FALSE, $tanggal);
		
		$detail=array();
		$pembayaran=array();
		foreach ($pembayaran_hari_ini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg, $tanggal);
		endforeach;
		$data['transaksihariini']=$pembayaran_hari_ini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function cetak_kwitansi($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('create_transaksi_success', $data);
    }
	
	public function cetak_kwitansi_besar(){
		$data['kwb_nomorkwitansi'] = $this->input->post('kwb_nomorkwitansi');
		$data['kwb_dari'] = $this->input->post('kwb_dari');
		$data['kwb_jumlah'] = $this->input->post('kwb_jumlah');
		$data['kwb_keperluan'] = $this->input->post('kwb_keperluan');
		$data['kwb_nominal'] = $this->input->post('kwb_nominal');
		$data['kwb_tanggal'] = $this->input->post('kwb_tanggal');
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	public function cetak_kwitansi_besar2($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	function formatRupiah($nilaiUang)
	{
	  $nilaiRupiah 	= "";
	  $jumlahAngka 	= strlen($nilaiUang);
	  while($jumlahAngka > 3)
	  {
		$nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
		$sisaNilai = strlen($nilaiUang) - 3;
		$nilaiUang = substr($nilaiUang,0,$sisaNilai);
		$jumlahAngka = strlen($nilaiUang);
	  }
	 
	  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
	  return $nilaiRupiah;
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
