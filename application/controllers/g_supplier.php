<?php if (!defined('BASEPATH')) die();
class G_supplier extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_supplier_model');
	}
	
   public function index()
	{
		$this->view_supplier(0);
	}
	
	public function view_supplier($status=0)
	{
		$data['supplier'] = $this->g_supplier_model->get_supplier();
		$data['status']=$status;
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_supplier', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('g_view_gudang_supplier', $data);
	}
	
	public function supplier_baru($state=0)
	{
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('create_supplier');
		$this->load->view('include/footer');
		*/
		$data['state']=$state;
		$this->load->view('g_register_supplier', $data);
	}
	
	public function create_supplier()
	{
		$state = $this->input->post('state');
		
		$inputNama = $this->input->post('inputNama');
		$inputNPWP = $this->input->post('inputNPWP');
		$alamat = $this->input->post('alamat');
		$inputKota = $this->input->post('inputKota');
		$inputKodepos = $this->input->post('inputKodepos');
		$inputEmail = $this->input->post('inputEmail');
		$inputTelepon = $this->input->post('inputTelepon');
		$fax = $this->input->post('fax');
		$hp = $this->input->post('hp');
		$inputCp = $this->input->post('inputCp');
		$keterangan = $this->input->post('keterangan');
		
		$sql="INSERT INTO  `t_supplier` (`nama_supplier` ,`alamat` ,`kota` ,`kodepos` ,`npwp` ,`telp` ,`fax` ,`hp` ,`email` ,`cp` ,`keterangan`, tgl_buat)
		VALUES (
		'".$inputNama."', '".$alamat."',  '".$inputKota."',  '".$inputKodepos."',  '".$inputNPWP."',  '".$inputTelepon."', '".$fax."',  '".$hp."',  '".$inputEmail."',  '".$inputCp."',  '".$keterangan."', NOW());";
		$this->db->query($sql);
		
		
		if($state==0)
		$this->view_supplier(1);
		else
		{
			$idsup= $this->g_supplier_model->get_last_supplier($inputNama);
			redirect("g_pembelian/create_pembelian/".$idsup['id_supplier']);
		}
	  
	}  
	  
	public function delete_supplier($id_supplier=FALSE)
	{
      
	  if($id_supplier === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$sql="delete from t_supplier where id_supplier=".$id_supplier;
		$this->db->query($sql);
		$this->view_supplier(3);
	  }
	  
	}
	
	public function edit_supplier($id_supplier=FALSE)
	{
      
	  if($id_supplier === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$data['supplier'] = $this->g_supplier_model->get_supplier($id_supplier);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('edit_supplier', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('g_edit_supplier', $data);
	  }
	  
	}
	
	public function update_supplier()
	{
      
		$id_supplier = $this->input->post('inputID');
		
		$inputNama = $this->input->post('inputNama');
		$inputNPWP = $this->input->post('inputNPWP');
		$alamat = $this->input->post('alamat');
		$inputKota = $this->input->post('inputKota');
		$inputKodepos = $this->input->post('inputKodepos');
		$inputEmail = $this->input->post('inputEmail');
		$inputTelepon = $this->input->post('inputTelepon');
		$fax = $this->input->post('fax');
		$hp = $this->input->post('hp');
		$inputCp = $this->input->post('inputCp');
		$keterangan = $this->input->post('keterangan');
		
		$sql="UPDATE `t_supplier` set `nama_supplier`='".$inputNama."' ,`alamat`='".$alamat."' ,`kota`='".$inputKota."' ,`kodepos`='".$inputKodepos."' ,`npwp`='".$inputNPWP."' ,`telp`='".$inputTelepon."' ,`fax`='".$fax."' ,`hp`='".$hp."' ,`email`='".$inputEmail."' ,`cp`='".$inputCp."' ,`keterangan`='".$keterangan."' WHERE id_supplier=".$id_supplier.";";
		
		$this->db->query($sql);
		
		$this->view_supplier(2);
	  
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
