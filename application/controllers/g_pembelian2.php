<?php if (!defined('BASEPATH')) die();
class G_pembelian extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_pembelian_model');
		$this->load->model('g_item_model');
		$this->load->model('g_supplier_model');
	}
	
   public function index()
	{
	  $this->load->view('include/header');
	  $this->load->view('templates/menubar');
      $this->load->view('view_transaksi');
      $this->load->view('include/footer');
	}
	
	public function view_transaksi($tanggal=FALSE)
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi2($tanggal);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_transaksi', $data);
	}
	
	public function view_all_transaksi()
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi2(0);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_transaksi', $data);
	}
	
	
	public function delete_transaksi($reg)
	{
		$sql="delete from transaksi where nomor_registrasi='$reg'";
		$this->db->query($sql);
		
		$sql="delete from detail_transaksi where nomor_register='$reg'";
		$this->db->query($sql);
		
		$sql="delete from pembayaran where nomor_registrasi='$reg'";
		$this->db->query($sql);
		
		
		$this->view_transaksi();
	}
	
	public function insert_item(){
 
        $data['title'] = 'Lorem ipsum';
        //$data['list'] = $this->test_model->get_data();
		
		$reg = $this->input->post('reg');
		$id_item = $this->input->post('id_item');
		$jumlah = $this->input->post('jumlah');
		$harga = $this->input->post('harga');
		$satuan = $this->input->post('satuan');
		$diskon = $this->input->post('diskon');
		$nominal_diskon = $this->input->post('nominal_diskon');
		$netto = $this->input->post('netto');
		$harga_subtotal = $this->input->post('harga_subtotal');
		$tanggalmasuk = $this->input->post('tanggalmasuk');
		$expdate = $this->input->post('expdate');
		
		$sql="INSERT INTO `t_item_beli` (`pembelian`, `item`, `jumlah`, `harga`, `diskon`, `nominal_diskon`, `total`, tgl_masuk, exp_date, satuan, subtotal) VALUES ($reg, $id_item, $jumlah, '$harga', $diskon, '$nominal_diskon', '$netto', STR_TO_DATE('$tanggalmasuk', '%d-%m-%Y'),STR_TO_DATE('$expdate', '%d-%m-%Y'),'$satuan','$harga_subtotal');";
		$this->db->query($sql);
		
		
		$sql="update t_pembelian set subtotal=subtotal+$netto, nominal_diskon=diskon*subtotal/100, total=subtotal-nominal_diskon WHERE id_pembelian=$reg";
		$this->db->query($sql);
		
		$data['list'] = $this->g_pembelian_model->get_detail($reg);
        $this->load->view('t_tabel_item', $data);
		
    }
	
	public function show_table()
	{
		$reg = $this->input->post('reg');
		$data['list'] = $this->g_pembelian_model->get_detail($reg);
        $this->load->view('t_tabel_item', $data);
	}
	
	public function show_detail_item()
	{
		$reg = $this->input->post('reg');
		$data['list'] = $this->g_pembelian_model->get_detail($reg);
        $this->load->view('t_tabel_item_detail', $data);
	}
	
	public function tambah_pembayaran($nomor)
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		$data['carabayar'] = $this->transaksi_model->get_carabayar();
        $this->load->view('tambah_pembayaran', $data);
	}
	
	public function edit_pembayaran($inputReg, $tanggal, $carabayar, $jumlahbayar)
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		$data['carabayar'] = $this->transaksi_model->get_carabayar();
		
		$tanggal=urldecode($tanggal);
		$sql="select * FROM `pembayaran` where nomor_registrasi='$inputReg' AND tanggal='$tanggal' AND carabayar=$carabayar AND jumlah=$jumlahbayar ;";
		$query = $this->db->query($sql);
		
		$data['pembayaran']=$query->result_array();
		
        $this->load->view('edit_pembayaran', $data);
	}
	
	public function insert_pembayaran()
	{
		$tanggal = $this->input->post('pb_tanggal');
		$inputReg = $this->input->post('pb_nota');
		
		$flag = $this->input->post('flag');
		
		$netto = $this->input->post('pb_totalharganetto');
		$carabayar = $this->input->post('pb_carabayar');
		$jumlahbayar = $this->input->post('pb_jumlahbayar2');
		$totalbayar = $this->input->post('pb_totalbayar')+$this->input->post('pb_jumlahbayar2');
		$sisa = $this->input->post('pb_sisa2');		
		$chargebank = $this->input->post('pb_chargebank');		
		$memo = $this->input->post('memo');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		

		$sql="INSERT INTO `pembayaran` (`nomor_registrasi`, `tanggal`, carabayar, jumlah, memo, chargebank, operator) VALUES ('$inputReg', NOW(), $carabayar, 			'$jumlahbayar', '$memo', '$chargebank','$operator');";

		$this->db->query($sql);
		
		$sql="UPDATE `transaksi` set bayar='$totalbayar', sisa='$sisa' where nomor_registrasi='$inputReg';";
		$this->db->query($sql);
		if($flag==0)
		redirect('transaksi/view_detail/'.$inputReg);
		else
		redirect('transaksi/edit_transaksi/'.$inputReg);
	}
	
	public function update_pembayaran()
	{
		$tanggal = $this->input->post('eb_tanggal');
		$inputReg = $this->input->post('eb_nota');
		
		$netto = $this->input->post('eb_totalharganetto');
		$carabayar = $this->input->post('eb_carabayar');
		$jumlahbayar = $this->input->post('eb_jumlahbayar2');
		$totalbayar = $this->input->post('eb_temp_bayar')+$this->input->post('eb_jumlahbayar2');
		$sisa = $this->input->post('eb_sisa2');
		$eb_chargebank = $this->input->post('eb_chargebank');
		
		$memo = $this->input->post('eb_memo');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		

		$sql="UPDATE `pembayaran` set carabayar=$carabayar, jumlah='$jumlahbayar', memo='$memo', operator='$operator', chargebank='$eb_chargebank' where `nomor_registrasi`='$inputReg' AND tanggal='$tanggal';";
		
		$this->db->query($sql);
		
		$sql="UPDATE `transaksi` set bayar='$totalbayar', sisa='$sisa' where nomor_registrasi='$inputReg';";
		$this->db->query($sql);
		
		redirect('transaksi/edit_transaksi/'.$inputReg);
		
	}
	
	public function delete_pembayaran($inputReg, $tanggal, $carabayar, $jumlahbayar)
	{
		$tanggal=urldecode($tanggal);
		$sql="DELETE FROM `pembayaran` where nomor_registrasi='$inputReg' AND tanggal='$tanggal' AND carabayar=$carabayar AND jumlah=$jumlahbayar ;";
		
		$this->db->query($sql);
		
		$sql="update transaksi set sisa=sisa+$jumlahbayar, 	bayar=bayar-$jumlahbayar where nomor_registrasi='$inputReg'";
		$this->db->query($sql);
		
		redirect('transaksi/edit_transaksi/'.$inputReg);
	}
	
	public function delete_item(){
        //$data['title'] = 'Lorem ipsum';
        //$data['list'] = $this->test_model->get_data();
		
		$reg = $this->input->post('reg');
		$netto = $this->input->post('netto');
		$pembelian = $this->input->post('pembelian');
		
		
		$sql="delete from `t_item_beli` where `id_item_beli`=$reg;";
		$this->db->query($sql);
		
		$sql="update t_pembelian set subtotal=subtotal-$netto, nominal_diskon=diskon*subtotal/100, total=subtotal-nominal_diskon WHERE id_pembelian=$reg";
		
		$this->db->query($sql);
		
		$data['list'] = $this->g_pembelian_model->get_detail($pembelian);
        $this->load->view('t_tabel_item', $data);
 
    }
	
	
	public function create_pembelian()
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$data['supplier'] = $this->g_supplier_model->get_supplier();
		  
			$sql="delete from t_item_beli where pembelian=0;";
			$this->db->query($sql);
		  
			$item = $this->g_item_model->get_item();
		  
			$testing3='';
			foreach ($item as $pasien_item):
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "harga": "'.$pasien_item["harga_beli"].'", "satuanbeli": "'.$pasien_item["satuan_beli"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$this->formatRupiah($pasien_item["harga_beli"]).'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;
		  
			$this->load->view('g_register_pembelian', $data);
		}
	}
	
	public function edit_transaksi($inputReg)
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
		  $dokter = $this->dokter_model->get_dokter();
		  $pasien = $this->pasien_model->get_pasien();
		  $layanan = $this->layanan_model->get_layanan();
		  $data['carabayar'] = $this->transaksi_model->get_carabayar();
		  
		  $testing='';
			foreach ($pasien as $pasien_item):
			$alamat=trim ( $pasien_item["alamat"] );
			$testing = $testing.'{"stateCode": "'.$pasien_item["id_pasien"].'", "stateDisplay": "'.$pasien_item["nama_pasien"].'", "stateName": "'.$pasien_item["id_pasien"].' | '.$pasien_item["nama_pasien"].' | '.$alamat.'"},';
					
			endforeach;
			 
			$testing=substr_replace($testing ,"",-1);
			$testing=trim(preg_replace('/\s+/', ' ', $testing));
			$data['testing']=$testing;
			
			$testing2='';
			foreach ($dokter as $pasien_item):
			$testing2 = $testing2.'{"stateCode": "'.$pasien_item["id_dokter"].'", "stateDisplay": "'.$pasien_item["nama_dokter"].'", "stateName": "'.$pasien_item["nama_dokter"].'"},';
					
			endforeach;
			
			$testing2=substr_replace($testing2 ,"",-1);
			$testing2=trim(preg_replace('/\s+/', ' ', $testing2));
			$data['testing2']=$testing2;
			
			$testing3='';
			foreach ($layanan as $pasien_item):
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_layanan"].'", "harga": "'.$pasien_item["harga"].'", "fee1": "'.$pasien_item["share1"].'", "fee2": "'.$pasien_item["share2"].'", "diskon": "'.$pasien_item["diskon"].'", "stateDisplay": "'.$pasien_item["nama_layanan"].'", "stateName": "'.$pasien_item["nama_layanan"].' | '.$this->formatRupiah($pasien_item["harga"]).'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;

		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran($inputReg);
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		/*  
		  $this->load->view('include/header');
		  $this->load->view('templates/menubar');
		  $this->load->view('edit_transaksi', $data);
		  $this->load->view('include/footer');
		*/  
		$this->load->view('register_edit', $data);
		
		}
	}
	
	public function insert_pembelian(){
 
		$nomorfaktur = $this->input->post('nomorfaktur');
		$tanggalfaktur = $this->input->post('tanggalfaktur');
		
		$supplier = $this->input->post('supplier');
		
		$harga = $this->input->post('totalharga');
		$diskon = $this->input->post('totaldiskon');
		$nominal_diskon = $this->input->post('totalnominaldiskon');
		
		$netto = $this->input->post('totalharganetto');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		
		$sql="INSERT INTO `t_pembelian` (`nomor_faktur`, `tgl_isi`, `supplier`, `tgl_faktur`, `subtotal`, `diskon`, `nominal_diskon`, `total`, `operator`) VALUES ('$nomorfaktur', NOW(), $supplier, STR_TO_DATE('$tanggalfaktur', '%d-%m-%Y'), $harga, '$diskon', '$nominal_diskon', '$netto', '$operator');";
		$this->db->query($sql);
		
		$last_pembelian = $this->g_pembelian_model->get_last_pembelian();
		
		$sql="update t_item_beli set pembelian=".$last_pembelian['id_pembelian']." where pembelian=0";
		$this->db->query($sql);
		
		//$this->cetak_kwitansi($inputReg);
		
		redirect('g_pembelian/view_detail/'.$pembelian);
		
    }
	
	public function update_transaksi(){
 
		$inputReg = $this->input->post('inputReg');
		$baru = $this->input->post('baru');
		if($baru==NULL)
		{
			$baru=0;
		}
		
		$id_pasien = $this->input->post('inputID');
		$id_dokter = $this->input->post('idreferensi');
		$harga = $this->input->post('totalharga');
		$diskon = $this->input->post('totaldiskon');
		$nominal_diskon = $this->input->post('totalnominaldiskon');
		
		$netto = $this->input->post('totalharganetto');
		$carabayar = $this->input->post('carabayar');
		$jumlahbayar = $this->input->post('jumlahbayar');
		$sisa = $this->input->post('sisa');
		
		$sql="UPDATE `transaksi` set `referensi`=$id_dokter, `pasien`='$id_pasien', `total_harga`=$harga, `diskon`='$diskon', `nominal_diskon`='$nominal_diskon', `harga_netto`='$netto', baru=$baru, bayar='$jumlahbayar', sisa='$sisa' where nomor_registrasi='$inputReg';";
		$this->db->query($sql);
		
		
		//$this->cetak_kwitansi($inputReg);
		
		redirect('transaksi/view_detail/'.$inputReg);
		
    }

	public function view_detail($inputReg=0){
		
		$data['pembelian'] = $this->g_pembelian_model->get_pembelian($inputReg);
		
		$data['detail'] = $this->g_pembelian_model->get_detail($inputReg);
		
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('success', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('g_detail_pembelian', $data);
    }
	
	public function receipt($inputReg=0){
		
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran($inputReg);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function cetak_pembayaran($inputReg=0, $tanggal){
		$tanggal=urldecode($tanggal);
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran_kwitansi($inputReg, $tanggal);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function laporan_harian($tanggal=FALSE){
		
		$transaksihariini = $this->transaksi_model->get_transaksi2($tanggal);

		$detail=array();
		$pembayaran=array();
		foreach ($transaksihariini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg);
		endforeach;
		$data['transaksihariini']=$transaksihariini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		if($tanggal === FALSE)
		{
			$tanggal=date('j-m-Y');
		}
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function laporan_harian_baru($tanggal=FALSE){
		
		
		if($this->input->post('mulai')!=NULL)
		{
			$tanggal=$this->input->post('mulai');
		}
		else if($tanggal === FALSE)
		{
			$tanggal=date('Y-m-j');
		}
		$tanggal =date('Y-m-j', strtotime($tanggal));
		
		$pembayaran_hari_ini=$this->transaksi_model->get_pembayaran(FALSE, $tanggal);
		
		$detail=array();
		$pembayaran=array();
		foreach ($pembayaran_hari_ini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg, $tanggal);
		endforeach;
		$data['transaksihariini']=$pembayaran_hari_ini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function cetak_kwitansi($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('create_transaksi_success', $data);
    }
	
	public function cetak_kwitansi_besar(){
		$data['kwb_nomorkwitansi'] = $this->input->post('kwb_nomorkwitansi');
		$data['kwb_dari'] = $this->input->post('kwb_dari');
		$data['kwb_jumlah'] = $this->input->post('kwb_jumlah');
		$data['kwb_keperluan'] = $this->input->post('kwb_keperluan');
		$data['kwb_nominal'] = $this->input->post('kwb_nominal');
		$data['kwb_tanggal'] = $this->input->post('kwb_tanggal');
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	public function cetak_kwitansi_besar2($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	function formatRupiah($nilaiUang)
	{
	  $nilaiRupiah 	= "";
	  $jumlahAngka 	= strlen($nilaiUang);
	  while($jumlahAngka > 3)
	  {
		$nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
		$sisaNilai = strlen($nilaiUang) - 3;
		$nilaiUang = substr($nilaiUang,0,$sisaNilai);
		$jumlahAngka = strlen($nilaiUang);
	  }
	 
	  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
	  return $nilaiRupiah;
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
