<?php if (!defined('BASEPATH')) die();
class G_tindakan extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_tindakan_model');
	}
	
   public function index()
	{
		$this->view_tindakan(0);
	}
	
	public function view_tindakan($status=0)
	{
		$data['tindakan'] = $this->g_tindakan_model->get_tindakan();
		$data['status']=$status;
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_tindakan', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('g_view_gudang_tindakan', $data);
	}
	
	public function tindakan_baru()
	{
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('create_tindakan');
		$this->load->view('include/footer');
		*/
		$this->load->view('g_register_tindakan');
	}
	
	public function create_tindakan()
	{
		$inputNama = $this->input->post('inputNama');
		$biaya = $this->input->post('biaya');
		
		$sql="INSERT INTO  `t_tindakan` (`nama_tindakan` ,`biaya`)
		VALUES (
		'".$inputNama."','".$biaya."');";
		$this->db->query($sql);
		
		$this->view_tindakan(1);
	  
	}  
	  
	public function delete_tindakan($id_tindakan=FALSE)
	{
      
	  if($id_tindakan === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$sql="delete from t_tindakan where id_tindakan=".$id_tindakan;
		$this->db->query($sql);
		$this->view_tindakan(3);
	  }
	  
	}
	
	public function edit_tindakan($id_tindakan=FALSE)
	{
      
	  if($id_tindakan === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$data['tindakan'] = $this->g_tindakan_model->get_tindakan($id_tindakan);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('edit_tindakan', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('g_edit_tindakan', $data);
	  }
	  
	}
	
	public function update_tindakan()
	{
      
		$id_tindakan = $this->input->post('inputID');
		
		$inputNama = $this->input->post('inputNama');
		$biaya = $this->input->post('biaya');
		
		$sql="UPDATE `t_tindakan` set `nama_tindakan`='".$inputNama."',`biaya`='".$biaya."' WHERE id_tindakan=".$id_tindakan.";";
		
		$this->db->query($sql);
		
		$this->view_tindakan(2);
	  
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
