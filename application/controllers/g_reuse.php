<?php if (!defined('BASEPATH')) die();
class G_reuse extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_reuse_model');
		$this->load->model('g_ruangan_model');
		$this->load->model('g_item_model');
		$this->load->model('g_stock_model');
	}
	
   public function index()
	{
		$this->view_reuse(0);
	}
	
	public function view_reuse($status=0)
	{
		$data['reuse'] = $this->g_reuse_model->get_reuse();
		$data['status']=$status;
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_reuse', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('g_view_gudang_reuse', $data);
	}
	
	public function reuse_baru()
	{
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('create_reuse');
		$this->load->view('include/footer');
		*/
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$item = $this->g_item_model->get_item();
		  
			$testing3='';
			foreach ($item as $pasien_item):
			
			$stock_gudang_plus_awal=$pasien_item["stock_gudang"]+$pasien_item["stock_awal"];
			
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "stock_gudang": "'.$stock_gudang_plus_awal.'", "satuanbeli": "'.$pasien_item["satuan_beli"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$stock_gudang_plus_awal.'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;
		  
			$this->load->view('g_register_reuse', $data);
		}
		
	}
	
	public function create_reuse()
	{
		$tgl_reuse = $this->input->post('tgl_reuse');
		$id_item = $this->input->post('id_item');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		
		
		$sql="INSERT INTO  `t_reuse` (`tgl_reuse` ,`tgl_isi`, item, jumlah, keterangan)
		VALUES (STR_TO_DATE('$tgl_reuse', '%d-%m-%Y'), NOW(), $id_item, $jumlah, '$keterangan');";
		$this->db->query($sql);
		
		$count=$this->g_stock_model->check_stockharian3($id_item, $tgl_reuse );
			
		if($count['ada']=='0')
		{
			$this->g_stock_model->create_stockharian3($id_item, $tgl_reuse);
		}
		
		$sql="update t_item set stock_gudang=stock_gudang+".$jumlah.", stock_sisa=stock_sisa+".$jumlah." where id_item=".$id_item;	
		$this->db->query($sql);
		
		$sql="update t_stockharian set stock_akhir=stock_akhir+".$jumlah." where item=".$id_item." AND DATE(tgl_stock)=STR_TO_DATE('$tgl_reuse', '%d-%m-%Y');";	
		$this->db->query($sql);
		
		$sql="update t_stockharian set stock_akhir=stock_akhir+".$jumlah.", stock_awal=stock_awal+".$jumlah." where item=".$id_item." AND DATE(tgl_stock)>STR_TO_DATE('$tgl_reuse', '%d-%m-%Y');";	
		$this->db->query($sql);
		
		$this->view_reuse(1);
	  
	}  
	  
	public function delete_reuse($id_reuse=FALSE)
	{
      
	  if($id_reuse === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$detail_item=$this->g_reuse_model->get_reuse($id_reuse);
		
		$sql="update t_item set stock_gudang=stock_gudang-".$detail_item['jumlah'].", stock_sisa=stock_sisa-".$detail_item['jumlah']." where id_item=".$detail_item['item'];	
		$this->db->query($sql);
		
		$sql="update t_stockharian set stock_akhir=stock_akhir-".$detail_item['jumlah']." where item=".$detail_item['item']." AND DATE(tgl_stock)='".$detail_item['tgl_reuse']."';";	
		$this->db->query($sql);
		
		$sql="update t_stockharian set stock_akhir=stock_akhir-".$detail_item['jumlah'].", stock_awal=stock_awal-".$detail_item['jumlah']." where item=".$detail_item['item']." AND DATE(tgl_stock)>'".$detail_item['tgl_reuse']."';";	
		$this->db->query($sql);
		
		$sql="delete from t_reuse where id_reuse=".$id_reuse;
		$this->db->query($sql);
		$this->view_reuse(3);
	  }
	  
	}
	
	public function edit_reuse($id_reuse=FALSE)
	{
      
	  if($id_reuse === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$data['reuse'] = $this->g_reuse_model->get_reuse($id_reuse);
		
		$item = $this->g_item_model->get_item();
		  
		$testing3='';
		foreach ($item as $pasien_item):
		
		$stock_gudang_plus_awal=$pasien_item["stock_gudang"]+$pasien_item["stock_awal"];
		
		$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "stock_gudang": "'.$stock_gudang_plus_awal.'", "satuanbeli": "'.$pasien_item["satuan_beli"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$stock_gudang_plus_awal.'"},';
				
		endforeach;
		
		$testing3=substr_replace($testing3 ,"",-1);
		$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
		$data['testing3']=$testing3;
		
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('edit_reuse', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('g_edit_reuse', $data);
	  }
	  
	}
	
	public function update_reuse()
	{
      
		$id_reuse = $this->input->post('id_reuse');
		$tgl_reuse = $this->input->post('tgl_reuse');
		$id_item = $this->input->post('id_item');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$selisih = $this->input->post('selisih');
		
		$detail_item = $this->g_reuse_model->get_reuse($id_reuse);
		
		$sql="update t_item set stock_gudang=stock_gudang-".$detail_item['jumlah'].", stock_sisa=stock_sisa-".$detail_item['jumlah']." where id_item=".$detail_item['item'];	
		$this->db->query($sql);
		
		$sql="update t_stockharian set stock_akhir=stock_akhir-".$detail_item['jumlah']." where item=".$detail_item['item']." AND DATE(tgl_stock)='".$detail_item['tgl_reuse']."';";	
		$this->db->query($sql);
		
		$sql="update t_stockharian set stock_akhir=stock_akhir-".$detail_item['jumlah'].", stock_awal=stock_awal-".$detail_item['jumlah']." where item=".$detail_item['item']." AND DATE(tgl_stock)>'".$detail_item['tgl_reuse']."';";	
		$this->db->query($sql);
		
		
		$sql="UPDATE `t_reuse` SET `tgl_reuse`=STR_TO_DATE('$tgl_reuse', '%d-%m-%Y'), item=$id_item, jumlah=$jumlah, keterangan='$keterangan' WHERE id_reuse=$id_reuse;";
		$this->db->query($sql);
		
		$count=$this->g_stock_model->check_stockharian3($id_item, $tgl_reuse );
			
		if($count['ada']=='0')
		{
			$this->g_stock_model->create_stockharian3($id_item, $tgl_reuse);
		}
		
		$sql="update t_item set stock_gudang=stock_gudang+".$jumlah.", stock_sisa=stock_sisa+".$jumlah." where id_item=".$id_item;	
		$this->db->query($sql);
		
		$sql="update t_stockharian set stock_akhir=stock_akhir+".$jumlah." where item=".$id_item." AND DATE(tgl_stock)=STR_TO_DATE('$tgl_reuse', '%d-%m-%Y');";	
		$this->db->query($sql);
		
		$sql="update t_stockharian set stock_akhir=stock_akhir+".$jumlah.", stock_awal=stock_awal+".$jumlah." where item=".$id_item." AND DATE(tgl_stock)>STR_TO_DATE('$tgl_reuse', '%d-%m-%Y');";	
		$this->db->query($sql);
		
		$this->view_reuse(2);
	  
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
