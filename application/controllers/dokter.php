<?php if (!defined('BASEPATH')) die();
class Dokter extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('dokter_model');
	}
	
   public function index()
	{
		$this->view_dokter(0);
	}
	
	public function view_dokter($status=0)
	{
		$data['dokter'] = $this->dokter_model->get_dokter();
		$data['status']=$status;
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_dokter', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_dokter', $data);
	}
	
	public function dokter_baru()
	{
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('create_dokter');
		$this->load->view('include/footer');
		*/
		$this->load->view('register_dokter');
	}
	
	public function create_dokter()
	{
		$member = $this->input->post('member');
		if($member==NULL)
		$member=0;
		
		$initial = $this->input->post('initial');
		$inputNama = $this->input->post('inputNama');
		$nomorsip = $this->input->post('nomorsip');
		$tanggallahir = $this->input->post('tanggallahir');
		
		$jeniskelamin = $this->input->post('jeniskelamin');
		//$goldar = $this->input->post('goldar');
		//$resus = $this->input->post('resus');
		$alamat = $this->input->post('alamat');
		$inputKota = $this->input->post('inputKota');
		$inputKodepos = $this->input->post('inputKodepos');
		$inputEmail = $this->input->post('inputEmail');
		
		$inputTelepon = $this->input->post('inputTelepon');
		$hp = $this->input->post('hp');
		$inputPerusahaan = $this->input->post('inputPerusahaan');
		$keterangan = $this->input->post('keterangan');
		
		$sql="INSERT INTO `dokter` (`nama_dokter`, `nomor_sip`, `internal`, `tanggal_lahir`, `jenis_kelamin`, `initial`, `alamat`, `kota`, `kodepos`, `email`, `telepon`, `hp`, `perusahaan`, `keterangan`)
		VALUES (
		'".$inputNama."', '".$nomorsip."',  ".$member.",  STR_TO_DATE('$tanggallahir', '%d-%m-%Y'),  ".$jeniskelamin.",  ".$initial.", '".$alamat."',  '".$inputKota."',  '".$inputKodepos."',  '".$inputEmail."', '".$inputTelepon."',  '".$hp."',  '".$inputPerusahaan."',  '".$keterangan."');";
		$this->db->query($sql);
		
		$this->view_dokter(1);
	  
	}  
	  
	public function delete_dokter($id_dokter=FALSE)
	{
      
	  if($id_dokter === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$sql="delete from dokter where id_dokter=".$id_dokter;
		$this->db->query($sql);
		$this->view_dokter(3);
	  }
	  
	}
	
	public function edit_dokter($id_dokter=FALSE)
	{
      
	  if($id_dokter === FALSE)
	  {
		show_404();
	  }
	  else
	  {
		$data['dokter'] = $this->dokter_model->get_dokter($id_dokter);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');  
		$this->load->view('edit_dokter', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('edit_register_dokter', $data);
	  }
	  
	}
	
	public function update_dokter()
	{
      
		$id_dokter = $this->input->post('inputID');
		$member = $this->input->post('member');
		if($member==NULL)
		$member=0;
		
		$initial = $this->input->post('initial');
		$inputNama = $this->input->post('inputNama');
		$nomorsip = $this->input->post('nomorsip');
		$tanggallahir = $this->input->post('tanggallahir');
		
		$jeniskelamin = $this->input->post('jeniskelamin');
		//$goldar = $this->input->post('goldar');
		//$resus = $this->input->post('resus');
		$alamat = $this->input->post('alamat');
		$inputKota = $this->input->post('inputKota');
		$inputKodepos = $this->input->post('inputKodepos');
		$inputEmail = $this->input->post('inputEmail');
		
		$inputTelepon = $this->input->post('inputTelepon');
		$hp = $this->input->post('hp');
		$inputPerusahaan = $this->input->post('inputPerusahaan');
		$keterangan = $this->input->post('keterangan');
		
		
		$sql="update `dokter` set `nama_dokter`='".$inputNama."',`nomor_sip`='".$nomorsip."', `internal`=".$member.", `tanggal_lahir`=STR_TO_DATE('$tanggallahir', '%d-%m-%Y'), `jenis_kelamin`=".$jeniskelamin.", `initial`=".$initial.", `alamat`='".$alamat."', `kota`='".$inputKota."', `kodepos`='".$inputKodepos."', `email`='".$inputEmail."', `telepon`='".$inputTelepon."', `hp`='".$hp."', `perusahaan`='".$inputPerusahaan."', `keterangan`='".$keterangan."' where `id_dokter`='".$id_dokter."'";
		$this->db->query($sql);
		
		$this->view_dokter(2);
	  
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
