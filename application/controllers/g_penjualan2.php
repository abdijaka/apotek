<?php if (!defined('BASEPATH')) die();
class G_penjualan extends Main_Controller {

   public function __construct()
	{
		parent::__construct();
		$this->load->model('g_penjualan_model');
		$this->load->model('g_item_model');
		$this->load->model('pasien_model');
		$this->load->model('g_stock_model');
		$this->load->model('g_carabayar_model');
	}
	
   public function index()
	{
	  /*
	  $this->load->view('include/header');
	  $this->load->view('templates/menubar');
      $this->load->view('view_transaksi');
      $this->load->view('include/footer');
	  */
	  $this->view_penjualan();
	}
	
	public function view_penjualan($status=0, $id_penjualan=FALSE )
	{
		$data['penjualan'] = $this->g_penjualan_model->get_penjualan($id_penjualan);
		$data['status']=$status;
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('g_view_gudang_penjualan', $data);
	}
	
	public function view_all_transaksi()
	{
		$data['transaksi'] = $this->transaksi_model->get_transaksi2(0);
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('view_transaksi', $data);
		$this->load->view('include/footer');
		*/
		$this->load->view('view_register_transaksi', $data);
	}
	
	public function checkfaktur()
	{
		$reg = $this->input->post('reg');
		$jumlah = $this->g_penjualan_model->get_jumlah_faktur($reg);
		echo $jumlah['jumlah'];
	}
	
	
	public function delete_penjualan($reg)
	{
		$detail_beli = $this->g_penjualan_model->get_detail($reg);
		foreach($detail_beli as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian2($item_detail['item'], $item_detail['tgl_masuk'] );
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2($item_detail['item'], $item_detail['tgl_masuk']);
			}
			
			$sql="update t_item set stock_gudang=stock_gudang+".$item_detail['jumlah'].", stock_sisa=stock_sisa+".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$item_detail['jumlah'].", stock_awal=stock_awal+".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
		endforeach;
		
		
		$sql="delete from t_penjualan where id_penjualan=$reg";
		$this->db->query($sql);
		
		$sql="delete from t_item_jual where penjualan=$reg";
		$this->db->query($sql);
		
		$sql="delete from t_pembayaran_jual where nomor_penjualan=$reg";
		$this->db->query($sql);
		
		$this->view_penjualan(3);
	}
	
	public function insert_item(){
 
        $data['title'] = 'Lorem ipsum';
        //$data['list'] = $this->test_model->get_data();
		
		$reg = $this->input->post('reg');
		$id_item = $this->input->post('id_item');
		$jumlah = $this->input->post('jumlah');
		$harga = $this->input->post('harga');
		$satuan = $this->input->post('satuan');
		$diskon = $this->input->post('diskon');
		$nominal_diskon = $this->input->post('nominal_diskon');
		$netto = $this->input->post('netto');
		$harga_subtotal = $this->input->post('harga_subtotal');
		$tanggalmasuk = $this->input->post('tanggalmasuk');
		$expdate = $this->input->post('expdate');
		
		$sql="INSERT INTO `t_item_jual` (`penjualan`, `item`, `jumlah`, `harga`, `diskon`, `nominal_diskon`, `total`, tgl_masuk, exp_date, satuan, subtotal) VALUES ($reg, $id_item, $jumlah, '$harga', $diskon, '$nominal_diskon', '$netto', STR_TO_DATE('$tanggalmasuk', '%d-%m-%Y'),STR_TO_DATE('$expdate', '%d-%m-%Y'),'$satuan','$harga_subtotal');";
		$this->db->query($sql);
		
		
		$sql="update t_penjualan set subtotal=subtotal+$netto, nominal_diskon=diskon*subtotal/100, total=subtotal-nominal_diskon WHERE id_penjualan=$reg";
		$this->db->query($sql);
		
		if($reg==0 || $reg=="0")
		{
			
		}
		else
		{
			$sql="update t_item set stock_gudang=stock_gudang-".$jumlah.", stock_sisa=stock_sisa-".$jumlah." where id_item=".$id_item;	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$jumlah." where item=".$id_item." AND DATE(tgl_stock)=STR_TO_DATE('$tanggalmasuk', '%d-%m-%Y');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$jumlah.", stock_awal=stock_awal-".$jumlah." where item=".$id_item." AND DATE(tgl_stock)>STR_TO_DATE('$tanggalmasuk', '%d-%m-%Y');";	
			$this->db->query($sql);
		}
		
		$data['list'] = $this->g_penjualan_model->get_detail($reg);
		
        $this->load->view('t_tabel_item_jual', $data);
		
    }
	
	public function show_table()
	{
		$reg = $this->input->post('reg');
		$data['list'] = $this->g_penjualan_model->get_detail($reg);
        $this->load->view('t_tabel_item_jual', $data);
	}
	
	public function show_detail_item()
	{
		$reg = $this->input->post('reg');
		$data['list'] = $this->g_penjualan_model->get_detail($reg);
        $this->load->view('t_tabel_item_jual_detail', $data);
	}
	
	public function delete_item(){
        //$data['title'] = 'Lorem ipsum';
        //$data['list'] = $this->test_model->get_data();
		
		$reg = $this->input->post('reg');
		$netto = $this->input->post('netto');
		$penjualan = $this->input->post('penjualan');
		
		if($penjualan==0 || $penjualan=="0")
		{
			
		}
		else
		{
			$item_detail = $this->g_penjualan_model->get_detail_itembeli($reg);
		
			$sql="update t_item set stock_gudang=stock_gudang+".$item_detail['jumlah'].", stock_sisa=stock_sisa+".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir+".$item_detail['jumlah'].", stock_awal=stock_awal+".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
		}
		
		$sql="delete from `t_item_jual` where `id_item_jual`=$reg;";
		$this->db->query($sql);
		
		$sql="update t_penjualan set subtotal=subtotal-$netto, nominal_diskon=diskon*subtotal/100, total=subtotal-nominal_diskon WHERE id_penjualan=$reg";
		
		$this->db->query($sql);
		
		$data['list'] = $this->g_penjualan_model->get_detail($penjualan);
        $this->load->view('t_tabel_item_jual', $data);
 
    }
	
	
	public function create_penjualan($idsup=0)
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$data['pasien'] = $this->pasien_model->get_pasien();
			$data['carabayar'] = $this->g_carabayar_model->get_carabayar();
			$data['idsup']=$idsup;
			
			$sql="delete from t_item_jual where penjualan=0;";
			$this->db->query($sql);
		  
			/*
			$item = $this->g_item_model->get_item();
		  	$item = array();
			$testing3='';
			foreach ($item as $pasien_item):
			$stock_gudang_all=$pasien_item["stock_awal"]+$pasien_item["stock_gudang"];
			
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "stock_gudang": "'.$stock_gudang_all.'", "harga": "'.$pasien_item["harga_jual"].'", "satuanbeli": "'.$pasien_item["satuan_jual"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$this->formatRupiah($pasien_item["harga_jual"]).'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;
			*/

			$last = $this->g_penjualan_model->get_last_penjualan_hari_ini();
			
			if($last==NULL)
			  {
				$date=date("md");
				$year=date("Y");
				$data['nomor_baru']= ''.substr($year, -2).''.$date.'001';
			  }
			  else
			  {
				  $last_number=substr($last['nomor_faktur'], -3);
				
				  $current_number=($last_number*1)+(1*1);
				  
				  
				  $date=date("md");
				  $year=date("Y");
				  
				  $nomor_baru= str_pad($current_number, 3, '0', STR_PAD_LEFT);
				  $data['nomor_baru']= ''.substr($year, -2).''.$date.''.$nomor_baru;
				  
			  }
		  
			$this->load->view('g_register_penjualan', $data);
		}
	}

	public function ajaxTypeheadItem(){

		$item = $this->g_item_model->get_item_by_name($this->input->post('src_param'));
		$testing3='[';
		foreach ($item as $pasien_item):
		$stock_gudang_all=$pasien_item["stock_awal"]+$pasien_item["stock_gudang"];
		
		$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "stock_gudang": "'.$stock_gudang_all.'", "harga": "'.$pasien_item["harga_jual"].'", "satuanbeli": "'.$pasien_item["satuan_jual"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$this->formatRupiah($pasien_item["harga_jual"]).'"},';
		
			//echo '{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "stock_gudang": "'.$stock_gudang_all.'", "harga": "'.$pasien_item["harga_jual"].'", "satuanbeli": "'.$pasien_item["satuan_jual"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$this->formatRupiah($pasien_item["harga_jual"]).'"},';

		endforeach;
		$testing3=substr_replace($testing3 ,"",-1);
		$testing3=$testing3.']';
		$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
		
		echo $testing3;

		//$data['testing3']=$testing3;
		//$testing3;
	}
	
	public function edit_penjualan($inputReg)
	{
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else
		{
			$data['pasien'] = $this->pasien_model->get_pasien();
		  
			/*$item = $this->g_item_model->get_item();
		  
			$testing3='';
			foreach ($item as $pasien_item):
			$testing3 = $testing3.'{"stateCode": "'.$pasien_item["id_item"].'", "nama": "'.$pasien_item["nama_item"].'", "harga": "'.$pasien_item["harga_jual"].'", "satuanbeli": "'.$pasien_item["satuan_jual"].'", "kode": "'.$pasien_item["kode"].'", "stateDisplay": "'.$pasien_item["nama_item"].'", "stateName": "'.$pasien_item["nama_item"].' | '.$this->formatRupiah($pasien_item["harga_jual"]).'"},';
					
			endforeach;
			
			$testing3=substr_replace($testing3 ,"",-1);
			$testing3=trim(preg_replace('/\s+/', ' ', $testing3));
			$data['testing3']=$testing3;
		  	*/

			$data['penjualan'] = $this->g_penjualan_model->get_penjualan($inputReg);
			//$data['detail'] = $this->g_penjualan_model->get_detail($inputReg);
			/*  
			  $this->load->view('include/header');
			  $this->load->view('templates/menubar');
			  $this->load->view('edit_transaksi', $data);
			  $this->load->view('include/footer');
			*/  
			
			$data['pembayaran'] = $this->g_penjualan_model->get_pembayaran($inputReg);
			$data['carabayar'] = $this->g_carabayar_model->get_carabayar();
			
			$this->load->view('g_edit_penjualan', $data);
		
		}
	}
	
	public function insert_penjualan(){
 
		$nomorfaktur = $this->input->post('nomorfaktur');
		$tanggalfaktur = $this->input->post('tanggalfaktur');
		
		$pasien = $this->input->post('pasien');
		
		$harga = $this->input->post('totalharga');
		
		$diskon = $this->input->post('totaldiskon');
		$nominal_diskon = $this->input->post('totalnominaldiskon');
		
		//$nominal_ppn = $this->input->post('totalnominalppn');
		
		$netto = $this->input->post('totalharganetto');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		
		$sql="INSERT INTO `t_penjualan` (`nomor_faktur`, `tgl_isi`, `pasien`, `tgl_faktur`, `subtotal`, `diskon`, `nominal_diskon`, `total`, `operator`) VALUES ('$nomorfaktur', NOW(), $pasien, STR_TO_DATE('$tanggalfaktur', '%d-%m-%Y'), '$harga', $diskon, '$nominal_diskon', '$netto', '$operator');";
		$this->db->query($sql);
		
		$last_penjualan = $this->g_penjualan_model->get_last_penjualan($nomorfaktur);
		
		$sql="update t_item_jual set penjualan=".$last_penjualan['id_penjualan']." where penjualan=0";
		$this->db->query($sql);
		
		$detail_beli = $this->g_penjualan_model->get_detail($last_penjualan['id_penjualan']);
		
		foreach($detail_beli as $item_detail):
			
			$count=$this->g_stock_model->check_stockharian2($item_detail['item'], $item_detail['tgl_masuk'] );
			
			if($count['ada']=='0')
			{
				$this->g_stock_model->create_stockharian2($item_detail['item'], $item_detail['tgl_masuk']);
			}
			
			$sql="update t_item set stock_gudang=stock_gudang-".$item_detail['jumlah'].", stock_sisa=stock_sisa-".$item_detail['jumlah']." where id_item=".$item_detail['item'];	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)=DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
			$sql="update t_stockharian set stock_akhir=stock_akhir-".$item_detail['jumlah'].", stock_awal=stock_awal-".$item_detail['jumlah']." where item=".$item_detail['item']." AND DATE(tgl_stock)>DATE('".$item_detail['tgl_masuk']."');";	
			$this->db->query($sql);
			
		endforeach;
		
		//$this->db->query($sql);
		
		//$this->cetak_kwitansi($inputReg);
		
		$carabayar = $this->input->post('carabayar');
		$jumlahbayar = $this->input->post('jumlahbayar');
		//$chargebank=$nominal_ppn;
		$chargebank=0;
		
		$sql="INSERT INTO `t_pembayaran_jual` (`nomor_penjualan`, `tanggal`, carabayar, chargebank, jumlah, memo, operator) VALUES ('".$last_penjualan['id_penjualan']."', NOW(), $carabayar, '$chargebank', '$jumlahbayar', '', '$operator');";
		$this->db->query($sql);
		
		redirect('g_penjualan/view_detail/'.$last_penjualan['id_penjualan']);
    }
	
	public function update_penjualan(){
 
		$inputReg = $this->input->post('id_penjualan');
		
		$nomorfaktur = $this->input->post('nomorfaktur');
		$tanggalfaktur = $this->input->post('tanggalfaktur');
		
		$pasien = $this->input->post('pasien');
		
		$harga = $this->input->post('totalharga');
		$diskon = $this->input->post('totaldiskon');
		$nominal_diskon = $this->input->post('totalnominaldiskon');
		
		//$nominal_ppn = $this->input->post('totalnominalppn');
		
		$netto = $this->input->post('totalharganetto');
		
		$user = $this->ion_auth->user()->row();
		$operator=$user->username;
		
		$sql="UPDATE `t_penjualan` set `nomor_faktur`='$nomorfaktur',`pasien`=$pasien, `tgl_faktur`=STR_TO_DATE('$tanggalfaktur', '%d-%m-%Y'), `subtotal`='$harga', `diskon`=$diskon, `nominal_diskon`='$nominal_diskon', `total`='$netto', `operator`='$operator' where id_penjualan=$inputReg";
		$this->db->query($sql);
		
		$carabayar = $this->input->post('carabayar');
		$jumlahbayar = $this->input->post('jumlahbayar');
		//$chargebank=$nominal_ppn;
		$chargebank=0;
		$memo='';
		
		$sql="UPDATE `t_pembayaran_jual` SET `tanggal`=NOW(), carabayar=$carabayar, chargebank='$chargebank', jumlah='$jumlahbayar', memo='$memo', operator='$operator' WHERE nomor_penjualan=$inputReg;";
		$this->db->query($sql);
		
		//$this->cetak_kwitansi($inputReg);
		
		redirect('g_penjualan/view_detail/'.$inputReg);
		
    }

	public function view_detail($inputReg=0){
		
		$data['penjualan'] = $this->g_penjualan_model->get_penjualan($inputReg);
		$data['pembayaran'] = $this->g_penjualan_model->get_pembayaran($inputReg);
		$data['detail'] = $this->g_penjualan_model->get_detail($inputReg);
		
		/*
		$this->load->view('include/header');
		$this->load->view('templates/menubar');
		$this->load->view('success', $data);
		$this->load->view('include/footer');
		*/
		
		$this->load->view('g_detail_penjualan', $data);
    }
	
	public function view_detail2($inputReg=0){
		
		$data['penjualan'] = $this->g_penjualan_model->get_penjualan($inputReg);
		$data['pembayaran'] = $this->g_penjualan_model->get_pembayaran($inputReg);
		
		$data['list'] = $this->g_penjualan_model->get_detail($inputReg);
		
		$this->load->view('g_cetak_laporan_penjualan', $data);
    }
	
	public function receipt($inputReg=0){
		
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran($inputReg);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function cetak_pembayaran($inputReg=0, $tanggal){
		$tanggal=urldecode($tanggal);
		$data['transaksi'] = $this->transaksi_model->get_transaksi($inputReg);
		
		$data['detail'] = $this->transaksi_model->get_detail2($inputReg);
		
		$data['pembayaran'] = $this->transaksi_model->get_pembayaran_kwitansi($inputReg, $tanggal);
		
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('sample', $data);
//		$this->load->view('include/footer');
		
		$this->load->view('view_register_kwitansi', $data);
		
    }
	
	public function laporan_harian($tanggal=FALSE){
		
		$transaksihariini = $this->transaksi_model->get_transaksi2($tanggal);

		$detail=array();
		$pembayaran=array();
		foreach ($transaksihariini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg);
		endforeach;
		$data['transaksihariini']=$transaksihariini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		if($tanggal === FALSE)
		{
			$tanggal=date('j-m-Y');
		}
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function laporan_harian_baru($tanggal=FALSE){
		
		
		if($this->input->post('mulai')!=NULL)
		{
			$tanggal=$this->input->post('mulai');
		}
		else if($tanggal === FALSE)
		{
			$tanggal=date('Y-m-j');
		}
		$tanggal =date('Y-m-j', strtotime($tanggal));
		
		$pembayaran_hari_ini=$this->transaksi_model->get_pembayaran(FALSE, $tanggal);
		
		$detail=array();
		$pembayaran=array();
		foreach ($pembayaran_hari_ini as $transaksi_item):
		$inputReg=$transaksi_item['nomor_registrasi'];
		
		$detail[''.$inputReg] = $this->transaksi_model->get_detail2($inputReg);
		$pembayaran[''.$inputReg] = $this->transaksi_model->get_pembayaran($inputReg, $tanggal);
		endforeach;
		$data['transaksihariini']=$pembayaran_hari_ini;
		$data['detail']=$detail;
		$data['pembayaran']=$pembayaran;
		
		$tanggal =date('j-m-Y', strtotime($tanggal));
		$data['tanggal']=$tanggal;
//		$this->load->view('include/header');
//		$this->load->view('templates/menubar');
		//$this->load->view('laporan_harian', $data);
//		$this->load->view('include/footer');

		$this->load->view('view_register_laporan_harian', $data);
    }
	
	public function cetak_kwitansi($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('create_transaksi_success', $data);
    }
	
	public function cetak_kwitansi_besar(){
		$data['kwb_nomorkwitansi'] = $this->input->post('kwb_nomorkwitansi');
		$data['kwb_dari'] = $this->input->post('kwb_dari');
		$data['kwb_jumlah'] = $this->input->post('kwb_jumlah');
		$data['kwb_keperluan'] = $this->input->post('kwb_keperluan');
		$data['kwb_nominal'] = $this->input->post('kwb_nominal');
		$data['kwb_tanggal'] = $this->input->post('kwb_tanggal');
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	public function cetak_kwitansi_besar2($nomor=0){
		$data['transaksi'] = $this->transaksi_model->get_transaksi($nomor);
		
		$data['detail'] = $this->transaksi_model->get_detail2($nomor);
		
		$this->load->view('view_register_kwitansi_besar', $data);
    }
	
	function formatRupiah($nilaiUang)
	{
	  $nilaiRupiah 	= "";
	  $jumlahAngka 	= strlen($nilaiUang);
	  while($jumlahAngka > 3)
	  {
		$nilaiRupiah = "." . substr($nilaiUang,-3) . $nilaiRupiah;
		$sisaNilai = strlen($nilaiUang) - 3;
		$nilaiUang = substr($nilaiUang,0,$sisaNilai);
		$jumlahAngka = strlen($nilaiUang);
	  }
	 
	  $nilaiRupiah = "Rp " . $nilaiUang . $nilaiRupiah . ",-";
	  return $nilaiRupiah;
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
