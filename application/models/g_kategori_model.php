<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_kategori_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_kategori($id_kategori = FALSE)
	{
		
		if ($id_kategori === FALSE)
		{
			$sql = "SELECT * from t_kategori where status=1 ORDER BY id_kategori ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from t_kategori where status=1 AND id_kategori=".$id_kategori."";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
}