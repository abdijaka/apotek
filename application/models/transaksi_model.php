<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaksi_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_transaksi($id_transaksi = FALSE)
	{
		
		if ($id_transaksi === FALSE)
		{
			$sql = "SELECT * from transaksi where status=1 ORDER BY nomor_registrasi ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT t.*, p.nama_pasien, p.initial,  d.nama_dokter, cb.nama as nama_bayar, cb.tipe as tipebayar from transaksi t, pasien p, dokter d, cara_bayar cb WHERE nomor_registrasi=".$id_transaksi." AND t.status=1 AND t.pasien=p.id_pasien AND d.id_dokter=t.referensi ORDER BY nomor_registrasi ASC ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_carabayar($id = FALSE)
	{
		
		if ($id === FALSE)
		{
			$sql = "SELECT * from cara_bayar ORDER BY id ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from cara_bayar where id=$id ORDER BY id ASC";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_transaksi2($tanggal = FALSE)
	{
		
		if ($tanggal === FALSE)
		{
			$sql = "SELECT t.*, p.nama_pasien, d.nama_dokter from transaksi t, pasien p, dokter d WHERE t.status=1 AND t.pasien=p.id_pasien AND d.id_dokter=t.referensi AND DATE(`tanggal`) = DATE(CURDATE()) ORDER BY nomor_registrasi DESC ";
		}
		else if($tanggal==0)
		{
			$sql = "SELECT t.*, p.nama_pasien, d.nama_dokter from transaksi t, pasien p, dokter d WHERE t.status=1 AND t.pasien=p.id_pasien AND d.id_dokter=t.referensi ORDER BY nomor_registrasi DESC ";
		}
		else
		{
			$sql = "SELECT t.*, p.nama_pasien, d.nama_dokter from transaksi t, pasien p, dokter d WHERE t.status=1 AND t.pasien=p.id_pasien AND d.id_dokter=t.referensi AND DATE(`tanggal`) = DATE('$tanggal') ORDER BY nomor_registrasi DESC ";
		}
		
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}
	
	public function get_transaksi_hutang()
	{
		$sql = "SELECT t.*, DATE(tanggal) as tanggal, p.nama_pasien, d.nama_dokter from transaksi t, pasien p, dokter d WHERE t.status=1 AND t.pasien=p.id_pasien AND d.id_dokter=t.referensi AND t.sisa!=0 ORDER BY nomor_registrasi ASC ";
		
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}
	
	public function get_last_transaksi()
	{
		$sql = "SELECT * from transaksi WHERE DATE(`tanggal`) = DATE(CURDATE()) AND status=1 ORDER BY nomor_registrasi DESC limit 1";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_detail($nomor)
	{
		$sql = "SELECT dt.*, l.nama_layanan, d.nama_dokter, dt.harga, dt.diskon, dt.netto from detail_transaksi dt, dokter d, layanan l WHERE dt.status=1 and nomor_register like '$nomor' and dt.id_dokter=d.id_dokter and dt.id_layanan=l.id_layanan";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail2($nomor)
	{
		$sql = "SELECT l.id_layanan, l.nama_layanan, d.nama_dokter, dt.harga, dt.diskon, dt.nominal_diskon, dt.netto from detail_transaksi dt, dokter d, layanan l WHERE dt.status=1 and nomor_register like '$nomor' and dt.id_dokter=d.id_dokter and dt.id_layanan=l.id_layanan";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_pembayaran($nomor, $tanggal=FALSE)
	{
		if ($tanggal === FALSE)
		{
			$sql = "SELECT p.*, c.nama as namacarabayar, c.tipe from pembayaran p, cara_bayar c WHERE p.carabayar=c.id and nomor_registrasi like '$nomor'";
		}
		else if($nomor === FALSE)
		{
			$sql = "SELECT t.*, pas.nama_pasien, pas.initial, d.nama_dokter, p.*, c.nama as namacarabayar, c.tipe from pembayaran p, cara_bayar c, transaksi t, pasien pas, dokter d WHERE p.carabayar=c.id AND DATE(p.tanggal) = DATE('$tanggal') AND t.status=1 AND t.pasien=pas.id_pasien AND d.id_dokter=t.referensi AND t.nomor_registrasi=p.nomor_registrasi ORDER BY p.tanggal";
		}
		else
		{
			$sql = "SELECT p.*, c.nama as namacarabayar, c.tipe from pembayaran p, cara_bayar c WHERE p.carabayar=c.id and nomor_registrasi like '$nomor' AND DATE(p.tanggal) = DATE('$tanggal')";
			
		}
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function get_pembayaran_kwitansi($nomor, $tanggal=FALSE)
	{
		if ($tanggal === FALSE)
		{
			$sql = "SELECT p.*, c.nama as namacarabayar, c.tipe from pembayaran p, cara_bayar c WHERE p.carabayar=c.id and nomor_registrasi like '$nomor'";
		}
		else if($nomor === FALSE)
		{
			$sql = "SELECT t.*, pas.nama_pasien, d.nama_dokter, p.*, c.nama as namacarabayar, c.tipe from pembayaran p, cara_bayar c, transaksi t, pasien pas, dokter d WHERE p.carabayar=c.id AND DATE(p.tanggal) = DATE('$tanggal') AND t.status=1 AND t.pasien=pas.id_pasien AND d.id_dokter=t.referensi AND t.nomor_registrasi=p.nomor_registrasi ORDER BY p.tanggal";
		}
		else
		{
			$sql = "SELECT p.*, c.nama as namacarabayar, c.tipe from pembayaran p, cara_bayar c WHERE p.carabayar=c.id and nomor_registrasi like '$nomor' AND p.tanggal='$tanggal';";
			
		}
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}
