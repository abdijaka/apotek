<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_penjualan_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_penjualan($id_penjualan = FALSE)
	{
		
		if ($id_penjualan === FALSE)
		{
			$sql = "SELECT p.*, s.nama_pasien from t_penjualan p, pasien s where p.pasien=s.id_pasien ORDER BY id_penjualan ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT p.*, s.nama_pasien from t_penjualan p, pasien s where p.pasien=s.id_pasien AND id_penjualan=".$id_penjualan."";  
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_penjualan_bulanan($bulan = FALSE)
	{
		
		if ($bulan === FALSE)
		{
			$sql = "SELECT p.*, s.nama_pasien from t_penjualan p, pasien s where p.pasien=s.id_pasien AND MONTH(tgl_faktur)=MONTH(CURDATE()) AND YEAR(tgl_faktur)=YEAR(CURDATE()) ORDER BY id_penjualan ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		$pieces = explode("-", $bulan);
		$sql = "SELECT p.*, s.nama_pasien from t_penjualan p, pasien s where p.pasien=s.id_pasien AND MONTH(tgl_faktur)=".$pieces[0]." AND YEAR(tgl_faktur)=".$pieces[1]." ORDER BY id_penjualan ASC";  
		
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_penjualan_harian($tgl = FALSE)
	{
		
		if ($tgl === FALSE)
		{
			$sql = "SELECT p.*, s.nama_pasien from t_penjualan p, pasien s where p.pasien=s.id_pasien AND DATE(tgl_faktur)=CURDATE() ORDER BY id_penjualan ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT p.*, s.nama_pasien from t_penjualan p, pasien s where p.pasien=s.id_pasien AND AND DATE(tgl_faktur)=DATE('$tgl') ORDER BY id_penjualan ASC";  
		
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_last_penjualan($nomorfaktur)
	{
		$sql = "SELECT id_penjualan from t_penjualan where nomor_faktur='$nomorfaktur' ORDER BY tgl_isi DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_last_penjualan_hari_ini()
	{
		$sql = "SELECT * from t_penjualan WHERE DATE(`tgl_isi`) = DATE(CURDATE()) ORDER BY id_penjualan DESC limit 1";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_detail($nomor)
	{
		$sql = "SELECT ib.item, ib.tgl_masuk, ib.id_item_jual, ib.penjualan, i.nama_item, ib.harga, ib.satuan, ib.subtotal, ib.jumlah, ib.nominal_diskon, ib.total from t_item_jual ib, t_item i WHERE ib.penjualan=$nomor and ib.item=i.id_item";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_peritem($nomor=1)
	{
		$sql = "SELECT p.nomor_faktur, p.tgl_faktur, ib.tgl_masuk, ib.item, ib.tgl_masuk, ib.id_item_jual, ib.penjualan, i.nama_item, ib.subtotal, ib.jumlah, ib.nominal_diskon, ib.total from t_item_jual ib, t_item i, t_penjualan p WHERE ib.item=i.id_item AND i.id_item=$nomor AND p.id_penjualan=ib.penjualan ORDER BY p.tgl_faktur, p.nomor_faktur DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_itembeli($nomor)
	{
		$sql = "SELECT * from t_item_jual WHERE id_item_jual=$nomor";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_jumlah_faktur($nomor)
	{
		$sql = "SELECT count(id_penjualan) as jumlah from t_penjualan WHERE nomor_faktur like '".$nomor."' ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_pembayaran($nomor, $tanggal=FALSE)
	{
		if ($tanggal === FALSE)
		{
			$sql = "SELECT p.*, c.nama as namacarabayar, c.tipe, c.id as id_carabayar from t_pembayaran_jual p, t_cara_bayar c WHERE p.carabayar=c.id and nomor_penjualan like '$nomor'";
		}
		/*
		else if($nomor === FALSE)
		{
			$sql = "SELECT t.*, pas.nama_pasien, pas.initial, d.nama_dokter, p.*, c.nama as namacarabayar, c.tipe from t_pembayaran_jual p, t_cara_bayar c, transaksi t, pasien pas, dokter d WHERE p.carabayar=c.id AND DATE(p.tanggal) = DATE('$tanggal') AND t.status=1 AND t.pasien=pas.id_pasien AND d.id_dokter=t.referensi AND t.nomor_registrasi=p.nomor_registrasi ORDER BY p.tanggal";
		}
		else
		{
			$sql = "SELECT p.*, c.nama as namacarabayar, c.tipe from t_pembayaran_jual p, t_cara_bayar c WHERE p.carabayar=c.id and nomor_registrasi like '$nomor' AND DATE(p.tanggal) = DATE('$tanggal')";
			
		}
		*/
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}