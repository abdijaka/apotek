<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_carabayar_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_carabayar()
	{
		$sql = "SELECT * from t_cara_bayar ORDER BY id ASC";  
		$query = $this->db->query($sql);
		return $query->result_array();
	
	}
}