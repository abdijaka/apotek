<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_distribusi_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_distribusi($id_distribusi = FALSE)
	{
		
		if ($id_distribusi === FALSE)
		{
			$sql = "SELECT d.*, r.nama_ruangan from t_distribusi d, t_ruangan r where d.ruangan=r.id_ruangan ORDER BY tgl_distribusi DESC, id_distribusi DESC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT d.*, r.nama_ruangan from t_distribusi d, t_ruangan r where d.ruangan=r.id_ruangan AND id_distribusi=".$id_distribusi."";  
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_distribusi_harian($tgl = FALSE)
	{
		
		if ($tgl === FALSE)
		{
			$sql = "SELECT d.*, r.nama_ruangan from t_distribusi d, t_ruangan r where d.ruangan=r.id_ruangan AND DATE(tgl_distribusi)=CURDATE() ORDER BY  id_distribusi ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
			
			$sql = "SELECT p.*, s.nama_supplier from t_pembelian p, t_supplier s where p.supplier=s.id_supplier AND DATE(tgl_faktur)=CURDATE() ORDER BY id_pembelian ASC";
		}
		

		$sql = "SELECT d.*, r.nama_ruangan from t_distribusi d, t_ruangan r where d.ruangan=r.id_ruangan AND DATE(tgl_distribusi)=DATE('$tgl') ORDER BY  id_distribusi ASC";  
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}
	
	public function get_last_distribusi($nomor_distribusi)
	{
		$sql = "SELECT id_distribusi from t_distribusi where nomor_distribusi='$nomor_distribusi' ORDER BY tgl_isi DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_last_distribusi_hari_ini()
	{
		$sql = "SELECT * from t_distribusi WHERE DATE(`tgl_isi`) = DATE(CURDATE()) ORDER BY id_distribusi DESC limit 1";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_detail($nomor)
	{
		if($nomor==0 || $nomor=="0")
		{
			$sql = "SELECT id.item, id.id_item_distribusi, id.distribusi, id.tgl_masuk, i.nama_item, id.jumlah, id.satuan from t_item_distribusi id, t_item i WHERE id.distribusi=$nomor and id.item=i.id_item";
		}
		else
		{
			$sql = "SELECT d.ruangan, id.item, id.id_item_distribusi, id.distribusi, id.tgl_masuk, i.nama_item, id.jumlah, id.satuan from t_item_distribusi id, t_item i, t_distribusi d WHERE id.distribusi=$nomor and id.item=i.id_item and id.distribusi=d.id_distribusi";
		}
		
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_peritem($nomor=1)
	{
		$sql = "SELECT p.nomor_faktur, p.tgl_faktur, ib.tgl_masuk, ib.item, ib.tgl_masuk, ib.id_item_beli, ib.distribusi, i.nama_item, ib.subtotal, ib.jumlah, ib.nominal_diskon, ib.total from t_item_beli ib, t_item i, t_distribusi p WHERE ib.item=i.id_item AND i.id_item=$nomor AND p.id_distribusi=ib.distribusi ORDER BY p.tgl_faktur, p.nomor_faktur DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_peritem2($nomor=1)
	{
		$sql = "SELECT d.nomor_distribusi, d.tgl_distribusi, id.tgl_masuk, id.item, id.id_item_distribusi, id.distribusi, i.nama_item, ib.subtotal, ib.jumlah, ib.nominal_diskon, ib.total from t_item_distribusi id, t_item i, t_distribusi d WHERE ib.item=i.id_item AND i.id_item=$nomor AND p.id_distribusi=ib.distribusi ORDER BY p.tgl_faktur, p.nomor_faktur DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_itembeli($nomor)
	{
		$sql = "SELECT * from t_item_beli WHERE id_item_beli=$nomor";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_detail_itemdistribusi($nomor)
	{
		$sql = "SELECT id.*, d.ruangan from t_item_distribusi id, t_distribusi d WHERE id.id_item_distribusi=$nomor AND id.distribusi=d.id_distribusi";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_jumlah_faktur($nomor)
	{
		$sql = "SELECT count(id_distribusi) as jumlah from t_distribusi WHERE nomor_distribusi like '".$nomor."' ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
}