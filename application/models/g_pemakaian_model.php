<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_pemakaian_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_pemakaian($id_pemakaian = FALSE)
	{
		
		if ($id_pemakaian === FALSE)
		{
			$sql = "SELECT p.nama_pasien, d.*, r.nama_ruangan from t_pemakaian d, t_ruangan r, pasien p where d.ruangan=r.id_ruangan AND p.id_pasien=d.pasien ORDER BY tgl_pakai DESC, id_pemakaian DESC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT p.nama_pasien, d.*, r.nama_ruangan from t_pemakaian d, t_ruangan r, pasien p where d.ruangan=r.id_ruangan AND p.id_pasien=d.pasien AND id_pemakaian=".$id_pemakaian."";  
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_last_pemakaian($nomor_pakai)
	{
		$sql = "SELECT id_pemakaian from t_pemakaian where nomor_pakai='$nomor_pakai' ORDER BY tgl_isi DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_detail($nomor)
	{
		if($nomor==0 || $nomor=="0")
		{
			$sql = "SELECT id.item, id.id_item_pakai, id.pemakaian, id.tgl_masuk, i.nama_item, id.jumlah, id.satuan from t_item_pakai id, t_item i WHERE id.pemakaian=$nomor and id.item=i.id_item";
		}
		else
		{
			$sql = "SELECT d.ruangan, id.item, id.id_item_pakai, id.pemakaian, id.tgl_masuk, i.nama_item, id.jumlah, id.satuan from t_item_pakai id, t_item i, t_pemakaian d WHERE id.pemakaian=$nomor and id.item=i.id_item and id.pemakaian=d.id_pemakaian";
		}
		
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_tindakan($nomor)
	{
		
		$sql = "SELECT * from t_tindakan_pakai id, t_tindakan i WHERE id.pemakaian=$nomor and id.tindakan=i.id_tindakan";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_dokter($nomor)
	{
		
		$sql = "SELECT * from t_dokter_pakai id, dokter i WHERE id.pemakaian=$nomor and id.dokter=i.id_dokter";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_peritem($nomor=1)
	{
		$sql = "SELECT p.nomor_faktur, p.tgl_faktur, ib.tgl_masuk, ib.item, ib.tgl_masuk, ib.id_item_beli, ib.pemakaian, i.nama_item, ib.subtotal, ib.jumlah, ib.nominal_diskon, ib.total from t_item_beli ib, t_item i, t_pemakaian p WHERE ib.item=i.id_item AND i.id_item=$nomor AND p.id_pemakaian=ib.pemakaian ORDER BY p.tgl_faktur, p.nomor_faktur DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_peritem2($nomor=1)
	{
		$sql = "SELECT d.nomor_pakai, d.tgl_pakai, id.tgl_masuk, id.item, id.id_item_pakai, id.pemakaian, i.nama_item, ib.subtotal, ib.jumlah, ib.nominal_diskon, ib.total from t_item_pakai id, t_item i, t_pemakaian d WHERE ib.item=i.id_item AND i.id_item=$nomor AND p.id_pemakaian=ib.pemakaian ORDER BY p.tgl_faktur, p.nomor_faktur DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_penjamin()
	{
		$sql = "SELECT * from penjamin order by id_penjamin ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_itembeli($nomor)
	{
		$sql = "SELECT * from t_item_beli WHERE id_item_beli=$nomor";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_detail_itempemakaian($nomor)
	{
		$sql = "SELECT id.*, d.ruangan from t_item_pakai id, t_pemakaian d WHERE id.id_item_pakai=$nomor AND id.pemakaian=d.id_pemakaian";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_jumlah_faktur($nomor)
	{
		$sql = "SELECT count(id_pemakaian) as jumlah from t_pemakaian WHERE nomor_pakai like '".$nomor."' ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_last_pemakaian_hari_ini()
	{
		$sql = "SELECT * from t_pemakaian WHERE DATE(`tgl_isi`) = DATE(CURDATE()) ORDER BY id_pemakaian DESC limit 1";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
}