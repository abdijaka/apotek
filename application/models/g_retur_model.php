<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_retur_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_retur($id_retur = FALSE)
	{
		
		if ($id_retur === FALSE)
		{
			$sql = "SELECT d.*, r.nama_ruangan from t_retur d, t_ruangan r where d.ruangan=r.id_ruangan ORDER BY tgl_retur DESC, id_retur DESC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT d.*, r.nama_ruangan from t_retur d, t_ruangan r where d.ruangan=r.id_ruangan AND id_retur=".$id_retur."";  
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_last_retur($nomor_retur)
	{
		$sql = "SELECT id_retur from t_retur where nomor_retur='$nomor_retur' ORDER BY tgl_isi DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_last_retur_hari_ini()
	{
		$sql = "SELECT * from t_retur WHERE DATE(`tgl_isi`) = DATE(CURDATE()) ORDER BY id_retur DESC limit 1";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_detail($nomor)
	{
		if($nomor==0 || $nomor=="0")
		{
			$sql = "SELECT id.item, id.id_item_retur, id.retur, id.tgl_masuk, i.nama_item, id.jumlah, id.satuan from t_item_retur id, t_item i WHERE id.retur=$nomor and id.item=i.id_item";
		}
		else
		{
			$sql = "SELECT d.ruangan, id.item, id.id_item_retur, id.retur, id.tgl_masuk, i.nama_item, id.jumlah, id.satuan from t_item_retur id, t_item i, t_retur d WHERE id.retur=$nomor and id.item=i.id_item and id.retur=d.id_retur";
		}
		
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_peritem($nomor=1)
	{
		$sql = "SELECT p.nomor_faktur, p.tgl_faktur, ib.tgl_masuk, ib.item, ib.tgl_masuk, ib.id_item_beli, ib.retur, i.nama_item, ib.subtotal, ib.jumlah, ib.nominal_diskon, ib.total from t_item_beli ib, t_item i, t_retur p WHERE ib.item=i.id_item AND i.id_item=$nomor AND p.id_retur=ib.retur ORDER BY p.tgl_faktur, p.nomor_faktur DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_peritem2($nomor=1)
	{
		$sql = "SELECT d.nomor_retur, d.tgl_retur, id.tgl_masuk, id.item, id.id_item_retur, id.retur, i.nama_item, ib.subtotal, ib.jumlah, ib.nominal_diskon, ib.total from t_item_retur id, t_item i, t_retur d WHERE ib.item=i.id_item AND i.id_item=$nomor AND p.id_retur=ib.retur ORDER BY p.tgl_faktur, p.nomor_faktur DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_itembeli($nomor)
	{
		$sql = "SELECT * from t_item_beli WHERE id_item_beli=$nomor";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_detail_itemretur($nomor)
	{
		$sql = "SELECT id.*, d.ruangan from t_item_retur id, t_retur d WHERE id.id_item_retur=$nomor AND id.retur=d.id_retur";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_jumlah_faktur($nomor)
	{
		$sql = "SELECT count(id_retur) as jumlah from t_retur WHERE nomor_retur like '".$nomor."' ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
}