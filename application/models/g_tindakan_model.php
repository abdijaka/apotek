<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_tindakan_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_tindakan($id_tindakan = FALSE)
	{
		
		if ($id_tindakan === FALSE)
		{
			$sql = "SELECT * from t_tindakan where status=1 ORDER BY id_tindakan ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from t_tindakan where status=1 AND id_tindakan=".$id_tindakan."";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
}