<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_supplier_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_supplier($id_supplier = FALSE)
	{
		
		if ($id_supplier === FALSE)
		{
			$sql = "SELECT * from t_supplier where status=1 ORDER BY id_supplier ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from t_supplier where status=1 AND id_supplier=".$id_supplier."";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_last_supplier($inputNama)
	{
		$sql = "SELECT id_supplier from t_supplier where nama_supplier like '$inputNama' ORDER BY tgl_buat DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
}