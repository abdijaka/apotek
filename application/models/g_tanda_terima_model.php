<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_tanda_terima_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_tanda_terima($id_tanda_terima = FALSE)
	{
		if($id_tanda_terima == FALSE)
		{
			$sql = "SELECT t.*, s.nama_supplier from t_tanda_terima as t, t_supplier as s WHERE t.supplier=s.id_supplier";  
			$query = $this->db->query($sql);
			
			return $query->result_array();
		}
		$sql = "SELECT t.*, s.nama_supplier from t_tanda_terima as t, t_supplier as s WHERE t.supplier=s.id_supplier AND t.id=$id_tanda_terima";
		$query = $this->db->query($sql);	
		return $query->row_array();
	}

	public function get_jumlah_tanda_terima($nomor)
	{
		$sql = "SELECT count(id) as jumlah from t_tanda_terima WHERE no_tanda_terima like '".$nomor."' ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}

	public function get_pembelian_belum_bayar($nomor_faktur)
	{
		$sql = "SELECT * from t_pembelian WHERE nomor_faktur = '$nomor_faktur' AND status = 0";
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}

	public function cek_faktur($nomor)
	{
		$sql = "SELECT id_tanda_terima from t_pembelian WHERE nomor_faktur = '$nomor'";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}

	public function get_faktur_table_content($id_tanda_terima)
	{
		$sql = "SELECT * from t_pembelian WHERE id_tanda_terima = '$id_tanda_terima'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_last_tanda_terima()
	{
		$sql = "SELECT * from t_tanda_terima ORDER BY id DESC LIMIT 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_pembayaran($id_tanda_terima)
	{
		$sql = "SELECT p.*, c.nama as namacarabayar, c.tipe, c.id as id_carabayar from t_pembayaran_tanda_terima p, t_cara_bayar c WHERE p.carabayar=c.id and p.id_tanda_terima like '$id_tanda_terima'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function count_tanda_terima_by_date($now)
	{
		$sql = "SELECT * from t_tanda_terima where no_tanda_terima LIKE '$now%'";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
}

?>