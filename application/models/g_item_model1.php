<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_item_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_item($id_item = FALSE)
	{
		
		if ($id_item === FALSE)
		{
			$sql = "SELECT * from t_item where status=1 ORDER BY nama_item, id_item ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from t_item where status=1 AND id_item=".$id_item."";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_item_ruangan($id_item = FALSE, $ruangan)
	{
		
		if ($id_item === FALSE)
		{
			$sql = "SELECT i.*, sh.stock_akhir from t_item i, t_stock_ruangan_harian sh  where status=1 AND i.id_item=sh.item AND ruangan=$ruangan AND DATE(tgl_stock)=DATE(NOW()) ORDER BY id_item ASC";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT i.*, sh.stock_akhir from t_item i, t_stock_ruangan_harian sh  where status=1 AND i.id_item=sh.item AND ruangan=$ruangan AND DATE(tgl_stock)=DATE(NOW())  AND i.id_item=".$id_item." ORDER BY id_item ASC";
		
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	public function get_last_item($inputNama)
	{	
		$sql = "SELECT id_item from t_item WHERE nama_item='$inputNama' ORDER BY tgl_buat DESC LIMIT 1 ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_item_history($tanggal = FALSE, $selesai = FALSE, $nomor=1)
	{	
		if ($tanggal === FALSE)
		{
			$sql = "SELECT tp.id_pemakaian, tp.tgl_pakai, tp.nomor_registrasi, tp.umur, tp.pasien, tp.nama_pasien, tp.nama_penjamin, tp.nama_dokter, q.nama_tindakan, q.biaya FROM (select d.nama_dokter, nama_pasien, p.umur, pj.nama_penjamin, tpp.* from `t_pemakaian` tpp left join pasien p on pasien=id_pasien left join dokter d on dokter_pengirim=id_dokter left join penjamin pj on tpp.penjamin=pj.id_penjamin ) tp left join (select * from t_tindakan_pakai left join t_tindakan on tindakan=id_tindakan ) q on tp.id_pemakaian=q.pemakaian WHERE DATE(tgl_pakai)=CURDATE() ORDER BY tgl_pakai, tp.id_pemakaian";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$time = strtotime($tanggal);
		$mulai = date('Y-m-d',$time);
		$time = strtotime($selesai);
		$selesai = date('Y-m-d',$time);
		$sql = "
		SELECT * FROM (
		SELECT p.nomor_faktur as nomor_faktur, p.tgl_faktur as tgl, ib.jumlah as beli, 0 as jual, 0 as retur, 0 as musnah from t_item_beli ib, t_pembelian p WHERE ib.item=$nomor AND p.id_pembelian=ib.pembelian AND DATE(p.tgl_faktur)>= '$mulai' and DATE(p.tgl_faktur) <= '$selesai'
		UNION 
		SELECT p.nomor_faktur as nomor_faktur, p.tgl_faktur as tgl, 0 as beli, ij.jumlah as jual, 0 as retur, 0 as musnah from t_item_jual ij, t_penjualan p WHERE ij.item=$nomor AND p.id_penjualan=ij.penjualan AND DATE(p.tgl_faktur)>= '$mulai' and DATE(p.tgl_faktur) <= '$selesai'
		UNION 
		SELECT p.nomor_retursupp as nomor_faktur, p.tgl_retursupp as tgl, 0 as beli, 0 as jual, ij.jumlah as retur, 0 as musnah from t_item_retursupp ij, t_retursupp p WHERE ij.item=$nomor AND p.id_retursupp=ij.retursupp AND DATE(p.tgl_retursupp)>= '$mulai' and DATE(p.tgl_retursupp) <= '$selesai'
		UNION 
		SELECT ik.id_itemkeluar as nomor_faktur, ik.tgl_isi as tgl, 0 as beli, 0 as jual, 0 as retur, ik.jumlah as musnah from t_itemkeluar ik where ik.item=$nomor AND DATE(ik.tgl_isi)>= '$mulai' and DATE(ik.tgl_isi) <= '$selesai')q ORDER BY tgl;
		";
		
		//echo $sql;
		$query = $this->db->query($sql);
		
		
		return $query->result_array();
	}
}