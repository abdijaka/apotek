<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function laporanbulananpasien($bulan1=0, $tahun1=0, $bulan2=0, $tahun2=0)
	{
		if($tahun1==0)
		$sql = "select MONTH(tanggal) as bulan, YEAR(tanggal) as tahun, count(nomor_registrasi) as jumlah, sum(baru) as jumlahbaru  from transaksi group by YEAR(tanggal), MONTH(tanggal) order by YEAR(tanggal) DESC, MONTH(tanggal) DESC limit 12";
		else
		{
			if($bulan2!=12)
			{
				$bulan2=$bulan2+1;
			}
			else {
				$bulan2=1;
				$tahun2=$tahun2+1;
			}
			$sql = "select MONTH(tanggal) as bulan, YEAR(tanggal) as tahun, count(nomor_registrasi) as jumlah, sum(baru) as jumlahbaru  from transaksi WHERE (tanggal BETWEEN '$tahun1-$bulan1-01 00:00:00' AND '$tahun2-$bulan2-01 00:00:00') group by YEAR(tanggal), MONTH(tanggal) order by YEAR(tanggal) DESC, MONTH(tanggal) DESC";
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporan_nama_pasien($bulan1=0, $tahun1=0)
	{
		$sql = "select tanggal, nama_pasien, kota  from transaksi t, pasien p where t.pasien=p.id_pasien AND t.baru=1 AND $bulan1=MONTH(tanggal) AND $tahun1=YEAR(tanggal)";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporankotapasien($bulan1, $tahun1, $bulan2, $tahun2)
	{
		if($tahun1==$tahun2)
		$sql = "select p.kota, count(nomor_registrasi) as jumlah, sum(baru) as jumlahbaru  from transaksi t, pasien p WHERE p.id_pasien=t.pasien AND MONTH(tanggal)>=$bulan1 AND MONTH(tanggal)<=$bulan2 group by p.kota order by jumlah DESC limit 10 ";
		else if($tahun1<$tahun2)
		{
			if($bulan2!=12)
			{
				$bulan2=$bulan2+1;
			}
			else {
				$bulan2=1;
				$tahun2=$tahun2+1;
			}
			$sql = "select p.kota, count(nomor_registrasi) as jumlah, sum(baru) as jumlahbaru  from transaksi t, pasien p WHERE p.id_pasien=t.pasien AND (tanggal BETWEEN '$tahun1-$bulan1-01 00:00:00' AND '$tahun2-$bulan2-01 00:00:00') group by p.kota order by jumlah DESC limit 10";
		}
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporanbulananpasienbaru()
	{
		$sql = "select YEAR(tanggal), MONTH(tanggal), IFNULL(count(nomor_registrasi), 0)  as jumlahbaru from transaksi where baru=1 group by YEAR(tanggal), MONTH(tanggal) order by YEAR(tanggal) DESC, MONTH(tanggal) DESC limit 12";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporanharianpasien()
	{
		$sql = "select MONTH(tanggal), YEAR(tanggal), IFNULL(count(nomor_registrasi), 0) as jumlah, sum(baru) as jumlahbaru from transaksi where CURDATE()=DATE(tanggal) group by YEAR(tanggal), MONTH(tanggal)";
		
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function laporanharianpasienbaru()
	{
		$sql = "select YEAR(tanggal), MONTH(tanggal), IFNULL(count(nomor_registrasi), 0) as jumlahbaru from transaksi where CURDATE()=DATE(tanggal) AND baru=1 group by YEAR(tanggal), MONTH(tanggal)";
		
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function laporanbulananpendapatan($bulan1=0, $tahun1=0, $bulan2=0, $tahun2=0)
	{
		if($tahun1==0)
			$sql = "select MONTH(tanggal) as bulan, YEAR(tanggal) as tahun, sum(harga_netto) as jumlah, count(nomor_registrasi) as jmlpasien from transaksi group by YEAR(tanggal), MONTH(tanggal) order by YEAR(tanggal) DESC, MONTH(tanggal) DESC limit 12";
		else
		{
			if($bulan2!=12)
			{
				$bulan2=$bulan2+1;
			}
			else {
				$bulan2=1;
				$tahun2=$tahun2+1;
			}
			$sql = "select MONTH(tanggal) as bulan, YEAR(tanggal) as tahun, sum(harga_netto) as jumlah, count(nomor_registrasi) as jmlpasien from transaksi WHERE (tanggal BETWEEN '$tahun1-$bulan1-01 00:00:00' AND '$tahun2-$bulan2-01 00:00:00') group by YEAR(tanggal), MONTH(tanggal) order by YEAR(tanggal) DESC, MONTH(tanggal) DESC limit 12";
			
		}
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporan_bulanan_dokter_pengirim($id_dokter, $bulan1=0, $tahun1=0, $bulan2=0, $tahun2=0)
	{
		
		if($bulan2!=12)
		{
			$bulan2=$bulan2+1;
		}
		else {
			$bulan2=1;
			$tahun2=$tahun2+1;
		}
		
		/*
		$sql = "SELECT p.nama_pasien, DATE_FORMAT(t.tanggal, '%d-%m-%Y') as tanggal, d2.nama_dokter as operator, d.nama_dokter, l.nama_layanan, dt.nomor_register, dt.harga, dt.nominal_diskon, dt.netto, IF((t.referensi=dt.id_dokter AND (dt.id_layanan=1 OR dt.id_layanan=20 OR dt.id_layanan=21 OR dt.id_layanan=22)), 0, dt.fee1) as fee1  from  detail_transaksi dt, transaksi t, layanan l, pasien p, dokter d, dokter d2 WHERE (tanggal BETWEEN '$tahun1-$bulan1-01 00:00:00' AND '$tahun2-$bulan2-01 00:00:00') AND dt.nomor_register=t.nomor_registrasi AND l.id_layanan=dt.id_layanan AND d.id_dokter=t.referensi AND t.referensi=$id_dokter AND dt.id_dokter=d2.id_dokter AND t.pasien=p.id_pasien order by t.tanggal";
		*/
		
		$sql = "SELECT p.nama_pasien, DATE_FORMAT(t.tanggal, '%d-%m-%Y') as tanggal, d2.nama_dokter as operator, d.nama_dokter, l.nama_layanan, dt.nomor_register, dt.harga, dt.nominal_diskon, dt.netto, IF((t.referensi=dt.id_dokter AND (dt.id_layanan=1 OR dt.id_layanan=20 OR dt.id_layanan=21 OR dt.id_layanan=22)), 0, dt.fee1) as fee1  from  detail_transaksi dt, transaksi t, layanan l, pasien p, dokter d, dokter d2 WHERE (tanggal BETWEEN '$tahun1-$bulan1-01 00:00:00' AND '$tahun2-$bulan2-01 00:00:00') AND dt.nomor_register=t.nomor_registrasi AND l.id_layanan=dt.id_layanan AND d.id_dokter=t.referensi AND t.referensi=$id_dokter AND dt.id_dokter=d2.id_dokter AND t.pasien=p.id_pasien AND dt.id_layanan!=14 AND dt.id_layanan!=16 order by t.tanggal";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporan_bulanan_dokter_pengirim_eecp_esmr($id_dokter, $bulan1=0, $tahun1=0, $bulan2=0, $tahun2=0)
	{
		
		if($bulan2!=12)
		{
			$bulan2=$bulan2+1;
		}
		else {
			$bulan2=1;
			$tahun2=$tahun2+1;
		}
		
		$sql="SELECT p.nama_pasien, DATE_FORMAT(pemb.tanggal, '%d-%m-%Y') as tanggal, d2.nama_dokter as operator, d.nama_dokter, l.nama_layanan, dt.nomor_register, dt.harga, dt.nominal_diskon, dt.netto, dt.fee1 as fee1, pemb.jumlah as jumlah_pemb, sum(pemb.jumlah) as total_pemb FROM transaksi t, detail_transaksi dt, layanan l, pasien p, dokter d, dokter d2, pembayaran pemb WHERE (pemb.tanggal BETWEEN '2014-01-01 00:00:00' AND '$tahun2-$bulan2-01 00:00:00') AND dt.nomor_register=t.nomor_registrasi AND l.id_layanan=dt.id_layanan AND d.id_dokter=t.referensi AND t.referensi=$id_dokter AND dt.id_dokter=d2.id_dokter AND t.pasien=p.id_pasien AND pemb.nomor_registrasi=t.nomor_registrasi AND ( dt.id_layanan=14 OR dt.id_layanan=16) group by pemb.nomor_registrasi order by t.nomor_registrasi";
		
		//echo $sql;
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporan_harian_dokter_pengirim($id_dokter, $tanggal)
	{
		$sql = "SELECT p.nama_pasien, DATE_FORMAT(t.tanggal, '%d-%m-%Y') as tanggal, d2.nama_dokter as operator, d.nama_dokter, l.nama_layanan, dt.nomor_register, dt.harga, dt.nominal_diskon, dt.netto,IF((t.referensi=dt.id_dokter AND (dt.id_layanan=1 OR dt.id_layanan=20 OR dt.id_layanan=21 OR dt.id_layanan=22)), 0, dt.fee1) as fee1  from  detail_transaksi dt, transaksi t, layanan l, pasien p, dokter d, dokter d2 WHERE DATE(t.tanggal) = '$tanggal' AND dt.nomor_register=t.nomor_registrasi AND l.id_layanan=dt.id_layanan AND d.id_dokter=t.referensi AND t.referensi=$id_dokter AND dt.id_dokter=d2.id_dokter AND t.pasien=p.id_pasien order by t.tanggal";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporan_bulanan_dokter_operator($id_dokter, $bulan1=0, $tahun1=0, $bulan2=0, $tahun2=0)
	{
		
		if($bulan2!=12)
		{
			$bulan2=$bulan2+1;
		}
		else {
			$bulan2=1;
			$tahun2=$tahun2+1;
		}
		
		$sql = "SELECT p.nama_pasien, DATE_FORMAT(t.tanggal, '%d-%m-%Y') as tanggal, d.nama_dokter, d2.nama_dokter as pengirim, l.nama_layanan, dt.nomor_register , dt.harga, dt.nominal_diskon, dt.netto, dt.fee2 from  detail_transaksi dt, transaksi t, layanan l, dokter d, dokter d2, pasien p WHERE (tanggal BETWEEN '$tahun1-$bulan1-01 00:00:00' AND '$tahun2-$bulan2-01 00:00:00') AND dt.nomor_register=t.nomor_registrasi AND l.id_layanan=dt.id_layanan AND d.id_dokter=dt.id_dokter AND dt.id_dokter=$id_dokter AND t.referensi=d2.id_dokter AND t.pasien=p.id_pasien order by t.tanggal";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporan_harian_dokter_operator($id_dokter, $tanggal)
	{
		$sql = "SELECT p.nama_pasien, DATE_FORMAT(t.tanggal, '%d-%m-%Y') as tanggal, d.nama_dokter, d2.nama_dokter as pengirim, l.nama_layanan, dt.nomor_register , dt.harga, dt.nominal_diskon, dt.netto, dt.fee2 from  detail_transaksi dt, transaksi t, layanan l, dokter d, dokter d2, pasien p WHERE DATE(t.tanggal) = '$tanggal' AND dt.nomor_register=t.nomor_registrasi AND l.id_layanan=dt.id_layanan AND d.id_dokter=dt.id_dokter AND dt.id_dokter=$id_dokter AND t.referensi=d2.id_dokter AND t.pasien=p.id_pasien order by t.tanggal";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporanbulananlayanan($bulan1=0, $tahun1=0, $bulan2=0, $tahun2=0, $layanan=1)
	{
		if($tahun1==0)
			$sql = "select MONTH(t.tanggal) as bulan, YEAR(t.tanggal) as tahun, dt.id_layanan, sum(dt.netto) as jumlah, count(dt.nomor_register) as jmlpasien from transaksi t, detail_transaksi dt WHERE dt.nomor_register=t.nomor_registrasi AND dt.id_layanan=$layanan GROUP BY dt.id_layanan, YEAR(tanggal), MONTH(tanggal) order by YEAR(tanggal) DESC, MONTH(tanggal) DESC limit 12";
		else
		{
			if($bulan2!=12)
			{
				$bulan2=$bulan2+1;
			}
			else {
				$bulan2=1;
				$tahun2=$tahun2+1;
			}
			
			$sql = "select MONTH(t.tanggal) as bulan, YEAR(t.tanggal) as tahun, dt.id_layanan, sum(dt.netto) as jumlah, count(dt.nomor_register) as jmlpasien from transaksi t, detail_transaksi dt WHERE dt.nomor_register=t.nomor_registrasi AND (tanggal BETWEEN '$tahun1-$bulan1-01 00:00:00' AND '$tahun2-$bulan2-01 00:00:00') AND dt.id_layanan=$layanan GROUP BY dt.id_layanan, YEAR(tanggal), MONTH(tanggal) order by YEAR(tanggal) DESC, MONTH(tanggal) DESC limit 12";
			
			/*
			$sql = "select l.id_layanan , detail.*, detail.jumlah from layanan l left join (select MONTH(t.tanggal) as bulan, YEAR(t.tanggal) as tahun, dt.id_layanan as det_id, sum(dt.netto) as jumlah, count(dt.nomor_register) as jmlpasien from transaksi t, detail_transaksi dt WHERE dt.nomor_register=t.nomor_registrasi AND (tanggal BETWEEN '$tahun1-$bulan1-01 00:00:00' AND '$tahun2-$bulan2-01 00:00:00') AND dt.id_layanan=$layanan GROUP BY dt.id_layanan, YEAR(tanggal), MONTH(tanggal) order by YEAR(tanggal) DESC, MONTH(tanggal) DESC limit 12)detail ON l.id_layanan=detail.det_id";
			
			*/
			
			//echo $sql;
			
		}
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporan_semualayanan($bulan1=0, $tahun1=0)
	{
		//$sql = "select l.id_layanan ,l.nama_layanan, detail.*, IFNULL(detail.jmlpasien, 0) AS jumlah_layanan, IFNULL(detail.jumlah, 0) AS jumlah_pemasukan  from layanan l left join (select dt.id_layanan as det_id, sum(dt.netto) as jumlah, count(dt.nomor_register) as jmlpasien from transaksi t, detail_transaksi dt WHERE dt.nomor_register=t.nomor_registrasi AND MONTH(tanggal)=$bulan1 AND YEAR(tanggal)=$tahun1 GROUP BY dt.id_layanan, YEAR(tanggal), MONTH(tanggal) )detail ON l.id_layanan=detail.det_id";
		/*
		$sql = "select l.id_layanan ,l.nama_layanan, detail.*, IFNULL(detail.jmlpasien, 0) AS jumlah_layanan, IFNULL(detail.jumlah, 0) AS jumlah_pemasukan  from layanan l left join (select dt.id_layanan as det_id, sum(pemb.chargebank+pemb.jumlah) as jumlah, count(dt.nomor_register) as jmlpasien from transaksi t, detail_transaksi dt, pembayaran pemb WHERE dt.nomor_register=t.nomor_registrasi AND pemb.nomor_registrasi=t.nomor_registrasi AND MONTH(pemb.tanggal)=$bulan1 AND YEAR(pemb.tanggal)=$tahun1 GROUP BY dt.id_layanan, YEAR(pemb.tanggal), MONTH(pemb.tanggal) )detail ON l.id_layanan=detail.det_id";
		*/
		$sql = "select l.id_layanan ,l.nama_layanan, detail.*, IFNULL(detail.jmlpasien, 0) AS jumlah_layanan from layanan l left join (select dt.id_layanan as det_id, count(dt.nomor_register) as jmlpasien from transaksi t, detail_transaksi dt WHERE dt.nomor_register=t.nomor_registrasi AND MONTH(t.tanggal)=$bulan1 AND YEAR(t.tanggal)=$tahun1 GROUP BY dt.id_layanan, YEAR(t.tanggal), MONTH(t.tanggal) )detail ON l.id_layanan=detail.det_id";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporan_pasien_layanan($bulan1=0, $tahun1=0, $id_layanan)
	{
		$sql = "select l.*, p.nama_pasien, DATE(t.tanggal) as tanggal, p.kota, dt.*, dt.netto as netto from layanan l, detail_transaksi dt, transaksi t, pasien p where l.id_layanan=dt.id_layanan AND dt.nomor_register=t.nomor_registrasi AND t.pasien=p.id_pasien AND l.id_layanan=$id_layanan AND MONTH(t.tanggal)=$bulan1 AND YEAR(t.tanggal)=$tahun1 order by dt.nomor_register";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporan_pembayaran_bulanan($bulan1=0, $tahun1=0)
	{
		$sql = "select DATE(tanggal) as tanggal, sum(jumlah) as jumlah, sum(chargebank) as charge  from pembayaran where MONTH(tanggal)=$bulan1 AND YEAR(tanggal)=$tahun1 group by DATE(tanggal)";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporanharianpendapatan()
	{
		$sql = "select MONTH(tanggal), YEAR(tanggal), sum(harga_netto) as jumlah from transaksi where CURDATE()=DATE(tanggal) group by YEAR(tanggal), MONTH(tanggal)";
		
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function bulanan($bulan1=0, $bulan2 = 0, $tahun='2013')
	{
		
		if ($bulan1==0)
		{
			$bulan1=date('n');
		}
		if ($bulan2==0)
		{
			$bulan2=date('n');
		}
		
		$sql = "SELECT l.nama_layanan, count(dt.nomor_register) as jumlah, sum(dt.harga) as jumlah_harga, sum(dt.nominal_diskon) as jumlah_diskon, sum(dt.netto) as total_netto from  detail_transaksi dt, transaksi t, layanan l WHERE MONTH(t.tanggal)>=$bulan1 AND MONTH(t.tanggal)<=$bulan2 AND YEAR(t.tanggal)=$tahun AND dt.nomor_register=t.nomor_registrasi AND l.id_layanan=dt.id_layanan group by dt.id_layanan order by t.tanggal";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function dokter_bulanan($bulan1=0, $bulan2 = 0, $tahun='2013')
	{
		
		if ($bulan1==0)
		{
			$bulan1=date('n');
		}
		if ($bulan2==0)
		{
			$bulan2=date('n');
		}
		
		$sql = "SELECT d.nama_dokter, l.nama_layanan, count(dt.nomor_register) as jumlah, sum(dt.harga) as jumlah_harga, sum(dt.nominal_diskon) as jumlah_diskon, sum(dt.netto) as total_netto from  detail_transaksi dt, transaksi t, layanan l, dokter d WHERE MONTH(t.tanggal)>=$bulan1 AND MONTH(t.tanggal)<=$bulan2 AND YEAR(t.tanggal)=$tahun AND dt.nomor_register=t.nomor_registrasi AND l.id_layanan=dt.id_layanan AND d.id_dokter=dt.id_dokter group by dt.id_layanan, dt.id_dokter order by t.tanggal";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function dokter_bulanan_pengirim($bulan1=0, $bulan2 = 0, $tahun='2013', $id_dokter=1)
	{
		
		if ($bulan1==0)
		{
			$bulan1=date('n');
		}
		if ($bulan2==0)
		{
			$bulan2=date('n');
		}
		
		
		$sql = "SELECT d.nama_dokter, l.nama_layanan, dt.nomor_register , dt.harga, dt.nominal_diskon, dt.netto, dt.fee1 from  detail_transaksi dt, transaksi t, layanan l, dokter d WHERE MONTH(t.tanggal)>=$bulan1 AND MONTH(t.tanggal)<=$bulan2 AND YEAR(t.tanggal)=$tahun AND dt.nomor_register=t.nomor_registrasi AND l.id_layanan=dt.id_layanan AND d.id_dokter=t.referensi AND t.referensi=$id_dokter AND t.referensi!=dt.id_dokter order by t.tanggal";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function dokter_bulanan_operator($bulan1=0, $bulan2 = 0, $tahun='2013', $id_dokter=2)
	{
		
		if ($bulan1==0)
		{
			$bulan1=date('n');
		}
		if ($bulan2==0)
		{
			$bulan2=date('n');
		}
		
		
		$sql = "SELECT d.nama_dokter, l.nama_layanan, dt.nomor_register , dt.harga, dt.nominal_diskon, dt.netto, dt.fee2 from  detail_transaksi dt, transaksi t, layanan l, dokter d WHERE MONTH(t.tanggal)>=$bulan1 AND MONTH(t.tanggal)<=$bulan2 AND YEAR(t.tanggal)=$tahun AND dt.nomor_register=t.nomor_registrasi AND l.id_layanan=dt.id_layanan AND d.id_dokter=dt.id_dokter AND dt.id_dokter=$id_dokter order by t.tanggal";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function laporan_harian()
	{
		$sql = "SELECT dt.*, l.nama_layanan, d.nama_dokter as dokter_operator, t.referensi, dt.harga, dt.diskon, dt.netto, d2.nama_dokter as dokter_pengirim from detail_transaksi dt, dokter d, dokter d2, layanan l, transaksi t, cara_bayar c WHERE t.referensi=d2.id_dokter AND dt.status=1 and dt.nomor_register=t.nomor_registrasi and dt.id_dokter=d.id_dokter and dt.id_layanan=l.id_layanan AND DATE(`tanggal`) = DATE(CURDATE()) ORDER BY nomor_registrasi ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}