<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_pemakaian_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_pemakaian($id_pemakaian = FALSE)
	{
		
		if ($id_pemakaian === FALSE)
		{
			$sql = "SELECT p.*, s.nama_supplier from t_pemakaian p, t_supplier s where p.supplier=s.id_supplier ORDER BY id_pemakaian ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT p.*, s.nama_supplier from t_pemakaian p, t_supplier s where p.supplier=s.id_supplier AND id_pemakaian=".$id_pemakaian."";  
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_last_pemakaian($nomorfaktur)
	{
		$sql = "SELECT id_pemakaian from t_pemakaian where nomor_faktur='$nomorfaktur' ORDER BY tgl_isi DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_detail($nomor)
	{
		$sql = "SELECT ib.item, ib.tgl_masuk, ib.id_item_pakai, ib.pemakaian, i.nama_item, ib.subtotal, ib.jumlah, ib.nominal_diskon, ib.total from t_item_pakai ib, t_item i WHERE ib.pemakaian=$nomor and ib.item=i.id_item";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_peritem($nomor=1)
	{
		$sql = "SELECT p.nomor_faktur, p.tgl_faktur, ib.tgl_masuk, ib.item, ib.tgl_masuk, ib.id_item_pakai, ib.pemakaian, i.nama_item, ib.subtotal, ib.jumlah, ib.nominal_diskon, ib.total from t_item_pakai ib, t_item i, t_pemakaian p WHERE ib.item=i.id_item AND i.id_item=$nomor AND p.id_pemakaian=ib.pemakaian ORDER BY p.tgl_faktur, p.nomor_faktur DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_itembeli($nomor)
	{
		$sql = "SELECT * from t_item_pakai WHERE id_item_pakai=$nomor";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_jumlah_faktur($nomor)
	{
		$sql = "SELECT count(id_pemakaian) as jumlah from t_pemakaian WHERE nomor_faktur like '".$nomor."' ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
}