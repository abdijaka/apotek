<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_retursupp_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_retursupp($id_retursupp = FALSE)
	{
		
		if ($id_retursupp === FALSE)
		{
			$sql = "SELECT d.*, r.nama_supplier from t_retursupp d, t_supplier r where d.supplier=r.id_supplier ORDER BY tgl_retursupp DESC, id_retursupp DESC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT d.*, r.nama_supplier from t_retursupp d, t_supplier r where d.supplier=r.id_supplier AND id_retursupp=".$id_retursupp."";  
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_last_retursupp($nomor_retursupp)
	{
		$sql = "SELECT id_retursupp from t_retursupp where nomor_retursupp='$nomor_retursupp' ORDER BY tgl_isi DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_last_retursupp_hari_ini()
	{
		$sql = "SELECT * from t_retursupp WHERE DATE(`tgl_isi`) = DATE(CURDATE()) ORDER BY id_retursupp DESC limit 1";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_detail($nomor)
	{
		if($nomor==0 || $nomor=="0")
		{
			$sql = "SELECT id.item, id.id_item_retursupp, id.retursupp, id.tgl_masuk, i.nama_item, id.jumlah, id.satuan from t_item_retursupp id, t_item i WHERE id.retursupp=$nomor and id.item=i.id_item";
		}
		else
		{
			$sql = "SELECT d.supplier, id.item, id.id_item_retursupp, id.retursupp, id.tgl_masuk, i.nama_item, id.jumlah, id.satuan from t_item_retursupp id, t_item i, t_retursupp d WHERE id.retursupp=$nomor and id.item=i.id_item and id.retursupp=d.id_retursupp";
		}
		
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_peritem($nomor=1)
	{
		$sql = "SELECT p.nomor_faktur, p.tgl_faktur, ib.tgl_masuk, ib.item, ib.tgl_masuk, ib.id_item_beli, ib.retursupp, i.nama_item, ib.subtotal, ib.jumlah, ib.nominal_diskon, ib.total from t_item_beli ib, t_item i, t_retursupp p WHERE ib.item=i.id_item AND i.id_item=$nomor AND p.id_retursupp=ib.retursupp ORDER BY p.tgl_faktur, p.nomor_faktur DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_peritem2($nomor=1)
	{
		$sql = "SELECT d.nomor_retursupp, d.tgl_retursupp, id.tgl_masuk, id.item, id.id_item_retursupp, id.retursupp, i.nama_item, ib.subtotal, ib.jumlah, ib.nominal_diskon, ib.total from t_item_retursupp id, t_item i, t_retursupp d WHERE ib.item=i.id_item AND i.id_item=$nomor AND p.id_retursupp=ib.retursupp ORDER BY p.tgl_faktur, p.nomor_faktur DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_itembeli($nomor)
	{
		$sql = "SELECT * from t_item_beli WHERE id_item_beli=$nomor";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_detail_itemretursupp($nomor)
	{
		$sql = "SELECT id.*, d.supplier from t_item_retursupp id, t_retursupp d WHERE id.id_item_retursupp=$nomor AND id.retursupp=d.id_retursupp";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_jumlah_faktur($nomor)
	{
		$sql = "SELECT count(id_retursupp) as jumlah from t_retursupp WHERE nomor_retursupp like '".$nomor."' ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
}