<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_stock_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_stock($id_stock = FALSE)
	{
		
		if ($id_stock === FALSE)
		{
			$sql = "SELECT * from t_stock where status=1 ORDER BY id_stock ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from t_stock where status=1 AND id_stock=".$id_stock."";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_stock_item()
	{
		$sql = "SELECT * from t_item where status=1 ORDER BY (stock_awal+stock_sisa) ASC";  
		$query = $this->db->query($sql);
		return $query->result_array();
		
	}
	
	public function create_stockharian($id_item = FALSE)
	{
		
		$sql = "SELECT stock_akhir from t_stockharian where item=$id_item ORDER BY tgl_stock DESC LIMIT 1";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		$temp_stock=0;
		foreach ($data as $data_item):
			$flag=1;
			$temp_stock=$data_item['stock_akhir'];
		endforeach;
		
		$sql = "INSERT INTO t_stockharian (tgl_stock, item, stock_awal, stock_akhir) VALUES( NOW(), $id_item, $temp_stock,$temp_stock)";  
		$query = $this->db->query($sql);
		
	}
	
	public function create_stockharian2($id_item, $tanggal)
	{
		
		$sql = "SELECT stock_akhir from t_stockharian where item=$id_item AND DATE(tgl_stock)<DATE('".$tanggal."') ORDER BY tgl_stock DESC LIMIT 1";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		$temp_stock=0;
		foreach ($data as $data_item):
			$flag=1;
			$temp_stock=$data_item['stock_akhir'];
		endforeach;
		
		$sql = "INSERT INTO t_stockharian (tgl_stock, item, stock_awal, stock_akhir) VALUES( '".$tanggal."', $id_item, $temp_stock,$temp_stock)";  
		$query = $this->db->query($sql);
		
	}
	
	public function create_stockharian3($id_item, $tanggal)
	{
		
		$sql = "SELECT stock_akhir from t_stockharian where item=$id_item AND DATE(tgl_stock)<STR_TO_DATE('$tanggal', '%d-%m-%Y') ORDER BY tgl_stock DESC LIMIT 1";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		$temp_stock=0;
		foreach ($data as $data_item):
			$flag=1;
			$temp_stock=$data_item['stock_akhir'];
		endforeach;
		
		$sql = "INSERT INTO t_stockharian (tgl_stock, item, stock_awal, stock_akhir) VALUES( STR_TO_DATE('$tanggal', '%d-%m-%Y'), $id_item, $temp_stock,$temp_stock)";  
		$query = $this->db->query($sql);
		
	}
	
	public function create_stockharian_ruangan($id_item = FALSE, $ruangan)
	{
		
		$sql = "SELECT stock_akhir from t_stock_ruangan_harian where item=$id_item AND ruangan=$ruangan AND  DATE(tgl_stock)<DATE(NOW()) ORDER BY tgl_stock DESC LIMIT 1";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		$temp_stock=0;
		foreach ($data as $data_item):
			$flag=1;
			$temp_stock=$data_item['stock_akhir'];
		endforeach;
		
		$sql = "INSERT INTO t_stock_ruangan_harian (tgl_stock, item, ruangan, stock_awal, stock_akhir) VALUES( NOW(), $id_item, $ruangan, $temp_stock,$temp_stock)";  
		$query = $this->db->query($sql);
		
	}
	
	public function create_stockharian2_ruangan($id_item, $tanggal, $ruangan)
	{
		
		$sql = "SELECT stock_akhir from t_stock_ruangan_harian where item=$id_item AND ruangan=$ruangan AND  DATE(tgl_stock)<DATE('".$tanggal."') ORDER BY tgl_stock DESC LIMIT 1";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		$temp_stock=0;
		foreach ($data as $data_item):
			$flag=1;
			$temp_stock=$data_item['stock_akhir'];
		endforeach;
		
		$sql = "INSERT INTO t_stock_ruangan_harian (tgl_stock, item, ruangan, stock_awal, stock_akhir) VALUES( '".$tanggal."', $id_item, $ruangan, $temp_stock,$temp_stock)";  
		$query = $this->db->query($sql);
		
	}
	
	public function create_stockharian3_ruangan($id_item, $tanggal, $ruangan)
	{
		
		$sql = "SELECT stock_akhir from t_stock_ruangan_harian where item=$id_item AND ruangan=$ruangan AND  DATE(tgl_stock)<STR_TO_DATE('$tanggal', '%d-%m-%Y') ORDER BY tgl_stock DESC LIMIT 1";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		$temp_stock=0;
		foreach ($data as $data_item):
			$flag=1;
			$temp_stock=$data_item['stock_akhir'];
		endforeach;
		
		$sql = "INSERT INTO t_stock_ruangan_harian (tgl_stock, item, ruangan, stock_awal, stock_akhir) VALUES( STR_TO_DATE('$tanggal', '%d-%m-%Y'), $id_item, $ruangan, $temp_stock,$temp_stock)";  
		$query = $this->db->query($sql);
		
		
	}
	
	public function check_stockharian($id_item = FALSE)
	{	
		$sql = "SELECT count(tgl_stock) as ada from t_stockharian where item=$id_item AND DATE(tgl_stock)=DATE(NOW()) ";
		$query = $this->db->query($sql);
		return $query->row_array();
		
	}
	
	public function check_stockharian2($id_item, $tanggal)
	{	
		$sql = "SELECT count(tgl_stock) as ada from t_stockharian where item=$id_item AND DATE(tgl_stock)=DATE('".$tanggal."') ";
		$query = $this->db->query($sql);
		return $query->row_array();
		
	}
	
	public function check_stockharian3($id_item, $tanggal)
	{	
		$sql = "SELECT count(tgl_stock) as ada from t_stockharian where item=$id_item AND DATE(tgl_stock)=STR_TO_DATE('$tanggal', '%d-%m-%Y') ";
		$query = $this->db->query($sql);
		return $query->row_array();
		
	}
	
	public function check_stockharian_ruangan($id_item = FALSE, $ruangan)
	{	
		$sql = "SELECT count(tgl_stock) as ada from t_stock_ruangan_harian where item=$id_item AND ruangan=$ruangan AND DATE(tgl_stock)=DATE(NOW()) ";
		$query = $this->db->query($sql);
		return $query->row_array();
		
	}
	
	public function check_stockharian2_ruangan($id_item, $tanggal, $ruangan)
	{	
		$sql = "SELECT count(tgl_stock) as ada from t_stock_ruangan_harian where item=$id_item AND ruangan=$ruangan AND DATE(tgl_stock)=DATE('".$tanggal."') ";
		$query = $this->db->query($sql);
		return $query->row_array();
		
	}
	
	public function check_stockharian3_ruangan($id_item, $tanggal, $ruangan)
	{	
		$sql = "SELECT count(tgl_stock) as ada from t_stock_ruangan_harian where item=$id_item AND ruangan=$ruangan AND DATE(tgl_stock)=STR_TO_DATE('$tanggal', '%d-%m-%Y') ";
		$query = $this->db->query($sql);
		return $query->row_array();
		
	}
}