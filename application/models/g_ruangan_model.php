<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_ruangan_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_ruangan($id_ruangan = FALSE)
	{
		
		if ($id_ruangan === FALSE)
		{
			$sql = "SELECT * from t_ruangan where status=1 ORDER BY id_ruangan ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from t_ruangan where status=1 AND id_ruangan=".$id_ruangan."";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
}