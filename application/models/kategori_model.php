<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dokter_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_dokter($id_dokter = FALSE)
	{
		
		if ($id_dokter === FALSE)
		{
			$sql = "SELECT * from dokter where status=1 ORDER BY id_dokter ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from dokter WHERE id_dokter=".$id_dokter." AND status=1 ORDER BY id_dokter ASC ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
}