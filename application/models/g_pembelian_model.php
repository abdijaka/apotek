<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_pembelian_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_pembelian($id_pembelian = FALSE)
	{
		
		if ($id_pembelian === FALSE)
		{
			$sql = "SELECT p.*, s.nama_supplier, t.no_tanda_terima FROM t_pembelian p LEFT JOIN t_supplier s ON p.supplier = s.id_supplier LEFT JOIN t_tanda_terima t ON p.id_tanda_terima = t.id ORDER BY date(tgl_faktur) DESC";  
			$query = $this->db->query($sql);
			$result = $query->result_array();
			foreach ($result as $key => $value) {
				$arr_bayar = $this->get_total_bayar($value['id_pembelian']);
				$result[$key]['jumlah_bayar'] = $arr_bayar['jumlah'];
			}
			return $result;
		}
		
		$sql = "SELECT p.*, s.nama_supplier from t_pembelian p, t_supplier s where p.supplier=s.id_supplier AND id_pembelian=".$id_pembelian."";  
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}

	public function get_pembelian_sort($rentang)
	{	
		if($rentang == null)
		{
			$sql = "SELECT p.*, s.nama_supplier, t.no_tanda_terima FROM t_pembelian p LEFT JOIN t_supplier s ON p.supplier = s.id_supplier LEFT JOIN t_tanda_terima t ON p.id_tanda_terima = t.id ORDER BY date(tgl_faktur) DESC";  
			$query = $this->db->query($sql);
			$result = $query->result_array();
			
			return $result;
		}
		else
		{
			$sql1 = "SELECT p.*, s.nama_supplier, t.no_tanda_terima FROM t_pembelian p LEFT JOIN t_supplier s ON p.supplier = s.id_supplier LEFT JOIN t_tanda_terima t ON p.id_tanda_terima = t.id where DATEDIFF(CURDATE(), p.jatuh_tempo) <= ".$rentang." ORDER BY date(jatuh_tempo) DESC";
			$sql = "SELECT p.*, s.nama_supplier from t_pembelian p, t_supplier s where p.supplier=s.id_supplier AND DATEDIFF(CURDATE(), p.jatuh_tempo) <= ".$rentang." ORDER BY date(jatuh_tempo) DESC";  
			$query = $this->db->query($sql1);
			$result = $query->result_array();
			
			return $result;
		}
	}

	public function get_pembelian_by_faktur($nomor_faktur)
	{
		$sql = "SELECT * from t_pembelian where nomor_faktur like '%$nomor_faktur%' AND status = 0 AND id_tanda_terima IS NULL ORDER BY nomor_faktur ASC LIMIT 10";
		$query = $this->db->query($sql);

		return $query->result_array();
	}
	
	public function get_pembelian_harian($tgl = FALSE)
	{
		
		if ($tgl === FALSE)
		{
			$sql = "SELECT p.*, s.nama_supplier, pb.carabayar from t_pembelian p, t_supplier s, t_pembayaran_beli pb where p.supplier=s.id_supplier AND pb.nomor_pembelian=p.id_pembelian AND DATE(tgl_faktur)=CURDATE() ORDER BY id_pembelian ASC";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT p.*, s.nama_supplier, pb.carabayar from t_pembelian p, t_supplier s, t_pembayaran_beli pb where p.supplier=s.id_supplier AND pb.nomor_pembelian=p.id_pembelian AND DATE(tgl_faktur)=DATE('$tgl') ORDER BY id_pembelian ASC";
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}
	
	public function get_pembelian_bulanan($bulan = FALSE)
	{
		
		if ($bulan === FALSE)
		{
			$sql = "SELECT p.*, s.nama_supplier, pb.carabayar from t_pembelian p, t_supplier s, t_pembayaran_beli pb where p.supplier=s.id_supplier AND pb.nomor_pembelian=p.id_pembelian AND MONTH(tgl_faktur)=MONTH(CURDATE()) AND YEAR(tgl_faktur)=YEAR(CURDATE()) ORDER BY id_pembelian ASC";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		$pieces = explode("-", $bulan);
		$sql = "SELECT p.*, s.nama_supplier, pb.carabayar from t_pembelian p, t_supplier s, t_pembayaran_beli pb where p.supplier=s.id_supplier AND pb.nomor_pembelian=p.id_pembelian AND MONTH(tgl_faktur)=".$pieces[0]." AND YEAR(tgl_faktur)=".$pieces[1]." ORDER BY id_pembelian ASC";
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}
	
	public function get_last_pembelian($nomorfaktur)
	{
		$sql = "SELECT id_pembelian from t_pembelian where nomor_faktur='$nomorfaktur' ORDER BY tgl_isi DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}

	public function get_last_pembayaran()
	{
		$sql = "SELECT * FROM `t_pembayaran_beli` ORDER BY tanggal DESC LIMIT 1";
	}
	
	public function get_detail($nomor)
	{
		$sql = "SELECT ib.item, ib.tgl_masuk, ib.id_item_beli, ib.pembelian, i.nama_item, ib.subtotal, ib.harga, ib.jumlah, ib.diskon, ib.nominal_diskon, ib.total from t_item_beli ib, t_item i WHERE ib.pembelian=$nomor and ib.item=i.id_item";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_peritem($nomor=1)
	{
		$sql = "SELECT p.nomor_faktur, p.tgl_faktur, ib.tgl_masuk, ib.item, ib.tgl_masuk, ib.id_item_beli, ib.pembelian, i.nama_item, ib.subtotal, ib.jumlah, ib.nominal_diskon, ib.total from t_item_beli ib, t_item i, t_pembelian p WHERE ib.item=i.id_item AND i.id_item=$nomor AND p.id_pembelian=ib.pembelian ORDER BY p.tgl_faktur, p.nomor_faktur DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_detail_itembeli($nomor)
	{
		$sql = "SELECT * from t_item_beli WHERE id_item_beli=$nomor";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_jumlah_faktur($nomor)
	{
		$sql = "SELECT count(id_pembelian) as jumlah from t_pembelian WHERE nomor_faktur like '".$nomor."' ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}

	public function get_jumlah_faktur_by_delivery($nomor)
	{
		$sql = "SELECT count(id_pembelian) as jumlah from t_pembelian WHERE no_delivery like '".$nomor."' ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_pembayaran($nomor, $tanggal=FALSE)
	{
		if ($tanggal === FALSE)
		{
			$sql = "SELECT p.*, c.nama as namacarabayar, c.tipe, c.id as id_carabayar from t_pembayaran_beli p, t_cara_bayar c WHERE p.carabayar=c.id and id_pembelian like '$nomor'";
		}
		/*
		else if($nomor === FALSE)
		{
			$sql = "SELECT t.*, pas.nama_pasien, pas.initial, d.nama_dokter, p.*, c.nama as namacarabayar, c.tipe from t_pembayaran_beli p, t_cara_bayar c, transaksi t, pasien pas, dokter d WHERE p.carabayar=c.id AND DATE(p.tanggal) = DATE('$tanggal') AND t.status=1 AND t.pasien=pas.id_pasien AND d.id_dokter=t.referensi AND t.nomor_registrasi=p.nomor_registrasi ORDER BY p.tanggal";
		}
		else
		{
			$sql = "SELECT p.*, c.nama as namacarabayar, c.tipe from t_pembayaran_beli p, t_cara_bayar c WHERE p.carabayar=c.id and nomor_registrasi like '$nomor' AND DATE(p.tanggal) = DATE('$tanggal')";
			
		}
		*/
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_nominal_pembayaran($nomor)
	{
		$sql = "SELECT diskon, nominal_diskon FROM t_pembelian WHERE t_pembelian.id_pembelian LIKE $nomor";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_total_bayar($id_pembelian)
	{
		$sql = "SELECT SUM(jumlah) as jumlah FROM t_pembayaran_beli WHERE nomor_pembelian = '$id_pembelian'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function get_item($id_pembelian)
	{
		//$sql = "SELECT * FROM t_item_beli WHERE ";
	}
}