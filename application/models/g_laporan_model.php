<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_laporan_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_laporan($id_laporan = FALSE)
	{
		
		if ($id_laporan === FALSE)
		{
			$sql = "SELECT * from t_laporan where status=1 ORDER BY id_laporan ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from t_laporan where status=1 AND id_laporan=".$id_laporan."";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_laporan_item()
	{
		$sql = "SELECT * from t_item where status=1 ORDER BY (laporan_awal+laporan_sisa) ASC";  
		$query = $this->db->query($sql);
		return $query->result_array();
		
	}
	
	public function create_laporanharian($id_item = FALSE)
	{
		
		$sql = "SELECT laporan_akhir from t_laporanharian where item=$id_item ORDER BY tgl_laporan DESC LIMIT 1";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		$temp_laporan=0;
		foreach ($data as $data_item):
			$flag=1;
			$temp_laporan=$data_item['laporan_akhir'];
		endforeach;
		
		$sql = "INSERT INTO t_laporanharian (tgl_laporan, item, laporan_awal, laporan_akhir) VALUES( NOW(), $id_item, $temp_laporan,$temp_laporan)";  
		$query = $this->db->query($sql);
		
	}
	
	public function create_laporanharian2($id_item, $tanggal)
	{
		
		$sql = "SELECT laporan_akhir from t_laporanharian where item=$id_item AND DATE(tgl_laporan)<DATE('".$tanggal."') ORDER BY tgl_laporan DESC LIMIT 1";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		$temp_laporan=0;
		foreach ($data as $data_item):
			$flag=1;
			$temp_laporan=$data_item['laporan_akhir'];
		endforeach;
		
		$sql = "INSERT INTO t_laporanharian (tgl_laporan, item, laporan_awal, laporan_akhir) VALUES( '".$tanggal."', $id_item, $temp_laporan,$temp_laporan)";  
		$query = $this->db->query($sql);
		
	}
	
	public function create_laporanharian3($id_item, $tanggal)
	{
		
		$sql = "SELECT laporan_akhir from t_laporanharian where item=$id_item AND DATE(tgl_laporan)<STR_TO_DATE('$tanggal', '%d-%m-%Y') ORDER BY tgl_laporan DESC LIMIT 1";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		$temp_laporan=0;
		foreach ($data as $data_item):
			$flag=1;
			$temp_laporan=$data_item['laporan_akhir'];
		endforeach;
		
		$sql = "INSERT INTO t_laporanharian (tgl_laporan, item, laporan_awal, laporan_akhir) VALUES( STR_TO_DATE('$tanggal', '%d-%m-%Y'), $id_item, $temp_laporan,$temp_laporan)";  
		$query = $this->db->query($sql);
		
	}
	
	public function create_laporanharian_ruangan($id_item = FALSE, $ruangan)
	{
		
		$sql = "SELECT laporan_akhir from t_laporan_ruangan_harian where item=$id_item AND ruangan=$ruangan AND  DATE(tgl_laporan)<DATE(NOW()) ORDER BY tgl_laporan DESC LIMIT 1";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		$temp_laporan=0;
		foreach ($data as $data_item):
			$flag=1;
			$temp_laporan=$data_item['laporan_akhir'];
		endforeach;
		
		$sql = "INSERT INTO t_laporan_ruangan_harian (tgl_laporan, item, ruangan, laporan_awal, laporan_akhir) VALUES( NOW(), $id_item, $ruangan, $temp_laporan,$temp_laporan)";  
		$query = $this->db->query($sql);
		
	}
	
	public function create_laporanharian2_ruangan($id_item, $tanggal, $ruangan)
	{
		
		$sql = "SELECT laporan_akhir from t_laporan_ruangan_harian where item=$id_item AND ruangan=$ruangan AND  DATE(tgl_laporan)<DATE('".$tanggal."') ORDER BY tgl_laporan DESC LIMIT 1";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		$temp_laporan=0;
		foreach ($data as $data_item):
			$flag=1;
			$temp_laporan=$data_item['laporan_akhir'];
		endforeach;
		
		$sql = "INSERT INTO t_laporan_ruangan_harian (tgl_laporan, item, ruangan, laporan_awal, laporan_akhir) VALUES( '".$tanggal."', $id_item, $ruangan, $temp_laporan,$temp_laporan)";  
		$query = $this->db->query($sql);
		
	}
	
	public function create_laporanharian3_ruangan($id_item, $tanggal, $ruangan)
	{
		
		$sql = "SELECT laporan_akhir from t_laporan_ruangan_harian where item=$id_item AND ruangan=$ruangan AND  DATE(tgl_laporan)<STR_TO_DATE('$tanggal', '%d-%m-%Y') ORDER BY tgl_laporan DESC LIMIT 1";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		$temp_laporan=0;
		foreach ($data as $data_item):
			$flag=1;
			$temp_laporan=$data_item['laporan_akhir'];
		endforeach;
		
		$sql = "INSERT INTO t_laporan_ruangan_harian (tgl_laporan, item, ruangan, laporan_awal, laporan_akhir) VALUES( STR_TO_DATE('$tanggal', '%d-%m-%Y'), $id_item, $ruangan, $temp_laporan,$temp_laporan)";  
		$query = $this->db->query($sql);
		
		
	}
	
	public function check_laporanharian($id_item = FALSE)
	{	
		$sql = "SELECT count(tgl_laporan) as ada from t_laporanharian where item=$id_item AND DATE(tgl_laporan)=DATE(NOW()) ";
		$query = $this->db->query($sql);
		return $query->row_array();
		
	}
	
	public function check_laporanharian2($id_item, $tanggal)
	{	
		$sql = "SELECT count(tgl_laporan) as ada from t_laporanharian where item=$id_item AND DATE(tgl_laporan)=DATE('".$tanggal."') ";
		$query = $this->db->query($sql);
		return $query->row_array();
		
	}
	
	public function check_laporanharian3($id_item, $tanggal)
	{	
		$sql = "SELECT count(tgl_laporan) as ada from t_laporanharian where item=$id_item AND DATE(tgl_laporan)=STR_TO_DATE('$tanggal', '%d-%m-%Y') ";
		$query = $this->db->query($sql);
		return $query->row_array();
		
	}
	
	public function check_laporanharian_ruangan($id_item = FALSE, $ruangan)
	{	
		$sql = "SELECT count(tgl_laporan) as ada from t_laporan_ruangan_harian where item=$id_item AND ruangan=$ruangan AND DATE(tgl_laporan)=DATE(NOW()) ";
		$query = $this->db->query($sql);
		return $query->row_array();
		
	}
	
	public function check_laporanharian2_ruangan($id_item, $tanggal, $ruangan)
	{	
		$sql = "SELECT count(tgl_laporan) as ada from t_laporan_ruangan_harian where item=$id_item AND ruangan=$ruangan AND DATE(tgl_laporan)=DATE('".$tanggal."') ";
		$query = $this->db->query($sql);
		return $query->row_array();
		
	}
	
	public function check_laporanharian3_ruangan($id_item, $tanggal, $ruangan)
	{	
		$sql = "SELECT count(tgl_laporan) as ada from t_laporan_ruangan_harian where item=$id_item AND ruangan=$ruangan AND DATE(tgl_laporan)=STR_TO_DATE('$tanggal', '%d-%m-%Y') ";
		$query = $this->db->query($sql);
		return $query->row_array();
		
	}
}