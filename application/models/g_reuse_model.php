<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_reuse_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_reuse($id_reuse = FALSE)
	{
		
		if ($id_reuse === FALSE)
		{
			$sql = "SELECT ik.*, i.nama_item from t_reuse ik, t_item i where ik.item=i.id_item ORDER BY id_reuse ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT ik.*, i.nama_item, i.stock_gudang, i.stock_awal from t_reuse ik, t_item i WHERE ik.item=i.id_item AND id_reuse=".$id_reuse."";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_reuse_item($id_item = 1)
	{
		
		$sql = "SELECT ik.*, i.nama_item from t_reuse ik, t_item i where ik.item=i.id_item AND i.id_item=$id_item ORDER BY tgl_reuse, id_reuse ASC";  
		$query = $this->db->query($sql);
		return $query->result_array();
		
	}
}