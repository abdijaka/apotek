<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_eresep_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_eresep($id_eresep = FALSE)
	{
		
		if ($id_eresep === FALSE)
		{
			$sql = "SELECT e.*, d.nama_dokter, s.nama_pasien , s.alamat FROM (t_eresep e LEFT JOIN pasien s ON e.pasien = s.id_pasien) LEFT JOIN dokter d ON e.dokter = d.id_dokter ORDER BY id_eresep ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT e.*, d.nama_dokter, s.nama_pasien, s.alamat FROM (t_eresep e LEFT JOIN pasien s ON e.pasien = s.id_pasien) LEFT JOIN dokter d ON e.dokter = d.id_dokter WHERE e.id_eresep = '" . $id_eresep . "' ORDER BY id_eresep ASC";  
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_detail($nomor)
	{
		$sql = "SELECT ib.item, ib.tgl_masuk, ib.id_item_eresep, ib.eresep, i.nama_item, ib.satuan, ib.jumlah, ib.aturan_pakai from t_item_eresep ib LEFT JOIN t_item i ON ib.item = i.id_item WHERE ib.eresep = " . $nomor;
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}