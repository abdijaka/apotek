<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pasien_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_pasien($id_pasien = FALSE)
	{
		
		if ($id_pasien === FALSE)
		{
			$sql = "SELECT * from pasien where status=1 ORDER BY nama_pasien, id_pasien ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from pasien WHERE id_pasien like '".$id_pasien."' AND status=1 ORDER BY nama_pasien, id_pasien ASC ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_jumlah_pasien($id_pasien)
	{
		$sql = "SELECT count(id_pasien) as jumlah from pasien WHERE ktp like '".$id_pasien."' AND status=1 ORDER BY id_pasien ASC ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_kota($id_kota = FALSE)
	{
		
		if ($id_kota === FALSE)
		{
			$sql = "SELECT * from inf_lokasi ORDER BY lokasi_ID ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from inf_lokasi WHERE lokasi_ID=".$id_kota." ORDER BY lokasi_ID ASC ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
	
	public function get_perusahaan($id_perusahaan = FALSE)
	{
		
		if ($id_perusahaan === FALSE)
		{
			$sql = "SELECT * from perusahaan ORDER BY id_perusahaan ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from perusahaan where id_perusahaan=$id_perusahaan ORDER BY id_perusahaan ASC";  
		
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
}