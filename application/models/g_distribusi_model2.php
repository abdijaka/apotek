<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class G_distribusi_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_distribusi($id_distribusi = FALSE)
	{
		
		if ($id_distribusi === FALSE)
		{
			$sql = "SELECT * from t_distribusi ORDER BY id_distribusi ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from t_distribusi where id_distribusi=".$id_distribusi."";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
}