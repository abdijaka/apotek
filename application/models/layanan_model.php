<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Layanan_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_layanan($id_layanan = FALSE)
	{
		
		if ($id_layanan === FALSE)
		{
			$sql = "SELECT * from layanan where status=1 ORDER BY id_layanan ASC";  
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		$sql = "SELECT * from layanan WHERE id_layanan=".$id_layanan." AND status=1 ORDER BY id_layanan ASC ";
		$query = $this->db->query($sql);
		
		return $query->row_array();
	}
}